package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.api.hc.vo.out.Statistics;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.hc.pojo.ProductTypeTree;
import com.sdkj.fixed.asset.pojo.hc.Product;
import com.sdkj.fixed.asset.pojo.hc.ProductType;
import feign.Param;

import java.util.List;

public interface ProductTypeMapper extends BaseMapper<ProductType> {
    /**
     * 校验分类名称是否存在
     *
     * @param typeName
     * @param orgId
     * @return
     */
    ProductType getTypeByName(@Param("typeName") String typeName, @Param("orgId") String orgId);

    /**
     * 树状图APP
     *
     * @param orgId
     * @return
     */
    List<ProductTypeTree> getTreeApp(@Param("orgId") String orgId);

    /**
     * 统计报表-物品
     *
     * @param params
     * @return
     */
    List<Product> getProductList(StatisticsParams params);

    /**
     * 统计报表-入库出库
     *
     * @param params
     * @return
     */
    List<Statistics> statisticsIn(StatisticsParams params);

    /**
     * 统计报表-入库
     *
     * @param params
     * @return
     */
    List<Statistics> statisticsOut(StatisticsParams params);

    /**
     * 统计报表-入库出库
     *
     * @param params
     * @return
     */
    List<Statistics> statisticsRest(StatisticsParams params);

    /**
     * 统计报表-物品分类
     *
     * @param params
     * @return
     */
    List<ProductType> getProductTypes(StatisticsParams params);

    /**
     * 统计报表-入库
     *
     * @param params
     * @return
     */
    Statistics getInStatistics(StatisticsParams params);

    /**
     * 统计报表-出库
     *
     * @param params
     * @return
     */
    Statistics getOutStatistics(StatisticsParams params);

    /**
     * 统计报表-结存
     *
     * @param params
     * @return
     */
    Statistics getRestStatistics(StatisticsParams params);

    ProductType getParent(@Param("pid") String pid);
}