package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.GetApplyListParams;
import com.sdkj.fixed.asset.api.hc.vo.in.GetCategoryDetail;
import com.sdkj.fixed.asset.api.hc.vo.in.GetProductUsedParams;
import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ReceiptApply;
import com.sdkj.fixed.asset.pojo.hc.ReceiptOutProduct;
import feign.Param;

import java.util.List;

public interface ReceiptApplyMapper extends BaseMapper<ReceiptApply> {
    /**
     * 查询我发起的单子
     *
     * @param params
     * @return
     */
    List<GetApplyList> getApplyList(GetApplyListParams params);

    /**
     * 我发起的子单
     *
     * @param id
     * @return
     */
    List<SonApply> getSonList(@Param("id") String id, @Param("state") Integer state);

    /**
     * 查看库存详情
     *
     * @param productId
     * @return
     */
    List<GetCategoryDetail> getCategoryDetail(@Param("productId") String productId, @Param("userId") String userId, @Param("isAdmin") Integer isAdmin);

    /**
     * 查看发放情况
     *
     * @param params
     * @return
     */
    List<SendDetail> getSendDetail(IdParams params);

    /**
     * 报表
     *
     * @param params
     * @return
     */
    List<GetProductUsed> statistic(GetProductUsedParams params);


    StatisticResult getStatistic(GetProductUsedParams params);
}