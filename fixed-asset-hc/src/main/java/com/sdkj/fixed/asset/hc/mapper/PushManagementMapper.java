package com.sdkj.fixed.asset.hc.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.PushManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PushManagementMapper extends BaseMapper<PushManagement> {
}