package com.sdkj.fixed.asset.hc.service;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.api.hc.vo.out.Statistics;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.hc.mapper.CategoryMapper;
import com.sdkj.fixed.asset.hc.mapper.ProductMapper;
import com.sdkj.fixed.asset.hc.mapper.ProductTypeMapper;
import com.sdkj.fixed.asset.hc.mapper.UserManagementMapper;
import com.sdkj.fixed.asset.hc.pojo.ProductTypeTree;
import com.sdkj.fixed.asset.hc.util.TreeUtil;
import com.sdkj.fixed.asset.pojo.hc.*;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/***
 *
 * @author scx
 *
 */
@Service
public class ProductTypeService extends BaseService<ProductType> {

    @Resource
    private ProductTypeMapper mapper;

    @Resource
    private ProductMapper productMapper;

    @Resource
    private UserManagementMapper userManagementMapper;

    @Override
    public BaseMapper<ProductType> getMapper() {
        return mapper;
    }

    /**
     * 初始化树节点
     *
     * @param id id
     */
    private void initTreeCode(String id) {
        ProductType entity = mapper.selectByPrimaryKey(id);
        ProductType parent = mapper.selectByPrimaryKey(entity.getPid());
        if (null == parent) {
            entity.setTreecode(entity.getPid() + entity.getId());
        } else {
            if (StrUtil.isNotBlank(parent.getTreecode())) {
                entity.setTreecode(parent.getTreecode() + "," + entity.getId());
            } else {
                entity.setTreecode(entity.getId());
            }
        }
        mapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * 校验code
     *
     * @param entity 产品
     * @param flag   新增/修改
     */
    private void checkCode(ProductType entity, Boolean flag) {
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", entity.getCode());
        criteria.andEqualTo("orgId", entity.getOrgId());
        criteria.andEqualTo("state", 1);
        if (flag) {
            criteria.andNotEqualTo("id", entity.getId());
        }
        if (0 != mapper.selectByExample(example).size()) {
            throw new LogicException("编码已存在");
        }
    }

    /**
     * 校验name
     *
     * @param entity 产品
     * @param flag   新增/修改
     */
    private void checkName(ProductType entity, Boolean flag) {
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("name", entity.getName());
        criteria.andEqualTo("orgId", entity.getOrgId());
        criteria.andEqualTo("state", 1);
        if (flag) {
            criteria.andNotEqualTo("id", entity.getId());
        }
        if (0 != mapper.selectByExample(example).size()) {
            throw new LogicException("名称已存在");
        }
    }

    /**
     * 校验子节点
     *
     * @param entity ProductType
     */
    private void checkChild(ProductType entity) {
        Example example = new Example(ProductType.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("pid", entity.getId());
        criteria.andEqualTo("state", 1);
        if (0 != mapper.selectByExample(example).size()) {
            throw new LogicException("类型下有子类型，不能删除");
        }
    }

    /**
     * 校验产品
     *
     * @param entity ProductType
     */
    private void checkProduct(ProductType entity) {
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("type", entity.getId());
        criteria.andNotEqualTo("state", 3);
        if (0 != productMapper.selectByExample(example).size()) {
            throw new LogicException("类型下有物品，不能删除");
        }
    }
    /*****************************************************************************************************************************************/

    /**
     * 新增
     *
     * @param entity ProductType
     * @return ProductType
     */
    public ProductType add(ProductType entity) {
        if (StrUtil.isBlank(entity.getPid())) {
            entity.setPid("");
        }
        //code唯一
        checkCode(entity, false);
        //name唯一
        checkName(entity, false);
        mapper.insertSelective(entity);
        //初始化树节点
        initTreeCode(entity.getId());
        return entity;
    }

    /**
     * 修改
     *
     * @param entity ProductType
     * @return ProductType
     */
    public ProductType edit(ProductType entity) {
        //code唯一
        checkCode(entity, true);
        //name唯一
        checkName(entity, true);
        mapper.updateByPrimaryKeySelective(entity);
        return entity;
    }

    /**
     * 删除
     *
     * @param entity ProductType
     * @return ProductType
     */
    public ProductType del(ProductType entity) {
        //校验子节点
        checkChild(entity);
        //校验物品
        checkProduct(entity);
        entity.setState(2);
        mapper.updateByPrimaryKeySelective(entity);
        return entity;
    }

    /**
     * 树状图
     *
     * @param orgId 机构
     * @return ProductType
     */
    public List<ProductType> getTree(String orgId) {
        Example example = new Example(ProductType.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("orgId", orgId);
        criteria.andEqualTo("state", 1);
        List<ProductType> productTypeList = mapper.selectByExample(example);
        for (ProductType productType : productTypeList) {
            if (mapper.selectByPrimaryKey(productType.getPid()) != null) {
                productType.setPname(mapper.selectByPrimaryKey(productType.getPid()).getName());
                productType.setPcode(mapper.selectByPrimaryKey(productType.getPid()).getCode());
            }
        }
        return productTypeList;
    }

    public List<ProductType> getAll(String companyId) {
        Example example = new Example(ProductType.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("orgId", companyId);
        criteria.andEqualTo("state", 1);
        List<ProductType> productTypeList = mapper.selectByExample(example);
        for (ProductType productType : productTypeList) {
            if (StrUtil.isNotBlank(productType.getPid())) {
                productType.setPcode(mapper.selectByPrimaryKey(productType.getPid()).getCode());
                productType.setPname(mapper.selectByPrimaryKey(productType.getPid()).getName());
            }
            productType.setCusername(userManagementMapper.selectByPrimaryKey(productType.getCuser()).getName());
        }
        return productTypeList;
    }

    public List<ProductTypeTree> getTreeApp(String orgId) {
        List<ProductTypeTree> productTypeTreeList = mapper.getTreeApp(orgId);
        return TreeUtil.bulidTree(productTypeTreeList, "");
    }

    public List<Product> productList(StatisticsParams params) {
        return mapper.getProductList(params);
    }

    public List<Statistics> statistics(StatisticsParams params) {
        UserManagement user = userManagementMapper.selectByPrimaryKey(params.getUserId());
        if (user.getDataIsAll() == 1) {
            params.setIsAdmin(1);
        } else {
            params.setIsAdmin(2);
        }
        List<ProductType> productTypes = mapper.getProductTypes(params);
        List<Statistics> resultList = new ArrayList<>();
        for (ProductType productType : productTypes) {
            params.setType(productType.getId());
            Statistics staIn = mapper.getInStatistics(params);
            Statistics staOut = mapper.getOutStatistics(params);
            Statistics staRest = mapper.getRestStatistics(params);
            Statistics result = new Statistics();
            result.setId(productType.getId());
            result.setPid(productType.getPid());
            result.setType(productType.getId());
            result.setTypeName(productType.getName());
            if (staIn == null) {
                result.setInNum(0);
                result.setInTotalPrice("0.00");
            } else {
                result.setInNum(staIn.getInNum());
                result.setInTotalPrice(staIn.getInTotalPrice());
            }
            if (staOut == null) {
                result.setOutNum(0);
                result.setOutTotalPrice("0.00");
            } else {
                result.setOutNum(staOut.getOutNum());
                result.setOutTotalPrice(staOut.getOutTotalPrice());
            }
            if (staRest == null) {
                result.setTotalNum(0);
                result.setTotalPrice("0.00");
            } else {
                result.setTotalNum(staRest.getTotalNum());
                result.setTotalPrice(staRest.getTotalPrice());
            }
            resultList.add(result);
        }
        return resultList;
    }

    public List<ProductType> productTypes(StatisticsParams params) {
        return mapper.getProductTypes(params);
    }
}
