package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptOutGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.ReceiptOut;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReceiptOutMapper extends BaseMapper<ReceiptOut> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    List<ReceiptOutGetPage> getPages(ReceiptOutGetPageParams params);

    /**
     * 分页查询新
     *
     * @param params
     * @return
     */
    List<ReceiptOutGetPageNew> getPage(ReceiptOutGetPageParams params);

    List<ReceiptOutCheckExtend> selByIdsCheck(@Param("idList") List<String> asList);

    List<ReceiptMoveProductExtend> getReceiptMoveExtend(@Param("id") String id);

    List<ReceiptOutExtend> selByIds(@Param("idList") List<String> asList);

    List<ReceiptOutCheck> exportCheck(ReceiptOutGetPageParams params);

    List<ReceipOutGetPageExtends> getPageExport(@Param("id") String id);

    List<ReceiptOutExport> export(ReceiptOutGetPageParams params);

}