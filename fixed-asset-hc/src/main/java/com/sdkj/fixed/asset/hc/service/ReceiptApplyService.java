package com.sdkj.fixed.asset.hc.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.hc.vo.in.*;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.utils.JpushClientUtil;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.hc.mapper.*;
import com.sdkj.fixed.asset.pojo.hc.*;
import com.sdkj.fixed.asset.pojo.system.PushManagement;
import com.sdkj.fixed.asset.pojo.system.UserDateAuth;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import static cn.hutool.core.date.DateUtil.formatDateTime;

/**
 * ReceiptApplyService
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/27 12:07
 */
@Service
public class ReceiptApplyService extends BaseService<ReceiptApply> {

    private static Logger log = LoggerFactory.getLogger(ReceiptApplyService.class);

    @Resource
    private ReceiptOutService receiptOutService;

    @Resource
    private ReceiptApplyService service;

    @Resource
    private ReceiptApplyMapper mapper;

    @Resource
    private ReceiptApplySonMapper sonMapper;

    @Resource
    private UserManagementMapper userManagementMapper;

    @Resource
    private UserDataAuthMapper userDataAuthMapper;

    @Resource
    private CategoryProductMapper categoryProductMapper;

    @Resource
    private PushManagementMapper pushMapper;

    @Resource
    private ProductMapper productMapper;

    @Resource
    private ReceiptOutMapper receiptOutMapper;

    @Override
    public BaseMapper<ReceiptApply> getMapper() {
        return mapper;
    }

    public BaseService<ReceiptApply> getService() {
        return service;
    }

    private void approvePush(ReceiptApply entity, String[] userIds) {
        int code = JpushClientUtil.sendToAsia(userIds, "耗材管理", "耗材领用申请",
                "收到一条新的耗材领用申请，单号：" + entity.getNumber() + ",请确认", "");
        for (String userId : userIds) {
            PushManagement pu = new PushManagement();
            pu.setCompanyId(entity.getOrgId());
            pu.setContent("收到一条新的耗材领用申请，单号：" + entity.getNumber() + ",请确认");
            pu.setCtime(DateUtil.now());
            pu.setCuser(entity.getApplyUser());
            pu.setSender(userId);
            pu.setResult(0);
            pushMapper.insertSelective(pu);
        }
    }


    private void confirmPush(ReceiptApplySon entity, String[] userIds) {
        ReceiptApply receiptApply = mapper.selectByPrimaryKey(entity.getReceiptId());
        String result = "";
        if (entity.getApprove() == 2) {
            result = "同意";
        } else {
            result = "拒绝";
        }
        int code = JpushClientUtil.sendToAsia(userIds, "耗材管理", "耗材领用申请",
                "耗材领用申请，单号：" + receiptApply.getNumber() + "处理完成，处理结果为" + result, "");
        for (String userId : userIds) {
            PushManagement pu = new PushManagement();
            pu.setCompanyId(entity.getOrgId());
            pu.setContent("耗材领用申请，单号：" + receiptApply.getNumber() + "处理完成，处理结果为" + result);
            pu.setCtime(DateUtil.now());
            pu.setCuser(entity.getApproveUser());
            pu.setSender(userId);
            pu.setResult(0);
            pushMapper.insertSelective(pu);
        }
    }

    @Transactional
    public ReceiptApply add(ReceiptApplyParams entity) {
        entity.setNumber("LY" + TimeTool.getTimeDate14());
        mapper.insertSelective(entity);
        //分子单
        //物品种类数
        int num = 0;
        List<String> userId = new ArrayList<>();
        for (ProductParams productParams : entity.getProductParamsList()) {
            ReceiptApplySon receiptApplySon = new ReceiptApplySon();
            num++;
            receiptApplySon.setProductId(productParams.getId());
            receiptApplySon.setReceiptId(entity.getId());
            receiptApplySon.setNum(productParams.getNum());
            receiptApplySon.setApprove(1);
            receiptApplySon.setOrgId(entity.getOrgId());
            receiptApplySon.setConfirm(1);
            sonMapper.insertSelective(receiptApplySon);
            //有该物品的所有仓库
            List<String> categoryIdList = new ArrayList<>();
            Example exampleCategoty = new Example(CategoryProduct.class);
            Example.Criteria criteriaCategoty = exampleCategoty.createCriteria();
            criteriaCategoty.andEqualTo("productId", productParams.getId());
            List<CategoryProduct> categoryProductList = categoryProductMapper.selectByExample(exampleCategoty);
            int totalnum = 0;
            for (CategoryProduct categoryProduct : categoryProductList) {
                //如果仓库下有该物品，推送给对应的管理员
                categoryIdList.add(categoryProduct.getCategoryId());
                totalnum += categoryProduct.getProductNum();
            }
            if (totalnum < productParams.getNum()) {
                throw new LogicException("库存不足");
            }
            //仓库下所有管理员

            for (String categoryId : categoryIdList) {
                Example example = new Example(UserDateAuth.class);
                Example.Criteria criteria = example.createCriteria();
                criteria.andEqualTo("authId", categoryId);
                criteria.andEqualTo("type", 4);
                List<UserDateAuth> userDateAuthList = userDataAuthMapper.selectByExample(example);
                for (UserDateAuth userDateAuth : userDateAuthList) {
                    userId.add(userDateAuth.getUserId());
                }
            }
            Example example1 = new Example(UserManagement.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("dataIsAll", 1);
            List<UserManagement> userDateAuthList = userManagementMapper.selectByExample(example1);
            for (UserManagement user : userDateAuthList) {
                userId.add(user.getId());
            }
        }
        List<String> userIdNew = new ArrayList<String>();
        for (String str : userId) {
            if (!userIdNew.contains(str)) {
                userIdNew.add(str);
            }
        }
        //推送
        String[] userIds = new String[userIdNew.size()];
        userIdNew.toArray(userIds);
        approvePush(entity, userIds);
        entity.setNum(num);
        mapper.updateByPrimaryKeySelective(entity);
        return entity;
    }

    @Transactional
    public ReceiptApplySon confirm(ReceiptApplySon entity) {
        ReceiptApplySon receiptApplySon = sonMapper.selectByPrimaryKey(entity.getId());
        receiptApplySon.setConfirm(2);
        receiptApplySon.setConfirmUser(entity.getConfirmUser());
        receiptApplySon.setConfirmTime(DateUtil.now());
        sonMapper.updateByPrimaryKeySelective(receiptApplySon);
        return entity;
    }

    @Transactional
    public ReceiptApplySon approve(ReceiptApplySon entity) throws Exception {
        ReceiptApplySon receiptApplySon = sonMapper.selectByPrimaryKey(entity.getId());

        if (receiptApplySon.getApprove() != 1) {
            throw new LogicException("订单已被处理不能再操作");
        }
        ReceiptApply receiptApply = mapper.selectByPrimaryKey(receiptApplySon.getReceiptId());
        if (receiptApply.getProcess() == 0) {
            throw new LogicException("该订单正在被处理，不可操作");
        }
        //进入处理状态
        receiptApply.setProcess(0);
        mapper.updateByPrimaryKeySelective(receiptApply);
        receiptApplySon.setApprove(entity.getApprove());
        receiptApplySon.setApproveTime(DateUtil.now());
        receiptApplySon.setApproveUser(entity.getApproveUser());
        sonMapper.updateByPrimaryKeySelective(receiptApplySon);
        //处理状态完成
        receiptApply.setProcess(1);
        mapper.updateByPrimaryKeySelective(receiptApply);
        //推
        confirmPush(receiptApplySon, receiptApply.getApplyUser().split(","));
        return entity;
    }

    public List<GetApplyList> getApplyList(PageParams<GetApplyListParams> params) {
        params.getParams().setEndTime(DateUtil.now());
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        if (params.getParams().getTime() == 0) {
            c.add(Calendar.MONTH, -1);
            params.getParams().setStartTime(formatDateTime(c.getTime()));
        }
        if (params.getParams().getTime() == 1) {
            c.add(Calendar.MONTH, -3);
            params.getParams().setStartTime(formatDateTime(c.getTime()));
        }
        if (params.getParams().getTime() == 2) {
            c.add(Calendar.MONTH, -6);
            params.getParams().setStartTime(formatDateTime(c.getTime()));
        }
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<GetApplyList> getApplyList = mapper.getApplyList(params.getParams());
        for (GetApplyList applyList : getApplyList) {
            applyList.setSonApplyList(mapper.getSonList(applyList.getId(),params.getParams().getState()));
        }
        return getApplyList;
    }

    public List<GetCategoryDetail> getCategoryDetail(ReceiptApplySon entity) {
        UserManagement user = userManagementMapper.selectByPrimaryKey(entity.getUserId());
        Integer isAdmin = 0;
        if (user.getDataIsAll() == 1) {
            isAdmin = 1;
        } else {
            isAdmin = 2;
        }
        ReceiptApplySon receiptApplySon = sonMapper.selectByPrimaryKey(entity.getId());
        return mapper.getCategoryDetail(receiptApplySon.getProductId(), entity.getUserId(), isAdmin);
    }

    public void createReceiptOut(CreateReceiptOut entity) throws Exception {

        ReceiptApplySon receiptApplySon = sonMapper.selectByPrimaryKey(entity.getId());
        if (receiptApplySon.getApprove() != 1) {
            throw new LogicException("订单已被处理不能再操作");
        }
        receiptApplySon.setApprove(2);
        receiptApplySon.setApproveTime(DateUtil.now());
        receiptApplySon.setApproveUser(entity.getCuser());
        sonMapper.updateByPrimaryKeySelective(receiptApplySon);
        ReceiptApply receiptApply = mapper.selectByPrimaryKey(sonMapper.selectByPrimaryKey(entity.getId()).getReceiptId());

        for (CreateReceiptOut product : entity.getProducts()) {
            if (StrUtil.isEmpty(product.getProductId())) {
                throw new LogicException("产品id不能为空");
            }
            if (StrUtil.isEmpty(product.getCategoryId())) {
                throw new LogicException("仓库id不能为空");
            }
            if (product.getNum() == null) {
                throw new LogicException("数量必填");
            }
            Example exampleCategoty = new Example(CategoryProduct.class);
            Example.Criteria criteriaCategoty = exampleCategoty.createCriteria();
            criteriaCategoty.andEqualTo("productId", product.getProductId());
            criteriaCategoty.andEqualTo("categoryId", product.getCategoryId());
            List<CategoryProduct> categoryProductList = categoryProductMapper.selectByExample(exampleCategoty);
            for (CategoryProduct categoryProduct : categoryProductList) {
                //新增出库单
                //生成出库单-物品关联
                String id = IdUtil.fastSimpleUUID();
                List<ReceiptOutProduct> receiptOutProductList = new ArrayList<>();
                ReceiptOutProduct receiptOutProduct = new ReceiptOutProduct();
                receiptOutProduct.setProductId(product.getProductId());
                receiptOutProduct.setReceiptId(id);
                Float totalPrice = Float.parseFloat(categoryProduct.getPrice()) * product.getNum();
                DecimalFormat df = new DecimalFormat("0.00");//设置保留位数
                String format = df.format(totalPrice);
                receiptOutProduct.setTotalPrice(format);
                receiptOutProduct.setPrice(categoryProduct.getPrice());
                if (product.getNum() == 0) {
                    throw new LogicException("数量不能为零");
                }
                receiptOutProduct.setNum(product.getNum());
                receiptOutProductList.add(receiptOutProduct);
                //生成出库单
                ReceiptOut receiptOut = new ReceiptOut();
                receiptOut.setId(id);
                receiptOut.setCategoryId(product.getCategoryId());
                receiptOut.setType(1);
                receiptOut.setCuser(entity.getCuser());
                receiptOut.setCtime(DateUtil.now());
                receiptOut.setBussinessDate(DateUtil.now());
                receiptOut.setOrgId(entity.getOrgId());
                receiptOut.setReceiveUser(receiptApply.getApplyUser());
                receiptOut.setReceiveDept(userManagementMapper.selectByPrimaryKey(receiptApply.getApplyUser()).getDeptId());
                receiptOut.setReceiveOrg(userManagementMapper.selectByPrimaryKey(receiptApply.getApplyUser()).getCompanyId());
                receiptOut.setEtime(DateUtil.now());
                receiptOut.setEuser(entity.getCuser());
                receiptOut.setState(1);
                receiptOut.setReceiptOutProduct(receiptOutProductList);
                receiptOut.setReceiptApplyId(receiptApplySon.getId());
                receiptOutService.add(receiptOut);
//                receiptApplySon.setReceiptOutId(out.getId());
//                sonMapper.updateByPrimaryKey(receiptApplySon);
                break;
            }
            confirmPush(receiptApplySon, receiptApply.getApplyUser().split(","));
        }
    }

    public List<SendDetail> getSendDetail(PageParams<IdParams> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<SendDetail> receiptOutProducts = mapper.getSendDetail(params.getParams());
        return receiptOutProducts;
    }

    public void refuse(CreateReceiptOut entity) {
        ReceiptApplySon receiptApplySon = sonMapper.selectByPrimaryKey(entity.getId());
        ReceiptApply receiptApply = mapper.selectByPrimaryKey(receiptApplySon.getReceiptId());
        if (receiptApplySon.getApprove() != 1) {
            throw new LogicException("订单已被处理不能再操作");
        }
        receiptApplySon.setApprove(3);
        receiptApplySon.setApproveTime(DateUtil.now());
        receiptApplySon.setApproveUser(entity.getCuser());
        sonMapper.updateByPrimaryKeySelective(receiptApplySon);
        confirmPush(receiptApplySon, receiptApply.getApplyUser().split(","));
    }

    public ApplyResult getSonApply(IdParams params) {
        ReceiptOut receiptOut = receiptOutMapper.selectByPrimaryKey(params.getId());
        Example example = new Example(ReceiptApplySon.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiptId", params.getId());
        if (params.getState() == 1) {
            criteria.andEqualTo("approve", 1);
            criteria.andEqualTo("confirm", 1);
        }
        if (params.getState() == 2) {
            criteria.andEqualTo("approve", 2);
            criteria.andEqualTo("confirm", 1);
        }
        if (params.getState() == 3) {
            criteria.andEqualTo("approve", 3);
            criteria.andEqualTo("confirm", 1);
        }
        if (params.getState() == 4) {
            criteria.andEqualTo("confirm", 2);
        }
        List<ReceiptApplySon> receiptApplySons = sonMapper.selectByExample(example);
        if (receiptApplySons.size() == 0) {
            throw new LogicException("错误的出库单id");
        }
        ReceiptApplySon receiptApplySon = receiptApplySons.get(0);
//        ReceiptApplySon receiptApplySon = sonMapper.selectByPrimaryKey(receiptOut.getReceiptApplyId());
        ApplyResult applyResult = new ApplyResult();
        List<SonApply> sonApplies = new ArrayList<>();
        List<ApplyDetail> applyDetails = new ArrayList<>();
        SonApply sonApply = new SonApply();
        sonApply.setSonId(params.getId());
        sonApply.setNum(receiptApplySon.getNum());
        sonApply.setName(productMapper.selectByPrimaryKey(receiptApplySon.getProductId()).getName());
        sonApply.setUnit(productMapper.selectByPrimaryKey(receiptApplySon.getProductId()).getUnit());
        sonApply.setModel(productMapper.selectByPrimaryKey(receiptApplySon.getProductId()).getModel());
        ApplyDetail applyDetail = new ApplyDetail();
        applyDetail.setNumber(mapper.selectByPrimaryKey(receiptApplySon.getReceiptId()).getNumber());
        applyDetail.setApproveUser(userManagementMapper.selectByPrimaryKey(receiptApplySon.getApproveUser()).getName());
        applyDetail.setApproveTime(receiptApplySon.getApproveTime());
        applyDetail.setProductCode(productMapper.selectByPrimaryKey(receiptApplySon.getProductId()).getCode());
        applyDetail.setSign(receiptOut.getSign());
        applyDetail.setProductName(productMapper.selectByPrimaryKey(receiptApplySon.getProductId()).getName());
        sonApplies.add(sonApply);
        applyDetails.add(applyDetail);
        applyResult.setSonApplies(sonApplies);
        applyResult.setApplyDetails(applyDetails);
        return applyResult;
    }

    public List<GetProductUsed> statistic(PageParams<GetProductUsedParams> params) {
        UserManagement user = userManagementMapper.selectByPrimaryKey(params.getParams().getUserId());
        if (user.getDataIsAll() == 1) {
            params.getParams().setIsAdmin(1);
        } else {
            params.getParams().setIsAdmin(2);
        }
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<GetProductUsed> getProductUseds = mapper.statistic(params.getParams());
        for (GetProductUsed getProductUsed : getProductUseds) {
            GetProductUsedNum getProductUsedNum = new GetProductUsedNum();
            GetProductUsedPrice getProductUsedPrice = new GetProductUsedPrice();
            GetProductUsedTotalPrice getProductUsedTotalPrice = new GetProductUsedTotalPrice();
            params.getParams().setProductId(getProductUsed.getId());
            if (StrUtil.isBlank(params.getParams().getYear())) {
                throw new LogicException("请选择查询年份");
            }
            if (StrUtil.isNotBlank(params.getParams().getYear())) {
                params.setParams(setDate(params.getParams()));
                params.getParams().setStart(params.getParams().getDate1s());
                params.getParams().setEnd(params.getParams().getDate1e());
                StatisticResult statisticResult1 = mapper.getStatistic(params.getParams());
                DecimalFormat df = new DecimalFormat("0.00");//设置保留位数
                getProductUsedNum.setNum1(statisticResult1.getNum());
                getProductUsedPrice.setPrice1(df.format(Float.parseFloat(statisticResult1.getPrice())));
                getProductUsedTotalPrice.setTotalPrice1(df.format(Float.parseFloat(statisticResult1.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate2s());
                params.getParams().setEnd(params.getParams().getDate2e());
                StatisticResult statisticResult2 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum2(statisticResult2.getNum());
                getProductUsedPrice.setPrice2(df.format(Float.parseFloat(statisticResult2.getPrice())));
                getProductUsedTotalPrice.setTotalPrice2(df.format(Float.parseFloat(statisticResult2.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate3s());
                params.getParams().setEnd(params.getParams().getDate3e());
                StatisticResult statisticResult3 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum3(statisticResult3.getNum());
                getProductUsedPrice.setPrice3(df.format(Float.parseFloat(statisticResult3.getPrice())));
                getProductUsedTotalPrice.setTotalPrice3(df.format(Float.parseFloat(statisticResult3.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate4s());
                params.getParams().setEnd(params.getParams().getDate4e());
                StatisticResult statisticResult4 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum4(statisticResult4.getNum());
                getProductUsedPrice.setPrice4(df.format(Float.parseFloat(statisticResult4.getPrice())));
                getProductUsedTotalPrice.setTotalPrice4(df.format(Float.parseFloat(statisticResult4.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate5s());
                params.getParams().setEnd(params.getParams().getDate5e());
                StatisticResult statisticResult5 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum5(statisticResult5.getNum());
                getProductUsedPrice.setPrice5(df.format(Float.parseFloat(statisticResult5.getPrice())));
                getProductUsedTotalPrice.setTotalPrice5(df.format(Float.parseFloat(statisticResult5.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate6s());
                params.getParams().setEnd(params.getParams().getDate6e());
                StatisticResult statisticResult6 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum6(statisticResult6.getNum());
                getProductUsedPrice.setPrice6(df.format(Float.parseFloat(statisticResult6.getPrice())));
                getProductUsedTotalPrice.setTotalPrice6(df.format(Float.parseFloat(statisticResult6.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate7s());
                params.getParams().setEnd(params.getParams().getDate7e());
                StatisticResult statisticResult7 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum7(statisticResult7.getNum());
                getProductUsedPrice.setPrice7(df.format(Float.parseFloat(statisticResult7.getPrice())));
                getProductUsedTotalPrice.setTotalPrice7(df.format(Float.parseFloat(statisticResult7.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate8s());
                params.getParams().setEnd(params.getParams().getDate8e());
                StatisticResult statisticResult8 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum8(statisticResult8.getNum());
                getProductUsedPrice.setPrice8(df.format(Float.parseFloat(statisticResult8.getPrice())));
                getProductUsedTotalPrice.setTotalPrice8(df.format(Float.parseFloat(statisticResult8.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate9s());
                params.getParams().setEnd(params.getParams().getDate9e());
                StatisticResult statisticResult9 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum9(statisticResult9.getNum());
                getProductUsedPrice.setPrice9(df.format(Float.parseFloat(statisticResult9.getPrice())));
                getProductUsedTotalPrice.setTotalPrice9(df.format(Float.parseFloat(statisticResult9.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate10s());
                params.getParams().setEnd(params.getParams().getDate10e());
                StatisticResult statisticResult10 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum10(statisticResult10.getNum());
                getProductUsedPrice.setPrice10(df.format(Float.parseFloat(statisticResult10.getPrice())));
                getProductUsedTotalPrice.setTotalPrice10(df.format(Float.parseFloat(statisticResult10.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate11s());
                params.getParams().setEnd(params.getParams().getDate11e());
                StatisticResult statisticResult11 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum11(statisticResult11.getNum());
                getProductUsedPrice.setPrice11(df.format(Float.parseFloat(statisticResult11.getPrice())));
                getProductUsedTotalPrice.setTotalPrice11(df.format(Float.parseFloat(statisticResult11.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDate12s());
                params.getParams().setEnd(params.getParams().getDate12e());
                StatisticResult statisticResult12 = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNum12(statisticResult12.getNum());
                getProductUsedPrice.setPrice12(df.format(Float.parseFloat(statisticResult12.getPrice())));
                getProductUsedTotalPrice.setTotalPrice12(df.format(Float.parseFloat(statisticResult12.getTotalPrice())));

                params.getParams().setStart(params.getParams().getDateAlls());
                params.getParams().setEnd(params.getParams().getDateAlle());
                StatisticResult statisticResult = mapper.getStatistic(params.getParams());
                getProductUsedNum.setNumTotal(statisticResult.getNum());
                getProductUsedPrice.setPriceAve(df.format(Float.parseFloat(statisticResult.getPrice())));
                getProductUsedTotalPrice.setTotalPriceTotal(df.format(Float.parseFloat(statisticResult.getTotalPrice())));

                getProductUsed.setProductUsedNums(getProductUsedNum);
                getProductUsed.setProductUsedPrices(getProductUsedPrice);
                getProductUsed.setProductUsedTotalPrices(getProductUsedTotalPrice);
            }
        }
//        getProductUseds.forEach(getProductUsed -> {
//
//
//        });
        return getProductUseds;
    }

    private GetProductUsedParams setDate(GetProductUsedParams params) {
        String date1s = params.getYear() + "-01-01 00:00:00";
        String date1e = params.getYear() + "-01-31 23:59:59";
        String date2s = params.getYear() + "-02-01 00:00:00";
        String date2e = params.getYear() + "-02-29 23:59:59";
        String date3s = params.getYear() + "-03-01 00:00:00";
        String date3e = params.getYear() + "-03-31 23:59:59";
        String date4s = params.getYear() + "-04-01 00:00:00";
        String date4e = params.getYear() + "-04-30 23:59:59";
        String date5s = params.getYear() + "-05-01 00:00:00";
        String date5e = params.getYear() + "-05-31 23:59:59";
        String date6s = params.getYear() + "-06-01 00:00:00";
        String date6e = params.getYear() + "-06-30 23:59:59";
        String date7s = params.getYear() + "-07-01 00:00:00";
        String date7e = params.getYear() + "-07-31 23:59:59";
        String date8s = params.getYear() + "-08-01 00:00:00";
        String date8e = params.getYear() + "-08-31 23:59:59";
        String date9s = params.getYear() + "-09-01 00:00:00";
        String date9e = params.getYear() + "-09-30 23:59:59";
        String date10s = params.getYear() + "-10-01 00:00:00";
        String date10e = params.getYear() + "-10-31 23:59:59";
        String date11s = params.getYear() + "-11-01 00:00:00";
        String date11e = params.getYear() + "-11-30 23:59:59";
        String date12s = params.getYear() + "-12-01 00:00:00";
        String date12e = params.getYear() + "-12-31 23:59:59";
        String datealls = params.getYear() + "-01-01 00:00:00";
        String datealle = params.getYear() + "-12-31 23:59:59";

        params.setDateAlle(datealle);
        params.setDateAlls(datealls);

        params.setDate1s(date1s);
        params.setDate1e(date1e);

        params.setDate2s(date2s);
        params.setDate2e(date2e);

        params.setDate3s(date3s);
        params.setDate3e(date3e);

        params.setDate4s(date4s);
        params.setDate4e(date4e);

        params.setDate5s(date5s);
        params.setDate5e(date5e);

        params.setDate6s(date6s);
        params.setDate6e(date6e);

        params.setDate7s(date7s);
        params.setDate7e(date7e);

        params.setDate8s(date8s);
        params.setDate8e(date8e);

        params.setDate9s(date9s);
        params.setDate9e(date9e);

        params.setDate10s(date10s);
        params.setDate10e(date10e);

        params.setDate11s(date11s);
        params.setDate11e(date11e);

        params.setDate12s(date12s);
        params.setDate12e(date12e);

        return params;
    }

    public List<GetProductUsed> export(GetProductUsedParams params) {
        UserManagement user = userManagementMapper.selectByPrimaryKey(params.getUserId());
        if (user.getDataIsAll() == 1) {
            params.setIsAdmin(1);
        } else {
            params.setIsAdmin(2);
        }
        List<GetProductUsed> getProductUseds = mapper.statistic(params);
        getProductUseds.forEach(getProductUsed -> {
            List<GetProductUsedNum> list1 = new ArrayList<>();
            List<GetProductUsedPrice> list2 = new ArrayList<>();
            List<GetProductUsedTotalPrice> list3 = new ArrayList<>();
            GetProductUsedNum getProductUsedNum = new GetProductUsedNum();
            GetProductUsedPrice getProductUsedPrice = new GetProductUsedPrice();
            GetProductUsedTotalPrice getProductUsedTotalPrice = new GetProductUsedTotalPrice();
            params.setProductId(getProductUsed.getId());
            if (StrUtil.isNotBlank(params.getYear())) {
                DecimalFormat df = new DecimalFormat("0.00");//设置保留位数
                setDate(params);
                params.setStart(params.getDate1s());
                params.setEnd(params.getDate1e());
                StatisticResult statisticResult1 = mapper.getStatistic(params);
                getProductUsedNum.setNum1(statisticResult1.getNum());
                getProductUsedPrice.setPrice1(df.format(Float.parseFloat(statisticResult1.getPrice())));
                getProductUsedTotalPrice.setTotalPrice1(df.format(Float.parseFloat(statisticResult1.getTotalPrice())));

                params.setStart(params.getDate2s());
                params.setEnd(params.getDate2e());
                StatisticResult statisticResult2 = mapper.getStatistic(params);
                getProductUsedNum.setNum2(statisticResult2.getNum());
                getProductUsedPrice.setPrice2(df.format(Float.parseFloat(statisticResult2.getPrice())));
                getProductUsedTotalPrice.setTotalPrice2(df.format(Float.parseFloat(statisticResult2.getTotalPrice())));

                params.setStart(params.getDate3s());
                params.setEnd(params.getDate3e());
                StatisticResult statisticResult3 = mapper.getStatistic(params);
                getProductUsedNum.setNum3(statisticResult3.getNum());
                getProductUsedPrice.setPrice3(df.format(Float.parseFloat(statisticResult3.getPrice())));
                getProductUsedTotalPrice.setTotalPrice3(df.format(Float.parseFloat(statisticResult3.getTotalPrice())));

                params.setStart(params.getDate4s());
                params.setEnd(params.getDate4e());
                StatisticResult statisticResult4 = mapper.getStatistic(params);
                getProductUsedNum.setNum4(statisticResult4.getNum());
                getProductUsedPrice.setPrice4(df.format(Float.parseFloat(statisticResult4.getPrice())));
                getProductUsedTotalPrice.setTotalPrice4(df.format(Float.parseFloat(statisticResult4.getTotalPrice())));

                params.setStart(params.getDate5s());
                params.setEnd(params.getDate5e());
                StatisticResult statisticResult5 = mapper.getStatistic(params);
                getProductUsedNum.setNum5(statisticResult5.getNum());
                getProductUsedPrice.setPrice5(df.format(Float.parseFloat(statisticResult5.getPrice())));
                getProductUsedTotalPrice.setTotalPrice5(df.format(Float.parseFloat(statisticResult5.getTotalPrice())));

                params.setStart(params.getDate6s());
                params.setEnd(params.getDate6e());
                StatisticResult statisticResult6 = mapper.getStatistic(params);
                getProductUsedNum.setNum6(statisticResult6.getNum());
                getProductUsedPrice.setPrice6(df.format(Float.parseFloat(statisticResult6.getPrice())));
                getProductUsedTotalPrice.setTotalPrice6(df.format(Float.parseFloat(statisticResult6.getTotalPrice())));

                params.setStart(params.getDate7s());
                params.setEnd(params.getDate7e());
                StatisticResult statisticResult7 = mapper.getStatistic(params);
                getProductUsedNum.setNum7(statisticResult7.getNum());
                getProductUsedPrice.setPrice7(df.format(Float.parseFloat(statisticResult7.getPrice())));
                getProductUsedTotalPrice.setTotalPrice7(df.format(Float.parseFloat(statisticResult7.getTotalPrice())));

                params.setStart(params.getDate8s());
                params.setEnd(params.getDate8e());
                StatisticResult statisticResult8 = mapper.getStatistic(params);
                getProductUsedNum.setNum8(statisticResult8.getNum());
                getProductUsedPrice.setPrice8(df.format(Float.parseFloat(statisticResult8.getPrice())));
                getProductUsedTotalPrice.setTotalPrice8(df.format(Float.parseFloat(statisticResult8.getTotalPrice())));

                params.setStart(params.getDate9s());
                params.setEnd(params.getDate9e());
                StatisticResult statisticResult9 = mapper.getStatistic(params);
                getProductUsedNum.setNum9(statisticResult9.getNum());
                getProductUsedPrice.setPrice9(df.format(Float.parseFloat(statisticResult9.getPrice())));
                getProductUsedTotalPrice.setTotalPrice9(df.format(Float.parseFloat(statisticResult9.getTotalPrice())));

                params.setStart(params.getDate10s());
                params.setEnd(params.getDate10e());
                StatisticResult statisticResult10 = mapper.getStatistic(params);
                getProductUsedNum.setNum10(statisticResult10.getNum());
                getProductUsedPrice.setPrice10(df.format(Float.parseFloat(statisticResult10.getPrice())));
                getProductUsedTotalPrice.setTotalPrice10(df.format(Float.parseFloat(statisticResult10.getTotalPrice())));

                params.setStart(params.getDate11s());
                params.setEnd(params.getDate11e());
                StatisticResult statisticResult11 = mapper.getStatistic(params);
                getProductUsedNum.setNum11(statisticResult11.getNum());
                getProductUsedPrice.setPrice11(df.format(Float.parseFloat(statisticResult11.getPrice())));
                getProductUsedTotalPrice.setTotalPrice11(df.format(Float.parseFloat(statisticResult11.getTotalPrice())));

                params.setStart(params.getDate12s());
                params.setEnd(params.getDate12e());
                StatisticResult statisticResult12 = mapper.getStatistic(params);
                getProductUsedNum.setNum12(statisticResult12.getNum());
                getProductUsedPrice.setPrice12(df.format(Float.parseFloat(statisticResult12.getPrice())));
                getProductUsedTotalPrice.setTotalPrice12(df.format(Float.parseFloat(statisticResult12.getTotalPrice())));

                params.setStart(params.getDateAlls());
                params.setEnd(params.getDateAlle());
                StatisticResult statisticResult = mapper.getStatistic(params);
                getProductUsedNum.setNumTotal(statisticResult.getNum());
                getProductUsedPrice.setPriceAve(df.format(Float.parseFloat(statisticResult.getPrice())));
                getProductUsedTotalPrice.setTotalPriceTotal(df.format(Float.parseFloat(statisticResult.getTotalPrice())));

                getProductUsed.setProductUsedNums(getProductUsedNum);
                getProductUsed.setProductUsedPrices(getProductUsedPrice);
                getProductUsed.setProductUsedTotalPrices(getProductUsedTotalPrice);
            }
            list1.add(getProductUsedNum);
            list2.add(getProductUsedPrice);
            list3.add(getProductUsedTotalPrice);
            getProductUsed.setListNum(list1);
            getProductUsed.setListPrice(list2);
            getProductUsed.setListTotalPrice(list3);
        });
        return getProductUseds;
    }
}
