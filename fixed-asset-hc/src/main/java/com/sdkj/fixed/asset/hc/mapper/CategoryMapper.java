package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.ConsumeParams;
import com.sdkj.fixed.asset.api.hc.vo.in.MablesParams;
import com.sdkj.fixed.asset.api.hc.vo.in.StockParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.Category;
import feign.Param;

import java.util.Collection;
import java.util.List;

public interface CategoryMapper extends BaseMapper<Category> {
    /**
     * 仓库分页查询
     *
     * @param orgId
     * @return
     */
    List<Category> getPages(@Param("orgId") String orgId);

    /**
     * 查管理员
     *
     * @return
     */
    ReceiptUser getAdmin(@Param("userId") String userId);

    /**
     * 即时库存查询
     *
     * @param
     * @return
     */
    List<ReceiptStock>getStockList(StockParams params);

    /**
     * 耗材领用查询
     *
     * @param
     * @return
     */
    List<ReceiptMables>getMablesList(MablesParams params);

    /**
     * 耗材领用list
     *
     * @param
     * @return
     */
    List<ReceiptCon>MablesList(@Param("id") String id,@Param("startdate") String startdate,@Param("enddate") String enddate);


    /**
     * 耗材领用表
     *
     * @param
     * @return
     */
    List<ReceiptConsume>getConsumeList(ConsumeParams params);

    /**
     * 耗材领用表数量list
     *
     * @param
     * @return
     */
    List<ReceiptConsum>getNumlist(@Param("id") String id,@Param("year") String year);
    /**
     * 耗材领用表单价list
     *
     * @param
     * @return
     */
    List<ReceiptConsum>getPricelist(@Param("id") String id,@Param("year") String year);
    /**
     * 耗材领用表金额list
     *
     * @param
     * @return
     */
    List<ReceiptConsum>getTpricelist(@Param("id") String id,@Param("year") String year);

    /**
     * 可用仓库（禁用删除除外）
     * @param orgId
     * @return
     */
    List<Category> getPagesNew(String orgId);

    List<Category> checkCode(@Param("code") String code,@Param("orgId") String orgId, @Param("id") String id);
    List<Category> checkName(@Param("name") String name,@Param("orgId") String orgId, @Param("id") String id);
}