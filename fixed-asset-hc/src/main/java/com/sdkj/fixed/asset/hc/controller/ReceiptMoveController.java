package com.sdkj.fixed.asset.hc.controller;


import cn.hutool.core.date.DateUtil;
import com.sdkj.fixed.asset.api.assets.in_vo.BorrowIssueExtend;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseResult;
import com.sdkj.fixed.asset.api.hc.ReceiptMoveApi;
import com.sdkj.fixed.asset.api.hc.ReceiptSetApi;
import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptMoveGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptSetGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.hc.service.ReceiptMoveService;
import com.sdkj.fixed.asset.hc.service.ReceiptSetService;
import com.sdkj.fixed.asset.hc.util.CacheUtil;
import com.sdkj.fixed.asset.hc.util.ExcelStyle;
import com.sdkj.fixed.asset.hc.util.ExcelUtils;
import com.sdkj.fixed.asset.pojo.hc.Product;
import com.sdkj.fixed.asset.pojo.hc.ReceiptMove;
import com.sdkj.fixed.asset.pojo.hc.ReceiptMoveProduct;
import com.sdkj.fixed.asset.pojo.hc.ReceiptSet;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptMove;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptMove;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 史晨星
 * @ClassName: ReceiptSetController
 * @Description: 调整单
 * @date 2020年7月20日
 */
@Validated
@RestController()
public class ReceiptMoveController extends BaseController<ReceiptMove> implements ReceiptMoveApi {

    private static Logger log = LoggerFactory.getLogger(ReceiptMoveController.class);

    @Resource
    private ReceiptMoveService service;

    @Resource
    private HttpServletRequest request;

    @Override
    public BaseService<ReceiptMove> getService() {
        return service;
    }

    @Resource
    private CacheUtil cacheUtil;

    @Resource
    private HttpServletResponse response;

    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo add(@Validated({AddReceiptMove.class}) ReceiptMove entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setCuser(userId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setOrgId(orgId);
        entity.setState(1);
        entity.setConfirm(2);
        ReceiptMove receiptMove = service.add(entity);
        return BaseResultVo.success("success", receiptMove.getId());
    }

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo edit(@Validated({EditReceiptMove.class}) ReceiptMove entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setUserId(userId);
        ReceiptMove receiptMove = service.edit(entity, false);
        return BaseResultVo.success("success", receiptMove.getId());
    }

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo del(@Validated IdParams entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        ReceiptMove receiptMove = service.del(entity);
        return BaseResultVo.success("success", receiptMove.getId());
    }

    /**
     * 确认
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo confirm(@Validated ReceiptMove entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setConfirmUser(userId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setConfirmUser(userId);
        ReceiptMove receiptMove = service.confirm(entity);
        return BaseResultVo.success("success", receiptMove.getId());
    }

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo view(@Validated IdParams entity) throws Exception {
        ReceiptMove receiptMove = service.view(entity.getId());
        return BaseResultVo.success("success", receiptMove);
    }

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getPages(@Validated PageParams<ReceiptMoveGetPageParams> params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.getParams().setOrgId(orgId);
        params.getParams().setUserId(userId);
        List<ReceiptMoveGetPageNew> pageList = service.getPages(params);
        PageBean<ReceiptMoveGetPageNew> page = new PageBean<>(pageList);
        return BaseResultVo.success(page);
    }

    /**
     * 导出
     *
     * @param number
     * @param companyId
     * @param inCategoryId
     * @param outCategoryId
     * @param userId
     * @param response
     * @throws Exception
     */
    @Override
    public void export(String number, String companyId, String inCategoryId, String outCategoryId, String userId, HttpServletResponse response) throws Exception {
        ReceiptMoveGetPageParams params = new ReceiptMoveGetPageParams();
        params.setInCategoryId(inCategoryId);
        params.setOutCategoryId(outCategoryId);
        params.setOrgId(companyId);
        params.setUserId(userId);
        params.setNumber(number);
        List<ReceiptMoveGetPage> pageList = service.export(params);
        String fileName = "耗材调拨单";
        ExcelUtils.exportExcel(pageList, fileName, fileName, ReceiptMoveGetPage.class, fileName + TimeTool.getTimeDate14(), response);
    }

    /**
     * 打印
     *
     * @param ids
     * @throws Exception
     */
    @Override
    public void print(String ids) throws Exception {
        List<ReceiptMoveExtend> extendss = service.print(ids);
        String fileName = "耗材调拨单卡片";
        HSSFWorkbook wb = new HSSFWorkbook();
        extendss.stream().forEach(extend -> {
            String title = "调拨单卡片";
            String sheetName = extend.getNumber();

            HSSFRow row = null;
            HSSFCell cell = null;
            int rownum = 0;
            // 建立新的sheet对象（excel的表单） 并设置sheet名字
            HSSFSheet sheet = wb.createSheet(sheetName);
            sheet.setDefaultColumnWidth(18);
            sheet.setColumnWidth(0, 256 * 6);
            sheet.setDefaultRowHeight((short) 512);

            // 在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
            row = sheet.createRow(rownum++);
            row.setHeight((short) 1000);
            // 创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
            cell = row.createCell(1);
            // 合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 8));
            cell.setCellValue(title);
            cell.setCellStyle(ExcelStyle.styleTitle(wb));

            String[] row1 = {"调拨单号：", extend.getNumber(), "", "调出仓库：", extend.getOutCategoryName(), "", "调出日期：", extend.getOutTime()};
            String[] row2 = {"调入仓库：", extend.getInCategoryName(), "", "调出经办人：", extend.getOutUser(), "", "调出经办时间：", extend.getOutTime()};
            String[] row3 = {"调出备注：", extend.getOutComment(), "", "", "", "", "", ""};
            String[] row4 = {"调入日期", extend.getInTime(), "", "调入经办人：", extend.getInUser(), "", "调入经办时间：", extend.getInBussinessDate()};
            String[] row5 = {"调入备注：", extend.getInComment(), "", "", "", "", "", ""};
            String[] row6 = {};
            String[] row7 = {"物品编码", "物品名称", "商品编码", "规格型号", "单位", "出库数量", "出库单价", "出库金额"};
            List<String[]> rowList = new ArrayList<>();
            rowList.add(row1);
            rowList.add(row2);
            rowList.add(row3);
            rowList.add(row4);
            rowList.add(row5);
            rowList.add(row6);
            rowList.add(row7);

            for (String[] rows : rowList) {
                row = sheet.createRow(rownum++);
                for (int i = 0; i < rows.length; i++) {
                    cell = row.createCell(i + 1);
                    cell.setCellValue(rows[i]);
                    if (rownum == 4) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                        row.setHeight((short) 700);
                    } else if (rownum == 6) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                        row.setHeight((short) 700);
                    } else if (rownum == 2) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 3) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 7) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 5) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    }

                }
            }

            //合并单元格
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 6));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 5, 6));
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 8));
            sheet.addMergedRegion(new CellRangeAddress(4, 4, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(4, 4, 5, 6));
            sheet.addMergedRegion(new CellRangeAddress(5, 5, 2, 8));
            //遍历数据
            int j = 1;
            for (ReceiptMoveProductExtend result : extend.getReceiptMoveProductList()) {
                row = sheet.createRow(rownum++);
                String[] cells = {result.getCode(), result.getName(), result.getBarCode(), result.getModel(), result.getUnit(), result.getNum().toString(), result.getPrice(), result.getTotalPrice()};
                for (int i = 0; i < cells.length; i++) {
                    cell = row.createCell(i + 1);
                    cell.setCellValue(cells[i]);
                    cell.setCellStyle(ExcelStyle.styleCellValue(wb));
                }
            }
        });


        // 输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(fileName + ".xlsx", "UTF-8"));    //filename =  文件名
        response.setContentType("application/msexcel");
        wb.write(output);
        output.close();
    }
}

