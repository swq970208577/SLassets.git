package com.sdkj.fixed.asset.hc.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.sdkj.fixed.asset.api.hc.ProductTypeApi;
import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptInGetPageNew;
import com.sdkj.fixed.asset.api.hc.vo.out.Statistics;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.hc.pojo.ProductTypeTree;
import com.sdkj.fixed.asset.hc.service.ProductTypeService;
import com.sdkj.fixed.asset.hc.util.CacheUtil;
import com.sdkj.fixed.asset.hc.util.ExcelUtils;
import com.sdkj.fixed.asset.pojo.hc.Category;
import com.sdkj.fixed.asset.pojo.hc.Product;
import com.sdkj.fixed.asset.pojo.hc.ProductType;
import com.sdkj.fixed.asset.pojo.hc.group.AddProductType;
import com.sdkj.fixed.asset.pojo.hc.group.DelProductType;
import com.sdkj.fixed.asset.pojo.hc.group.EditProductType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 史晨星
 * @ClassName: CategoryController
 * @Description: 物品档案-分类管理
 * @date 2020年7月20日
 */
@Validated
@RestController()
public class ProductTypeController extends BaseController<ProductType> implements ProductTypeApi {

    private static Logger log = LoggerFactory.getLogger(ProductTypeController.class);

    @Resource
    private ProductTypeService service;

    @Resource
    private HttpServletRequest request;

    @Override
    public BaseService<ProductType> getService() {
        return service;
    }

    @Resource
    private CacheUtil cacheUtil;

    /**
     * 新增分类
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo add(@Validated({AddProductType.class}) ProductType entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setCuser(userId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setCtime(DateUtil.now());
        entity.setState(1);
        ProductType productType = service.add(entity);
        return BaseResultVo.success("success", productType.getId());
    }

    /**
     * 修改分类
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo edit(@Validated({EditProductType.class}) ProductType entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        ProductType productType = service.edit(entity);
        return BaseResultVo.success("success", productType.getId());
    }

    /**
     * 删除分类
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo del(@Validated({DelProductType.class}) ProductType entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        ProductType productType = service.del(entity);
        return BaseResultVo.success("success", productType.getId());
    }

    /**
     * 分类树状图
     *
     * @param
     * @return
     */
    @Override
    public BaseResultVo getTree() throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        List<ProductType> productTypeList = service.getTree(orgId);
        return BaseResultVo.success("success", productTypeList);
    }

    /**
     * 分类树状图App
     *
     * @param
     * @return
     */
    @Override
    public BaseResultVo getTreeApp() throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        List<ProductTypeTree> productTypeList = service.getTreeApp(orgId);
        return BaseResultVo.success("success", productTypeList);
    }

    /**
     * 导出
     *
     * @param companyId
     * @param response
     * @throws Exception
     */
    @Override
    public void export(String companyId, HttpServletResponse response) throws Exception {
        List<ProductType> list = service.getAll(companyId);
        String fileName = "耗材分类" + TimeTool.getTimeDate14();
        ExcelUtils.exportExcel(list, "耗材分类", "耗材分类", ProductType.class, fileName, response);
    }

    /**
     * 导出
     *
     * @param userId
     * @param orgId
     * @param categoryId
     * @param start
     * @param end
     * @param response
     * @throws Exception
     */
    @Override
    public void exportStatistics(String userId, String orgId, String categoryId, String start, String end, HttpServletResponse response) throws Exception {
        StatisticsParams params = new StatisticsParams();
        params.setOrgId(orgId);
        params.setCategoryId(categoryId);

        if (StrUtil.isNotBlank(params.getStart())) {
            byte[] space = new byte[]{(byte) 0xc2, (byte) 0xa0};
            String UTFSpace = null;
            try {
                UTFSpace = new String(space, "utf-8");
            } catch (Exception e) {
                log.error("", e);
            }
            // 这里的smsContent就是前端传过来的包含乱码的值。
            params.setStart(start.replace(UTFSpace, " "));
            params.setEnd(end.replace(UTFSpace, " "));
        }

        params.setUserId(userId);
        List<Statistics> productTypeList = service.statistics(params);
        String fileName = "耗材分类统计" + TimeTool.getTimeDate14();
        ExcelUtils.exportExcel(productTypeList, "耗材分类统计", "耗材分类统计", Statistics.class, fileName, response);
    }

    /**
     * 报表
     *
     * @param
     * @return
     */
    @Override
    public BaseResultVo statistics(StatisticsParams params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.setUserId(userId);
        params.setOrgId(orgId);


        if (StrUtil.isNotBlank(params.getStart())) {
            byte[] space = new byte[]{(byte) 0xc2, (byte) 0xa0};
            String UTFSpace = null;
            try {
                UTFSpace = new String(space, "utf-8");
            } catch (Exception e) {
                log.error("", e);
            }
            // 这里的smsContent就是前端传过来的包含乱码的值。
            params.setStart(params.getStart().replace(UTFSpace, " "));
            params.setEnd(params.getEnd().replace(UTFSpace, " "));
        }

        List<Statistics> productTypeList = service.statistics(params);
        return BaseResultVo.success("success", productTypeList);
    }

    /**
     * 报表横轴
     *
     * @param
     * @return
     */
    @Override
    public BaseResultVo productList(StatisticsParams params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.setUserId(userId);
        params.setOrgId(orgId);
        List<Product> productList = service.productList(params);
        return BaseResultVo.success("success", productList);
    }

    /**
     * 分类树状图App
     *
     * @param
     * @return
     */
    @Override
    public BaseResultVo productTypes(StatisticsParams params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.setUserId(userId);
        params.setOrgId(orgId);
        List<ProductType> productTypes = service.productTypes(params);
        return BaseResultVo.success("success", productTypes);
    }

}

