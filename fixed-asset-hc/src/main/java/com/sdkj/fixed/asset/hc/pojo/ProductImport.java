package com.sdkj.fixed.asset.hc.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelDataModel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;

import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 用户
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
public class ProductImport implements Serializable, IExcelDataModel, IExcelModel {
    /**
     * 行号
     */

    private int rowNum;

    /**
     * 错误消息
     */

    private String errorMsg;


    @NotBlank(message = "物品分类名称-不能为空")
    @Size(max = 32,message = "物品分类名称-最大长度为32")
    @Excel(name="物品分类名称（必填）")
    private String typeName;


    @Excel(name="物品编码（必填）")
    @NotBlank(message = "物品编码-不能为空")
    @Size(max = 32,message = "物品编码-最大长度为32")
    private String code;

    @NotBlank(message = "物品名称-不能为空")
    @Size(max = 16,message = "物品名称-最大长度为16")
    @Excel(name="物品名称（必填）")
    private String name;

    @NotBlank(message = "商品码-不能为空")
    @Size(max = 32,message = "商品码-最大长度为-32")
    @Excel(name="商品码（必填）")
    private String barCode;

    @Size(max = 16,message = "规格型号-最大长度为-16")
    @Excel(name="规格型号")
    private String model;

    @Size(max = 8,message = "单价-最大长度为-8")
    @Excel(name="单价")
    private String price;

    @Size(max = 8,message = "单位-最大长度为-8")
    @Excel(name="单位")
    private String unit;

    @Excel(name="安全库存下限")
    private Integer safeMin;

    @Excel(name="安全库存上限")
    private Integer safeMax;

    @Excel(name="是否禁用（Y表示禁用，N表示不禁用，默认N）")
    private String forbidden;

    @Override
    public int getRowNum() {
        return rowNum;
    }

    @Override
    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getSafeMin() {
        return safeMin;
    }

    public void setSafeMin(Integer safeMin) {
        this.safeMin = safeMin;
    }

    public Integer getSafeMax() {
        return safeMax;
    }

    public void setSafeMax(Integer safeMax) {
        this.safeMax = safeMax;
    }

    public String getForbidden() {
        return forbidden;
    }

    public void setForbidden(String forbidden) {
        this.forbidden = forbidden;
    }
}