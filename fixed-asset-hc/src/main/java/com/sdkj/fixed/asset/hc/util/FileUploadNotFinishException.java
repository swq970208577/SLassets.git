package com.sdkj.fixed.asset.hc.util;
/**
 * 逻辑错误
 * @author zhangxin
 *
 */
public class FileUploadNotFinishException extends RuntimeException {


	private static final long serialVersionUID = -3185738799338959542L;

	public FileUploadNotFinishException(String message) {
		super(message);
	}
}
