package com.sdkj.fixed.asset.hc.controller;

/**
 * Demo
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/14 21:14
 */
public class Demo {
    private static void qq(float i) {
        float j = (float) 0.00;
        int k = 0;
        while (j < 1000) {
            j = (float) (i * 1.02);
            i *= 1.02;
            k++;
            System.out.println("day:"+k+"   "+"total:"+j);
        }
    }

    public static void main(String[] args) {
        qq(5);
    }
}
