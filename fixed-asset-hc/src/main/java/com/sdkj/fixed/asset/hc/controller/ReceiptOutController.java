package com.sdkj.fixed.asset.hc.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.sdkj.fixed.asset.api.hc.ReceiptOutApi;
import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptOutGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.hc.mapper.OrgManagementMapper;
import com.sdkj.fixed.asset.hc.mapper.UserManagementMapper;
import com.sdkj.fixed.asset.hc.service.ReceiptOutService;
import com.sdkj.fixed.asset.hc.util.CacheUtil;
import com.sdkj.fixed.asset.hc.util.ExcelStyle;
import com.sdkj.fixed.asset.hc.util.ExcelUtils;
import com.sdkj.fixed.asset.pojo.hc.ReceiptOut;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptOut;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptOut;
import com.sdkj.fixed.asset.pojo.hc.group.Sign;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 史晨星
 * @ClassName: ReceiptOutController
 * @Description: 出库单
 * @date 2020年7月20日
 */
@Validated
@RestController()
public class ReceiptOutController extends BaseController<ReceiptOut> implements ReceiptOutApi {

    private static Logger log = LoggerFactory.getLogger(ReceiptOutController.class);

    @Resource
    private ReceiptOutService service;

    @Resource
    private UserManagementMapper userManagementMapper;

    @Resource
    private OrgManagementMapper orgManagementMapper;

    @Resource
    private HttpServletRequest request;

    @Override
    public BaseService<ReceiptOut> getService() {
        return service;
    }

    @Resource
    private HttpServletResponse response;

    @Resource
    private CacheUtil cacheUtil;

    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo add(@Validated({AddReceiptOut.class}) ReceiptOut entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setCtime(DateUtil.now());
        entity.setCuser(userId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setOrgId(orgId);
        entity.setState(1);
        ReceiptOut receiptOut = service.add(entity);
        return BaseResultVo.success("success", receiptOut.getId());
    }

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo edit(@Validated({EditReceiptOut.class}) ReceiptOut entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setUserId(userId);
        ReceiptOut receiptOut = service.edit(entity, false);
        return BaseResultVo.success("success", receiptOut.getId());
    }

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo del(@Validated IdParams entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        ReceiptOut receiptOut = service.del(entity);
        return BaseResultVo.success("success", receiptOut.getId());
    }

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo view(@Validated IdParams entity) throws Exception {
        ReceiptOut receiptOut = service.view(entity.getId());
        if (StrUtil.isNotBlank(receiptOut.getReceiveUser())) {
            receiptOut.setReceiveUserName(userManagementMapper.selectByPrimaryKey(receiptOut.getReceiveUser()).getName());
            receiptOut.setReceiveOrgName(orgManagementMapper.selectByPrimaryKey(receiptOut.getReceiveOrg()).getName());
            receiptOut.setOrgTreeCode(orgManagementMapper.selectByPrimaryKey(receiptOut.getReceiveOrg()).getTreecode());
            receiptOut.setReceiveDeptName(orgManagementMapper.selectByPrimaryKey(receiptOut.getReceiveDept()).getName());
            receiptOut.setDeptTreeCode(orgManagementMapper.selectByPrimaryKey(receiptOut.getReceiveDept()).getTreecode());
        } else {
            receiptOut.setReceiveUserName("");
            receiptOut.setReceiveOrgName("");
            receiptOut.setReceiveDeptName("");
            receiptOut.setOrgTreeCode("");
            receiptOut.setDeptTreeCode("");
        }
        return BaseResultVo.success("success", receiptOut);
    }

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getPages(@Validated PageParams<ReceiptOutGetPageParams> params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.getParams().setOrgId(orgId);
        params.getParams().setUserId(userId);
        if (StrUtil.isNotBlank(params.getParams().getStart())) {
            byte[] space = new byte[]{(byte) 0xc2, (byte) 0xa0};
            String UTFSpace = null;
            try {
                UTFSpace = new String(space, "utf-8");
            } catch (Exception e) {
                log.error("", e);
            }
            // 这里的smsContent就是前端传过来的包含乱码的值。
            params.getParams().setStart(params.getParams().getStart().replace(UTFSpace, " "));
            params.getParams().setEnd(params.getParams().getEnd().replace(UTFSpace, " "));
        }
        List<ReceiptOutGetPageNew> pageList = service.getPages(params);
        PageBean<ReceiptOutGetPageNew> page = new PageBean<>(pageList);
        return BaseResultVo.success(page);
    }


    /**
     * 签字
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo sign(@Validated({Sign.class}) ReceiptOut entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setUserId(userId);
        ReceiptOut receiptOut = service.sign(entity);
        return BaseResultVo.success("success", receiptOut);
    }

    /**
     * 打印盘亏出库
     *
     * @param ids
     * @throws Exception
     */
    @Override
    public void printCheck(String ids) throws Exception {
        List<ReceiptOutCheckExtend> extendss = service.printCheck(ids);
        String fileName = "盘亏出库单卡片";
        HSSFWorkbook wb = new HSSFWorkbook();
        extendss.stream().forEach(extend -> {
            String title = "盘亏出库单卡片";
            String sheetName = extend.getNumber();

            HSSFRow row = null;
            HSSFCell cell = null;
            int rownum = 0;
            // 建立新的sheet对象（excel的表单） 并设置sheet名字
            HSSFSheet sheet = wb.createSheet(sheetName);
            sheet.setDefaultColumnWidth(18);
            sheet.setColumnWidth(0, 256 * 6);
            sheet.setDefaultRowHeight((short) 512);

            // 在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
            row = sheet.createRow(rownum++);
            row.setHeight((short) 1000);
            // 创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
            cell = row.createCell(1);
            // 合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 8));
            cell.setCellValue(title);
            cell.setCellStyle(ExcelStyle.styleTitle(wb));

            String[] row1 = {"出库单号：", extend.getNumber(), "", "出库仓库：", extend.getCategoryName(), "", "业务时间：", extend.getBussinessDate()};
            String[] row2 = {"经办人：", extend.getCuser(), "", "经办时间：", extend.getCtime(), "", "", ""};
            String[] row3 = {"出库备注：", extend.getComment(), "", "", "", "", "", ""};
            String[] row4 = {};
            String[] row5 = {"物品编码", "物品名称", "商品编码", "规格型号", "单位", "出库数量", "出库单价", "出库金额"};
            List<String[]> rowList = new ArrayList<>();
            rowList.add(row1);
            rowList.add(row2);
            rowList.add(row3);
            rowList.add(row4);
            rowList.add(row5);
            for (String[] rows : rowList) {
                row = sheet.createRow(rownum++);
                for (int i = 0; i < rows.length; i++) {
                    cell = row.createCell(i + 1);
                    cell.setCellValue(rows[i]);
                    if (rownum == 4) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                        row.setHeight((short) 700);
                    } else if (rownum == 6) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 2) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 3) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 7) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 5) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    }

                }
            }

            //合并单元格
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 6));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 5, 8));
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 8));
            //遍历数据
            int j = 1;
            for (ReceiptMoveProductExtend result : extend.getReceiptMoveProductList()) {
                row = sheet.createRow(rownum++);
                String[] cells = {result.getCode(), result.getName(), result.getBarCode(), result.getModel(), result.getUnit(), result.getNum().toString(), result.getPrice(), result.getTotalPrice()};
                for (int i = 0; i < cells.length; i++) {
                    cell = row.createCell(i + 1);
                    cell.setCellValue(cells[i]);
                    cell.setCellStyle(ExcelStyle.styleCellValue(wb));
                }
            }
        });


        // 输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(fileName + ".xlsx", "UTF-8"));    //filename =  文件名
        response.setContentType("application/msexcel");
        wb.write(output);
        output.close();
    }

    /**
     * 打印出库
     *
     * @param ids
     * @throws Exception
     */
    @Override
    public void print(String ids) throws Exception {
        List<ReceiptOutExtend> extendss = service.print(ids);
        String fileName = "耗材出库单卡片";
        HSSFWorkbook wb = new HSSFWorkbook();
        extendss.stream().forEach(extend -> {
            String title = "";
            if (extend.getType() == 1) {
                title = "出库单卡片";
            } else {
                title = "退库单卡片";
            }
            String sheetName = extend.getNumber();

            HSSFRow row = null;
            HSSFCell cell = null;
            int rownum = 0;
            // 建立新的sheet对象（excel的表单） 并设置sheet名字
            HSSFSheet sheet = wb.createSheet(sheetName);
            sheet.setDefaultColumnWidth(18);
            sheet.setColumnWidth(0, 256 * 6);
            sheet.setDefaultRowHeight((short) 512);

            // 在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
            row = sheet.createRow(rownum++);
            row.setHeight((short) 1000);
            // 创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
            cell = row.createCell(1);
            // 合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 8));
            cell.setCellValue(title);
            cell.setCellStyle(ExcelStyle.styleTitle(wb));

            String[] row1 = {"出库单号：", extend.getNumber(), "", "出库仓库：", extend.getCategoryName(), "", "业务时间：", extend.getBussinessDate()};
            String[] row2 = {"领用公司：", extend.getReceiveOrg(), "", "领用部门：", extend.getReceiveDept(), "", "领用人", extend.getReceiveUser()};
            String[] row3 = {"经办人", extend.getCuser(), "", "经办时间：", extend.getCtime(), "", "", ""};
            String[] row4 = {"出库备注：", extend.getComment(), "", "", "", "", "", ""};
            String[] row5 = {};
            String[] row6 = {"物品编码", "物品名称", "商品编码", "规格型号", "单位", "出库数量", "出库单价", "出库金额"};
            List<String[]> rowList = new ArrayList<>();
            rowList.add(row1);
            rowList.add(row2);
            rowList.add(row3);
            rowList.add(row4);
            rowList.add(row5);
            rowList.add(row6);

            for (String[] rows : rowList) {
                row = sheet.createRow(rownum++);
                for (int i = 0; i < rows.length; i++) {
                    cell = row.createCell(i + 1);
                    cell.setCellValue(rows[i]);
                    if (rownum == 4) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 6) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 2) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 3) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 7) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    } else if (rownum == 5) {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                        row.setHeight((short) 700);
                    } else {
                        cell.setCellStyle(ExcelStyle.styleCellName(wb));
                    }

                }
            }
            //合并单元格
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 6));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 5, 6));
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 5, 8));
            sheet.addMergedRegion(new CellRangeAddress(4, 4, 2, 8));
            //遍历数据
            int j = 1;
            for (ReceiptMoveProductExtend result : extend.getReceiptMoveProductList()) {
                row = sheet.createRow(rownum++);
                String[] cells = {result.getCode(), result.getName(), result.getBarCode(), result.getModel(), result.getUnit(), result.getNum().toString(), result.getPrice(), result.getTotalPrice()};
                for (int i = 0; i < cells.length; i++) {
                    cell = row.createCell(i + 1);
                    cell.setCellValue(cells[i]);
                    cell.setCellStyle(ExcelStyle.styleCellValue(wb));
                }
            }
            String[] lastRow = {"领用人签字：", "", "", "", "", "", "签字时间：", ""};
            row = sheet.createRow(rownum++); // 创建最后一行
            row.setHeight((short) 700);
            for (int i = 0; i < lastRow.length; i++) {
                cell = row.createCell(i + 1);
                cell.setCellStyle(ExcelStyle.styleNotBorder(wb, 2));
            }
        });
        // 输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(fileName + ".xlsx", "UTF-8"));    //filename =  文件名
        response.setContentType("application/msexcel");
        wb.write(output);
        output.close();
    }

    /**
     * 导出盘亏
     *
     * @param number
     * @param userId
     * @param companyId
     * @param categoryId
     * @param response
     * @throws Exception
     */
    @Override
    public void exportCheck(String number, String userId, String companyId, String categoryId, HttpServletResponse response) throws Exception {
        ReceiptOutGetPageParams params = new ReceiptOutGetPageParams();
        params.setNumber(number);
        params.setCategoryId(categoryId);
        params.setOrgId(companyId);
        params.setUserId(userId);
        List<ReceiptOutCheck> list = service.exportCheck(params);
        String fileName = "耗材盘亏出库单";
        ExcelUtils.exportExcel(list, fileName, fileName, ReceiptOutCheck.class, fileName + TimeTool.getTimeDate14(), response);
    }

    /**
     * 导出
     *
     * @param number
     * @param userId
     * @param companyId
     * @param categoryId
     * @param start
     * @param end
     * @param response
     * @throws Exception
     */
    @Override
    public void export(String number, String userId, String companyId, String categoryId, String start, String end, HttpServletResponse response) throws Exception {
        ReceiptOutGetPageParams params = new ReceiptOutGetPageParams();
        params.setNumber(number);
        params.setCategoryId(categoryId);
        params.setOrgId(companyId);
        params.setUserId(userId);
        if (StrUtil.isNotBlank(start)) {
            byte[] space = new byte[]{(byte) 0xc2, (byte) 0xa0};
            String UTFSpace = null;
            try {
                UTFSpace = new String(space, "utf-8");
            } catch (Exception e) {
                log.error("", e);
            }
            // 这里的smsContent就是前端传过来的包含乱码的值。
            start = start.replace(UTFSpace, " ");
            end = end.replace(UTFSpace, " ");
        }
        params.setStart(start);
        params.setEnd(end);
        params.setType(2);
        List<ReceiptOutExport> list = service.export(params);
        String fileName = "耗材出库单表";
        ExcelUtils.exportExcel(list, fileName, fileName, ReceiptOutExport.class, fileName + TimeTool.getTimeDate14(), response);
    }
}

