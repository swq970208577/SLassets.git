package com.sdkj.fixed.asset.hc.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.OrgManagement;


/**
 * @ClassName OrgMapper
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/20 15:51
 */
public interface OrgManagementMapper extends BaseMapper<OrgManagement> {
}
