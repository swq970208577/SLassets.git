package com.sdkj.fixed.asset.hc.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.nacos.common.util.UuidUtils;
import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.hc.vo.in.ProductExtends;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckConfirmParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckProductGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptCheckDetail;
import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptCheckInfo;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.hc.mapper.*;
import com.sdkj.fixed.asset.pojo.hc.*;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import com.sun.org.apache.bcel.internal.generic.NEWARRAY;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/***
 *
 * @author scx
 *
 */
@Service
public class ReceiptCheckService extends BaseService<ReceiptCheck> {

    private static Logger log = LoggerFactory.getLogger(ReceiptCheckService.class);

    @Resource
    private ReceiptCheckMapper mapper;

    @Resource
    private ProductMapper productMapper;

    @Resource
    private ProductTypeMapper productTypeMapper;

    @Resource
    private UserManagementMapper userManagementMapper;

    @Resource
    private CategoryMapper categoryMapper;

    @Resource
    private ReceiptCheckProductMapper receiptCheckProductMapper;

    @Resource
    private ReceiptInProductMapper receiptInProductMapper;

    @Resource
    private ReceiptOutProductMapper receiptOutProductMapper;

    @Resource
    private ProductService productService;

    @Resource
    private ReceiptInService receiptInService;

    @Resource
    private ReceiptOutService receiptOutService;

    @Resource
    private CategoryProductMapper categoryProductMapper;

    @Override
    public BaseMapper<ReceiptCheck> getMapper() {
        return mapper;
    }

    @Resource
    private ReceiptCheckUserMapper receiptCheckUserMapper;


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @Transactional
    public ReceiptCheck add(ReceiptCheck entity) throws Exception {
        entity.setStarttime(DateUtil.now());
        if (categoryMapper.selectByPrimaryKey(entity.getCategoryId()).getIsCheck() == 1) {
            throw new LogicException("仓库正在盘点中");
        }
        mapper.insertSelective(entity);
        //用户-盘点单
        String executor = entity.getExecutor();
        if (StrUtil.isEmpty(entity.getExecutor())) {
            throw new LogicException("必须分配用户");
        }
        if (!entity.getExecutor().contains(entity.getCuser())) {
            executor = entity.getExecutor() + "," + entity.getCuser();
        }
        for (String s : executor.split(",")) {
            ReceiptCheckUser receiptCheckUser = new ReceiptCheckUser();
            receiptCheckUser.setReceiptId(entity.getId());
            receiptCheckUser.setUserId(s);
            receiptCheckUserMapper.insertSelective(receiptCheckUser);
        }
        //锁仓库
        Category category = categoryMapper.selectByPrimaryKey(entity.getCategoryId());
        category.setIsCheck(1);
        categoryMapper.updateByPrimaryKeySelective(category);
        //生成仓库下盘点详情
        String categoryId = entity.getCategoryId();
        Example example = new Example(CategoryProduct.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("categoryId", categoryId);
        List<CategoryProduct> categoryProductList = categoryProductMapper.selectByExample(example);
        if (categoryProductList.size() == 0) {
            throw new LogicException("仓库下没有物品不能盘点");
        }
        for (CategoryProduct categoryProduct : categoryProductList) {
            ReceiptCheckProduct receiptCheckProduct = new ReceiptCheckProduct();
            receiptCheckProduct.setReceiptId(entity.getId());
            receiptCheckProduct.setProductId(categoryProduct.getProductId());
            receiptCheckProduct.setTotalPrice(categoryProduct.getTotalPrice());
            receiptCheckProduct.setRealNum(0);
            receiptCheckProduct.setRealTotalPrice("0.00");
            receiptCheckProduct.setNum(categoryProduct.getProductNum());
            receiptCheckProduct.setPrice(categoryProduct.getPrice());
            receiptCheckProduct.setState(1);
            receiptCheckProduct.setSurplus(2);
            receiptCheckProductMapper.insertSelective(receiptCheckProduct);
        }
        return entity;
    }

    /**
     * 删除
     *
     * @param entity
     * @return
     * @throws Exception
     */
    @Transactional
    public ReceiptCheck del(ReceiptCheck entity) throws Exception {
        ReceiptCheck receiptCheck = mapper.selectByPrimaryKey(entity.getId());
        if (receiptCheck.getFinish() == 1) {
            throw new LogicException("已完成的盘点单不能删除");
        }
        Category category = categoryMapper.selectByPrimaryKey(receiptCheck.getCategoryId());
        category.setIsCheck(2);
        categoryMapper.updateByPrimaryKeySelective(category);
        receiptCheck.setState(2);
        mapper.updateByPrimaryKeySelective(receiptCheck);
        Example example = new Example(ReceiptCheckProduct.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiptId", entity.getId());
        List<ReceiptCheckProduct> receiptCheckProductList = receiptCheckProductMapper.selectByExample(example);
        for (ReceiptCheckProduct receiptCheckProduct : receiptCheckProductList) {
            receiptCheckProductMapper.delete(receiptCheckProduct);
            //删除新增盘盈时创建的物品
            if (receiptCheckProduct.getSurplus() == 1) {
                productMapper.deleteByPrimaryKey(receiptCheckProduct.getProductId());
            }
        }
        return entity;
    }

    /**
     * 分配用户
     *
     * @param entity
     * @return
     * @throws Exception
     */
    @Transactional
    public ReceiptCheck setExecutor(ReceiptCheck entity) throws Exception {
        ReceiptCheck receiptCheck = mapper.selectByPrimaryKey(entity.getId());
        if (receiptCheck.getFinish() == 1) {
            throw new LogicException("已完成的盘点单不能分配用户");
        }
        Example example = new Example(ReceiptCheckUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiptId", receiptCheck.getId());
        receiptCheckUserMapper.deleteByExample(example);
        String executor = entity.getExecutor();
        if (!entity.getExecutor().contains(entity.getCuser())) {
            executor = entity.getExecutor() + "," + entity.getCuser();
        }
        for (String s : executor.split(",")) {
            ReceiptCheckUser receiptCheckUser = new ReceiptCheckUser();
            receiptCheckUser.setReceiptId(receiptCheck.getId());
            receiptCheckUser.setUserId(s);
            receiptCheckUserMapper.insertSelective(receiptCheckUser);
        }
        return entity;
    }


    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Transactional
    public List<ReceiptCheckDetail> getPages(PageParams<ReceiptCheckProductGetPageParams> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ReceiptCheckDetail> pageList = mapper.getPages(params.getParams());
        DecimalFormat df = new DecimalFormat("0.00");//设置保留位数
        for (ReceiptCheckDetail receiptCheckDetail : pageList) {
            receiptCheckDetail.setPrice(df.format(Float.parseFloat(receiptCheckDetail.getPrice())));
            receiptCheckDetail.setTotalPrice(df.format(Float.parseFloat(receiptCheckDetail.getTotalPrice())));
            receiptCheckDetail.setTypeName(productTypeMapper.selectByPrimaryKey(receiptCheckDetail.getType()).getName());
            if (StrUtil.isNotBlank(receiptCheckDetail.getCheckUser())) {
                receiptCheckDetail.setCheckUserName(userManagementMapper.selectByPrimaryKey(receiptCheckDetail.getCheckUser()).getName());
            }
        }
        return pageList;
    }

    /**
     * 查看
     *
     * @param id
     * @return
     */
    @Transactional
    public ReceiptCheckInfo view(String id) {
        ReceiptCheck receiptCheck = mapper.selectByPrimaryKey(id);
        Example example1 = new Example(ReceiptCheckUser.class);
        Example.Criteria criteria1 = example1.createCriteria();
        criteria1.andEqualTo("receiptId", receiptCheck.getId());
        List<ReceiptCheckUser> receiptCheckUserList = receiptCheckUserMapper.selectByExample(example1);
        StringBuffer sb = new StringBuffer();
        StringBuffer sb1 = new StringBuffer();
        int i = 0;
        for (ReceiptCheckUser receiptCheckUser : receiptCheckUserList) {
            sb1.append(receiptCheckUser.getUserId());
            sb.append(userManagementMapper.selectByPrimaryKey(receiptCheckUser.getUserId()).getName());
            if (i < receiptCheckUserList.size()) {
                sb.append(",");
                sb1.append(",");
            }
        }
        ReceiptCheckInfo receiptCheckInfo = new ReceiptCheckInfo();
        receiptCheckInfo.setName(receiptCheck.getName());
        receiptCheckInfo.setExecutorName(sb.toString());
        receiptCheckInfo.setStartTime(receiptCheck.getCtime());
        receiptCheckInfo.setFinish(receiptCheck.getFinish());
        receiptCheckInfo.setExecutor(sb1.toString());
        if (StrUtil.isNotBlank(receiptCheck.getEndtime())) {
            receiptCheckInfo.setEndTime(receiptCheck.getEndtime());
        } else {
            receiptCheckInfo.setEndTime("未完成");
        }
        receiptCheckInfo.setCuserName(userManagementMapper.selectByPrimaryKey(receiptCheck.getCuser()).getName());
        receiptCheckInfo.setCategoryName(categoryMapper.selectByPrimaryKey(receiptCheck.getCategoryId()).getName());
        int finished = 0;
        int notFinished = 0;
        Example example = new Example(ReceiptCheckProduct.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiptId", receiptCheck.getId());
        List<ReceiptCheckProduct> receiptCheckProductList = receiptCheckProductMapper.selectByExample(example);
        for (ReceiptCheckProduct receiptCheckProduct : receiptCheckProductList) {
            if (receiptCheckProduct.getState() == 1) {
                notFinished++;
            } else {
                finished++;
            }
        }
        receiptCheckInfo.setNum(finished + notFinished);
        receiptCheckInfo.setFinished(finished);
        receiptCheckInfo.setNotFinished(notFinished);
        return receiptCheckInfo;
    }

    /**
     * 新增盘盈
     *
     * @param entity
     * @return
     */
    @Transactional
    public ReceiptCheckProduct surplus(ProductExtends entity) {
        if (mapper.selectByPrimaryKey(entity.getRecepitId()).getFinish() == 1) {
            throw new LogicException("盘点单已完成");
        }
        //新增盘盈物品，标记为临时物品
        entity.setState(1);
        Product product = productService.add(entity);
        //新增盘盈记录
        ReceiptCheckProduct receiptCheckProduct = new ReceiptCheckProduct();
        receiptCheckProduct.setReceiptId(entity.getRecepitId());
        receiptCheckProduct.setProductId(product.getId());
        receiptCheckProduct.setTotalPrice("0");
        receiptCheckProduct.setNum(0);
        receiptCheckProduct.setPrice("0");
        receiptCheckProduct.setCheckUser(entity.getCuser());
        receiptCheckProduct.setCheckTime(DateUtil.now());
        receiptCheckProduct.setComment(entity.getComment());
        receiptCheckProduct.setRealNum(entity.getRealNum());
        receiptCheckProduct.setRealTotalPrice(entity.getRealTotalPrice());
        receiptCheckProduct.setState(3);
        receiptCheckProduct.setSurplus(1);
        receiptCheckProductMapper.insertSelective(receiptCheckProduct);
        return receiptCheckProduct;
    }

    /**
     * 编辑盘盈单（手工盘点）
     *
     * @param entity
     * @return
     */
    @Transactional
    public ReceiptCheckProduct edit(ReceiptCheckProduct entity) {
        ReceiptCheckProduct receiptCheckProduct = receiptCheckProductMapper.selectByPrimaryKey(entity.getId());
        if (mapper.selectByPrimaryKey(receiptCheckProduct.getReceiptId()).getFinish() == 1) {
            throw new LogicException("盘点单已完成");
        }
        receiptCheckProduct.setRealNum(entity.getRealNum());
        receiptCheckProduct.setRealTotalPrice(entity.getRealTotalPrice());
        receiptCheckProduct.setCheckUser(entity.getCheckUser());
        receiptCheckProduct.setCheckTime(DateUtil.now());
        if (StrUtil.isEmpty(entity.getComment())) {
            entity.setComment("");
        }
        receiptCheckProduct.setComment(entity.getComment());
        if (entity.getRealNum() < receiptCheckProduct.getNum()) {
            receiptCheckProduct.setState(4);
        }
        if (entity.getRealNum() > receiptCheckProduct.getNum()) {
            receiptCheckProduct.setState(3);
        }
        if (entity.getRealNum() == receiptCheckProduct.getNum()) {
            receiptCheckProduct.setState(2);
        }
        receiptCheckProductMapper.updateByPrimaryKeySelective(receiptCheckProduct);
        return receiptCheckProduct;
    }

    /**
     * 删除盘盈单（只删除新增盘盈创建的记录）
     *
     * @param entity
     * @return
     */
    @Transactional
    public ReceiptCheckProduct delCheckProduct(ReceiptCheckProduct entity) {
        ReceiptCheckProduct receiptCheckProduct = receiptCheckProductMapper.selectByPrimaryKey(entity.getId());
        if (receiptCheckProductMapper.selectByPrimaryKey(entity.getId()).getSurplus() != 1) {
            throw new LogicException("只能删除盘盈物品");
        }
        receiptCheckProductMapper.deleteByPrimaryKey(entity.getId());
        productMapper.deleteByPrimaryKey(receiptCheckProduct.getProductId());
        return entity;
    }

    /**
     * 确认盘点结果
     *
     * @param entity
     * @throws Exception
     */
    @Transactional
    public void confirm(ReceiptCheckConfirmParams entity) throws Exception {
        ReceiptCheck receiptCheck = mapper.selectByPrimaryKey(entity.getRecepitId());
        if (receiptCheck.getFinish() == 1) {
            throw new LogicException("盘点单已完成");
        }
        receiptCheck.setFinish(1);
        receiptCheck.setEndtime(DateUtil.now());
        mapper.updateByPrimaryKeySelective(receiptCheck);
        //解锁
        Category category = categoryMapper.selectByPrimaryKey(receiptCheck.getCategoryId());
        category.setIsCheck(2);
        categoryMapper.updateByPrimaryKeySelective(category);
        Example example = new Example(ReceiptCheckProduct.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiptId", entity.getRecepitId());
        List<ReceiptCheckProduct> receiptCheckProductList = receiptCheckProductMapper.selectByExample(example);
        for (ReceiptCheckProduct receiptCheckProduct : receiptCheckProductList) {
            if (receiptCheckProduct.getState() == 1) {
                throw new LogicException("有未完成的盘点单，不能提交");
            }
            //盘盈
            if (receiptCheckProduct.getState() == 3) {
                //生成入库单-物品关联
                List<ReceiptInProduct> receiptInProductList = new ArrayList<>();
                ReceiptInProduct receiptInProduct = new ReceiptInProduct();
                receiptInProduct.setProductId(receiptCheckProduct.getProductId());
                receiptInProduct.setReceiptId(receiptCheckProduct.getId());
                String format = "";
                DecimalFormat df = new DecimalFormat("0.00");//设置保留位数

                Float totalPrice = Float.parseFloat(receiptCheckProduct.getRealTotalPrice());
                format = df.format((float) totalPrice);
                String price = df.format((float) totalPrice / receiptCheckProduct.getRealNum());
                receiptInProduct.setPrice(price);
                //物品状态变成正常
                Product product = productMapper.selectByPrimaryKey(receiptCheckProduct.getProductId());
                log.info(receiptCheckProduct.getProductId());
                product.setTemporary(2);
                productMapper.updateByPrimaryKeySelective(product);

                receiptInProduct.setTotalPrice(String.valueOf(format));
                receiptInProduct.setNum(receiptCheckProduct.getRealNum() - receiptCheckProduct.getNum());
                receiptInProductList.add(receiptInProduct);
                //生成入库单
                ReceiptIn receiptIn = new ReceiptIn();
                receiptIn.setType(2);
                receiptIn.setBussinessDate(DateUtil.now());
                receiptIn.setCategoryId(receiptCheck.getCategoryId());
                receiptIn.setCuser(entity.getUserId());
                receiptIn.setOrgId(entity.getOrgId());
                receiptIn.setCtime(DateUtil.now());
                receiptIn.setEtime(DateUtil.now());
                receiptIn.setEuser(entity.getUserId());
                receiptIn.setState(entity.getCreateReceiptIn());
                receiptIn.setReceiptInProduct(receiptInProductList);
                receiptInService.add(receiptIn);
            }
            //盘亏
            if (receiptCheckProduct.getState() == 4) {
                //生成出库单-物品关联
                List<ReceiptOutProduct> receiptOutProductList = new ArrayList<>();
                ReceiptOutProduct receiptOutProduct = new ReceiptOutProduct();
                receiptOutProduct.setProductId(receiptCheckProduct.getProductId());
                receiptOutProduct.setReceiptId(receiptCheckProduct.getId());
                String format = "";
                DecimalFormat df = new DecimalFormat("0.00");//设置保留位数
                if (receiptCheckProduct.getSurplus() == 2) {
                    Float totalPrice = Float.parseFloat(receiptCheckProduct.getTotalPrice()) - Float.parseFloat(receiptCheckProduct.getRealTotalPrice());
                    format = df.format((float) totalPrice);
                } else {
                    Float totalPrice = Float.parseFloat(receiptCheckProduct.getRealTotalPrice());
                    format = df.format((float) totalPrice);
                }
                receiptOutProduct.setTotalPrice(format);
                receiptOutProduct.setPrice(receiptCheckProduct.getPrice());
                receiptOutProduct.setNum(receiptCheckProduct.getNum() - receiptCheckProduct.getRealNum());
                receiptOutProductList.add(receiptOutProduct);
                //生成出库单
                ReceiptOut receiptOut = new ReceiptOut();
                receiptOut.setType(2);
                receiptOut.setBussinessDate(DateUtil.now());
                receiptOut.setCategoryId(receiptCheck.getCategoryId());
                receiptOut.setCuser(entity.getUserId());
                receiptOut.setOrgId(entity.getOrgId());
                receiptOut.setCtime(DateUtil.now());
                receiptOut.setEtime(DateUtil.now());
                receiptOut.setEuser(entity.getUserId());
                receiptOut.setState(entity.getCreateReceiptOut());
                receiptOut.setReceiptOutProduct(receiptOutProductList);
                receiptOutService.add(receiptOut);
            }
        }
    }

    public List<ReceiptCheckDetail> getPageDetailApp(PageParams<ReceiptCheckProductGetPageParams> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ReceiptCheckDetail> pageDetailApp = mapper.getPageDetailApp(params.getParams());
        for (ReceiptCheckDetail receiptCheckDetail : pageDetailApp) {
            receiptCheckDetail.setTypeName(productTypeMapper.selectByPrimaryKey(receiptCheckDetail.getType()).getName());
        }
        return pageDetailApp;
    }

    public List<ReceiptCheck> getMainPages(PageParams<ReceiptCheckGetPageParams> params) {

        UserManagement user = userManagementMapper.selectByPrimaryKey(params.getParams().getUserId());
        if (user.getDataIsAll() == 1) {
            params.getParams().setIsAdmin(1);
        } else {
            params.getParams().setIsAdmin(2);
        }

        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ReceiptCheck> receiptCheckList = mapper.getMainPages(params.getParams());
        for (ReceiptCheck receiptCheck : receiptCheckList) {
            receiptCheck.setCategoryName(categoryMapper.selectByPrimaryKey(receiptCheck.getCategoryId()).getName());
            receiptCheck.setCuserName(userManagementMapper.selectByPrimaryKey(receiptCheck.getCuser()).getName());
            Example example1 = new Example(ReceiptCheckUser.class);
            Example.Criteria criteria1 = example1.createCriteria();
            criteria1.andEqualTo("receiptId", receiptCheck.getId());
            List<ReceiptCheckUser> receiptCheckUserList = receiptCheckUserMapper.selectByExample(example1);
            StringBuffer sb = new StringBuffer();
            int i = 0;
            for (ReceiptCheckUser receiptCheckUser : receiptCheckUserList) {
                if (StrUtil.isNotEmpty(receiptCheckUser.getUserId())) {
                    sb.append(userManagementMapper.selectByPrimaryKey(receiptCheckUser.getUserId()).getName());
                    if (i < receiptCheckUserList.size()) {
                        sb.append(",");
                    }
                }
            }
            receiptCheck.setExecutorName(sb.toString());
        }
        return receiptCheckList;
    }

    public ReceiptCheckProduct checkNone(ReceiptCheckProduct entity) {
        ReceiptCheckProduct receiptCheckProduct = receiptCheckProductMapper.selectByPrimaryKey(entity.getId());
        ReceiptCheck receiptCheck = mapper.selectByPrimaryKey(receiptCheckProduct.getReceiptId());
        if (receiptCheck.getFinish() == 1) {
            throw new LogicException("盘点单已完成");
        }
        if (receiptCheckProduct.getNum() != 0) {
            receiptCheckProduct.setState(4);
        } else {
            receiptCheckProduct.setState(2);
        }
        receiptCheckProduct.setRealNum(0);
        receiptCheckProduct.setRealTotalPrice("0.00");
        receiptCheckProduct.setCheckTime(DateUtil.now());
        receiptCheckProduct.setCheckUser(entity.getCheckUser());
        receiptCheckProductMapper.updateByPrimaryKeySelective(receiptCheckProduct);
        return receiptCheckProduct;
    }

    public List<ReceiptCheck> export(String companyId, String categoryId, String userId) {
        ReceiptCheckGetPageParams params = new ReceiptCheckGetPageParams();
        params.setOrgId(companyId);
        params.setUserId(userId);
        params.setCategoryId(categoryId);
        List<ReceiptCheck> receiptCheckList = mapper.getMainPages(params);
        for (ReceiptCheck receiptCheck : receiptCheckList) {
            receiptCheck.setCategoryName(categoryMapper.selectByPrimaryKey(receiptCheck.getCategoryId()).getName());
            receiptCheck.setCuserName(userManagementMapper.selectByPrimaryKey(receiptCheck.getCuser()).getName());
            if (receiptCheck.getFinish() == 1) {
                receiptCheck.setIfFinish("已完成");
            } else {
                receiptCheck.setIfFinish("未完成");
            }
        }
        return receiptCheckList;
    }

    public List<ReceiptCheckDetail> exportDetail(String receiptId) {
        ReceiptCheckProductGetPageParams params = new ReceiptCheckProductGetPageParams();
        params.setReceiptId(receiptId);
        List<ReceiptCheckDetail> pageList = mapper.getPages(params);
        for (ReceiptCheckDetail receiptCheckDetail : pageList) {
            receiptCheckDetail.setTypeName(productTypeMapper.selectByPrimaryKey(receiptCheckDetail.getType()).getName());
            if (StrUtil.isNotBlank(receiptCheckDetail.getCheckUser())) {
                receiptCheckDetail.setCheckUserName(userManagementMapper.selectByPrimaryKey(receiptCheckDetail.getCheckUser()).getName());
            }
            if (receiptCheckDetail.getState() == 1) {
                receiptCheckDetail.setState1("未盘");
            } else if (receiptCheckDetail.getState() == 2) {
                receiptCheckDetail.setState1("已盘");
            } else if (receiptCheckDetail.getState() == 3) {
                receiptCheckDetail.setState1("盘盈");
            } else if (receiptCheckDetail.getState() == 4) {
                receiptCheckDetail.setState1("盘亏");
            }
        }
        return pageList;
    }
}
