package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptMoveGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.ReceiptMove;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReceiptMoveMapper extends BaseMapper<ReceiptMove> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    List<ReceiptMoveGetPage> getPages(ReceiptMoveGetPageParams params);

    /**
     * 分页查询新
     *
     * @param params
     * @return
     */
    List<ReceiptMoveGetPageNew> getPage(ReceiptMoveGetPageParams params);

    /**
     * 导出
     *
     * @param asList
     * @return
     */
    List<ReceiptMoveExtend> selByIds(@Param("idList") List<String> asList);

    /**
     * 查询调拨详情
     *
     * @param id
     * @return
     */
    List<ReceiptMoveProductExtend> getReceiptMoveExtend(@Param("id") String id);

    /**
     * @param id
     * @return
     */
    List<ReceiptMoveGetPageExtends> getPageExport(@Param("id") String id);

    List<ReceiptMoveGetPage> export(ReceiptMoveGetPageParams params);
}