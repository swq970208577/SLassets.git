package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.ReceiptApplySon;

public interface ReceiptApplySonMapper extends BaseMapper<ReceiptApplySon> {
}