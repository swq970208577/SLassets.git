package com.sdkj.fixed.asset.hc.controller;


import cn.hutool.core.date.DateUtil;
import com.sdkj.fixed.asset.api.hc.ReceiptSetApi;
import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptSetGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptSetGetPageNew;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.hc.service.ReceiptSetService;
import com.sdkj.fixed.asset.hc.util.CacheUtil;
import com.sdkj.fixed.asset.pojo.hc.ReceiptSet;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptSet;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 史晨星
 * @ClassName: ReceiptSetController
 * @Description: 调整单
 * @date 2020年7月20日
 */
@Validated
@RestController()
public class ReceiptSetController extends BaseController<ReceiptSet> implements ReceiptSetApi {

    private static Logger log = LoggerFactory.getLogger(ReceiptSetController.class);

    @Resource
    private ReceiptSetService service;

    @Resource
    private HttpServletRequest request;

    @Override
    public BaseService<ReceiptSet> getService() {
        return service;
    }

    @Resource
    private CacheUtil cacheUtil;

    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo add(@Validated({AddReceiptSet.class}) ReceiptSet entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setCuser(userId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setCtime(DateUtil.now());
        entity.setOrgId(orgId);
        entity.setState(1);
        ReceiptSet receiptSet = service.add(entity);
        return BaseResultVo.success("success", receiptSet.getId());
    }

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo edit(@Validated({EditReceiptSet.class}) ReceiptSet entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        entity.setState(1);
        entity.setUserId(userId);
        ReceiptSet receiptSet = service.edit(entity, false);
        return BaseResultVo.success("success", receiptSet.getId());
    }

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo del(@Validated IdParams entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setOrgId(orgId);
        entity.setEuser(userId);
        entity.setEtime(DateUtil.now());
        ReceiptSet receiptSet = service.del(entity);
        return BaseResultVo.success("success", receiptSet.getId());
    }

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo view(@Validated IdParams entity) throws Exception {
        ReceiptSet receiptSet = service.view(entity.getId());
        return BaseResultVo.success("success", receiptSet);
    }

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getPages(@Validated PageParams<ReceiptSetGetPageParams> params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.getParams().setOrgId(orgId);
        params.getParams().setUserId(userId);
        List<ReceiptSetGetPageNew> pageList = service.getPages(params);
        PageBean<ReceiptSetGetPageNew> page = new PageBean<>(pageList);
        return BaseResultVo.success(page);
    }
}

