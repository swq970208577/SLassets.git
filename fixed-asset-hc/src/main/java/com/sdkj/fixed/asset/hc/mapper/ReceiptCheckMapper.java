package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckProductGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptCheckDetail;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.ReceiptCheck;

import java.util.List;

public interface ReceiptCheckMapper extends BaseMapper<ReceiptCheck> {
    /**
     * 盘点单详情分页查询
     *
     * @param params
     * @return
     */
    List<ReceiptCheckDetail> getPages(ReceiptCheckProductGetPageParams params);

    /**
     * 盘点单详情分页查询-APP
     *
     * @param params
     * @return
     */
    List<ReceiptCheckDetail> getPageDetailApp(ReceiptCheckProductGetPageParams params);

    /**
     * 盘点单-主单分页查询
     *
     * @param params
     * @return
     */
    List<ReceiptCheck> getMainPages(ReceiptCheckGetPageParams params);
}