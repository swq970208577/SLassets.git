package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptInGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.ReceiptIn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReceiptInMapper extends BaseMapper<ReceiptIn> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    List<ReceiptInGetPage> getPages(ReceiptInGetPageParams params);

    /**
     * 分页查询
     * @param params
     * @return
     */
    List<ReceiptInGetPageNew> getPage(ReceiptInGetPageParams params);

    List<ReceiptInExtend> selByIds(@Param("idList")List<String> asList);

    List<ReceiptMoveProductExtend> getReceiptMoveExtend(@Param("id") String id);

    List<ReceiptInGetPageExtends> getPageExport(@Param("id")String id);
}