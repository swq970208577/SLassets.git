package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.Qwert;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

public interface QwertMapper extends BaseMapper<Qwert> {
    List<Qwert> qwer(StatisticsParams params);
}