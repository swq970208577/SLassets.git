package com.sdkj.fixed.asset.hc.pojo;

import com.sdkj.fixed.asset.hc.util.TreeNode;
import com.sdkj.fixed.asset.pojo.hc.ProductType;

/**
 * ProductTypeTree
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/5 11:29
 */
public class ProductTypeTree extends TreeNode {

    private String id;

    private String pid;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
