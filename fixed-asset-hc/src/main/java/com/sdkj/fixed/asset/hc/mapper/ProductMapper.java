package com.sdkj.fixed.asset.hc.mapper;

import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.api.hc.vo.out.CategotryProduct;
import com.sdkj.fixed.asset.api.hc.vo.out.ProductStatistics;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.hc.Product;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;

import java.util.List;

public interface ProductMapper extends BaseMapper<Product> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    List<Product> getPages(Product params);
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    List<Product> getPagesIn(Product params);

    /**
     * 查询库存物品列表（分页）
     * @return
     * @param params
     */
    List<CategotryProduct> getProductByTypeAndCategory(Product params);

    List<ProductStatistics> getStatistics(StatisticsParams params);

    ProductStatistics getInStatistics(StatisticsParams params);

    ProductStatistics getOutStatistics(StatisticsParams params);

    ProductStatistics getRestStatistics(StatisticsParams params);

    public UsageManagement selUsage(String companyId);
}