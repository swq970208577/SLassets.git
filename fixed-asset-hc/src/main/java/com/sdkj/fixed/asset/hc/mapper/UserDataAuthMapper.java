package com.sdkj.fixed.asset.hc.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.UserDateAuth;

public interface UserDataAuthMapper extends BaseMapper<UserDateAuth> {
}