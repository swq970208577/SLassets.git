package com.sdkj.fixed.asset.hc.controller;


import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.Page;
import com.sdkj.fixed.asset.api.hc.ReceiptApplyApi;
import com.sdkj.fixed.asset.api.hc.vo.in.*;
import com.sdkj.fixed.asset.api.hc.vo.out.*;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.hc.service.ReceiptApplyService;
import com.sdkj.fixed.asset.hc.util.CacheUtil;
import com.sdkj.fixed.asset.hc.util.ExcelUtils;
import com.sdkj.fixed.asset.hc.util.RedisUtil;
import com.sdkj.fixed.asset.pojo.hc.*;
import com.sdkj.fixed.asset.pojo.hc.group.ApproveGroup;
import com.sdkj.fixed.asset.pojo.hc.group.ConfirmGroup;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 史晨星
 * @ClassName: ReceiptApplyController
 * @Description: 物品领用申请
 * @date 2020年7月20日
 */
@Validated
@RestController()
public class ReceiptApplyController extends BaseController<ReceiptApply> implements ReceiptApplyApi {

    @Resource
    private ReceiptApplyService service;

    @Resource
    private HttpServletRequest request;

    @Override
    public BaseService<ReceiptApply> getService() {
        return service;
    }

    @Resource
    private CacheUtil cacheUtil;

    @Resource
    private RedisUtil redisUtil;

    /**
     * 发起
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo add(@Validated ReceiptApplyParams entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setApplyUser(userId);
        entity.setApplyTime(DateUtil.now());
        entity.setState(1);
        entity.setConfirm(1);
        entity.setApprove(1);
        entity.setOrgId(orgId);
        ReceiptApply receiptApply = service.add(entity);
        return BaseResultVo.success("success", receiptApply.getId());
    }

    /**
     * 确认
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo confirm(@Validated({ConfirmGroup.class}) ReceiptApplySon entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setConfirmUser(userId);
        ReceiptApplySon receiptApplySon = service.confirm(entity);
        return BaseResultVo.success("success", receiptApplySon.getId());
    }

    /**
     * 审批
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo approve(@Validated({ApproveGroup.class}) ReceiptApplySon entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setApproveUser(userId);
        entity.setApproveTime(DateUtil.now());
        ReceiptApplySon receiptApplySon = new ReceiptApplySon();
        boolean isLock = false;
        while (!isLock) {
            isLock = redisUtil.setnx("RECEIPTAPPLYSON_APPROVE", 5000);
            if (isLock) {
                try {
                    receiptApplySon = service.approve(entity);
                    redisUtil.del("RECEIPTAPPLYSON_APPROVE");
                } catch (Exception e) {
                    redisUtil.del("RECEIPTAPPLYSON_APPROVE");
                    throw e;
                }
                continue;
            } else {
                Thread.sleep(5000);
            }
        }
        return BaseResultVo.success("success", receiptApplySon.getId());
    }

    /**
     * 全部领用单
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo getApplyList(@Validated PageParams<GetApplyListParams> params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.getParams().setCuser(userId);
        List<GetApplyList> getApplyList = service.getApplyList(params);
        PageBean<GetApplyList> page = new PageBean<GetApplyList>(getApplyList);
        return BaseResultVo.success("success", page);
    }

    /**
     * 查询库存记录
     *
     * @param receiptApplySon
     * @return
     */
    @Override
    public BaseResultVo getCategoryDetail(@Validated ReceiptApplySon receiptApplySon) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        receiptApplySon.setUserId(userId);
        List<GetCategoryDetail> getCategoryDetail = service.getCategoryDetail(receiptApplySon);
        return BaseResultVo.success("success", getCategoryDetail);
    }

    /**
     * 同意
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo createReceiptOut(@Validated CreateReceiptOut entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setCuser(userId);
        entity.setOrgId(orgId);
        service.createReceiptOut(entity);
        return BaseResultVo.success("success");
    }

    /**
     * 拒绝
     *
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo refuse(@Validated CreateReceiptOut entity) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        entity.setCuser(userId);
        entity.setOrgId(orgId);
        service.refuse(entity);
        return BaseResultVo.success("success");
    }

    /**
     * 发放详情
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo getSendDetail(@Validated PageParams<IdParams> params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.getParams().setCuser(userId);
        params.getParams().setOrgId(orgId);
        List<SendDetail> receiptOutProductList = service.getSendDetail(params);
        PageBean<SendDetail> page = new PageBean<SendDetail>(receiptOutProductList);
        return BaseResultVo.success("success", page);
    }

    /**
     * 申请详情
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo getSonApply(@Validated IdParams params) throws Exception {
        ApplyResult sonApply = service.getSonApply(params);
        return BaseResultVo.success("success", sonApply);
    }

    /**
     * 申请详情
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo statistic(PageParams<GetProductUsedParams> params) throws Exception {
        String token = request.getHeader("token");
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        params.getParams().setUserId(userId);
        params.getParams().setOrgId1(orgId);
        List<GetProductUsed> getProductUseds = service.statistic(params);
        PageBean<GetProductUsed> page = new PageBean<GetProductUsed>(getProductUseds);
        return BaseResultVo.success("success", page);
    }

    /**
     * @param orgId
     * @param deptId
     * @param year
     * @param token
     * @param response
     * @throws Exception
     */
    @Override
    public void export(String orgId, String deptId, String year, String token, HttpServletResponse response) throws Exception {
        GetProductUsedParams param = new GetProductUsedParams();
        param.setOrgId(orgId);
        param.setDeptId(deptId);
        String userId = cacheUtil.getUser(token).getUserId();
        String orgId1 = cacheUtil.getUser(token).getCompanys().get(0).getCompanyId();
        param.setYear(year);
        param.setUserId(userId);
        param.setOrgId1(orgId1);
        List<GetProductUsed> list = service.export(param);
        String fileName = "耗材领用表" + TimeTool.getTimeDate14();
        ExcelUtils.exportExcel(list, GetProductUsed.class, fileName, response);
    }
}

