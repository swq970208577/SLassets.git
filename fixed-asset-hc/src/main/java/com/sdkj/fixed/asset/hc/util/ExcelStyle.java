package com.sdkj.fixed.asset.hc.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

/**
 * @Author zhangjinfei
 * @Description //TODO 表格样式
 * @Date 2020/7/29 14:27
 */
public class ExcelStyle {

    public static HSSFCellStyle  styleTitle(HSSFWorkbook workbook){
        HSSFCellStyle style_title = workbook.createCellStyle();
        HSSFFont font_title = workbook.createFont();
        font_title.setFontName("微软雅黑");// 设置字体
        font_title.setFontHeightInPoints((short) 20);// 设置字体大小
        font_title.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 设置字体加粗
        style_title.setFont(font_title);
        style_title.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 设置垂直居中
//        style_title.setWrapText(true);
        style_title.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 设置文本居中
        return style_title;
    }

    public static HSSFCellStyle  styleCellName(HSSFWorkbook workbook){
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setFontName("微软雅黑");// 设置字体
        font.setFontHeightInPoints((short) 10);// 设置字体大小
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 设置字体加粗
        style.setFont(font);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 设置垂直居中
        style.setWrapText(true);
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 设置文本靠左
        style.setBorderBottom((short) 1);
        style.setBorderLeft((short) 1);
        style.setBorderTop((short) 1);
        style.setBorderRight((short) 1);
        return style;
    }

    public static HSSFCellStyle  styleCellValue(HSSFWorkbook workbook){
        HSSFCellStyle style = workbook.createCellStyle();
        // 字体设置
        HSSFFont font = workbook.createFont();
        font.setFontName("微软雅黑");// 设置字体
        font.setFontHeightInPoints((short) 10);// 设置字体大小
        style.setFont(font);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 设置垂直居中
        style.setWrapText(true);
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 设置文本居左
        style.setBorderBottom((short) 1);
        style.setBorderLeft((short) 1);
        style.setBorderTop((short) 1);
        style.setBorderRight((short) 1);
        return style;
    }

    /**
     * 设置边框
     * @param wb
     * @param sheet
     * @param cell
     */
    public static void setBorder(HSSFWorkbook wb, HSSFSheet sheet, CellRangeAddress cell){
        RegionUtil.setBorderBottom(1, cell, sheet,wb); // 下边框
        RegionUtil.setBorderLeft(1, cell, sheet,wb); // 左边框
        RegionUtil.setBorderRight(1, cell, sheet,wb); // 有边框
        RegionUtil.setBorderTop(1, cell, sheet,wb); // 上边框
        sheet.addMergedRegion(cell);
    }

    /**
     * 无边框
     * @param workbook
     * @return
     */
    public static HSSFCellStyle  styleNotBorder(HSSFWorkbook workbook, int i){
        HSSFCellStyle style = workbook.createCellStyle();
        // 字体设置
        HSSFFont font = workbook.createFont();
        font.setFontName("微软雅黑");// 设置字体
        font.setFontHeightInPoints((short) 10);// 设置字体大小
        style.setFont(font);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 设置垂直居中
        style.setWrapText(true);
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 设置文本居左
        if(i == 1){
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 设置字体加粗
        }
        if(i==3){
            style.setVerticalAlignment(HSSFCellStyle.VERTICAL_BOTTOM);// 设置垂直居中
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 设置字体加粗
        }
        return style;
    }


    public static void printSetup(HSSFSheet sheet){
        HSSFPrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE); // 纸张bai
        printSetup.setLandscape(false);
        sheet.setDisplayGridlines(false);
        sheet.setMargin(HSSFSheet.TopMargin,( double ) 0.2 ); // 上边距du
        sheet.setMargin(HSSFSheet.BottomMargin,( double ) 0.2 ); // 下边zhi距
        sheet.setMargin(HSSFSheet.LeftMargin,( double ) 0.2 ); // 左边dao距
        sheet.setMargin(HSSFSheet.RightMargin,( double ) 0.2 ); // 右边距

    }

}
