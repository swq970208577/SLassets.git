package com.sdkj.fixed.asset.hc.util;

import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.common.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName CacheUtile
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 16:42
 */
@Component
public class CacheUtil {
    @Autowired
    private RedisUtil redisUtil;
    public LoginUser getUser(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        return (LoginUser) redisUtil.get(userId);
    }
}
