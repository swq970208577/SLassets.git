import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;

/**
 * @Author zhangjinfei
 * @Description //TODO
 * @Date 2020/7/22 9:43
 */
public class Test {

    public static void main(String[] args) throws ParseException {
        StringJoiner joiner = new StringJoiner(",");
        joiner.add("111");
        joiner.add("222");
//        joiner.add("333");


        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        long current=System.currentTimeMillis();//当前时间毫秒数

        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis()));
        System.out.println(format);
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-08-20 23:59:59").getTime());

    }

}
