package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.out_vo.WithdrawalApplyResult;
import com.sdkj.fixed.asset.api.assets.out_vo.WithdrawalResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.Withdrawal;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WithdrawalMapper extends BaseMapper<Withdrawal> {
    List<WithdrawalResult> queryPages(String searchName,String orgId,String userId);

    WithdrawalResult selectByWithdrawalId(String withdrawalId);

    List<WithdrawalApplyResult> getAllWithdrawalApply(String createTime, String userId,Integer state);

    WithdrawalApplyResult getWithdrawalApplyById(String id,Integer state);

    List<String> getModelOrAssetAdmin(String orgId, String assetId, Integer type);
}