package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.out_vo.SetClassEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.SetClass;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SetClassMapper extends BaseMapper<SetClass> {

    /**
     * 根据ID 查询
     * @param id
     * @return
     */
    public SetClassEntity selById(String id);

    public String[] getUserIdByModelId(@Param("idList") List<String> idList);

    public List<SetClassEntity> getAllByPid(String orgId, String pid);

    public List<SetClass> getClassAuth(String userId, String orgId);

    public long selDateAuthCount(String authId, int type);
}