package com.sdkj.fixed.asset.assets.util;

/**
 * @Author zhangjinfei
 * @Description //TODO 日志内容
 * @Date 2020/7/27 15:44
 */
public class LogContext {

    /**
     *  模块
     */
    private String modelName;
    /**
     * 使用人
     */
    private String beforeName;
    private String afterName;
    /**
     * 状态
     */
    private String beforeState;
    private String afterState;
    /**
     * 区域
     */
    private String beforeArea;
    private String afterArea;
    /**
     * 存放地址
     */
    private String beforeAddr;
    private String afterAddr;

    private String aftersupplierName ;
    private String aftersuppliercontact ;
    private String aftersupplierTel ;
    private String afteruserName;
    private String afterexpireDate;
    private String afterexpireNote;
    private String beforsupplierName ;
    private String beforsuppliercontact ;
    private String beforsupplierTel ;
    private String beforuserName;
    private String beforexpireDate;
    private String beforexpireNote;

    private String beforeassetName;
    private String beforeassetClassName;
    private String beforestandardModel;
    private String beforespecificationModel;
    private String beforeunitMeasurement;
    private String beforesnNumber;
    private String beforesource;
    private String beforepurchaseTime;
    private String beforeuseCompanyName;
    private String beforeuseDepartmentName;
    private String beforehandlerUserName;
    private String beforeserviceLife;
    private String beforeareaName;
    private String beforestorageLocation;
    private String beforeremark;
    private String beforephoto;
    private String afterassetName;
    private String afterassetClassName;
    private String afterstandardModel;
    private String afterspecificationModel;
    private String afterunitMeasurement;
    private String aftersnNumber;
    private String aftersource;
    private String afterpurchaseTime;
    private String afteruseCompanyName;
    private String afteruseDepartmentName;
    private String afterhandlerUserName;
    private String afterserviceLife;
    private String afterareaName;
    private String afterstorageLocation;
    private String afterremark;
    private String afterphoto;

    public LogContext() {
    }

    public LogContext(String modelName, String beforeName, String afterName, String beforeState, String afterState) {
        this.modelName = modelName;
        this.beforeName = beforeName;
        this.afterName = afterName;
        this.beforeState = beforeState;
        this.afterState = afterState;
    }

    public LogContext(String beforeState, String afterState) {
        this.beforeState = beforeState;
        this.afterState = afterState;
    }

    public LogContext(String modelName, String beforeState, String afterState) {
        this.modelName = modelName;
        this.beforeState = beforeState;
        this.afterState = afterState;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getBeforeName() {
        return beforeName;
    }

    public void setBeforeName(String beforeName) {
        this.beforeName = beforeName;
    }

    public String getAfterName() {
        return afterName;
    }

    public void setAfterName(String afterName) {
        this.afterName = afterName;
    }

    public String getBeforeState() {
        return beforeState;
    }

    public void setBeforeState(String beforeState) {
        this.beforeState = beforeState;
    }

    public String getAfterState() {
        return afterState;
    }

    public void setAfterState(String afterState) {
        this.afterState = afterState;
    }

    public String getBeforeArea() {
        return beforeArea;
    }

    public void setBeforeArea(String beforeArea) {
        this.beforeArea = beforeArea;
    }

    public String getAfterArea() {
        return afterArea;
    }

    public void setAfterArea(String afterArea) {
        this.afterArea = afterArea;
    }

    public String getBeforeAddr() {
        return beforeAddr;
    }

    public void setBeforeAddr(String beforeAddr) {
        this.beforeAddr = beforeAddr;
    }

    public String getAfterAddr() {
        return afterAddr;
    }

    public void setAfterAddr(String afterAddr) {
        this.afterAddr = afterAddr;
    }

    public String getAftersupplierName() {
        return aftersupplierName;
    }

    public void setAftersupplierName(String aftersupplierName) {
        this.aftersupplierName = aftersupplierName;
    }

    public String getAftersuppliercontact() {
        return aftersuppliercontact;
    }

    public void setAftersuppliercontact(String aftersuppliercontact) {
        this.aftersuppliercontact = aftersuppliercontact;
    }

    public String getAftersupplierTel() {
        return aftersupplierTel;
    }

    public void setAftersupplierTel(String aftersupplierTel) {
        this.aftersupplierTel = aftersupplierTel;
    }

    public String getAfteruserName() {
        return afteruserName;
    }

    public void setAfteruserName(String afteruserName) {
        this.afteruserName = afteruserName;
    }

    public String getAfterexpireDate() {
        return afterexpireDate;
    }

    public void setAfterexpireDate(String afterexpireDate) {
        this.afterexpireDate = afterexpireDate;
    }

    public String getAfterexpireNote() {
        return afterexpireNote;
    }

    public void setAfterexpireNote(String afterexpireNote) {
        this.afterexpireNote = afterexpireNote;
    }

    public String getBeforsupplierName() {
        return beforsupplierName;
    }

    public void setBeforsupplierName(String beforsupplierName) {
        this.beforsupplierName = beforsupplierName;
    }

    public String getBeforsuppliercontact() {
        return beforsuppliercontact;
    }

    public void setBeforsuppliercontact(String beforsuppliercontact) {
        this.beforsuppliercontact = beforsuppliercontact;
    }

    public String getBeforsupplierTel() {
        return beforsupplierTel;
    }

    public void setBeforsupplierTel(String beforsupplierTel) {
        this.beforsupplierTel = beforsupplierTel;
    }

    public String getBeforuserName() {
        return beforuserName;
    }

    public void setBeforuserName(String beforuserName) {
        this.beforuserName = beforuserName;
    }

    public String getBeforexpireDate() {
        return beforexpireDate;
    }

    public void setBeforexpireDate(String beforexpireDate) {
        this.beforexpireDate = beforexpireDate;
    }

    public String getBeforexpireNote() {
        return beforexpireNote;
    }

    public void setBeforexpireNote(String beforexpireNote) {
        this.beforexpireNote = beforexpireNote;
    }

    public String getBeforeassetName() {
        return beforeassetName;
    }

    public void setBeforeassetName(String beforeassetName) {
        this.beforeassetName = beforeassetName;
    }

    public String getBeforeassetClassName() {
        return beforeassetClassName;
    }

    public void setBeforeassetClassName(String beforeassetClassName) {
        this.beforeassetClassName = beforeassetClassName;
    }

    public String getBeforestandardModel() {
        return beforestandardModel;
    }

    public void setBeforestandardModel(String beforestandardModel) {
        this.beforestandardModel = beforestandardModel;
    }

    public String getBeforespecificationModel() {
        return beforespecificationModel;
    }

    public void setBeforespecificationModel(String beforespecificationModel) {
        this.beforespecificationModel = beforespecificationModel;
    }

    public String getBeforeunitMeasurement() {
        return beforeunitMeasurement;
    }

    public void setBeforeunitMeasurement(String beforeunitMeasurement) {
        this.beforeunitMeasurement = beforeunitMeasurement;
    }

    public String getBeforesnNumber() {
        return beforesnNumber;
    }

    public void setBeforesnNumber(String beforesnNumber) {
        this.beforesnNumber = beforesnNumber;
    }

    public String getBeforesource() {
        return beforesource;
    }

    public void setBeforesource(String beforesource) {
        this.beforesource = beforesource;
    }

    public String getBeforepurchaseTime() {
        return beforepurchaseTime;
    }

    public void setBeforepurchaseTime(String beforepurchaseTime) {
        this.beforepurchaseTime = beforepurchaseTime;
    }

    public String getBeforeuseCompanyName() {
        return beforeuseCompanyName;
    }

    public void setBeforeuseCompanyName(String beforeuseCompanyName) {
        this.beforeuseCompanyName = beforeuseCompanyName;
    }

    public String getBeforeuseDepartmentName() {
        return beforeuseDepartmentName;
    }

    public void setBeforeuseDepartmentName(String beforeuseDepartmentName) {
        this.beforeuseDepartmentName = beforeuseDepartmentName;
    }

    public String getBeforehandlerUserName() {
        return beforehandlerUserName;
    }

    public void setBeforehandlerUserName(String beforehandlerUserName) {
        this.beforehandlerUserName = beforehandlerUserName;
    }

    public String getBeforeserviceLife() {
        return beforeserviceLife;
    }

    public void setBeforeserviceLife(String beforeserviceLife) {
        this.beforeserviceLife = beforeserviceLife;
    }

    public String getBeforeareaName() {
        return beforeareaName;
    }

    public void setBeforeareaName(String beforeareaName) {
        this.beforeareaName = beforeareaName;
    }

    public String getBeforestorageLocation() {
        return beforestorageLocation;
    }

    public void setBeforestorageLocation(String beforestorageLocation) {
        this.beforestorageLocation = beforestorageLocation;
    }

    public String getBeforeremark() {
        return beforeremark;
    }

    public void setBeforeremark(String beforeremark) {
        this.beforeremark = beforeremark;
    }

    public String getBeforephoto() {
        return beforephoto;
    }

    public void setBeforephoto(String beforephoto) {
        this.beforephoto = beforephoto;
    }

    public String getAfterassetName() {
        return afterassetName;
    }

    public void setAfterassetName(String afterassetName) {
        this.afterassetName = afterassetName;
    }

    public String getAfterassetClassName() {
        return afterassetClassName;
    }

    public void setAfterassetClassName(String afterassetClassName) {
        this.afterassetClassName = afterassetClassName;
    }

    public String getAfterstandardModel() {
        return afterstandardModel;
    }

    public void setAfterstandardModel(String afterstandardModel) {
        this.afterstandardModel = afterstandardModel;
    }

    public String getAfterspecificationModel() {
        return afterspecificationModel;
    }

    public void setAfterspecificationModel(String afterspecificationModel) {
        this.afterspecificationModel = afterspecificationModel;
    }

    public String getAfterunitMeasurement() {
        return afterunitMeasurement;
    }

    public void setAfterunitMeasurement(String afterunitMeasurement) {
        this.afterunitMeasurement = afterunitMeasurement;
    }

    public String getAftersnNumber() {
        return aftersnNumber;
    }

    public void setAftersnNumber(String aftersnNumber) {
        this.aftersnNumber = aftersnNumber;
    }

    public String getAftersource() {
        return aftersource;
    }

    public void setAftersource(String aftersource) {
        this.aftersource = aftersource;
    }

    public String getAfterpurchaseTime() {
        return afterpurchaseTime;
    }

    public void setAfterpurchaseTime(String afterpurchaseTime) {
        this.afterpurchaseTime = afterpurchaseTime;
    }

    public String getAfteruseCompanyName() {
        return afteruseCompanyName;
    }

    public void setAfteruseCompanyName(String afteruseCompanyName) {
        this.afteruseCompanyName = afteruseCompanyName;
    }

    public String getAfteruseDepartmentName() {
        return afteruseDepartmentName;
    }

    public void setAfteruseDepartmentName(String afteruseDepartmentName) {
        this.afteruseDepartmentName = afteruseDepartmentName;
    }

    public String getAfterhandlerUserName() {
        return afterhandlerUserName;
    }

    public void setAfterhandlerUserName(String afterhandlerUserName) {
        this.afterhandlerUserName = afterhandlerUserName;
    }

    public String getAfterserviceLife() {
        return afterserviceLife;
    }

    public void setAfterserviceLife(String afterserviceLife) {
        this.afterserviceLife = afterserviceLife;
    }

    public String getAfterareaName() {
        return afterareaName;
    }

    public void setAfterareaName(String afterareaName) {
        this.afterareaName = afterareaName;
    }

    public String getAfterstorageLocation() {
        return afterstorageLocation;
    }

    public void setAfterstorageLocation(String afterstorageLocation) {
        this.afterstorageLocation = afterstorageLocation;
    }

    public String getAfterremark() {
        return afterremark;
    }

    public void setAfterremark(String afterremark) {
        this.afterremark = afterremark;
    }

    public String getAfterphoto() {
        return afterphoto;
    }

    public void setAfterphoto(String afterphoto) {
        this.afterphoto = afterphoto;
    }
}
