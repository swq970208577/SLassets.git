package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.MaintainRegisterAssets;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MaintainRegisterAssetsMapper extends BaseMapper<MaintainRegisterAssets> {
}