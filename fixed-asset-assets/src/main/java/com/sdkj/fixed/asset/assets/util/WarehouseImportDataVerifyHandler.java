package com.sdkj.fixed.asset.assets.util;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.sdkj.fixed.asset.assets.mapper.WarehouseMapper;
import com.sdkj.fixed.asset.assets.service.SetService;
import com.sdkj.fixed.asset.pojo.assets.*;
import com.sdkj.fixed.asset.pojo.system.OrgManagement;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.StringJoiner;

@Component
public class WarehouseImportDataVerifyHandler implements IExcelVerifyHandler<WarehouseImportData> {

    @Autowired
    private SetService setService;
    @Autowired
    private WarehouseMapper warehouseMapper;
    @Autowired
    private HttpServletRequest requset;
    @Autowired
    private CacheUtils cacheUtils;

    @Override
    public ExcelVerifyHandlerResult verifyHandler(WarehouseImportData data) {
        String token = requset.getHeader("token");
        String orgId = cacheUtils.getUserCompanyId(token);

        StringJoiner joiner = new StringJoiner(",");
        if(data==null){
            joiner.add("请核对数据进行上传");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }
        if(StringUtils.isEmpty(data.getAssetClassNo())){
            joiner.add("【资产类别编码】不能为空");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }
//        资产类别编码*
        SetClass setClass = setService.queryAssetClassByNum(data.getAssetClassNo());
        if (setClass!=null) {
            data.setAssetClassId(setClass.getId());
            data.setClassTreecode(setClass.getTreecode());
        }else {
            joiner.add("【资产类别编码】与数据库数据不匹配");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }
//        规格型号
        if(data.getSpecificationModel()!=null && !"".equals(data.getSpecificationModel().trim()) && setClass!=null && setClass.getId()!=null){
            StandModel standModel = setService.queryStandModelByName(data.getSpecificationModel());
            if(standModel!=null){
                data.setStandardModel(standModel.getId());
            }else{
                joiner.add("【规格型号】与数据库数据不匹配");
                return new ExcelVerifyHandlerResult(false, joiner.toString());
            }
        }

//        使用公司编码*
        OrgManagement useOrg = warehouseMapper.queryOrgByCode(data.getUseCompanyNo(),orgId);
        if(useOrg!=null){
            data.setUseCompany(useOrg.getId());
            data.setUseCompanyTreecode(useOrg.getTreecode());
        }else{
            joiner.add("【使用公司编码】与数据库数据不匹配");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }
        if(StringUtils.isNotEmpty(data.getDeptName())){
            if(StringUtils.isEmpty(data.getDeptNo())){
                joiner.add("【使用部门】和【使用部门编码】不匹配");
                return new ExcelVerifyHandlerResult(false, joiner.toString());
            }
        }
//        使用部门编码
        if(data.getDeptNo()!=null  && !"".equals(data.getDeptNo().trim()) && useOrg!=null && useOrg.getId()!=null){
            OrgManagement useDept = warehouseMapper.queryDeptByOrgIdAndCode(useOrg.getId(),data.getDeptNo());
            if(useDept!=null){
                data.setUseDepartment(useDept.getId());
                data.setUseDeptTreecode(useDept.getTreecode());
            }else{
                joiner.add("【使用部门编码】与数据库数据不匹配");
                return new ExcelVerifyHandlerResult(false, joiner.toString());
            }
        }

//       区域编码*
        SetArea setArea = setService.queryAreaByAreaNum(data.getAreaNo());
        if(setArea!=null){
            data.setArea(setArea.getId());
        }else{
            joiner.add("【区域编码】与数据库数据不匹配");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }
//        员工编号+员工姓名（使用人)
        if (data.getHandlerUser() != null && !"".equals(data.getHandlerUser().trim())) {
            UserManagement useUser = warehouseMapper.queryUserByUsername(data.getHandlerUser(), orgId,"2");
            if (useUser != null) {
                data.setHandlerUserId(useUser.getId());
            } else {
                joiner.add("【员工姓名】与数据库数据不匹配");
                return new ExcelVerifyHandlerResult(false, joiner.toString());
            }
        }
//        管理员姓名*
        UserManagement admin = warehouseMapper.queryUserByUsername(data.getAdmin(),orgId,"1");
        if(admin!=null){
            data.setAdminId(admin.getId());
        }else{
            joiner.add("【管理员姓名】与数据库数据不匹配");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }

//        所属公司编码*
        OrgManagement org =  warehouseMapper.queryOrgByCode(data.getCompanyNo(),orgId);
        if(org!=null){
            data.setCompany(org.getId());
            data.setCompanyTreecode(org.getTreecode());
        }else{
            joiner.add("【所属公司编码】与数据库数据不匹配");
            return new ExcelVerifyHandlerResult(false, joiner.toString());
        }
//        供应商
        if(StringUtils.isNotEmpty(data.getSupplierName())) {
            SetSupplier setSupplier = setService.querySupplierByName(data.getSupplierName());
            if (setSupplier != null) {
                data.setSupplierId(setSupplier.getId());
                data.setSupplierName(setSupplier.getName());
            } else {
                joiner.add("【供应商】与数据库数据不匹配");
                return new ExcelVerifyHandlerResult(false, joiner.toString());
            }
        }
//        维保负责人
        if(StringUtils.isNotEmpty(data.getSupplierUser())){
            UserManagement supplierUser =  warehouseMapper.queryUserByUsername(data.getSupplierUser(),orgId,"1");
            if(supplierUser!=null){
                data.setSupplierUserId(supplierUser.getId());
                data.setSupplierUser(supplierUser.getName());
            }else{
                joiner.add("【维保负责人】与数据库数据不匹配");
                return new ExcelVerifyHandlerResult(false, joiner.toString());
            }
        }
//        if (joiner.length() != 0) {
//            return new ExcelVerifyHandlerResult(false, joiner.toString());
//        }
        return new ExcelVerifyHandlerResult(true);
    }
}