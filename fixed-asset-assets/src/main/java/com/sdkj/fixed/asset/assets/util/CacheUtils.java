package com.sdkj.fixed.asset.assets.util;

import com.sdkj.fixed.asset.api.login.pojo.LoginRole;
import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName CacheUtile
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 16:42
 */
@Component
public class CacheUtils {
    @Autowired
    private RedisUtil redisUtil;
    public LoginUser getUser(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser)redisUtil.get(userId);
        if(loginUser==null){
            throw new LogicException("token失效，请重新登录");
        }
        return loginUser ;
    }

    public String getUserId(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        if(loginUser==null){
            throw new LogicException("token失效，请重新登录");
        }
        return loginUser.getUserId();
    }

    public String getUserCompanyId(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        if(loginUser==null){
            throw new LogicException("token失效，请重新登录");
        }
        return loginUser.getCompanys().get(0).getCompanyId();
    }

    public List<String> getUserRole(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        if(loginUser==null){
            throw new LogicException("token失效，请重新登录");
        }
        List<LoginRole> loginRoles = loginUser.getLoginRoles();
        List<String> roles = loginRoles.stream().map(LoginRole::getRoleLevel).collect(Collectors.toList());
        return roles;
    }
}
