package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.in_vo.ClassIdParam;
import com.sdkj.fixed.asset.api.assets.in_vo.ClassInDeParam;
import com.sdkj.fixed.asset.api.assets.in_vo.CompanyDeptParam;
import com.sdkj.fixed.asset.api.assets.in_vo.WarehouseReportParam;
import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.AssetReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AssetReportMapper extends BaseMapper<AssetReport> {

    List<StandardModelReport> standardModelReport(@Param("userId")String userId,@Param("orgId") String orgId, @Param("name") String name);

    public List<UserInfo> userInfo(String orgId, String userId, String search, String isOnJob);

    List<AssetsReportResult>  assetsReport(WarehouseReportParam param);

    public List<AssetsReportResult> queryAllScrap(String startDate, String endDate, String orgId, String userId);

    DueAssetsRecord dueAssetsReportRecord(String assetId);

    List<CompanyReport> companyDeptReport(ClassIdParam param);

    List<UseClassReport> useClassReport(CompanyDeptParam param);

    List<ClassIncreaseDecrease> classIncreaseDecrease(ClassInDeParam param);

    public UserInfo selectUser(@Param("userId")String userId);
}
