package com.sdkj.fixed.asset.assets.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @Author zhangjinfei
 * @Description //TODO 文件位置
 * @Date 2020/7/22 13:47
 */
public class FileManager {
    private static Logger log = LoggerFactory.getLogger(FileManager.class);

    /**
     * 获取资源文件路径
     * @param oPath
     * @param fileName
     * @return
     */
    public static String getReportPath(String oPath, String fileName) {
        String classPath = FileManager.class.getClassLoader().getResource("").getPath();
        classPath = classPath.substring(1);
        classPath = classPath + oPath + File.separator + fileName;
        return classPath;
    }
}