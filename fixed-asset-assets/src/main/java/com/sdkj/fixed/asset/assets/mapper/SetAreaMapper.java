package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.SetArea;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SetAreaMapper extends BaseMapper<SetArea> {

    public List<SetArea> getAllAreaOrAuth(String userId);
}