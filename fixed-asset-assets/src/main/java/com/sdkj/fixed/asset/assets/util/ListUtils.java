package com.sdkj.fixed.asset.assets.util;

import java.util.*;

public class ListUtils {
    /**
     * 注！！！！！！→ 集合中的元素必须重写equals方法自行判断元素是否相同
     * 哈希地址相同 返回true
     * 如果两个参数都为空，则返回true
     * 如果有一项为空，则返回false
     * 如果数据长度不相同，则返回false
     * 集合1包含集合2中的所有元素，并且集合2也包含集合1中的所有元素 则返回true
     * 注！！！！！！→ 集合中的元素必须重写equals方法自行判断元素是否相同
     * @param l0
     * @param l1
     * @return
     */
    public static boolean isListEqual(List l0, List l1){
        if (l0 == l1)
            return true;
        if (l0 == null && l1 == null)
            return true;
        if (l0 == null || l1 == null)
            return false;
        if (l0.size() != l1.size())
            return false;
        if (isEqualCollection(l0 ,l1) && l0.containsAll(l1) && l1.containsAll(l0)){
            return true;
        }
        return false;
    }

    private static final Integer INTEGER_ONE = 1;
    public static boolean isEqualCollection(Collection a, Collection b){
        if (a.size() !=b.size()) {  // size是最简单的相等条件
            return false;
        }
        Map mapa = getCardinalityMap(a);
        Map mapb = getCardinalityMap(b);

        // 转换map后，能去掉重复的，这时候size就是非重复项，也是先决条件
        if (mapa.size() !=mapb.size()) {
            return false;
        }
        Iterator it =mapa.keySet().iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            // 查询同一个obj，首先两边都要有，而且还要校验重复个数，就是map.value
            if (getFreq(obj,mapa) != getFreq(obj, mapb)) {
                return false;
            }
        }
        return true;
    }
    /**
     * 以obj为key，可以防止重复，如果重复就value++
     * 这样实际上记录了元素以及出现的次数
     */
    public static Map getCardinalityMap(Collection coll) {
        Map count = new HashMap();
        for (Iterator it = coll.iterator(); it.hasNext();) {
            Object obj =it.next();
            Integer c =(Integer) count.get(obj);
            if (c == null)
                count.put(obj, INTEGER_ONE);
            else {
                count.put(obj, new Integer(c.intValue() + 1));
            }
        }
        return count;
    }
    private static final int getFreq(Object obj, Map freqMap) {
        Integer count =(Integer) freqMap.get(obj);
        if (count != null) {
            return count.intValue();
        }
        return 0;
    }

}
