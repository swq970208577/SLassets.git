package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.BorrowIssueExtend;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.BorrowIssue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BorrowIssueMapper extends BaseMapper<BorrowIssue> {

    public List<BorrowIssueExtend> getBorrowIssue(String startDate, String endDate, String orgId, String userId, String id, String borrowUserId, String state);

    public List<String> getBorrowAssetId(String id);

    public List<BorrowIssueExtend> selByIds(@Param("idList") List<String> idList);

    public BorrowIssueExtend selByAssetId(String assetId);
}