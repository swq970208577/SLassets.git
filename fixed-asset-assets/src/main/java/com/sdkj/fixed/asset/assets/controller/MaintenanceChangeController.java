package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.MaintenanceChangeApi;
import com.sdkj.fixed.asset.api.assets.in_vo.MaintenanceChangeAssetsExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.MaintenanceChangeExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.SelPrams;
import com.sdkj.fixed.asset.assets.service.MaintenanceChangeService;
import com.sdkj.fixed.asset.assets.util.ExcelStyle;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description 维保信息变更
 * @date 2020/7/27 11:24
 */
@Controller
public class MaintenanceChangeController implements MaintenanceChangeApi {
    @Autowired
    private MaintenanceChangeService service;
    @Autowired
    private HttpServletResponse response;
    @Override
    public BaseResultVo add(MaintenanceChangeExtend change) {
        service.add(change);
        return BaseResultVo.success(change.getId());
    }

    @Override
    public BaseResultVo getAllPage(PageParams<SelPrams> param) {
        List<MaintenanceChangeExtend> list = service.getAllPage(param);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo selById(Params params) {
        MaintenanceChangeExtend extend = service.selById(params.getId());
        return BaseResultVo.success(extend);
    }

    @Override
    public void print(String id) throws IOException {
        MaintenanceChangeExtend extend = service.print(id);
        String title = "资产维保信息变更单";
        String sheetName = "资产维保信息变更";
        String fileName = "资产维保信息变更单";

//        创建HSSFWorkbook对象(excel的文档对象)
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFRow row = null;
        HSSFCell cell = null;
        int rownum = 0;
        // 建立新的sheet对象（excel的表单） 并设置sheet名字
        HSSFSheet sheet = wb.createSheet(sheetName);
        ExcelStyle.printSetup(sheet);
        sheet.setDefaultColumnWidth(16);
        sheet.setColumnWidth(0, 256 * 6);
        sheet.setDefaultRowHeight((short) 512);

        // 在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
        row = sheet.createRow(rownum++);
        row.setHeight((short) 1000 );
        // 创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
        cell = row.createCell(1);
        // 合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 8));
        cell.setCellValue(title);
        cell.setCellStyle(ExcelStyle.styleTitle(wb));

        String[] row1 = {"变更单号：", extend.getNumber(), "", "","业务日期：", extend.getDate(), "操作员：", extend.getHandleUserName(), ""};
        String[] row2 = {"变更内容：", "", "", "", "", "", "", ""};
        String[] row3 = {"供应商：", extend.getSupplierName(), "", "", "联系人：", extend.getSuppliercontact(), "联系方式：", extend.getSupplierTel(), ""};
        String[] row4 = {"负责人：", extend.getUserName(), "", "", "", "维保到期日", extend.getExpireDate(), "",""};
        String[] row5 = {"维保说明：", extend.getNote(), "", "", "", "", "", "", ""};
        String[] row6 = {"变更明细："};
        String[] row7 = {"序号","资产编码","资产名称","变更前供应商","变更前联系人","变更前联系方式","变更前负责人","变更前维保到期日", "变更前维保说明"};

        List<String[]> rowList = new ArrayList<>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);
        rowList.add(row4);
        rowList.add(row5);
        rowList.add(row6);
        rowList.add(row7);

        for (String[] rows : rowList) {
            row = sheet.createRow(rownum++);
            for (int i = 0; i < rows.length; i++) {
                cell = row.createCell(i + 1);
                cell.setCellValue(rows[i]);
                if(rownum == 7 || rownum == 3){
                    cell.setCellStyle(ExcelStyle.styleNotBorder(wb, 3));
                    row.setHeight((short)600);
                } else if (rownum == 8){
                    cell.setCellStyle(ExcelStyle.styleCellName(wb));
                } else {
                    cell.setCellStyle(ExcelStyle.styleNotBorder(wb, 2));
                }

            }
        }

        //遍历数据
        int j = 1;
        for (MaintenanceChangeAssetsExtend result : extend.getMaintenanceChangeAssetsExtendist()) {
            row = sheet.createRow(rownum++);
            String[] cells = {"", result.getAssetCode(), result.getAssetName(), result.getSupplierName(), result.getSuppliercontact(), result.getSupplierTel(), result.getUserName(), result.getExpireDate(), result.getNote()};
            for (int i = 0; i < cells.length; i++) {
                cell = row.createCell(i + 1);
                if(i==0){
                    cell.setCellValue(j++);
                } else {
                    cell.setCellValue(cells[i]);
                }
                cell.setCellStyle(ExcelStyle.styleCellValue(wb));
            }
        }

        // 输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setHeader("Content-disposition","attachment; filename="+ URLEncoder.encode(fileName + ".xlsx", "UTF-8"));    //filename =  文件名
        response.setContentType("application/msexcel");
        wb.write(output);
        output.close();
    }
}
