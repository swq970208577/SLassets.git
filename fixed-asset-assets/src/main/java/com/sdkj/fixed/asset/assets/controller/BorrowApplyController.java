package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.BorrowApplyApi;
import com.sdkj.fixed.asset.api.assets.in_vo.ApplySelParam;
import com.sdkj.fixed.asset.api.assets.in_vo.ApprovalParams;
import com.sdkj.fixed.asset.api.assets.in_vo.BorrowApplyExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.SignSelParam;
import com.sdkj.fixed.asset.assets.service.BorrowApplyService;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/28 18:19
 */
@Controller
public class BorrowApplyController implements BorrowApplyApi {
    @Autowired
    private BorrowApplyService service;

    @Override
    public BaseResultVo add(BorrowApplyExtend extend) {
        service.add(extend);
        return BaseResultVo.success(extend.getId());
    }

    @Override
    public BaseResultVo getAllPage(PageParams<ApplySelParam> params) {
        List<BorrowApplyExtend> extendList = service.getAllPage(params);
        PageBean pageBean =  new PageBean(extendList);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo updState(ApprovalParams params) {
        service.updState(params);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo getApplyIssue(ApprovalParams params) {
        return null;
    }

    @Override
    public BaseResultVo queryDeail(SignSelParam params) {
        BorrowApplyExtend extend = service.queryDeail(params.getId(), params.getState());
        return BaseResultVo.success(extend);
    }
}
