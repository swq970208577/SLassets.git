package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.WithdrawalApi;
import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.WithdrawalApplyResult;
import com.sdkj.fixed.asset.api.assets.out_vo.WithdrawalExport;
import com.sdkj.fixed.asset.api.assets.out_vo.WithdrawalResult;
import com.sdkj.fixed.asset.assets.service.WarehouseService;
import com.sdkj.fixed.asset.assets.service.WithdrawalService;
import com.sdkj.fixed.asset.assets.util.ExcelUtils;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.pojo.assets.Withdrawal;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 退库
 * @Date 2020/7/29 13:25
 */
@RestController
public class WithdrawalController implements WithdrawalApi {

    @Autowired
    private WithdrawalService service;
    @Autowired
    private WarehouseService warehouseService;

    /**
     * 分页查询
     * @param params
     * @return
     */
    @Override
    public BaseResultVo<WithdrawalResult> queryPages(PageParams<SearchNameParam> params) {
        List<WithdrawalResult> list = service.queryPages(params);
        PageBean pageBean =  new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    /**
     * 查看
     *
     * @param withdrawalId
     * @return
     */
    @Override
    public BaseResultVo<WithdrawalResult> queryWithdrawal(String withdrawalId) {
        WithdrawalResult result= service.queryWithdrawal(withdrawalId);
        return BaseResultVo.success(result);
    }

    /**
     * 添加退库
     *
     * @param withdrawal
     * @return
     */
    @Override
    public BaseResultVo insertWithdrawal(WithdrawalParam withdrawal) {

        List<String> assetIdList = withdrawal.getAssetIdList();
        String handlerUserId = warehouseService.queryByAssetIdList(assetIdList);
        if(handlerUserId==null){
            return BaseResultVo.failure("请选择相同使用人的资产进行退库");
        }
        withdrawal.setWithdrawalUserId(handlerUserId);
        Integer i = service.insertWithdrawal(withdrawal);
        return BaseResultVo.success(i);
    }


    /**
     * 修改退库
     *
     * @param withdrawal
     * @return
     */
    @Override
    public BaseResultVo updateWithdrawal(WithdrawalParam withdrawal) throws Exception {
        Withdrawal withdrawalEntity = service.selectByPrimaryKey(withdrawal.getWithdrawalId());
        if(withdrawalEntity.getState()==2){
            return BaseResultVo.failure("只能修改未签字的单据");
        }
        if(withdrawalEntity.getApplyId()!=null){
            return BaseResultVo.failure("审批单据不能删除");
        }
        Integer i = service.updateWithdrawal(withdrawal);
        return BaseResultVo.success(i);
    }

    /**
     * 退库签字
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo updateSign(SignatureParam param) throws Exception {
        Integer i = service.updateSign(param);
        return BaseResultVo.success(i);
    }

    /**
     * 删除退库
     *
     * @param withdrawalId
     * @return
     */
    @Override
    public BaseResultVo deleteWithdrawal(String withdrawalId) throws Exception {
        Withdrawal withdrawal = service.selectByPrimaryKey(withdrawalId);
        if(withdrawal.getState()==2){
            return BaseResultVo.failure("只能删除未签字的单据");
        }
        if(withdrawal.getApplyId()!=null){
            return BaseResultVo.failure("审批单据不能删除");
        }
        Integer i = service.deleteWithdrawal(withdrawalId);
        return BaseResultVo.success(i);
    }

    /**
     * 退库导出
     *
     * @param response
     */
    @Override
    public void exportWithdrawal(HttpServletResponse response, String token, String searchName) throws Exception {
        String fileName = "退库单"+ TimeTool.getTimeDate14();
        List<WithdrawalExport> list = service.queryAll(token,searchName);
        String title = "退库单";
        String sheetName=title;
        ExcelUtils.exportExcel(list,title,sheetName, WithdrawalExport.class,fileName,response);
    }

    /**
     * 调拨打印
     *
     * @param response
     */
    @Override
    public void printWithdrawal(HttpServletResponse response,String withdrawalIds) throws Exception {
       if(StringUtils.isBlank(withdrawalIds)){
           throw new LogicException("请选择单据");
       }
        List<String> withdrawalIdList = Arrays.asList(withdrawalIds.split(","));
        String fileName = "资产退库单"+ TimeTool.getTimeDate14();
        Workbook workbook=service.printWithdrawal(withdrawalIdList);
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
        workbook.write(response.getOutputStream());
    }
// ================================
    /**
     * 添加退库申请
     *
     * @param withdrawal
     * @return
     */
    @Override
    public BaseResultVo insertWithdrawalApply(WithdrawalApplyParam withdrawal) {
        Integer i = service.insertWithdrawalApply(withdrawal);
        return BaseResultVo.success(i);
    }


    /**
     * 退库申请同意
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo withdrawalApplyAgree(WithdrawalApplyAgreeParam param) {
        Integer i = service.withdrawalApplyAgree(param);
        return BaseResultVo.success(i);
    }

    /**
     * 退库申请拒绝
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo withdrawalApplyRefuse(WithdrawalApplyAgreeParam param) {
        Integer i = service.withdrawalApplyRefuse(param);
        return BaseResultVo.success(i);
    }

    /**
     * 審批查詢
     *
     * @return
     */
    @Override
    public BaseResultVo<WithdrawalApplyResult> getAllWithdrawalApply(ApplySelParam params) {
        List<WithdrawalApplyResult> list = service.getAllWithdrawalApply(params);
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo<WithdrawalApplyResult> getWithdrawalApplyById(String id,Integer state) {
        WithdrawalApplyResult withdrawalApplyResult = service.getWithdrawalApplyById(id,state);
        return BaseResultVo.success(withdrawalApplyResult);
    }
}
