package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.AssetReportApi;
import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.assets.mapper.ReportMapper;
import com.sdkj.fixed.asset.assets.service.AssetReportService;
import com.sdkj.fixed.asset.assets.util.ExcelUtils;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.pojo.assets.AssetLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 资产报表
 * @Date 2020/8/11 9:41
 */
@RestController
public class AssetReportController implements AssetReportApi {

    @Autowired
    private AssetReportService service;
    @Autowired
    private HttpServletResponse response;
    @Autowired
    private HttpServletRequest request;


    @Override
    public BaseResultVo queryScrapPage(PageParams<SelPrams> param) {
        List<AssetsReportResult> list = service.queryScrapPage(param);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public void exportScrap(String startDate, String endDate, String orgId, String token) throws IOException {
        service.exportScrap(startDate, endDate, orgId, token);
    }

    @Override
    public BaseResultVo userAssetInfo(PageParams<UserAssetSelParam> params) {
        List<UserInfo> list = service.userAssetInfo(params);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public void exportUserAssetInfo(String search, String isOnJob, String orgId, String token) throws IOException {
        service.exportUserAssetInfo(search, isOnJob, orgId, token);
    }

    @Override
    public BaseResultVo standardModelReport(String name) {
        String token = request.getHeader("token");
        List<StandardModelReport> list = service.standardModelReport(token,name);
        return BaseResultVo.success(list);
    }

    @Override
    public void exportStandardModelReport(String token, String name) throws IOException {
        String fileName = "标准资产型号统计表"+ TimeTool.getTimeDate14();
        List<StandardModelReport> list = service.standardModelReport(token,name);
        ExcelUtils.exportExcel(list,StandardModelReport.class,fileName,response);
    }

    /**
     * 到期资产
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo<AssetsReportResult> dueAssetsReport(PageParams<WarehouseReportParam> param) {
        List<AssetsReportResult> list = service.assetsReport(param);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo<DueAssetsRecord> dueAssetsReportRecord(String assetId) {
        DueAssetsRecord record = service.dueAssetsReportRecord(assetId);
        return BaseResultVo.success(record);
    }

    @Override
    public BaseResultVo<AssetLog> dueAssetsReportRecordLog(PageParams<AssetIdParam> params) {
        List<AssetLog> logList = service.dueAssetsReportRecordLog(params);
        PageBean pageBean = new PageBean(logList);
        return BaseResultVo.success(pageBean);
    }
    /**
     * 到期资产导出
     *
     * @param token
     * @param dueDate
     * @param contain
     * @throws IOException
     */
    @Override
    public void exportDueAssetsReport(String token, String dueDate, Integer contain) throws IOException {
        String fileName = "到期资产表"+ TimeTool.getTimeDate14();
        WarehouseReportParam param = new WarehouseReportParam();
        param.setDueDate(dueDate);
        param.setContain(contain);
        List<AssetsReportResult> list = service.exportAssetsReport(token,param);
        ExcelUtils.exportExcel(list,AssetsReportResult.class,fileName,response);
    }

    /**
     * 月增加对账
     * @param param
     * @return
     */
    @Override
    public BaseResultVo monthlyIncreasReport(PageParams<WarehouseReportParam> param) {
        List<AssetsReportResult> list =service.assetsReport(param);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    /**
     * 月增加对账导出
     * @param token
     * @param purchaseMonth
     * @param company
     * @throws IOException
     */
    @Override
    public void exportMonthlyIncreasReport(String token, String purchaseMonth, String company) throws IOException {
        String fileName = "月增加对账表"+ TimeTool.getTimeDate14();
        WarehouseReportParam param = new WarehouseReportParam();
        param.setPurchaseMonth(purchaseMonth);
        param.setCompany(company);
        List<AssetsReportResult> list = service.exportAssetsReport(token,param);
        ExcelUtils.exportExcel(list,AssetsReportResult.class,fileName,response);
    }

    /**
     * 月增加对账
     * @param param
     * @return
     */
    @Override
    public BaseResultVo<CompanyReport> companyDeptReport(ClassIdParam param) {
        List<CompanyReport> list =service.companyDeptReport(param);
        return BaseResultVo.success(list);
    }

    @Override
    public void exportCompanyDeptReport(String token,String classId) throws IOException {
        String fileName = "公司部门汇总表"+ TimeTool.getTimeDate14();
        List<CompanyReport> list = service.exportCompanyDeptReport(token,classId);
        ExcelUtils.exportExcel(list,CompanyReport.class,fileName,response);
    }

    /**
     * 分类使用情况表
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo<UseClassReport> useClassReport(CompanyDeptParam param) {
        List<UseClassReport> list =service.useClassReport(param);
        return BaseResultVo.success(list);
    }

    /**
     * 分类使用情况表导出
     * @param token
     * @param companyId
     * @param deptId
     * @throws IOException
     */
    @Override
    public void exportUseClassReport(String token, String companyId, String deptId) throws IOException {
        String fileName = "分类使用情况表"+ TimeTool.getTimeDate14();
        CompanyDeptParam param = new CompanyDeptParam();
        param.setCompanyId(companyId);
        param.setDeptId(deptId);
        List<UseClassReport> list = service.exportUseClassReport(token,param);
        ExcelUtils.exportExcel(list,UseClassReport.class,fileName,response);
    }

    /**
     * 分类增减表
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo<ClassIncreaseDecrease> classIncreaseDecrease(ClassInDeParam param) {
        List<ClassIncreaseDecrease> list  = service.classIncreaseDecrease(param);
        return BaseResultVo.success(list);
    }

    /**
     * 分类增减表导出
     *
     * @param token
     * @param companyId
     * @param startTime
     * @param endTime
     */
    @Override
    public void exportClassIncreaseDecreaset(String token, String companyId, String startTime, String endTime) throws IOException {
        String fileName = "资产分类汇总表"+ TimeTool.getTimeDate14();
        ClassInDeParam param = new ClassInDeParam();
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setCompanyId(companyId);
        List<ClassIncreaseDecrease> list = service.exportClassIncreaseDecrease(token,param);
        ExcelUtils.exportExcel(list,ClassIncreaseDecrease.class,fileName,response);
    }
}
