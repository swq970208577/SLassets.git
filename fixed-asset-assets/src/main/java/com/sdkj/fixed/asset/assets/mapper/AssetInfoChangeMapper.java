package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.AssetInfoChangeAssetExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.AssetInfoChangeExtend;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.AssetInfoChange;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AssetInfoChangeMapper extends BaseMapper<AssetInfoChange> {

    public List<AssetInfoChangeExtend> getChange(String startDate, String endDate, String orgId, String userId, List<String> idList);

    public AssetInfoChangeExtend selById(String id);

    public List<String> getChangeAssetId(String id);

    public List<AssetInfoChangeAssetExtend> changeBefor(String id);
}