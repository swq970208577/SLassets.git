package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.out_vo.AssetSupplierResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.AssetSupplier;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetSupplierMapper extends BaseMapper<AssetSupplier> {

    AssetSupplierResult queryMaintainByAssetId(String assetId);

}