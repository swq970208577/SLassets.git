package com.sdkj.fixed.asset.assets.controller;

import com.alibaba.fastjson.JSONObject;
import com.sdkj.fixed.asset.api.assets.SetApi;
import com.sdkj.fixed.asset.api.assets.in_vo.AppModelSelParam;
import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.SetEmployeeSelf;
import com.sdkj.fixed.asset.api.assets.out_vo.SetClassEntity;
import com.sdkj.fixed.asset.api.assets.out_vo.StandModelEntity;
import com.sdkj.fixed.asset.assets.service.SetService;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.SetArea;
import com.sdkj.fixed.asset.pojo.assets.SetClass;
import com.sdkj.fixed.asset.pojo.assets.SetSupplier;
import com.sdkj.fixed.asset.pojo.assets.StandModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author niuliwei
 * @description 资产管理设置
 * @date 2020/7/21 9:11
 */
@RestController
public class SetController implements SetApi {
    @Autowired
    private SetService setService;

    @Override
    public BaseResultVo getAllEnClass() {
        List<SetClass> list = setService.getAllEnClass();
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo getAllClass() {
        List<SetClassEntity> list = setService.getAllClass();
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo getAllClassPage(PageParams<Params> params) {
        List<SetClassEntity> list = setService.getAllClassPage(params);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo addClass(SetClass setClass) {
        setService.addClass(setClass);
        return BaseResultVo.success(setClass.getId());
    }

    @Override
    public BaseResultVo addLowerClass(SetClass setClass) {
        setService.addLowerClass(setClass);
        return BaseResultVo.success(setClass.getId());
    }

    @Override
    public BaseResultVo updClass(SetClass setClass) {
        setService.updClass(setClass);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo delClassById(Params params) {
        setService.delClassById(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo classEnOrdisable(SetClass setClass) {
        setService.classEnOrdisable(setClass);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo selClassById(Params params) {
        SetClassEntity resultVo = setService.selClassById(params.getId());
        return BaseResultVo.success(resultVo);
    }

    @Override
    public BaseResultVo getClassTree() {
        List<SetClass> list = setService.getClassTree();
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo getAllStandBySetId(AppModelSelParam params) {
        List<StandModelEntity> list = setService.getAllStandBySetId(params);
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo getStandPageBySetId(PageParams<Params> params) {
        List<StandModelEntity> list = setService.getStandPageBySetId(params);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo addStandModel(StandModel standModel) {
        setService.addStandModel(standModel);
        return BaseResultVo.success(standModel);
    }

    @Override
    public BaseResultVo delStandModelById(Params params) {
        setService.delStandModelById(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo selStandModelById(Params params) {
        StandModelEntity resultVo = setService.selStandModelById(params.getId());
        return BaseResultVo.success(resultVo);
    }

    @Override
    public BaseResultVo standModelEnOrdisable(StandModel standModel) {
        setService.standModelEnOrdisable(standModel);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo updStandModel(StandModel standModel) {
        setService.updStandModel(standModel);
        return BaseResultVo.success(standModel);
    }

    /*================区域设置==================*/
    @Override
    public BaseResultVo<SetArea> addArea(SetArea area) {
        SetArea setArea = setService.addArea(area);
        return BaseResultVo.success(setArea.getId());
    }

    @Override
    public BaseResultVo enOrDisArea(SetArea area) {
        setService.enOrDisArea(area);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo updArea(SetArea area) {
        SetArea setArea = setService.updArea(area);
        return BaseResultVo.success(setArea);
    }

    @Override
    public BaseResultVo delArea(Params params) {
        setService.delArea(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo getAllPageArea(PageParams<SetArea> pageParams) {
        List<SetArea> resuleVo =  setService.getAllPageArea(pageParams);
        PageBean pageBean = new PageBean(resuleVo);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo getAllArea() {
        List<SetArea> list = setService.getAllArea();
        return BaseResultVo.success(list);
    }

    /*================供应商设置==================*/
    @Override
    public BaseResultVo addSupplier(SetSupplier supplier) {
        SetSupplier resultVo = setService.addSupplier(supplier);
        return BaseResultVo.success(resultVo);
    }

    @Override
    public BaseResultVo enOrDisSupplier(SetSupplier supplier) {
        setService.enOrDisSupplier(supplier);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo updSupplier(SetSupplier supplier) {
//        System.out.println("============"+JSONObject.toJSONString(supplier));
        SetSupplier resultVo = setService.updSupplier(supplier);
        return BaseResultVo.success(resultVo);
    }

    @Override
    public BaseResultVo delSupplier(Params params) {
        setService.delSupplier(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo getAllPageSupplier(PageParams<SetSupplier> pageParams) {
        List<SetSupplier> list = setService.getAllPageSupplier(pageParams);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo getAllSupplier() {
        List<SetSupplier> list = setService.getAllSupplier();
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo setEmployeeSelf(SetEmployeeSelf ss) {
        setService.setEmployeeSelf(ss);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo delEmployeeSelf(SetEmployeeSelf ss) {
        setService.delEmployeeSelf(ss);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo selEmployeeSelf() {
        List<SetClass> list = setService.selEmployeeSelf();
        return BaseResultVo.success(list);
    }
}
