package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.CollectApi;
import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.assets.service.CollectService;
import com.sdkj.fixed.asset.assets.util.ExcelUtils;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.pojo.assets.Collect;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用控制
 * @Date 2020/7/27 9:57
 */
@RestController
public class CollectController implements CollectApi {

    @Autowired
    private CollectService service;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo<CollectResult> queryPages(PageParams<SearchNameParam> params) {
        List<CollectResult> list = service.queryPages(params);
        PageBean pageBean =  new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    /**
     * 查看
     *
     * @param collectId
     * @return
     */
    @Override
    public BaseResultVo<CollectResult> queryCollect(String collectId) {
        CollectResult result= service.queryCollect(collectId);
        return BaseResultVo.success(result);
    }
    /**
     * 添加领用
     *
     * @param collect
     * @return
     */
    @Override
    public BaseResultVo insertCollect(CollectParam collect) {
        Integer i = service.insertCollect(collect);
        return BaseResultVo.success(i);
    }

    /**
     * 修改领用
     * @param collect
     * @return
     */
    @Override
    public BaseResultVo updateCollect(CollectParam collect) throws Exception {
        Collect collectEntity = service.selectByPrimaryKey(collect.getReceiveId());
        if(collectEntity.getState()==2){
            return BaseResultVo.failure("不能修改已签字或空的单据");
        }
        if(collect.getApplyId()!=null){
            return BaseResultVo.failure("审批单据不能修改");
        }
        Integer i = service.updateCollect(collect);
        return BaseResultVo.success(i);
    }

    /**
     * 领用签字
     *
     * @param param
     * @return
     */
    @Override
    public BaseResultVo updateSign(SignatureParam param) {
        Integer i = service.updateSign(param);
        return BaseResultVo.success(i);
    }

    /**
     * 删除领用
     *
     * @param receiveId
     * @return
     */
    @Override
    public BaseResultVo deleteCollect(String receiveId) throws Exception {
        Collect collect = service.selectByPrimaryKey(receiveId);
        if(collect.getState()==2){
            return BaseResultVo.failure("只能删除未签字的单据");
        }
        if(collect.getApplyId()!=null){
            return BaseResultVo.failure("审批单据不能删除");
        }
        Integer i = service.deleteCollect(receiveId);
        return BaseResultVo.success(i);
    }

    @Override
    public void exportCollect(HttpServletResponse response,String token,String searchName) throws Exception {
        String fileName = "资产领用"+ TimeTool.getTimeDate14();
        List<CollectExport> list = service.queryAll(token,searchName);
        String title = "资产领用";
        String sheetName=fileName;
        ExcelUtils.exportExcel(list,title,sheetName,CollectExport.class,fileName,response);
    }

    @Override
    public void printCollect(HttpServletResponse response, String collectIds) throws Exception {
        List<String> collectIdList = Arrays.asList(collectIds.split(","));
        String fileName = "资产领用单"+ TimeTool.getTimeDate14();
        Workbook workbook=service.printCollect(collectIdList);
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
        workbook.write(response.getOutputStream());
    }

    /**
     * 员工申请领用
     * @param collect
     * @return
     */
    public BaseResultVo insertCollectApply(@RequestBody CollectApplyParam collect){
        Integer i = service.insertCollectApply(collect);
        return BaseResultVo.success(i);
    }

    /**
     * 同意审批
     *
     * @param collect
     * @return
     */
    @Override
    public BaseResultVo collectApplyAgree(CollectApplyAgreeParam collect) {
        Integer i = service.collectApplyAgree(collect);
        return BaseResultVo.success(i);
    }

    /**
     * 拒绝审批
     *
     * @param collect
     * @return
     */
    @Override
    public BaseResultVo collectApplyRefuse(CollectApplyAgreeParam collect) {
        Integer i = service.collectApplyRefuse(collect);
        return BaseResultVo.success(i);
    }

    /**
     * 审批情况查询
     * @param param
     * @retu
     */
    @Override
    public BaseResultVo<CollectApplyResult> getAllCollectApply(ApplySelParam param) {
        List<CollectApplyResult> list = service.getAllCollectApply(param);
        return BaseResultVo.success(list);
    }

    public BaseResultVo<CollectApplyResult> getCollectApplyById(String id,Integer state) {
        CollectApplyResult collectApplyResult = service.getCollectApplyById(id,state);
        return BaseResultVo.success(collectApplyResult);
    }
}
