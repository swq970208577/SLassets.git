package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.AssetWithdrawal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetWithdrawalMapper extends BaseMapper<AssetWithdrawal> {
}