package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.InventoryApi;
import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.assets.service.InventoryService;
import com.sdkj.fixed.asset.assets.util.DateTools;
import com.sdkj.fixed.asset.assets.util.ExcelUtils;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.AssetIntermediate;
import com.sdkj.fixed.asset.pojo.assets.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * @author niuliwei
 * @description 盘点管理
 * @date 2020/8/3 16:46
 */
@Controller
public class InventoryController implements InventoryApi {

    @Autowired
    private InventoryService service;
    @Autowired
    private HttpServletResponse response;

    @Override
    public BaseResultVo add(Inventory inventory) {
        service.add(inventory);
        return BaseResultVo.success(inventory.getInventoryId());
    }

    @Override
    public BaseResultVo delete(Params params) {
        service.delete(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public void export(String orgId, String state, String token) throws IOException {
        List<InventoryExtend> list = service.export(orgId, state, token);
        String fileName = "盘点列表" + DateTools.dateToString(new Date(), "yyyyMMddHHmmss");
        ExcelUtils.exportExcel(list, InventoryExtend.class, fileName, response);
    }

    @Override
    public BaseResultVo queryInvInfo(Params params) {
        Inventory inventory = service.queryInvInfo(params.getId());
        return BaseResultVo.success(inventory);
    }

    @Override
    public void download(String inventoryId) throws IOException {
        service.download(inventoryId);
    }

    @Override
    public BaseResultVo getAllPage(PageParams<StateParam> pageParams) {
        List<InventoryExtend> list = service.getAllPage(pageParams);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo stateNum() {
        StateNum num = service.stateNum();
        return BaseResultVo.success(num);
    }

    @Override
    public BaseResultVo allotUser(Inventory inventory) {
        service.allotUser(inventory);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo queryInvResult(PageParams<AssetIntermediate> pageParams) {
        List<AssetIntermediate> list = service.queryInvResult(pageParams);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo resultStatistical(Params params) {
        InventoryStateNum num =service.resultStatistical(params.getId());
        return BaseResultVo.success(num);
    }

    @Override
    public void exportInvResult(String inventoryId) throws IOException {
        List<AssetIntermediate> list = service.exportInvResult(inventoryId);
        if(list == null || list.size()<=0){
            list = new ArrayList<>();
        }
        String fileName = "盘点结果"+ DateTools.dateToString(new Date(), "yyyyMMddHHmmss");
        ExcelUtils.exportExcel(list, AssetIntermediate.class, fileName, response);
    }

    @Override
    public BaseResultVo submitInvResult(SubInvResultParam param) {
        service.submitInvResult(param);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo queryNoInvUser(PageParams<Search> params) {
        List<UserInfo> list = service.queryNoInvUserInfo(params);
        PageBean pageBean = new PageBean(list);
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo addInvSurplus(AssetIntermediateExtend intermediate) {
        service.addInvSurplus(intermediate);
        return BaseResultVo.success(intermediate.getId());
    }

    @Override
    public BaseResultVo delInvSurplus(Params params) {
        service.delInvSurplus(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo saveUpdInvResultByIds(AssetIntermediate intermediate) {
        service.saveUpdInvResultByIds(intermediate);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo pushNoInvUser(Params params) {
        service.pushNoInvUser(params.getId());
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo appInvList(AppInvSelParam param) {
        List<AppInvEntity> list = service.appInvList(param);
        return BaseResultVo.success(list);
    }

    @Override
    public BaseResultVo appInvOrder(AppInvSelParam param) {
        AppInvOrder inv = service.appInvOrder(param);
        return BaseResultVo.success(inv);
    }

    @Override
    public BaseResultVo appUpdInvState(AppUpdInvState params) {
        service.appUpdInvState(params);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo appSweepGunInv(SweepGunInvParam param) {
        service.appSweepGunInv(param);
        return BaseResultVo.success();
    }

}