package com.sdkj.fixed.asset.assets.service;

import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.assets.in_vo.AppSignParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssMationParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroAppParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroParam;
import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptUser;
import com.sdkj.fixed.asset.assets.mapper.AssApprovalMapper;
import com.sdkj.fixed.asset.assets.util.CacheUtils;
import com.sdkj.fixed.asset.assets.util.JwtTokenUtil;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.AssApproval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * AssApprovalService
 *
 * @author zhaozheyu
 * @Description 我的审批
 * @date 2020/7/30 17:48
 */
@Service
public class AssApprovalService extends BaseService<AssApproval> {
    @Autowired
    private AssApprovalMapper mapper;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtils cacheUtils;

    @Override
    public BaseMapper<AssApproval> getMapper() {
        return mapper;
    }


    /**
     * 我的审批前台查询
     *
     * @param params
     * @return
     */
    public List<AssApproval> getAssAppList(PageParams<AssroParam> params) {
        String userId = params.getParams().getUserid();
        //权限
        AssUser assUser= mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 1) {
            params.getParams().setIfadmin(2);
        } else {
            params.getParams().setIfadmin(1);
        }
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<AssApproval> pageList = mapper.getAssAppList(params.getParams());
        return pageList;
    }

    /**
     * 我的审批前台基本信息查询
     *
     * @param params
     * @return
     */
    public List<ApprovalMation> getInformation(PageParams<AssMationParam> params) {
        String userId = params.getParams().getUserid();
        AssUser assUser = mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 2) {
            params.getParams().setIfadmin(1);
        }
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalMation> pageList = mapper.getInformation(params.getParams());
        return pageList;
    }

    /**
     * 基本信息审批情况查询
     *
     * @param params
     * @return
     */
    public List<ApprovalCon> getExamination(PageParams<AssMationParam> params) {
        String userId = params.getParams().getUserid();
        AssUser assUser = mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 2) {
            params.getParams().setIfadmin(1);
        }
        //权限
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalCon> pageList = mapper.getExamination(params.getParams());
        return pageList;
    }

    /**
     * 基本信息发放情况查询
     *
     * @param params
     * @return
     */
    public List<ApprovalCon> getRelease(PageParams<AssMationParam> params) {
        String userId = params.getParams().getUserid();
        AssUser assUser = mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 2) {
            params.getParams().setIfadmin(1);
        }
        //权限
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalCon> pageList = mapper.getRelease(params.getParams());
        return pageList;
    }


    /**
     * 我的审批app查询
     *
     * @param params
     * @return
     */
    public List<AssApproval> getAppList(PageParams<AssroAppParam> params) {
        String userId = params.getParams().getUserid();
        //权限
        AssUser assUser= mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 1) {
            params.getParams().setIfadmin(2);
        } else {
            params.getParams().setIfadmin(1);
        }
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<AssApproval> pageList = mapper.getAppList(params.getParams());
        return pageList;
    }

    /**
     * 我的审批app基本信息查询
     *
     * @param params
     * @return
     */
    public List<ApprovalMation> getAppInformation(PageParams<AssMationParam> params) {
        String userId = params.getParams().getUserid();
        AssUser assUser = mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 2) {
            params.getParams().setIfadmin(1);
        }
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalMation> pageList = mapper.getAppInformation(params.getParams());
        return pageList;
    }

    /**
     * 我的审批app待审批数量 进行中数量
     *
     * @param
     * @return
     */
    public Assnum getAppnum(AssroAppParam assroAppParam) {
        //资产领用数量
        assroAppParam.setProcessed(1);
        assroAppParam.setDig(1);
        assroAppParam.setAdmin(3);
        Assnum usenumber = mapper.getAppnum(assroAppParam);
        usenumber.setUsenumber(usenumber.getNumber());
        //资产审借数量
        assroAppParam.setProcessed(1);
        assroAppParam.setDig(2);
        assroAppParam.setAdmin(3);
        Assnum borrownumber = mapper.getAppnum(assroAppParam);
        usenumber.setBorrownumber(borrownumber.getNumber());
        //资产审退数量
        assroAppParam.setProcessed(1);
        assroAppParam.setDig(3);
        assroAppParam.setAdmin(3);
        Assnum returnnumber = mapper.getAppnum(assroAppParam);
        usenumber.setReturnnumber(returnnumber.getNumber());
        //物品领用数量数量
        assroAppParam.setProcessed(null);
        assroAppParam.setDig(null);
        assroAppParam.setAdmin(null);
        Assnum collectnumber = mapper.getCollectNum(assroAppParam);
        usenumber.setCollectnumber(collectnumber.getNumber());
        //待审批数量 这里有权限控制
        String userId = assroAppParam.getUserid();
        //权限
        AssUser assUser= mapper.getAdmin(userId);
        if ((assUser.getType().equals("1")) && assUser.getDia() == 1) {
            assroAppParam.setAdmin(2);
        } else {
            assroAppParam.setAdmin(1);
        }
        assroAppParam.setProcessed(2);
        assroAppParam.setDig(null);
        Assnum approvalnumber = mapper.getAppnum(assroAppParam);
        usenumber.setApprovalnumber(approvalnumber.getNumber());
        return usenumber;
    }


    /**
     * APP收货签字
     *
     * @param params
     * @return
     */
    public List<AppSignRes> getAppsign(PageParams<AppSignParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<AppSignRes> pageList = mapper.getAppsign(params.getParams());
        for (AppSignRes appSignRes : pageList) {
            String id = appSignRes.getId();
            List<AppsignResult> appsignList = mapper.getAppsignList(id);
            appSignRes.setAppsignResult(appsignList);
        }
        return pageList;
    }
}
