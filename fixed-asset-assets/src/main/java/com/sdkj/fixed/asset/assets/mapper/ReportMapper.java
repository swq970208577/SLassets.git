package com.sdkj.fixed.asset.assets.mapper;




import com.sdkj.fixed.asset.api.assets.in_vo.report.AssetId;
import com.sdkj.fixed.asset.api.assets.in_vo.report.UseStatusParam;
import com.sdkj.fixed.asset.api.assets.out_vo.report.*;


import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface ReportMapper{
    /**
     * 查询用户是否有全部权限
     * @param userId
     * @return
     */
    public Integer selectUserIsAllDataAuth(@Param("userId") String userId);
    /**
     * 查询用户是否有全部权限
     * @param userId
     * @return
     */
    public List<UserManagement> selectUser(@Param("userId") String userId);

    /**
     * 查询部门或公司权限，type1：公司；2：部门
     * @param userId
     * @param type
     * @return
     */
    public List<String> selectCompanyOrDeptAuth(@Param("userId") String userId,@Param("type") Integer type);
    /**
     * 查询数据权限
     * @param userId
     * @param type
     * @return
     */
    public List<String> selectUserAuth(@Param("userId") String userId,@Param("type") Integer type);
    /**
     * 查询资产清单
     * @param param
     * @return
     */
    public List<AssetListReport> getPageAssetList(Map<String,Object> param);

    /**
     * 查询处理记录
     * @param assetId
     * @return
     */
    public  List<AssetHandleRecord> viewAssetHandleRecord(AssetId assetId);

    /**
     * 导出资产清单查询
     * @param param
     * @return
     */
    public List<AssetListReport> exportQuery(Map<String,Object> param);

    /**
     * 查询资产履历
     * @param param
     * @return
     */
    public List<AssetHandleRecords> getPageAssetHandleRecord(Map<String,Object> param);

    /**
     * 查询资产履历明细
     * @param assetId
     * @return
     */
    public List<AssetHandleRecordDetailed> getAssetHandleRecordDetail(@Param("assetId") String assetId);

    /**
     * 资产类别统计
     * @param param
     * @return
     */
    public List<AssetClassStatistics> getAssetClassStatistics(Map<String,Object> param);

    /**
     * 资产维保到期
     * @param param
     * @return
     */
    public List<maintenanceXxpirationtReport> StatisticsOfMaintenanceExpiration(Map<String,Object> param);

    /**
     * 查询待审批
     * @return
     */
    public int getPendingApprovalCount(Map<String ,Object > param);
    /**
     * 查询待签字
     * @return
     */
    public Integer gettoBeSignedCount(@Param("borrower") String borrower);

    /**
     * 查询维保到期、报修资产数、待确认挑拨单统计
     * @return
     */
    public HomePageStatistics getExpireAndRepairAndTransferCount(Map<String ,Object > param);

    /**
     * 查询首页资产状况
     * @param param
     * @return
     */
    public AssetStatus getAssetStatus(Map<String,Object> param);
    /**
     * 查询首页资产状况
     * @param param
     * @return
     */
    public AssetAllStatus getAssetStatusPer(Map<String,Object> param);

    /**
     * 资产分类统计
     * @param param
     * @return
     */
    public List<Map<String,Object>>getAssetTypeSum(Map<String,Object> param);

    /**
     * 查询权限内启用的根节点类别
     * @param param
     * @return
     */
    public List<Map<String,String>> getEnableAssetType(Map<String,Object> param);

    /**
     * 查询耗材领用情况统计
     * @param param
     * @return
     */
    public List<Map<String,Object>> getHcUseStatus(Map<String,Object> param);

    /**
     * 查询耗材分类
     * @param TopCompanyId
     * @return
     */
    public List<Map<String,String>> getHcType(@Param("TopCompanyId") String TopCompanyId);

    /**
     * 查询固资使用情况
     * @param param
     * @return
     */
    public List<Map<String,Object>> getAssetUseStatus(Map<String,Object> param);
}