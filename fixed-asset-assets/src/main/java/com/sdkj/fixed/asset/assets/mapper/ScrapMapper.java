package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.ScrapExtend;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.Scrap;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ScrapMapper extends BaseMapper<Scrap> {

    public List<ScrapExtend> getScrap(String startDate, String endDate, String orgId, String userId, List<String> idList);

    public ScrapExtend selById(String id);

    public List<String> getScrapAssetId(@Param("idList") List<String> idList);

    public void delInv(String userId, String orgId, List<String> assetIdList);

}