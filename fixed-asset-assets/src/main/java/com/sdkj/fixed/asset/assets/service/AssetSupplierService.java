package com.sdkj.fixed.asset.assets.service;

import com.sdkj.fixed.asset.api.assets.out_vo.AssetSupplierResult;
import com.sdkj.fixed.asset.assets.mapper.AssetSupplierMapper;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.pojo.assets.AssetSupplier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author zhangjinfei
 * @Description //TODO 资产维保信息
 * @Date 2020/7/23 16:38
 */
@Service
public class AssetSupplierService extends BaseService<AssetSupplier> {

    @Resource
    private AssetSupplierMapper mapper;

    @Override
    public BaseMapper getMapper() {
        return mapper;
    }

    public AssetSupplierResult queryMaintainByAssetId(String assetId){
        AssetSupplierResult assetSupplierResults = mapper.queryMaintainByAssetId(assetId);
        return assetSupplierResults;
    }
}
