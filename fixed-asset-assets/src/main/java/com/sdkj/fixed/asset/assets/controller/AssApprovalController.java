package com.sdkj.fixed.asset.assets.controller;


import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.assets.ApprovalApi;
import com.sdkj.fixed.asset.api.assets.in_vo.AppSignParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssMationParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroAppParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroParam;
import com.sdkj.fixed.asset.api.assets.out_vo.AppSignRes;
import com.sdkj.fixed.asset.api.assets.out_vo.ApprovalCon;
import com.sdkj.fixed.asset.api.assets.out_vo.ApprovalMation;
import com.sdkj.fixed.asset.api.assets.out_vo.Assnum;
import com.sdkj.fixed.asset.assets.service.AssApprovalService;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.pojo.assets.AssApproval;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * AssApprovalController
 *
 * @author zhaozheyu
 * @Description 我的审批
 * @date 2020/7/30 17:44
 */
@Controller
public class AssApprovalController extends BaseController<AssApproval> implements ApprovalApi {
    @Autowired
    private AssApprovalService service;
    @Override
    public BaseService getService() {
        return service;
    }


    @ApiOperation(value = "我的审批前台列表", notes = "我的审批前台列表", httpMethod = "POST")
    @Override
    public BaseResultVo<AssApproval> getAssAppList(@RequestBody PageParams<AssroParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<AssApproval> pageList = service.getAssAppList(params);
        PageBean<AssApproval> page = new PageBean<AssApproval>(pageList);
        return BaseResultVo.success(page);
    }


    @ApiOperation(value = "我的审批前台基本信息查询", notes = "我的审批前台基本信息查询", httpMethod = "POST")
    @Override
    public BaseResultVo<ApprovalMation> getInformation(@RequestBody PageParams<AssMationParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalMation> pageList = service.getInformation(params);
        PageBean<ApprovalMation> page = new PageBean<ApprovalMation>(pageList);
        return BaseResultVo.success(page);
    }


    @ApiOperation(value = "基本信息审批情况查询", notes = "基本信息审批情况查询", httpMethod = "POST")
    @Override
    public BaseResultVo<ApprovalCon> getExamination(@RequestBody PageParams<AssMationParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalCon> pageList = service.getExamination(params);
        PageBean<ApprovalCon> page = new PageBean<ApprovalCon>(pageList);
        return BaseResultVo.success(page);
    }

    @ApiOperation(value = "基本信息发放情况查询", notes = "基本信息发放情况查询", httpMethod = "POST")
    @Override
    public BaseResultVo<ApprovalCon> getRelease(@RequestBody PageParams<AssMationParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalCon> pageList = service.getRelease(params);
        PageBean<ApprovalCon> page = new PageBean<ApprovalCon>(pageList);
        return BaseResultVo.success(page);
    }


    @ApiOperation(value = "我的审批app列表", notes = "我的审批app列表", httpMethod = "POST")
    @Override
    public BaseResultVo<AssApproval> getAppList(@RequestBody PageParams<AssroAppParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<AssApproval> pageList = service.getAppList(params);
        PageBean<AssApproval> page = new PageBean<AssApproval>(pageList);
        return BaseResultVo.success(page);
    }

    @ApiOperation(value = "我的审批app基本信息查询", notes = "我的审批app基本信息查询", httpMethod = "POST")
    @Override
    public BaseResultVo<ApprovalMation> getAppInformation(@RequestBody PageParams<AssMationParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<ApprovalMation> pageList = service.getAppInformation(params);
        PageBean<ApprovalMation> page = new PageBean<ApprovalMation>(pageList);
        return BaseResultVo.success(page);
    }

    @ApiOperation(value = "我的审批app数量", notes = "我的审批app数量", httpMethod = "POST")
    @Override
    public BaseResultVo<Assnum> getAppnum(@RequestBody AssroAppParam assroAppParam) {
        Assnum assnum = service.getAppnum(assroAppParam);
        return BaseResultVo.success(assnum);
    }

    @ApiOperation(value = "收货签字app", notes = "收货签字app", httpMethod = "POST")
    @Override
    public BaseResultVo<AppSignRes> getAppsign(@RequestBody PageParams<AppSignParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<AppSignRes> pageList = service.getAppsign(params);
        PageBean<AppSignRes> page = new PageBean<AppSignRes>(pageList);
        return BaseResultVo.success(page);
    }


}
