package com.sdkj.fixed.asset.assets.controller;

import com.sdkj.fixed.asset.api.assets.TransferApi;
import com.sdkj.fixed.asset.api.assets.in_vo.TransferParam;
import com.sdkj.fixed.asset.api.assets.in_vo.TransferSearchParam;
import com.sdkj.fixed.asset.api.assets.out_vo.TransferResult;
import com.sdkj.fixed.asset.assets.service.TransferService;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.pojo.assets.Transfer;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 调拨
 * @Date 2020/7/30 16:35
 */
@RestController
public class TransferController implements TransferApi {

    @Autowired
    private TransferService service;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo<TransferResult> queryPages(PageParams<TransferSearchParam> params) {
        List<TransferResult> transferResults = service.queryPages(params);
        PageBean pageBean =new PageBean(transferResults);
        return BaseResultVo.success(pageBean);
    }

    /**
     * 查看
     *
     * @param transferId
     * @return
     */
    @Override
    public BaseResultVo<TransferResult> queryTransfer(String transferId) {
        TransferResult result = service.queryTransfer(transferId);
        return BaseResultVo.success(result);
    }

    /**
     * 添加调拨
     *
     * @param transfer
     * @return
     */
    @Override
    public BaseResultVo insertTransfer(TransferParam transfer) {
        Integer i = service.insertTransfer(transfer);
        return BaseResultVo.success(i);
    }

    /**
     * 调拨调入
     *
     * @param transfer
     * @return
     */
    @Override
    public BaseResultVo transferSuccess(TransferParam transfer) throws Exception {
        Integer i = service.transferSuccess(transfer);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo transferBack(TransferParam transfer) throws Exception {
        Integer i = service.transferBack(transfer);
        return BaseResultVo.success();
    }

    /**
     * 调拨取消
     *
     * @param transferId
     * @return
     */
    @Override
    public BaseResultVo updateCancel(String transferId) throws Exception {
        Transfer transfer = service.selectByPrimaryKey(transferId);
        if(!StringUtils.equals("0",transfer.getTransferState()) || service.checkTransfer(transferId)){
            return BaseResultVo.failure("已完成或已取消的单据不能再操作。");
        }
        Integer i = service.updateCancel(transferId);
        return BaseResultVo.success(i);
    }

    /**
     * 调拨打印
     *
     * @param response
     */
    @Override
    public void printTransfer(HttpServletResponse response,String transferIds) throws IOException {
            if(StringUtils.isBlank(transferIds)){
                throw new LogicException("请选择单据");
            }
            List<String> transferIdList = Arrays.asList(transferIds.split(","));
            String fileName = "资产调拨单"+ TimeTool.getTimeDate14();
            Workbook workbook=service.printTransfer(transferIdList);
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
            workbook.write(response.getOutputStream());
    }

    /**
     * 查询调入公司管理员
     *
     * @param companyId
     */
    @Override
    public BaseResultVo<UserManagement> transferInAdmin(String companyId) throws IOException {
       List<UserManagement>  list = service.transferInAdmin(companyId);
       return BaseResultVo.success(list);
    }
}
