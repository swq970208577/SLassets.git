package com.sdkj.fixed.asset.assets.service;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * @ClassName printApiJni
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/3 18:04
 */
public interface printApiJni  extends Library {
    /**
     * * - 对于 203DPI 的打印机来说, 1mm = 8dot
     *   - 对于 300DPI 的打印机来说, 1mm = 12dot
     *   - 对于 600DPI 的打印机来说, 1mm = 24dot
     */

    public static final printApiJni printApi = (printApiJni) Native.loadLibrary(printApiJni.class.getResource("/")
                    .getPath().replaceFirst("/", "")+"print/CDFPSK",
            printApiJni.class);

    /**
     * 使用网络端口连接一台打印机
     * @param IPAddr
     * @param netPort
     * @return
     */
    int  PTK_Connect_Timer(String  IPAddr, int  netPort,int time_sec);
    /**
     * 断开和打印机的网络连接
     * @return
     */
    int  PTK_CloseConnect();

    /**
     * 解析错误代码
     * @param error_n
     * @param errorInfo
     * @param infoSize
     * @return
     */
    int  PTK_GetErrorInfo(int error_n, byte[] errorInfo, int infoSize);

    /**
     * 打印一维条码
     * @param px 设置 X 坐标,以点(dots)为单位
     * @param py  设置 Y 坐标,以点(dots)为单位
     * @param pdirec  选择条码的打印方向. 0—不旋转;1—旋转 90°; 2—旋转 180°; 3—旋转 270°.
     * @param pCode  选择要打印的条码类型 (不同类型条码有字符限制或字符个数等限制，请参考具体标准)
     * @param NarrowWidth 设置条码中窄单元的宽度,以点(dots)为单位.
     * @param pHorizontal 设置条码中宽单元的宽度,以点(dots)为单位
     * @param pVertical 设置条码高度,以点(dots)为单位.
     * @param ptext
     * @param pstr 一个长度为 1-100 的字符串。
     * @return
     */
    int  PTK_DrawBarcode( int px,  int py,  int pdirec, String pCode, int NarrowWidth,
                                  int pHorizontal, int pVertical,char ptext,
                                 String pstr);

    /**
     * 命令打印机开始打印标签内容
     * @param number 打印标签的数量1—65535
     * @param cpnumber 每张标签的复制份数1—65535
     * @return
     */
    int PTK_PrintLabel( int number,  int cpnumber);

    /**
     * 清除缓冲
     *
     * @return
     */
    int  PTK_ClearBuffer();

    /**
     *设置 RFID 标签打印参数,单次设置有效
     * @param nReservationParameters 预留参数，默认输入 0
     * @param nReadWriteLocation  RFID 读写位置, 范围：0-999, 默认为 0. 单位 mm
     * @param ReadWriteArea 预留参数。默认值为 0
     * @param nMaxErrNum 读写错误重试次数 0~9 默认 1
     * @param nErrProcessingMethod 预留参数。默认值为 0
     * @return
     */
    int  PTK_SetRFID( int nReservationParameters,  int nReadWriteLocation, int ReadWriteArea,  int nMaxErrNum,  int nErrProcessingMethod);

    /**
     *写 RFID 标签数据
     * @param nRWMode RFID 操作方式. 0—(预留：暂无功能)；1—写 RFID；
     * @param nWForm RFID 写入格式. 0—HEX（十六进制）；1—ASCII；
     * @param nStartBlock 写入起始块.
     * @param nWDataNum 写入字节数.
     * @param nWArea 写入区域. 0—Reserved（保留区）；1—EPC； 3—USER；
     * @param pstr 一个常量字符串。（格式由参数 P2 限制）
     * @return
     */
    int  PTK_RWRFIDLabel( int nRWMode,  int nWForm,  int nStartBlock, int nWDataNum,  int nWArea, String pstr);

    /**
     *打印一行文字
     * @param px 设置 X 坐标,以点(dots)为单位.
     * @param py 设置 Y 坐标,以点(dots)为单位.
     * @param pdirec 选择文字的打印方向. 0—不旋转;1—旋转 90°; 2—旋转 180°; 3—旋转 270°.
     * @param pFont 选择内置字体或软字体. 1—5: 为为打印机内置 5 种西文点阵字体; 6：为打印机内置 1 种中文字体；
     * @param pHorizontal 当 pFont 设置为内置字体时（1~6），此时 pHorizontal 为设置点阵水平放大系数. 可 选择:1—24.
     * @param pVertical  当 pFont 设置为内置字体时（1~6），此时 pVertical 为设置点阵垂直放大系数. 择:1—24.
     * 当 pFont 选择 TrueType 字体时（A~Z），此时 pVertical 为设置字体的高度，单位为像素
     * 点（不限大小）。
     * @param pColor : 选’N’ 对应 ASCII 值 78 则打印正常文本(如黑字白底文本),
     * 选’R’ 对应 ASCII 值 82 则打印文本反色文本(如白字黑底文本).
     * @param p 一个长度为 1-100 的字符串。
     * @return
     */
    int  PTK_DrawText( int px,  int py, int pdirec,  int pFont,
                               int pHorizontal,  int pVertical,String  pColor, String p);

    int  PTK_DrawText_TrueType( int x,  int y,  int FHeight,  int
            FWidth,String FType,  int Fspin,  int FWeight, boolean FItalic,boolean FUnline, boolean FStrikeOut,
                                       String data);

    /**
     * 设置打印坐标原点
     * @param px
     * @param py
     * @return
     */
    int  PTK_SetCoordinateOrigin( int px,  int py);

    /**
     * 设置标签纸高度
     * @param lheight
     * @param gapH
     * @param gapOffset
     * @param bFlag
     * @return
     */

    int  PTK_SetLabelHeight( int lheight,  int gapH, int gapOffset, boolean bFlag);

    /**
     * 设置标签纸宽度
     * @param lwidth
     * @return
     */
    int  PTK_SetLabelWidth( int lwidth);

    /**
     * 设置标签打印方向，T 从标签左上角开始正常打印;B: 将从标签右下角开始打印
     * @param direct
     * @return
     */
    int  PTK_SetDirection(char direct);

    /**
     * 画一根线
     * @param px
     * @param py
     * @param pL
     * @param pH
     * @return
     */
    int  PTK_DrawLineXor( int px,  int py, int pL,  int pH);

    int  PTK_ReadRFIDLabelData( int nDataBlock,  int nRFPower,  int bFeed,
                                       byte[] data, int dataSize);
    int  PTK_RFIDEndPrintLabel( int block, byte[] data, int dataSize);

    /**
     * 命令打印机进行纸张校准
     * @return
     */
    int  PTK_MediaDetect();

    /**
     * 打印一张标签，然后返回打印的 RFID 标签数据和打印机的状态
     * @param block 选择数据区域; 0: TID , 1: EPC , 2: TID+EPC， 3：USER
     * @param data 用于存储获取 RFID 标签数据信息。
     * @param dataSize  data 的空间大小
     * @param printerStatus 打印机当前的状态，返回值格式为 W1XXXX，XXXX 为打印机状态代码
     * @param statusSize
     * @return
     */
    int  PTK_RFIDEndPrintLabelFeedBack( int block, byte[] data, int dataSize,byte[]
            printerStatus, int statusSize);
}
