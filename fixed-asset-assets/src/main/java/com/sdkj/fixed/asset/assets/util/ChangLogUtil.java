package com.sdkj.fixed.asset.assets.util;

import com.sun.javafx.binding.StringFormatter;
import org.apache.commons.lang3.StringUtils;

/**
 * @Author zhangjinfei
 * @Description //TODO 日志模板
 * @Date 2020/7/27 15:32
 */
public class ChangLogUtil {

    /**
     * 借用、归还
     * @param log
     * @return
     */
    public static String simpleLogContxt(LogContext log){

        String context = "%s将";

        /*维保信息变更*/
        String supplierName = "【供应商名称】字段由 \"%s\" 变更为 \"%s\";";
        String suppliercontact = "【供应商联系人】字段由 \"%s\" 变更为 \"%s\";";
        String supplierTel = "【供应商电话】字段由 \"%s\" 变更为 \"%s\";";
        String userName = "【负责人】字段由 \"%s\" 变更为 \"%s\";";
        String expireDate = "【维保到期时间】字段由 \"%s\" 变更为 \"%s\";";
        String expireNote = "【维保说明】字段由 \"%s\" 变更为 \"%s\";";

        /*资产实物信息变更*/
        String assetName = "【资产名称】字段由 \"%s\" 变更为 \"%s\";";
        String assetClassName = "【资产分类】字段由 \"%s\" 变更为 \"%s\";";
        String standardModel = "【标准型号】字段由 \"%s\" 变更为 \"%s\";";
        String specificationModel = "【规格型号】字段由 \"%s\" 变更为 \"%s\";";
        String unitMeasurement = "【计量单位】字段由 \"%s\" 变更为 \"%s\";";
        String snNumber = "【SN号】字段由 \"%s\" 变更为 \"%s\";";
        String source = "【来源】字段由 \"%s\" 变更为 \"%s\";";
        String purchaseTime = "【购入时间】字段由 \"%s\" 变更为 \"%s\";";
        String useCompanyName = "【使用公司】字段由 \"%s\" 变更为 \"%s\";";
        String useDepartmentName = "【使用部门】字段由 \"%s\" 变更为 \"%s\";";
        String handlerUserName = "【使用人】字段由 \"%s\" 变更为 \"%s\";";
        String serviceLife = "【使用年限】字段由 \"%s\" 变更为 \"%s\";";
        String areaName = "【区域】字段由 \"%s\" 变更为 \"%s\";";
        String storageLocation = "【存放地址】字段由 \"%s\" 变更为 \"%s\";";
        String remark = "【备注】字段由 \"%s\" 变更为 \"%s\";";
        String photo = "【照片】字段由 \"%s\" 变更为 \"%s\";";

        String useUser = "【使用人】字段由 \"%s\" 变更为 \"%s\";";
        String state = "【状态】字段由 \"%s\" 变更为 \"%s\";";
        String area = "【区域】字段由 \"%s\" 变更为 \"%s\";";
        String address = "【存放地点】字段由 \"%s\" 变更为 \"%s\";";
        context = StringFormatter.format(context,log.getModelName()).getValue();
        if(log.getModelName()==null){
            context="";
        }

        supplierName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforsupplierName(), log.getAftersupplierName(),supplierName);
        suppliercontact = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforsuppliercontact(), log.getAftersuppliercontact(),suppliercontact);
        supplierTel = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforsupplierTel(), log.getAftersupplierTel(),supplierTel);
        userName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforuserName(), log.getAfteruserName(),userName);
        expireDate = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforexpireDate(), log.getAfterexpireDate(),expireDate);
        expireNote = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforexpireNote(), log.getAfterexpireNote(),expireNote);

        assetName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeassetName(), log.getAfterassetName(),assetName);
        assetClassName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeassetClassName(), log.getAfterassetClassName(),assetClassName);
        standardModel = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforestandardModel(), log.getAfterstandardModel(),standardModel);
        specificationModel = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforespecificationModel(), log.getAfterspecificationModel(),specificationModel);
        unitMeasurement = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeunitMeasurement(), log.getAfterunitMeasurement(),unitMeasurement);
        snNumber = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforesnNumber(), log.getAftersnNumber(),snNumber);
        source = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforesource(), log.getAftersource(),source);
        purchaseTime = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforepurchaseTime(), log.getAfterpurchaseTime(),purchaseTime);
        useCompanyName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeuseCompanyName(), log.getAfteruseCompanyName(),useCompanyName);
        useDepartmentName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeuseDepartmentName(), log.getAfteruseDepartmentName(),useDepartmentName);
        handlerUserName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforehandlerUserName(), log.getAfterhandlerUserName(),handlerUserName);
        serviceLife = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeserviceLife(), log.getAfterserviceLife(),serviceLife);
        areaName = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeareaName(), log.getAfterareaName(),areaName);
        storageLocation = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforestorageLocation(), log.getAfterstorageLocation(),storageLocation);
        remark = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeremark(), log.getAfterremark(),remark);
        photo = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforephoto(), log.getAfterphoto(),photo);

        // 使用人
        useUser = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeName(), log.getAfterName(),useUser);
        // 状态
        state = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeState(), log.getAfterState(),state);
        // 区域
        area = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeArea(), log.getAfterArea(),area);
        // 存放地址
        address = equalsContent((s1,s2)->{ return StringUtils.equals(s1, s2);}, log.getBeforeAddr(), log.getAfterAddr(),address);

        StringBuilder sb  =  new StringBuilder();
        return sb.append(context).append(useUser).append(state).append(area).append(address)
                .append(supplierName).append(suppliercontact).append(supplierTel).append(userName).append(expireDate).append(expireNote)
                .append(assetName)
                .append(assetClassName)
                .append(standardModel)
                .append(specificationModel)
                .append(unitMeasurement)
                .append(snNumber)
                .append(source)
                .append(purchaseTime)
                .append(useCompanyName)
                .append(useDepartmentName)
                .append(handlerUserName)
                .append(serviceLife)
                .append(areaName)
                .append(storageLocation)
                .append(remark)
                .append(photo).toString();
    }

    public static String equalsContent(EqualsContent<String, Boolean> fun, String before, String after,String text){
        if(StringUtils.isBlank(after)){
            return "";
        }
        Boolean flag = fun.equalsText(before, after);
        if(StringUtils.isBlank(before)){
            before = "<空>";
        }
        if(flag){
            text="";
        }else{
            text =  StringFormatter.format(text, before, after).getValue();
        }
        return text;
    }
}
interface EqualsContent<T,R>{
    public R equalsText(T t1,T t2);
}

