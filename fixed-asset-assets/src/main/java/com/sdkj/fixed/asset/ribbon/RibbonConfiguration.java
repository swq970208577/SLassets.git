package com.sdkj.fixed.asset.ribbon;

import com.netflix.loadbalancer.IRule;
import com.sdkj.fixed.asset.common.nacos.NacosWeightedRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ribbon的配置
 *
 *王艳
 */
@Configuration
public class RibbonConfiguration {
    @Bean
    public IRule ribbonRule() {
        return new NacosWeightedRule();
    }
}
