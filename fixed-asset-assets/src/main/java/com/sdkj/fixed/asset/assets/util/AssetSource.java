package com.sdkj.fixed.asset.assets.util;

public enum AssetSource {

    A("购入"),
    B("自建"),
    C("租赁"),
    D("捐赠"),
    E("其他"),
    F("内部购入");



    private String source;
    private AssetSource(String source) {
        this.source = source ;
    }
    public String getSource() {
        return source;
    }
}
