package com.sdkj.fixed.asset.assets.controller;

import cn.hutool.core.util.StrUtil;
import com.sdkj.fixed.asset.api.assets.ReportApi;
import com.sdkj.fixed.asset.api.assets.in_vo.report.*;
import com.sdkj.fixed.asset.api.assets.out_vo.report.*;
import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.assets.service.ReportService;
import com.sdkj.fixed.asset.assets.util.ExcelStyle;
import com.sdkj.fixed.asset.assets.util.ExcelUtils;
import com.sdkj.fixed.asset.assets.util.RedisUtil;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.jwt.JwtTokenUtil;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.*;

/**
 * @ClassName ReportController
 * @Description 报表
 * @Author 张欣
 * @Date 2020/7/30 15:56
 */
@Controller
public class ReportController implements ReportApi {
    /**
     * 查询资产清单
     * @param pageParams
     * @return
     */
    @Autowired
    private ReportService reportService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private RedisUtil redisUtil;
    //获取当前登录用户
    private LoginUser getUser(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        return (LoginUser) redisUtil.get(userId);
    }

    /**
     * 查询资产清单
     * @param pageParams
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getPageAssetList(PageParams<AssetListParam> pageParams) throws Exception{
        List<AssetListReport> assetList = reportService.getPageAssetList(pageParams, getUser(request.getHeader("token")));
        PageBean pageBean = new PageBean(assetList);
        return BaseResultVo.success(pageBean);
    }
    /**
     * 查看资产处理记录
     * @param assetId
     * @return
     */
    @Override
    public BaseResultVo viewAssetHandleRecord(@RequestBody @Validated  AssetId assetId) throws Exception{
        return BaseResultVo.success(reportService.viewAssetHandleRecord(assetId));
    }
    /**
     * 导出资产清单
     * @param
     * @return
     */
    @Override
    public void exportAssetList(String useCompanyId, String search, String loginUserId, HttpServletResponse response) throws Exception{
        if(StrUtil.isBlank(loginUserId)){
            throw new LogicException("当前登录用户id不能为空");
        }
        List<AssetListReport> exportData = reportService.getExportData(useCompanyId, search, loginUserId);
        String fileName = "资产清单" + TimeTool.getTimeDate14();
        ExcelUtils.exportExcel(exportData,AssetListReport.class,fileName,response);
    }
    /**
     * 查询资产履历
     * @param pageParams
     * @return
     */
    @Override
    public BaseResultVo getPageAssetHandleRecord(@RequestBody PageParams<AssetHandleRecordParam> pageParams) throws Exception{
        List<AssetHandleRecords> assetHandleRecord = reportService.getPageAssetHandleRecord(pageParams.getParams(),pageParams.getCurrentPage(),pageParams.getPerPageTotal(), getUser(request.getHeader("token")));
        PageBean pageBean = new PageBean(assetHandleRecord);
        return  BaseResultVo.success(pageBean);
    }
    /**
     * 导出资产履历
     * @param token
     * @param startTime
     * @param endTime
     * @param response
     * @throws Exception
     */
    @Override
    public void exportAssetHandleRecord(String token,String startTime,String endTime, HttpServletResponse response)throws Exception{
        AssetHandleRecordParam param = new AssetHandleRecordParam();
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        //列表数据
        HSSFWorkbook wb = reportService.exportAssetHandleRecords(param, getUser(token));
        String fileName = "资产履历"+ TimeTool.getTimeDate14();
        // 输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setHeader("Content-disposition","attachment; filename="+ URLEncoder.encode(fileName + ".xlsx", "UTF-8"));
        response.setContentType("application/msexcel");
        wb.write(output);
        output.close();

    }
    /**
     * 资产分类汇总表
     * @param param
     * @return
     */
    @Override
    public BaseResultVo<AssetClassStatistics> getAssetClassStatistics(@RequestBody AssetClassParam param) throws Exception{
        return BaseResultVo.success(reportService.getAssetClassStatistics(param, getUser(request.getHeader("token"))));
    }
    /**
     * 导出资产分类汇总表
     * @param token
     * @param companyId
     * @param useDeptId
     * @param response
     * @throws Exception
     */
    @Override
    public void exportAssetClassStatistics(String token,String companyId,String useDeptId, HttpServletResponse response)throws Exception{
        AssetClassParam param = new AssetClassParam();
        param.setCompanyId(companyId);
        param.setDeptId(useDeptId);
        List<AssetClassStatistics> exportData = reportService.getAssetClassStatistics(param, getUser(token));
        String fileName = "资产分类汇总表";
        ExcelUtils.exportExcel(exportData,AssetClassStatistics.class,fileName,response);
   }
    /**
     * 维保到期统计表
     * @param param
     * @return
     */
    @Override
    public BaseResultVo StatisticsOfMaintenanceExpiration(@RequestBody @Validated  PageParams<MaintenanceExpirationParam> param) throws Exception{
        List<maintenanceXxpirationtReport> list = reportService.StatisticsOfMaintenanceExpiration(param.getParams(), param.getCurrentPage(), param.getPerPageTotal(), getUser(request.getHeader("token")));
        return BaseResultVo.success(new PageBean(list));
    }
    /**
     * 维保到期统计表
     * @param
     * @return
     */
    @Override
    public void exportStatisticsOfMaintenanceExpiration(String startTime, String endTime,String search, String token, HttpServletResponse response) throws Exception{
        if(StrUtil.isBlank(token)){
            throw new LogicException("token不能为空");
        }
        if(StrUtil.isBlank(startTime)){
            throw new LogicException("开始时间不能为空");
        }
        if(StrUtil.isBlank(endTime)){
            throw new LogicException("结束时间不能为空");
        }
        MaintenanceExpirationParam param = new MaintenanceExpirationParam();
        param.setEndTime(endTime);
        param.setStartTime(startTime);
        param.setSearch(search);
        List<maintenanceXxpirationtReport> exportData = reportService.StatisticsOfMaintenanceExpiration(param, null,null, getUser(token));
        String fileName = "维保到期统计表" + TimeTool.getTimeDate14();
        ExcelUtils.exportExcel(exportData,maintenanceXxpirationtReport.class,fileName,response);
    }
    /**
     * 查询首页头部统计信息（待审批、待签字、维保到期、报修资产数、待确认挑拨单）
     *
     * @return
     */
    @Override
    public BaseResultVo getHeaderData() throws Exception{
       return BaseResultVo.success(reportService.getHeaderData(getUser(request.getHeader("token"))));
    }
    /**
     * 查询资产状况
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<AssetStatus> getAssetStatus() throws Exception{
       return BaseResultVo.success(reportService.getAssetStatus(getUser(request.getHeader("token"))));
    }
    /**
     * 查询资产状况占比
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<AssetAllStatus> getAssetStatPer() throws Exception{
        return BaseResultVo.success(reportService.getAssetStatusPer(getUser(request.getHeader("token"))));
    }
    /**
     * 资产分类统计
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<HomePageStatisticsResult> getAssetTypeSum(String companyId) throws Exception{
        return BaseResultVo.success(reportService.getAssetTypeSum(getUser(request.getHeader("token")),companyId));
    }
    /**
     * 耗材领用情况
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getHcUseStatus(@RequestBody @Validated   UseStatusParam param) throws Exception{
        return BaseResultVo.success(reportService.getHcUseStatus(param,getUser(request.getHeader("token"))));
    }
    /**
     * 固资使用情况
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getAssetUseStatus(@RequestBody @Validated  UseStatusParam param) throws Exception{
        return BaseResultVo.success(reportService.getAssetUseStatus(param,getUser(request.getHeader("token"))));

    }
}
