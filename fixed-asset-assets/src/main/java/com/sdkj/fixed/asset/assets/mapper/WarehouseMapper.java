package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.in_vo.WarehousePagesParam;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.Warehouse;
import com.sdkj.fixed.asset.pojo.system.OrgManagement;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WarehouseMapper extends BaseMapper<Warehouse> {

    List<WarehouseResult> queryPages(WarehousePagesParam warehouse);

    /**
     * 权限通用查询
     * @param userId
     * @param type
     * @return
     */
    List<String> queryRelationIdLists(@Param("userId") String userId, @Param("type") String type);

    WarehouseResult queryByAssetId(String assetId);

    /**
     * 资产ID集合 查询
     * @param assetIds
     * @return
     */
    List<WarehouseResult> queryByAssetIds(@Param("assetIds")  List<String> assetIds);

    List<WarehouseResult> selectAssetRelation(@Param("tableName")String tableName, @Param("columnName")String columnName, @Param("otherId")String otherId);

    OrgManagement queryOrgByCode(@Param("code") String code,@Param("orgId")String orgId);

    OrgManagement queryOrgById(@Param("id") String id);

    OrgManagement queryDeptByOrgIdAndCode(@Param("orgId") String orgId,@Param("code") String code);

    UserManagement queryUserByUsername(@Param("name") String name,@Param("orgId") String orgId,@Param("type")String type);

    UserManagement queryUserByUserId(String userId);

    String queryClassName(List<String> classList);

    public UsageManagement selUsage(String companyId);
}