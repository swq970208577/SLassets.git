package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.MaintenanceChangeAssetsExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.MaintenanceChangeExtend;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.MaintenanceChange;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MaintenanceChangeMapper extends BaseMapper<MaintenanceChange> {

    public List<MaintenanceChangeExtend> getChange(String startDate, String endDate, String orgId, String userId, List<String> idList);

    public MaintenanceChangeExtend selById(String id);

    public List<String> getChangeAssetId(String id);

    public List<MaintenanceChangeAssetsExtend> selChangeBefor(String id);
}