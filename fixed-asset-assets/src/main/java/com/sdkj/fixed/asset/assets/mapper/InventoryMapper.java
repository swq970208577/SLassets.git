package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.Inventory;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface InventoryMapper extends BaseMapper<Inventory> {


    public List<String> userNameList(@Param("userIdList") List<String> userIdList);

    public List<InventoryExtend> getAll(String orgId, String state, List<String> ids, String userId);

    public StateNum stateNum(String orgId, String userId);

    public InventoryStateNum invResultNum(String inventoryId);

    public String[] getInvUser(String inventoryId, String inventoryState);

    public List<UserInfo> getNoInvUserInfo(String inventoryId, String search);

    public List<AppInvEntity> queryInvList(String orgId, String userId, String inventoryState, String search, String inventoryId);

    public List<Inventory> queryInvByAsset(String orgId, String userId, String assetId, String inventoryState, String inventoryId, String username);

    public AppInvOrder invOrder(String userId, String inventoryId);

    public UserManagement userInfo(@Param("id") String id);

}