package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.BorrowApplyExtend;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.BorrowApply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BorrowApplyMapper extends BaseMapper<BorrowApply> {

    public List<BorrowApplyExtend> getAllBorrowApply(String createDate, String userId, String orgId, Integer state, String id);

    public List<String> getAssetIdByModel(String modelId, Integer num);

    public List<String> getModelOrAssetAdmin(String orgId, String assetId, Integer type);

    public BorrowApply selBorrowApplyAssetId(String BorrowApplyAssetId);

}