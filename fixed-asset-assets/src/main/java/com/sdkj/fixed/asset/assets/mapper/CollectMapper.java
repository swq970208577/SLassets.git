package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.out_vo.CollectApplyResult;
import com.sdkj.fixed.asset.api.assets.out_vo.CollectResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.Collect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface CollectMapper extends BaseMapper<Collect> {

    List<CollectResult> queryPages(@Param("searchName")String searchName,@Param("orgId")String orgId,@Param("userId")String userId);

    CollectResult selectByReceiveId(@Param("receiveId") String receiveId);

    List<CollectApplyResult> getAllCollectApply(String createTime, String userId,Integer state);

    CollectApplyResult getCollectApplyById(String id, Integer state);

    List<String> getModelOrAssetAdmin(String orgId, String assetId, Integer type);
}