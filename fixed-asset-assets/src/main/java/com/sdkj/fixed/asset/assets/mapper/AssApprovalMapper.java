package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.AppSignParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssMationParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroAppParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroParam;
import com.sdkj.fixed.asset.api.assets.out_vo.*;
import com.sdkj.fixed.asset.pojo.assets.AssApproval;
import feign.Param;
import org.apache.ibatis.annotations.Mapper;
import com.sdkj.fixed.asset.common.base.BaseMapper;

import java.util.List;

@Mapper
public interface AssApprovalMapper extends BaseMapper<AssApproval> {

    /**
     * 审批前台查询
     */
    List<AssApproval> getAssAppList(AssroParam params);

    /**
     * 我的审批前台基本信息查询
     */
    List<ApprovalMation> getInformation(AssMationParam params);

    /**
     * 基本信息审批情况查询
     */
    List<ApprovalCon> getExamination(AssMationParam params);

    /**
     * 基本信息发放情况查询
     */
    List<ApprovalCon> getRelease(AssMationParam params);

    /**
     * 我的审批app基本信息查询
     */
    List<ApprovalMation> getAppInformation(AssMationParam params);

    /**
     * 审批app查询
     */
    List<AssApproval> getAppList(AssroAppParam params);

    /**
     * 我的审批app数量
     */
    Assnum getAppnum(AssroAppParam assroAppParam);

    /**
     * 我的审批 app 物品领用数量
     */
    Assnum getCollectNum(AssroAppParam assroAppParam);

    /**
     * APP收货签字
     */
    List<AppSignRes> getAppsign(AppSignParam params);

    /**
     * APP收货签字物品
     */
    List<AppsignResult> getAppsignList(@Param("id") String id);

    /**
     * 查管理员
     *
     * @return
     */
    AssUser getAdmin(@Param("userId") String userId);
}
