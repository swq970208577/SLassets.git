package com.sdkj.fixed.asset.assets.service;

import com.sdkj.fixed.asset.assets.mapper.AssetLogMapper;
import com.sdkj.fixed.asset.assets.util.DateTools;
import com.sdkj.fixed.asset.assets.util.JwtTokenUtil;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.pojo.assets.AssetLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 处理记录
 * @Date 2020/7/23 12:05
 */
@Service
public class AssetLogService extends BaseService<AssetLog> {

    @Resource
    private AssetLogMapper mapper;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public BaseMapper getMapper() {
        return mapper;
    }
    /**
     * 通过资产ID 查询处理记录
     * @param assetId
     * @return
     */
    public List<AssetLog> queryLogByAssetId(String assetId) {
        List<AssetLog> assetLogList =  mapper.queryLogByAssetId(assetId);
        return assetLogList;
    }

    /**
     * 添加处理记录
     * @param assetLog
     * @return
     */
    public Integer insertAssetLog(AssetLog assetLog) {
        String token = request.getHeader("token");
        String userIdFromToken = jwtTokenUtil.getUserIdFromToken(token);
        String userId = userIdFromToken.split("_")[1];
        assetLog.setCreateTime(DateTools.nowDate());
        assetLog.setCreateUser(userId);
        assetLog.setUpdateTime(DateTools.nowDate());
        assetLog.setUpdateUser(userId);
       Integer result =  mapper.insertSelective(assetLog);
        return result;
    }


}
