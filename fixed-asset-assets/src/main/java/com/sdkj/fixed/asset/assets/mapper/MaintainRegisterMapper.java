package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.in_vo.MaintainRegisterExtend;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.MaintainRegister;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MaintainRegisterMapper extends BaseMapper<MaintainRegister> {

    public List<MaintainRegisterExtend> getMaintainRegister(String startDate, String endDate, String orgId, String userId, List<String> idList);

    public MaintainRegisterExtend selById(String id);

    public List<String> getMaintainRegAssetId(String id);
}