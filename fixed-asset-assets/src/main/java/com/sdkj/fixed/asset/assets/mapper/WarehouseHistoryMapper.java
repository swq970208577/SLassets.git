package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseHistoryResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.WarehouseHistory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WarehouseHistoryMapper extends BaseMapper<WarehouseHistory> {

    void batchInsert(List<String> assetIdList ,String handerId,Integer handerType);

    List<WarehouseHistoryResult> queryWarehouseHistory(String handerId, Integer handerType);

    /**
     * 资产调拨 使用
     * @param handerId
     * @param handerType
     * @return
     */
    List<WarehouseHistoryResult> queryWarehouseTransfer(String handerId, Integer handerType);
}