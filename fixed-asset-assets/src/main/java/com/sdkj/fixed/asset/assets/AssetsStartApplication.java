package com.sdkj.fixed.asset.assets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/20 15:15
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class AssetsStartApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(AssetsStartApplication.class, args);
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AssetsStartApplication.class);
    }
}
