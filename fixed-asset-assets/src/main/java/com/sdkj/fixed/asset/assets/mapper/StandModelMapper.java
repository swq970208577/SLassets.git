package com.sdkj.fixed.asset.assets.mapper;


import com.sdkj.fixed.asset.api.assets.out_vo.StandModelEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.StandModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StandModelMapper extends BaseMapper<StandModel> {

    /**
     * 根据id查询标准类型
     * @param id
     * @return
     */
    public StandModelEntity selById(String id);

    /**
     * 根据分类id查询标准类型
     * @param setId
     * @return
     */
    public List<StandModelEntity> getStandBySetId(String setId, List<String> idList, String search, String state);

}