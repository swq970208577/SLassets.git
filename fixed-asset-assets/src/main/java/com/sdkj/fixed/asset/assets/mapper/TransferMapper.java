package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.api.assets.out_vo.TransferResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.Transfer;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TransferMapper extends BaseMapper<Transfer> {

    List<TransferResult> queryPages(@Param("searchName")String searchName , @Param("state")String state, @Param("orgId")String orgId, @Param("userId")String userId );

    TransferResult selectByTransferId(@Param("transferId") String transferId);

    List<UserManagement> transferInAdmin(@Param("companyId") String companyId);
}