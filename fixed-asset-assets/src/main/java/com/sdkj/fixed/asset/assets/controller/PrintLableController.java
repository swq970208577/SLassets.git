package com.sdkj.fixed.asset.assets.controller;


import com.sdkj.fixed.asset.api.assets.PrintLableApi;
import com.sdkj.fixed.asset.api.assets.in_vo.PrintLabelParam;
import com.sdkj.fixed.asset.assets.service.PrintLabelService;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName PrintLable
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/3 17:15
 */
@Controller

public class PrintLableController implements PrintLableApi {
    @Autowired
    private PrintLabelService printLabelService;
    @Override
    public BaseResultVo printLabel(@RequestBody @Validated PrintLabelParam param ) throws Exception{
        printLabelService.printLabel(param);
        return BaseResultVo.success();
    }
}
