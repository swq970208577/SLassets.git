package com.sdkj.fixed.asset.assets.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author zhangjinfei
 * @Description //TODO 时间工具包
 * @Date 2020/7/21 15:13
 */
public class DateTools {

    public static String dateToString(Date date , String pattern){
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat(pattern);
        String format = simpleDateFormat.format(date);
        return format;
    }
    public static String nowDate(){
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = simpleDateFormat.format(new Date());
        return now;
    }

    public static String nowDay(){
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd");
        String now = simpleDateFormat.format(new Date());
        return now;
    }

    public static String nowDayStr(){
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyyMMdd");
        String now = simpleDateFormat.format(new Date());
        return now;
    }

    public static String currentDay(){
        int i = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        String day = String.valueOf(i<0 ? "0"+i : i);
        return day;
    }

    public static String currentMonth(){
        int i = Calendar.getInstance().get(Calendar.MONTH) + 1;
        String month = i>0 ? "0"+i : ""+i;
        return month;
    }
    public static String currentYear(){
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        return year;
    }
    public static String getnewDate(String data, String recordDate) {
        String dataStr[] = data.split("-");
        //年份
        int  year = (Integer.parseInt(dataStr[1]) + Integer.parseInt(recordDate))/12;
        //月份
        int yue = (Integer.parseInt(dataStr[1]) + Integer.parseInt(recordDate))%12;
        String a = "";
        if(yue<10){
            if(yue<1){
                a = "12";
            }else{
                a = "0"+yue;
            }
        }else {
            a = yue+"";
        }
        dataStr[0]=String.valueOf(Integer.parseInt(dataStr[0]) + year);
        dataStr[1]=a;
        String  newdata = dataStr[0]+"-"+dataStr[1];
        return newdata;
    }

    public static void main(String[] args) {
        String s1 = getnewDate("2020-01", "15");
        String s = s1;
        System.out.println(s);
        Period between = Period.between(LocalDate.parse("2020-01-02"), LocalDate.parse("2021-04-01"));

    }

    /**
     * 获取当天剩余秒数
     * @return
     */
    public static Long expiresTime(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return (cal.getTimeInMillis() - System.currentTimeMillis()) / 1000;
    }
}
