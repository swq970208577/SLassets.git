package com.sdkj.fixed.asset.assets.mapper;

import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.AssetLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AssetLogMapper extends BaseMapper<AssetLog> {

    List<AssetLog> queryLogByAssetId(String assetId);
}