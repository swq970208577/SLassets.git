package ribbon;

import com.netflix.loadbalancer.IRule;

import com.sdkj.fixed.asset.common.nacos.NacosWeightedRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ribbon的配置
 */
@Configuration
public class RibbonConfiguration {

    /*@Bean
    public IRule ribbonRule() {
        return new NacosSameClusterWeightedRule();
    }*/
    @Bean
    public IRule ribbonRule() {
        return new NacosWeightedRule();
    }
}
