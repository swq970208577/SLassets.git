package com.sdkj.fixed.asset.gateway.core.filters;

/**
 * @ClassName NoValidateRequest
 * @Description TODO
 * @Author 张欣
 * @Date 2020/5/14 21:44
 */
public enum NoValidateRequest {
    /**
     * pc登录
     */
    PC_LOGIN("pc登录", "/login/login/api/loginPC"),
    /**
     * 移动端登录
     */
    MOBILE_LOGIN("移动端登录", "/login/login/api/mobile"),
    /**
     * 下载公司模板
     */
    DOWN_COMPANY_TEMP("下载公司模板", "/system/org/api/downCompanyTemp"),
    /**
     * 下载部门模板
     */
    DOWN_DEPT_TEMP("下载公司模板", "/system/org/api/downDeptTemp"),
    /**
     * 下载用户模板
     */
    DOWN_USER_TEMP("下载用户模板", "/system/user/api/downUserTemp"),
    /**
     * 导出用户
     */
    EXPORT_USER("导出用户", "/system/user/api/exportUser"),


    /**
     * 耗材-入库单-打印
     */
    HC_RECEIPTIN_PRINT("入库单-打印", "/hc/receiptIn/api/print"),
    /**
     * 耗材-盘点-导出
     */
    HC_RECEIPTCHECK_EXPORT("盘点-导出", "/hc/receiptCheck/api/export"),
    /**
     * 耗材-盘点子单-导出
     */
    HC_RECEIPTCHECK_EXPORTDETAIL("盘点子单-导出", "/hc/receiptCheck/api/exportDetail"),
    /**
     * 物品分类-导出
     */
    HC_PRODUCTTYPE_EXPORT("物品分类-导出", "/hc/productType/api/export"),
    /**
     * 物品-导出
     */
    HC_PRODUCT_EXPORT("物品-导出", "/hc/product/api/export"),
    /**
     * 物品-下载导入模板
     */
    HC_PRODUCT_DOWNTEMP("物品-下载导入模板", "/hc/product/api/downTemp"),
    /**
     * 物品-导入
     */
    HC_PRODUCT_IMPORTPRODUCT("物品-导入", "/hc/product/api/importProduct"),
    /**
     * 仓库-导出
     */
    HC_CATEGORY_EXPORT("仓库-导出", "/hc/category/api/export"),
    /**
     * 入库单-导出
     */
    HC_RECEIPTIN_EXPORT("入库单-导出", "/hc/receiptIn/api/export"),
    /**
     * 出库单-打印
     */
    HC_RECEIPTOUT_PRINT("出库单-打印", "/hc/receiptOut/api/print"),
    /**
     * 出库单-导出
     */
    HC_RECEIPTOUT_EXPORT("出库单-导出","/hc/receiptOut/api/check"),
    /**
     * 盘亏出库-打印
     */
    HC_RECEIPTOUT_PRINTCHECK("盘亏出库-打印","/hc/receiptOut/api/printCheck"),
    /**
     * 盘亏出库-导出
     */
    HC_RECEIPTOUT_EXPORTCHECK("盘亏出库-导出","/hc/receiptOut/api/exportCheck"),
    /**
     * 调拨单-导出
     */
    HC_RECEIPTMOVE_EXPORT("调拨单-导出","/hc/receiptMove/api/export"),

    /**
     * 调拨单-打印
     */
    HC_RECEIPTMOVE_PRINT("调拨单-打印","/hc/receiptMove/api/print"),

    HC_MABLEXPORT("耗材领用查询-导出","/hc/category/api/MablExport"),

    HC_STOCK_EXPORT("即时库存查询-导出","/hc/category/api/StockExport"),

    HC_PRODUCTTYPE_STATS_EXPORT("耗材分类统计-导出","/hc/productType/api/exportStatistics"),

    HC_PRODUCT_STATS_EXPORT("收发存汇总表-导出","/hc/product/api/exportStatistics"),

    HC_PRODUCT_STATSDETAIL_EXOPRT("收发存明细表-导出","/hc/product/api/exportStatisticsDetail"),

    HC_RECEIPTAPPLY_EXPORT("耗材领用表-导出","/hc/receiptApply/api/export"),

    HC_IMG_UPLOAD("上传图片","/hc/product/api/saveImg"),

    PRINT_SCRAP("清理报废-打印", "/assets/scrap/api/print"),
    PRINT_REGISTER("维修信息登记-打印", "/assets/register/api/print"),
    PRINT_MAINTENANCE_CHANGE("维保信息变更-打印", "/assets/maintenanceChange/api/print"),
    PRINT_ASSET_INFO_CHANGE("实物信息变更-打印", "/assets/assetInfoChange/api/print"),
    PRINT_BORROW("借用与归还-打印", "/assets/borrow/api/print"),
    EXPORT_MAINTAIN_REGISTER("维修信息登记-导出", "/assets/register/api/exportExcel"),

    EXPORT_SCRAP("清理报废-导出", "/assets/scrap/api/exportExcel"),
    EXPORT_INVENTORY("盘点管理-下载", "/assets/inventory/api/download"),
    DOWN_INVENTORY("盘点管理-导出", "/assets/inventory/api/export"),

    DOWN_IMPORT_TEMPLATE("资产入库-下载模板", "/assets/warehouse/api/importTemplate"),
    EXPORT_WAREHOUSE_RESUME("资产入库-导出资产及单据", "/assets/warehouse/api/exportWarehouseResume"),
    EXPORT_WAREHOUSE("资产入库-导出资产", "/assets/warehouse/api/exportWarehouse"),
    EXPORT_INVENTORY_DETAIL("盘点管理-导出", "/assets/inventory/api/exportInvResult"),

    PRINT_CARD("资产入库-打印资产卡片", "/assets/warehouse/api/printCard"),
    PRINT_WAREHOUSING("资产入库-打印入库单", "/assets/warehouse/api/printWarehousing"),

    EXPORT_COLLECT("资产领用-导出", "/assets/collect/api/exportCollect"),
    PRINT_COLLECT("资产领用-打印", "/assets/collect/api/printCollect"),

    EXPORT_WITHDRAWAL("资产退库-导出", "/assets/withdrawal/api/exportWithdrawal"),
    PRINT_WITHDRAWAL("资产退库-打印", "/assets/withdrawal/api/printWithdrawal"),

    PRINT_TRANSFER("资产调拨-打印", "/assets/transfer/api/printTransfer"),
    /**
     * 报表
     */
    EXPORT_ASSETLIST("导出资产清单", "/assets/report/api/exportAssetList"),

    EXPORT_ASSETHANDLERECORD("导出资产履历", "/assets/report/api/exportAssetHandleRecord"),
    EXPORT_ASSETCLASSSTATISTICS("导出分类汇总表", "/assets/report/api/exportAssetClassStatistics"),
    EXPORT_MAINTENANCEEXPIRATION("导出资产维修到期统计表", "/assets/report/api/exportStatisticsOfMaintenanceExpiration"),

    EXPORT_REPORT_SCRAP("导出报表-清理清单", "/assets/assetReport/api/exportScrap"),
    EXPORT_REPORT_USERASSETINFO("导出报表-员工资产统计", "/assets/assetReport/api/exportUserAssetInfo"),
    EXPORT_STANDARD_MODEL("导出报表-标准资产型号统计导出", "/assets/assetReport/api/exportStandardModelReport"),
    EXPORT_DUE_ASSETS("导出报表-到期资产导出", "/assets/assetReport/api/exportDueAssetsReport"),
    EXPORT_MONTHLY_INCREAS("导出报表-月增加对账导出", "/assets/assetReport/api/exportMonthlyIncreasReport"),
    EXPORT_USE_CLASS("导出报表-分类使用情况表导出", "/assets/assetReport/api/exportUseClassReport"),
    EXPORT_COMPANY_DEPT("导出报表-公司部门汇总表导出", "/assets/assetReport/api/exportCompanyDeptReport"),
    EXPORT_CLASS_INCREASE_DECREASET("导出报表-资产分类增减表导出", "/assets/assetReport/api/exportClassIncreaseDecreaset"),

    AUTH_CODE("注册验证码", "/system/register/api/authCode"),
    REGISTER("注册", "/system/register/api/register"),

    FED_ADD("问题反馈添加", "/system/fed/api/add");

    private String url;
    private String mes;

    NoValidateRequest(String mes, String url) {
        this.url = url;
        this.mes = mes;
    }

    public String getUrl() {
        return url;
    }

    public String getMes() {
        return mes;
    }
}
