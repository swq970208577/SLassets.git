package com.sdkj.fixed.asset.gateway.config;


import com.sdkj.fixed.asset.gateway.core.filters.JwtTokenGatewayFilterFactory;
import com.sdkj.fixed.asset.gateway.core.filters.RequestNoFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 过滤器的配置
 */
@Configuration
public class FilterConfig {
    /**
     * 请求号header过滤器
     */
    @Bean
    public RequestNoFilter requestNoFilter() {
        return new RequestNoFilter();
    }
    /**
     * token校验过滤器
     *
     */
    @Bean
    public JwtTokenGatewayFilterFactory jwtTokenGatewayFilterFactory() {
        return new JwtTokenGatewayFilterFactory();
    }
}
