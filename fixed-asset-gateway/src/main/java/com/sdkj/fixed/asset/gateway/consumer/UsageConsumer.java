package com.sdkj.fixed.asset.gateway.consumer;

import com.sdkj.fixed.asset.api.system.UsageConsumerApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/13 16:36
 */
@FeignClient(name = "fixed-asset-system")
public interface UsageConsumer extends UsageConsumerApi {
}
