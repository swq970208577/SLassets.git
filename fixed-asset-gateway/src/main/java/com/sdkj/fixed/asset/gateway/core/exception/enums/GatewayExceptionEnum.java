package com.sdkj.fixed.asset.gateway.core.exception.enums;

/**
 * 签名异常
 */
public enum GatewayExceptionEnum{
    /**
     * 参数不合法
     */
    NOTNULL_PARAMS(100001,"参数不合法:"),
    /**
     * 用户名或密码错误
     */
    ERROR_USERORPASSWORD(100002,"用户名或密码错误"),
    /**
     * 用户已停用
     */
    NOTVALID_USER(100012,"该用户已停用"),
    /**
     * 用户名错误
     */
    ISNULL_USERNAME(100003,"用户名不能为空"),
    /**
     * 密码错误
     */
    ISNULL_PASSWORD(100004,"密码不能为空"),
    /**
     * 逻辑错误
     */
    ERROR_LOGIC(100005,"逻辑错误:"),

    /**
     * 文件上传失败
     */
    ERROR_FILE_UPLOAD(100006,"文件上传失败:"),
    /**
     * 文件下载失败
     */
    ERROR_FILE_DOWNLOAD(100007,"文件下载失败:"),
    /**
     * token失效
     */
    INVALID_TOKEN(110001,"token失效"),
    /**
     * token过期
     */
    TIMEOUT_TOKEN(110011,"登录超时，请重新登录"),
    /**
     * token非法
     */
    ERROR_TOKEN(110021,"token非法"),

    TOKEN_EMPTY(110031, "登录令牌为空，请检查是否已经登录"),
    /**
     * 鉴权错误
     */
    INVALID_AUTHOR(110002,"鉴权错误"),
    /**
     * 鉴权错误
     */
    EXPIRES_AUTHOR(110003,"授权过期"),
    /**
     * 系统异常
     */
    EXCEPTION_SYS(120001,"系统异常--"),
    PARKID_EMPTY(130001,"登录园区为空,请联系管理员"),
    PARK_STATUS(130002,"园区被禁用"),
    USER_EMPTY(130003,"登录用户为空"),
    ORGID_EMPTY(120001,"请先创建公司"),

    ORG_STATUS(140002,"机构被禁用"),

    /**
     * 公司使用时间已过期
     */
    EXPIRE_DAY(150001,"公司使用时间已过期，到期后数据只保留15天，请联系我们");


    GatewayExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int code;

    private String message;

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}