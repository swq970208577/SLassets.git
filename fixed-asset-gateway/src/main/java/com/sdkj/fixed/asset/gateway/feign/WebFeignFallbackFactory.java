package com.sdkj.fixed.asset.gateway.feign;



import com.sdkj.fixed.asset.common.base.BaseResultVo;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class WebFeignFallbackFactory implements FallbackFactory<BaseResultVo>{
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public BaseResultVo create(Throwable arg0) {
		log.error("服务降级处理抛出异常信息",arg0);
		return BaseResultVo.failure();
	}
}