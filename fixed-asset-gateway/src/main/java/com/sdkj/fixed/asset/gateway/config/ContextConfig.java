package com.sdkj.fixed.asset.gateway.config;

//import com.sdkj.cloud.WitPark.gateway.core.exception.GatewayExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

/**
 * 网关全局配置
 */
@Configuration
public class ContextConfig {

    /**
     * 全局错误拦截器
     *
     * @author fengshuonan
     * @Date 2019/5/12 21:21
     */
 /*   @Bean
    public GatewayExceptionHandler witParkGatewayExceptionHandler() {
        return new GatewayExceptionHandler();
    }
*/
    /**
     * 跨域过滤器
     *
     * @author fengshuonan
     * @Date 2019-06-07 15:02
     */
    @Bean
    public CorsWebFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedMethod("*");
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
        source.registerCorsConfiguration("/**", config);

        return new CorsWebFilter(source);
    }

}

