/*
package com.sdkj.fixed.asset.gateway.core.exception;
package com.sdkj.cloud.WitPark.gateway.core.exception;

import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.bouncycastle.asn1.ocsp.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.core.annotation.Order;

import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebExceptionHandler;
import reactor.core.publisher.Mono;


*/
/*
*
 * 全局错误拦截器
 *
 * @author fengshuonan
 * @Date 2019/5/12 21:15
*//*



@Order(-200)
public class GatewayExceptionHandler implements ErrorWebExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GatewayExceptionHandler.class);

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {

        if(ex instanceof MethodArgumentNotValidException){
            MethodArgumentNotValidException exs = (MethodArgumentNotValidException) ex;
            BindingResult bindingResult = exs.getBindingResult();
            StringBuffer buffer = new StringBuffer();
            if (bindingResult.hasErrors()) {
                String errorMsg = bindingResult.getFieldError().getDefaultMessage();
                buffer.append(errorMsg);
            }
            return BaseResultVo.failure(buffer.toString());

            log.error(buffer.toString());
        }
        log.error("网关异常！", ex);


        //定义返回结果
        ResponseData responseData;

        //设置response的header
        ServerHttpResponse response = exchange.getResponse();

        //如果是Guns网关异常
        if (throwable instanceof GunsGatewayException) {
            GunsGatewayException gunsGatewayException = (GunsGatewayException) throwable;

            Integer code = gunsGatewayException.getCode();
            String errorMessage = gunsGatewayException.getErrorMessage();

            responseData = new ErrorResponseData(code, errorMessage);

            //设置响应状态码
            response.setStatusCode(HttpStatus.valueOf(code));

        } else if (throwable instanceof ApiServiceException) {

            //如果是接口调用异常
            ApiServiceException apiServiceException = (ApiServiceException) throwable;

            Integer code = apiServiceException.getCode();
            String errorMessage = apiServiceException.getErrorMessage();

            responseData = new ErrorResponseData(code, errorMessage);

            //设置响应状态码
            response.setStatusCode(HttpStatus.valueOf(code));

        } else {

            //如果是运行时异常，不可知的异常
            responseData = ResponseData.error(GATEWAY_ERROR.getMessage());

            //设置响应状态码
            response.setStatusCode(HttpStatus.valueOf(GATEWAY_ERROR.getCode()));
        }

        //设置headers
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");

        //渲染响应信息
        String resultString = JSON.toJSONString(responseData);
        byte[] bytes = (resultString).getBytes(StandardCharsets.UTF_8);
        DataBuffer wrap = exchange.getResponse().bufferFactory().wrap(bytes);

        return exchange.getResponse().writeWith(Flux.just(wrap));

       return null;
    }
}
*/
