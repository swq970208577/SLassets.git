package com.sdkj.fixed.asset.gateway.consumer;



import com.sdkj.fixed.asset.api.system.LogApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "fixed-asset-system")
public interface LogConsumer extends LogApi {

}
