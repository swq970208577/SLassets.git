package com.sdkj.fixed.asset.gateway.core.filters;

/**
 * @ClassName NoNeedCompany
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/14 15:44
 */
public enum NoNeedCompany {
    MENU("菜单","/system/auth/api/"),
    COMPANY("公司","/system/org/api/"),
    DIC_TYPE("数据字典项","/system/dicType/api"),
    LOGIN("数据字典","/login/login/api"),
    DIC("数据字典","/system/dic/api");
    private String url;
    private String mes;

    NoNeedCompany(String mes, String url) {
        this.url = url;
        this.mes = mes;
    }

    public String getUrl() {
        return url;
    }

    public String getMes() {
        return mes;
    }
}
