package com.sdkj.fixed.asset.gateway.core.consts;

/**
 * 常量
 */
public interface GatewayConstants {

    /**
     * 鉴权请求头名称
     */
    String AUTH_HEADER = "Authorization";
    /**
     * 请求号header标识
     */
    String REQUEST_NO_HEADER_NAME = "Request-No";

    /**
     * header中的spanId，传递规则：request header中传递本服务的id
     */
    String SPAN_ID_HEADER_NAME = "Span-Id";
    /**
     * 请求号过滤器的顺序
     */
    public static final int REQUEST_NO_ORDER = -100;

    /**
     * 添加auth头的过滤器的顺序
     */
    public static final int ADD_AUTH_HEADER_ORDER = -200;

}
