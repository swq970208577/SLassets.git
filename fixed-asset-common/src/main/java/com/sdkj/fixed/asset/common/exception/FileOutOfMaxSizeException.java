package com.sdkj.fixed.asset.common.exception;
/**
 * 超出最大支持文件大小
 * @author lzx
 *
 */
public class FileOutOfMaxSizeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3185738799938959542L;
	
	public FileOutOfMaxSizeException(String message) {
		super(message);
	}
}
