package com.sdkj.fixed.asset.common.cache;

import org.springframework.cache.Cache;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheWriter;

import java.util.concurrent.Callable;

/**
 * 
 * @ClassName: LocalCache
 * @Description: 
 * @author  wangyan
 * @date 2019年10月9日
 */
public class LocalCache extends RedisCache{
	private  Cache localCache;
	public LocalCache(String name, RedisCacheWriter cacheWriter, 
			RedisCacheConfiguration cacheConfig,Cache cache) {
		super(name, cacheWriter, cacheConfig);
		this.localCache=cache;
	}

	@Override
	public Cache.ValueWrapper get(Object key) {
		 Cache.ValueWrapper valueWrapper = localCache.get(key);
	        if (valueWrapper == null) {  
	            valueWrapper = super.get(key);  
	            if (valueWrapper != null) {  
	                localCache.put(key, valueWrapper.get());  
	            }  
	        }  
	        return valueWrapper; 
	}
	@Override
	public <T> T get(Object key, Class<T> type) {
		 T value = localCache.get(key, type);  
	        if (value == null) {  
	            value = super.get(key, type);  
	            if (value != null) {  
	                localCache.put(key, value);  
	            }  
	        }  
	        return value;  
	}
	@Override
	public <T> T get(Object key, Callable<T> valueLoader) {
		 T value = localCache.get(key, valueLoader);  
	        if (value == null) {  
	            //本地一级缓存不存在，读取redis二级缓存  
	            value = super.get(key, valueLoader);  
	            if (value != null) {  
	                //redis二级缓存存在，存入本地一级缓存  
	                localCache.put(key, value);  
	            }  
	        }  
	        return value;  
	}
	@Override
	public void put(Object key, Object value) {
		 super.put(key, value);
	     localCache.put(key, value);  
	}
	@Override
	public Cache.ValueWrapper putIfAbsent(Object key, Object value) {
		Cache.ValueWrapper vw1 = localCache.putIfAbsent(key, value);
        Cache.ValueWrapper vw2 = super.putIfAbsent(key, value);
        return vw1 == null ? vw2 : vw1;  
	}
	@Override
	public void evict(Object key) {
		localCache.evict(key);  
        super.evict(key);  
	}
	@Override
	public void clear() {
		 localCache.clear();  
	     super.clear();  
	}
}
