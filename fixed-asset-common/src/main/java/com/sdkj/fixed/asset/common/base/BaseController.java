package com.sdkj.fixed.asset.common.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 通用controller类，里面包含基本的增、删、改、查功能
 * @description:
 * @author:王艳
 */
public abstract class BaseController<T> {

	private static Logger log = LoggerFactory.getLogger(BaseController.class);
    
    @SuppressWarnings("rawtypes")
	public abstract BaseService getService();
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/getAll",method = RequestMethod.GET)
    @ResponseBody
    public BaseResultVo getAll(){
    	try {
    		List<T> list = getService().selectAll();
    		return BaseResultVo.success(list);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			return BaseResultVo.failure();
		}
    }
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/selectById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo selectById( String id)throws Exception{
    	try {
    		T simple = (T) getService().selectByPrimaryKey(id);
    		return BaseResultVo.success(simple);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			return BaseResultVo.failure();
		}
    }
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertSimple",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo insertSimple(@RequestBody T entity) throws Exception{
    	try {
    		getService().insertSelective(entity);
			return BaseResultVo.success();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			return BaseResultVo.failure();
		}
    }
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertUseGeneratedKeys",method = RequestMethod.POST)
	@ResponseBody
	public BaseResultVo insertUseGeneratedKeys(@RequestBody T entity) throws Exception{
		try {
			getService().insertUseGeneratedKeys(entity);
			return BaseResultVo.success();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			return BaseResultVo.failure();
		}
	}
    @RequestMapping(value = "/deleteById",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo deleteById(@RequestBody String id) throws Exception{
    	 try {
        	getService().deleteByPrimaryKey(id);
 			return BaseResultVo.success();
 		} catch (Exception e) {
 			log.error(e.getLocalizedMessage());
 			return BaseResultVo.failure();
 		}
    }

    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/updateById",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo updateById(@RequestBody T entity) throws Exception{
    	try {
        	getService().updateByPrimaryKey(entity);
 			return BaseResultVo.success();
 		} catch (Exception e) {
 			log.error(e.getLocalizedMessage());
 			return BaseResultVo.failure();
 		}
    }

}
