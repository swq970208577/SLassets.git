package com.sdkj.fixed.asset.common.base;

import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * 通用service，被其他service所继承
 * 
 * @description:
 * @author:王艳
 */

@Service
@Transactional(rollbackFor = Exception.class)
public abstract class BaseService<T> {

	@SuppressWarnings("rawtypes")
	public abstract BaseMapper getMapper();

	@SuppressWarnings("unchecked")
	public void insertList(List<T> recordList) {
		getMapper().insertList(recordList);
	}

	@SuppressWarnings("unchecked")
	public void insertUseGeneratedKeys(T record) throws Exception {
		getMapper().insertUseGeneratedKeys(record);
	}

	@SuppressWarnings("unchecked")
	public void insertSelective(T t) throws Exception {
		getMapper().insertSelective(t);
	}

	public void deleteByExample(Example example) throws Exception {
		getMapper().deleteByExample(example);
	}

	public void deleteByPrimaryKey(Object key) throws Exception {
		getMapper().deleteByPrimaryKey(key);
	}

	@SuppressWarnings("unchecked")
	public void updateByPrimaryKeySelective(T record) throws Exception {
		getMapper().updateByPrimaryKeySelective(record);
	}

	@SuppressWarnings("unchecked")
	public void updateByExampleSelective(T record, Example example) throws Exception {
		getMapper().updateByExampleSelective(record, example);
	}

	@SuppressWarnings("unchecked")
	public void updateByExample(T record, Example example) throws Exception {
		getMapper().updateByExample(record, example);
	}

	@SuppressWarnings("unchecked")
	public void updateByPrimaryKey(T t) throws Exception {
		getMapper().updateByPrimaryKey(t);
	}

	public int selectCountByExample(Example example) throws Exception {
		return getMapper().selectCountByExample(example);
	}
	@SuppressWarnings("unchecked")
	public int selectCount(T record) throws Exception {
		return getMapper().selectCount(record);
	}
	@SuppressWarnings("unchecked")
	public List<T> selectByExample(Example example) throws Exception {
		return getMapper().selectByExample(example);
	}
	@SuppressWarnings("unchecked")
	public T selectByPrimaryKey(Object key) throws Exception {
		return (T) getMapper().selectByPrimaryKey(key);
	}
	@SuppressWarnings("unchecked")
	public List<T> selectAll() throws Exception {
		return getMapper().selectAll();
	}
	public List<T> getPageList(Example example,int currentPage, int perPageTotal) throws Exception{
		PageHelper.startPage(currentPage, perPageTotal);
		return getMapper().selectByExample(example);
	}
}
