package com.sdkj.fixed.asset.common.exception;

public class LoginUserException extends RuntimeException {
	public LoginUserException(String message) {
		super(message);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 6482296763929242398L;

}
