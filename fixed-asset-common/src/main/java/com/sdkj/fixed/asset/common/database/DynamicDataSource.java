package com.sdkj.fixed.asset.common.database;


import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.io.Serializable;

/**
 * 动态数据源（需要继承AbstractRoutingDataSource）
 */
public class DynamicDataSource extends AbstractRoutingDataSource implements Serializable {
    protected Object determineCurrentLookupKey() {
        return DatabaseContextHolder.getDatabaseType();
    }
}