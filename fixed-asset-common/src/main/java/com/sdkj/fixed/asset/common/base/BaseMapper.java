package com.sdkj.fixed.asset.common.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 通用mapper，被其他mapper所继承 
 * @description:
 * @author:王艳
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {    
}