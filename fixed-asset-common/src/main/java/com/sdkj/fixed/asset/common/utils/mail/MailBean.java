package com.sdkj.fixed.asset.common.utils.mail;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author zhangjinfei
 * @Description //TODO 邮箱配置信息
 * @Date 2020/4/29 16:07
 */
public class MailBean {

    private String toEmail;      // 邮件接收人的邮件地址
    private String title;  // 发送邮件标题
    private String emailMessage; // 发送邮件内容
    private MailConfig mailConfig;

    public MailConfig getMailConfig() {
        return mailConfig;
    }

    public void setMailConfig(MailConfig mailConfig) {
        this.mailConfig = mailConfig;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

}
