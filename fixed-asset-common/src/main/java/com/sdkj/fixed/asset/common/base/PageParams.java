package com.sdkj.fixed.asset.common.base;



import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;


public class PageParams<T> {
    @Valid
    /**
     * 参数里有必填项时使用
     * zx
     */
    @NotNull(message = "参数不能为空",groups = ParamNotNullGroup.class)
	private T params; // 结果集
	private int currentPage;
	private int perPageTotal;

    public T getParams() {
        return params;
    }

    public void setParams(T params) {
        this.params = params;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPerPageTotal() {
        return perPageTotal;
    }

    public void setPerPageTotal(int perPageTotal) {
        this.perPageTotal = perPageTotal;
    }
    public interface ParamNotNullGroup extends Default {}
}