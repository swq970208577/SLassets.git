package com.sdkj.fixed.asset.common.utils;

import org.apache.commons.lang.RandomStringUtils;

import java.util.Random;

public class RandomUtils {
	/**
	 * 生成大小写字母加数字随机数
	 *
	 * @param length 生成的位数
	 */
	public static String randomLetAndNum(int length) {
		String val = "";
		java.util.Random random = new java.util.Random();
		for (int i = 0; i < length; i++) {//定义随机数位数
			// 输出字母还是数字
			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 字符串
			if ("char".equalsIgnoreCase(charOrNum)) {
				// 取得大写字母还是小写字母
				int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
				val += (char) (choice + random.nextInt(26));
			} else if ("num".equalsIgnoreCase(charOrNum)) { // 数字
				val += String.valueOf(random.nextInt(10));
			}
		}
		return val;
	}

	public static void main(String[] args) {
		System.out.println(RandomUtils.randomLetAndNum(8));
		System.out.println(RandomStringUtils.random(8, true, true));
		System.out.println(RandomStringUtils.random(4, false, true));
	}

}