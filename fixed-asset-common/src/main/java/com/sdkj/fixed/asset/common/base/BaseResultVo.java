package com.sdkj.fixed.asset.common.base;



import java.io.Serializable;


public class BaseResultVo<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer code;

	private String msg;

    private T data;
	public BaseResultVo(){
	}
    public BaseResultVo(Integer code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
    public BaseResultVo(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

    public static BaseResultVo success(String msg,Object obj){
    	return new BaseResultVo(0,msg,obj);
    }

    public static BaseResultVo success(){
		return new BaseResultVo(0, "success");
    }
    
    public static BaseResultVo success(Object obj){
    	return new BaseResultVo(0,"success",obj);
    }

    public static BaseResultVo failure(){
    	return new BaseResultVo(-1, "failure");
    }

    public static BaseResultVo failure(String msg){
    	return new BaseResultVo(-1, msg);
    }


	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
