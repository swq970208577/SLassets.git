package com.sdkj.fixed.asset.common.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author scx
 * @date 2020/3/23 16:34
 */
public class TimeTool {
    /**
     * 获得12位的日期编码
     *
     * @return String
     */
    public static String getTimeDate12() {
        // yyyyMMddhhmm
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = new Date();
        return sdf.format(date);
    }

    /**
     * 获得14位的日期编码
     */
    public static String getTimeDate14() {
        /* yyyyMMddhhmmss */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();

        return sdf.format(date);
    }

    /**
     * 获得19位当前日期编码
     */
    public static String getCurrentTimeStr() {
        /* yyyy-MM-dd HH:mm:ss */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return sdf.format(date);
    }

    /**
     * 获得当前时间戳
     *
     * @return Timestamp
     */
    public static Timestamp getCurrentTime() {
        Date date = new Date();
        Timestamp timeStamp = new Timestamp(date.getTime());
        return timeStamp;
    }

    /**
     * 解析日期
     *
     * @param dateStr  要解析的时间
     * @param yyyyMMdd 格式
     * @return Date
     * @throws ParseException sss
     */
    public static Date parseDate(String dateStr, String yyyyMMdd) {
        SimpleDateFormat df = (SimpleDateFormat) DateFormat.getDateInstance();
        df.applyPattern(yyyyMMdd);
        Date ddTest = null;
        try {
            ddTest = df.parse(dateStr);
        } catch (ParseException e) {
            try {
                throw e;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }
        return ddTest;
    }

    /**
     * 获得8位的日期编码 yyyyMMdd
     *
     * @return String
     */
    public static String getTimeDate8() {
        // yyyyMMdd
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return sdf.format(date);
    }
    /**
     * 获得10位的日期编码
     *
     * @return String
     */
    public static String getTimeDate10() {
        // yyyyMMddhhmm
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return sdf.format(date);
    }

    /**
     * 获得N天之前的时间
     *
     * @param index
     */
    public static String getBeforDate(Integer index) {
        Calendar calendar = Calendar.getInstance();
        // 得到前index天
        calendar.add(Calendar.DATE, index);
        Date date = calendar.getTime();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(date);
    }

    /**
     * 获得N月之前的时间
     *
     * @param index
     */
    public static String getBeforMonth(Integer index) {
        Calendar calendar = Calendar.getInstance();
        // 得到前index月
        calendar.add(Calendar.MONTH, index);
        Date date = calendar.getTime();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(date);
    }

    /**
     * 获取cron表达式
     *
     * @param date
     * @return
     */
    public static String getCron(Date date) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String formatTimeStr = null;
        if (date != null) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;

    }

    /**
     * 获取N天后的零点时间，
     *
     * @param past
     * @return
     */
    public static Date getFutureDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date futureDay = calendar.getTime();
        /*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String result = format.format(futureDay);*/
        return futureDay;
    }

    /**
     * 计算两个Date间的相差天数
     *
     * @param dateBefore
     *            早一点的时间
     * @param dateCurrent
     *            晚一点的时间
     * @return
     */
    public static int countDayMinus(Date dateBefore, Date dateCurrent) {
        // 当前时间
        Date curentTime = dateCurrent;
        // 当前时间
        Calendar curentC = Calendar.getInstance();
        curentC.setTime(curentTime);
        // 上次访问时间
        Calendar curentV = Calendar.getInstance();
        curentV.setTime(dateBefore);
        // 相差分钟数
        long timeMillis = (curentC.getTimeInMillis() - curentV.getTimeInMillis()) / (1000 * 60 * 60 * 24);
        int time = (int) timeMillis;
        return time;
    }
}
