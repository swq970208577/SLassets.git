package com.sdkj.fixed.asset.common.base;

import com.github.pagehelper.Page;

import java.util.List;

public class PageBean<T> {
	private long totalRecords; // 总记录数
	private int totalPages; // 总页数
	private int pageIndex; // 第几页
	private int pageSize; // 页面大小
	private List<T> data; // 结果集

	/**
	 * 包装Page对象，因为直接返回Page对象，在JSON处理以及 其他情况下会被当成List来处理而出现一些问题。
	 * 
	 * @param list
	 *            page结果
	 */
	public PageBean(List<T> list) {
		if (list instanceof Page) {
			Page<T> page = (Page<T>) list;
			this.data = page;
			this.totalRecords = page.getTotal();
			this.totalPages = page.getPages();
			this.pageIndex = page.getPageNum();
			this.pageSize = page.getPageSize();
		}
	}
	public PageBean(int code, String msg, List<T> list, int pageIndex, int pageSize) {
			this.data = list;
			this.totalRecords = (list==null?0:list.size()) ;
			this.totalPages = (int) Math.ceil(Double.valueOf(totalRecords)/pageSize);
			this.pageIndex = pageIndex;
			this.pageSize = pageSize;
	}

	public PageBean() {

	}


	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}