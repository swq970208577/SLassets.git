package com.sdkj.fixed.asset.common.cache;

import org.springframework.cache.Cache;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.util.Assert;

import java.time.Duration;

/**
 * 
 * @ClassName: LocalRedisCacheManager
 * @Description: 
 * @author  wangyan
 * @date 2019年10月8日
 */
public class LocalRedisCacheManager extends RedisCacheManager {  
  
	private RedisCacheWriter redisCacheWriter;
    private  Cache localCache;  
    private  RedisCacheConfiguration redisCacheConfiguration;
    public LocalRedisCacheManager(RedisCacheWriter cacheWriter,Cache localCache, long seconds) {  
    	super(cacheWriter,RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(seconds)));  
        this.redisCacheWriter=cacheWriter;
        this.localCache = localCache;  
        this.redisCacheConfiguration=RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(seconds));
         check();  
    }  
    public RedisCache createRedisCache() {  
        return new LocalCache(localCache.getName(), redisCacheWriter, redisCacheConfiguration , localCache);  
    }  
    private void check() {  
        Assert.notNull(localCache, "localCache must not be null");  
    }    
}  