package com.sdkj.fixed.asset.common.exception;
/**
 * 逻辑错误
 * @author zhangxin
 *
 */
public class LogicException extends RuntimeException {

	
	private static final long serialVersionUID = -3185738799938959542L;

	public LogicException(String message) {
		super(message);
	}
}
