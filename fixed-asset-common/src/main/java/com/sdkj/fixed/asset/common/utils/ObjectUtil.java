package com.sdkj.fixed.asset.common.utils;

import java.lang.reflect.Field;

import org.apache.commons.lang3.StringUtils;

/**
 * ObjectUtil
 *
 * @author shichenxing
 * @Description
 * @date 2020/11/27 13:40
 */
public class ObjectUtil {
    /**
     * 判断对象中属性值是否全为空
     *
     * @param object
     * @return
     */
    public static boolean checkObjAllFieldsIsNull(Object object) {
        if (null == object) {
            return true;
        }

        try {
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);
//                System.out.print(f.getName() + ":");
//                System.out.println(f.get(object));
                if (!f.getName().equals("rowNum") && !f.getName().equals("errorMsg")) {
                    if (f.get(object) != null && StringUtils.isNotBlank(f.get(object).toString())) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}

