package com.sdkj.fixed.asset.common.utils.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * MailConfig
 *
 * @author shichenxing
 * @Description
 * @date 2020/9/10 15:01
 */
@Component
public class MailConfig{

    @Value("${email.sendemail}")
    private  String sendEmail;  // 邮件发送人的邮件地址
    @Value("${email.password}")
    private  String password;   //发件人的邮件授权码
    @Value("${email.sendserver}")
    private  String sendServer; // 发件服务器
    @Value("${email.port}")
    private  int port; // 端口


    public String getSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(String sendEmail) {
        this.sendEmail = sendEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSendServer() {
        return sendServer;
    }

    public void setSendServer(String sendServer) {
        this.sendServer = sendServer;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
