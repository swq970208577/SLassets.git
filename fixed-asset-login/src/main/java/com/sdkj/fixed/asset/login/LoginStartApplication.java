package com.sdkj.fixed.asset.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 登录启动入口
 * @author 张欣
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class LoginStartApplication extends SpringBootServletInitializer {
    private static final Logger log = LoggerFactory.getLogger(LoginStartApplication.class);
    public static ConfigurableApplicationContext ac;
    public static void main(String[] args) {
        LoginStartApplication.ac = SpringApplication.run(LoginStartApplication.class, args);
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(LoginStartApplication.class);
    }
}
