package com.sdkj.fixed.asset.login.cache;



import com.sdkj.fixed.asset.login.service.LoginService;
import com.sdkj.fixed.asset.login.util.RedisUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupRunner implements CommandLineRunner {
    @Autowired
    private RedisUtil redis;
    @Autowired
    private LoginService loginService;
    private static final Logger log = LoggerFactory.getLogger(StartupRunner.class);
    @Override
    public void run(String... args) throws Exception {
        log.info("加载数据字典");
        loginService.dicInCache();
    }
}
