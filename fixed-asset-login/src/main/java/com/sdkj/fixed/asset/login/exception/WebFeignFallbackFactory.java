package com.sdkj.fixed.asset.login.exception;


import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;

@Component
public class WebFeignFallbackFactory implements FallbackFactory<BaseResultVo>{

	@Override
	public BaseResultVo create(Throwable arg0) {
		System.out.print(arg0.getStackTrace());
		return BaseResultVo.failure("服务降级处理异常抛出");
	}
}