package com.sdkj.fixed.asset.login.mapper;


import com.sdkj.fixed.asset.api.login.out_vo.CompanyBaseListResult;
import com.sdkj.fixed.asset.api.login.out_vo.MenuResultEntity;
import com.sdkj.fixed.asset.api.login.pojo.LoginRole;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.DictionaryEntity;
import com.sdkj.fixed.asset.pojo.system.DictionaryType;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface LoginMapper extends BaseMapper<UserManagement> {
   /**
     * 查询用户所属公司信息
     * @param userId
     * @return
     */
    public List<CompanyBaseListResult> getUserCompany(String userId);
    /**
     * 查询用户所属公司信息
     * @param orgId
     * @return
     */
    public List<CompanyBaseListResult> getUserRootCompany(String orgId);

/**
     * 获取用户角色信息
     * @param userId
     * @return
     */
    public List<LoginRole> getUserRoles(String userId);

    /**
     * 获取登录用户菜单
     * @param roleIds
     * @return
     */
    public List<MenuResultEntity> getAuthMenus(List<String> roleIds);
    /**
     * 获取菜单
     * @param menuIds
     * @return
     */
    public List<MenuResultEntity> getMenusByMenuId(Set<String> menuIds);
    /**
     * 获取所有菜单
     * @return
     */
    public List<MenuResultEntity> getAllMenus();


    /**
     * 获取所有园区,app使用
     * @return
     */
    public List<CompanyBaseListResult> getAllCompanys();


    public String getAllParentId(String id);

    List<DictionaryType> dicInCache()throws Exception;
    public List<DictionaryEntity> getAllDicEntity();

    /**
     * 获取公司使用情况
     * @param companyId
     * @return
     */
    public UsageManagement getUsage(String companyId);
}
