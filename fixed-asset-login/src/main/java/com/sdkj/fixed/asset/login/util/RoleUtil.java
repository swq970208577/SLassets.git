package com.sdkj.fixed.asset.login.util;


import com.sdkj.fixed.asset.api.login.pojo.LoginRole;
import com.sdkj.fixed.asset.api.login.pojo.LoginUserBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 登录信息缓冲工具类
 * @author 张欣
 */
@Component
public class RoleUtil {
	@Autowired
	private RedisUtil redis;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	/**
	 * 获取当前登录角色
	 * @param token
	 * @return
	 */
	public  List<LoginRole> getCurRoles(String token)  {

		LoginUserBase loginUser = (LoginUserBase)redis.get(jwtTokenUtil.getUserIdFromToken(token));
		if(loginUser == null){
			return new ArrayList<>();
		}
		return loginUser.getLoginRoles();
	}

	/**
	 * 获取当前登录用户角色某园区或机构的等级信息
	 * @param token
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public  List<String> getCurOrgRoleLevels(String token,String orgId)  {

		List<LoginRole> loginRoles =getCurRoles(token);
		List<String> level = new ArrayList<>();
	/*	for(LoginRole roles : loginRoles){
            if(roles.getOrgId().equals(orgId)){
				if(!level.contains(roles.getRoleLevel())){
					level.add(roles.getRoleLevel());
				}
			}
		}*/
		return level;
	}
	public  List<String> getCurOrgRoleLevels(String token)  {

		List<LoginRole> loginRoles =getCurRoles(token);
		List<String> level = new ArrayList<>();
		for(LoginRole roles : loginRoles){
			if(!level.contains(roles.getRoleLevel())){
				level.add(roles.getRoleLevel());
			}
		}
		return level;
	}
	/**
	 * 获取当前登录用户角色某园区或机构的id信息
	 * @param token
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public  List<String> getCurOrgRoleIds(String token,String orgId)  {
		List<LoginRole> loginRoles =getCurRoles(token);
		List<String> level = new ArrayList<>();
		List<String> roleIds = new ArrayList<>();
		for(LoginRole roles : loginRoles){
		/*	if(roles.getOrgId().equals(orgId)){
				if(!roleIds.contains(roles.getRoleId())){
					roleIds.add(roles.getRoleId());
				}
			}*/
		}
		return roleIds;
	}
	public  Boolean isSysadmin(String token){
		List<String> levels = getCurOrgRoleLevels(token);
		if(levels != null && levels.contains("ROLE_LEVEL_3")){
			return true;
		}
		return false;
	}
	public  Boolean isSysadmin(String token,String orgId){
		List<String> levels = getCurOrgRoleLevels(token,orgId);
		if(levels != null && levels.contains("ROLE_LEVEL_3")){
			return true;
		}
		return false;
	}
}
