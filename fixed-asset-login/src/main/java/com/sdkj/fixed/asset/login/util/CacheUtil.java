package com.sdkj.fixed.asset.login.util;

import com.sdkj.fixed.asset.api.login.pojo.LoginRole;
import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.common.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName CacheUtile
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 16:42
 */
@Component
public class CacheUtil {
    @Autowired
    private RedisUtil redisUtil;
    public LoginUser getUser(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        return (LoginUser) redisUtil.get(userId);
    }

    public String getUserId(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        return loginUser.getUserId();
    }

    public String getUserCompanyId(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        return loginUser.getCompanys().get(0).getCompanyId();
    }

    public List<String> getUserRole(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        List<LoginRole> loginRoles = loginUser.getLoginRoles();
        List<String> roles = loginRoles.stream().map(LoginRole::getRoleLevel).collect(Collectors.toList());
        return roles;
    }
    public List<String> getUserRoleId(String token){
        String userId = JwtTokenUtil.getUserIdFromToken(token);
        LoginUser loginUser = (LoginUser) redisUtil.get(userId);
        List<LoginRole> loginRoles = loginUser.getLoginRoles();
        List<String> roles = loginRoles.stream().map(LoginRole::getRoleId).collect(Collectors.toList());
        return roles;
    }
}
