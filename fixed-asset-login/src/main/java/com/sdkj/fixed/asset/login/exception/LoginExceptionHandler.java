package com.sdkj.fixed.asset.login.exception;



import com.sdkj.fixed.asset.common.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class LoginExceptionHandler extends GlobalExceptionHandler {

}