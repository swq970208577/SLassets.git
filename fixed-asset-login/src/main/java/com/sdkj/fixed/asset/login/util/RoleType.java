package com.sdkj.fixed.asset.login.util;

/**
 * @ClassName RoleType
 * @Description 角色类型
 * @Author 张欣
 * @Date 2020/7/20 9:34
 */
public enum RoleType {
    /**
     * 超级管理员
     */
    SYSADMIN("1"),
    /**
     * 公司管理员
     */
    ADMIN("2"),
    /**
     * 普通用户
     */
    ORDINARY_USER("3");
    private String roleType;
    RoleType(String roleType) {
        this.roleType = roleType ;
    }

    public String getRoleType() {
        return roleType;
    }
}
