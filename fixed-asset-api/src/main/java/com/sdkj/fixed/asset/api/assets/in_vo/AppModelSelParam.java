package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/6 17:13
 */
public class AppModelSelParam {

    private String id;
    private String search;
    private String state;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
