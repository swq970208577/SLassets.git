package com.sdkj.fixed.asset.api.system;


import com.sdkj.fixed.asset.api.system.in_vo.UserBindRole;
import com.sdkj.fixed.asset.api.system.in_vo.UserConditionEntity;
import com.sdkj.fixed.asset.api.system.in_vo.UserDataAuth;
import com.sdkj.fixed.asset.api.system.in_vo.UserEditPassword;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName OrgApi
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/20 14:40
 */
@RequestMapping({"/system/user/api"})
public interface UserApi {
    /**
     * 设置为管理员
     * @param userManagement
     * @return
     */
    @RequestMapping(value = "/setUserAdmin", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo setUserAdmin(@RequestBody UserManagement userManagement) throws Exception;
    /**
     * 禁用或启用员工
     * @param userManagement
     * @return
     */
    @RequestMapping(value = "/forbiddenOrEnable", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo forbiddenOrEnable(@RequestBody UserManagement userManagement) throws Exception;
    /**
     * 删除员工
     * @param userManagement
     * @return
     */
    @RequestMapping(value = "/deleteOrRecoverUser", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo deleteOrRecoverUser(@RequestBody UserManagement userManagement) throws Exception;
    /**
     * 查询已绑定的角色
     * @param userManagement
     * @return
     */
    @RequestMapping(value = "/queryHasRole", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryHasRole(@RequestBody UserManagement userManagement) throws Exception;
    /**
     * 绑定角色
     * @param userBindRole
     * @return
     */
    @RequestMapping(value = "/bindRole", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo bindRole(@RequestBody UserBindRole userBindRole) throws Exception;
    /**
     * 绑定角色
     * @param userDataAuth
     * @return
     */
    @RequestMapping(value = "/bindDataAuth", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo bindDataAuth(@RequestBody UserDataAuth userDataAuth) throws Exception;
    /**
     * 查看已有的数据权限
     * @param userManagement
     * @return
     */
    @RequestMapping(value = "/queryHasDataAuth", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryHasDataAuth(@RequestBody UserManagement userManagement) throws Exception;
    /**
     * 修改密码
     * @param userEditPassword
     * @return
     */
    @RequestMapping(value = "/editPassword", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo editPassword(@RequestBody UserEditPassword userEditPassword) throws Exception;
    /**
     * 获取员工详情
     * @param userManagement
     * @return
     */
    @RequestMapping(value = "/queryUserInfo", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryUserInfo(@RequestBody UserManagement userManagement) throws Exception;
    /**
     * 查询所有启用的系统用户
     *
     * @return
     */
    @RequestMapping(value = "/queryAllEnableUser", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryAllEnableUser(String companyId) throws Exception;

    /**
     * 分页查询用户
     *
     * @return
     */
    @RequestMapping(value = "/getPageUser", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPageUser(@RequestBody PageParams<UserConditionEntity> params)  throws Exception;
    /**
     * 下载导入人员模板
     * @throws Exception
     */
    @RequestMapping(value = "/downUserTemp", method = RequestMethod.GET)
    public void downUserTemp(HttpServletResponse response)  throws Exception;
    /**
     *导入
     */
    @RequestMapping(value = "/importUser", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo importUser(MultipartFile file)  throws Exception;
    /**
     *导出
     */
    @RequestMapping(value = "/exportUser", method = RequestMethod.GET)
    public void exportUser(String companyId,HttpServletResponse response)  throws Exception;

    /**
     *查询所有资产启用状态分类
     */
    @RequestMapping(value = "/getAllEnableAssetClass", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAllEnableAssetClass()  throws Exception;
    /**
     *查询所有启用状态的区域分类
     */
    @RequestMapping(value = "/getAllEnableArea", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAllEnableArea()  throws Exception;

}
