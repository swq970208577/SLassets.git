package com.sdkj.fixed.asset.api.login.out_vo;


import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * PC登录返参
 * @author 张欣
 */
public class LoginUserResultPC extends LoginUser implements Serializable {


}
