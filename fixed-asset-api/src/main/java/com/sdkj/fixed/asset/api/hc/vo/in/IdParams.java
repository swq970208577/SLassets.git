package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * IdParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/4 11:11
 */
public class IdParams {
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空")
    private String id;

    private String cuser;

    private String orgId;

    private String euser;

    private String etime;

    private String ctime;
    /**
     * 状态（1审批中2已批准3已驳回4已签收）
     */
    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getEuser() {
        return euser;
    }

    public void setEuser(String euser) {
        this.euser = euser;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
