package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/13 10:30
 */
public class SweepGunInvParam {
    @NotBlank(message = "盘点单不能为空")
    @Size(max = 32, message = "盘点单超长")
    private String inventoryId;
    @NotEmpty(message = "盘点资产不能为空")
    private List<String> assetCodeList;

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public List<String> getAssetCodeList() {
        return assetCodeList;
    }

    public void setAssetCodeList(List<String> assetCodeList) {
        this.assetCodeList = assetCodeList;
    }
}
