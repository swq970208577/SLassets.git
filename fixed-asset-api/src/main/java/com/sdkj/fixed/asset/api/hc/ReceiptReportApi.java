package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.Category;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * @author 史晨星
 * @ClassName: ReceiptReportApi
 * @Description: 数据报表
 * @date 2020年7月21日
 */
@RequestMapping("/hc/report/api")
public interface ReceiptReportApi {


}

