package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.Withdrawal;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO
 * @Date 2020/8/14 0:19
 */
public class WithdrawalApplyResult extends Withdrawal {

    /**
     * 审批
     */
    private List<AssetWithdrawalApply> applylist;
    /**
     * 退库明细
     */
    private List<WithdrawalResult> resultList;

    public List<AssetWithdrawalApply> getApplylist() {
        return applylist;
    }

    public void setApplylist(List<AssetWithdrawalApply> applylist) {
        this.applylist = applylist;
    }

    public List<WithdrawalResult> getResultList() {
        return resultList;
    }

    public void setResultList(List<WithdrawalResult> resultList) {
        this.resultList = resultList;
    }
}
