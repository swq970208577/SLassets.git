package com.sdkj.fixed.asset.api.system;


import com.sdkj.fixed.asset.api.system.in_vo.BindAuthParamEntity;
import com.sdkj.fixed.asset.api.system.in_vo.RoleBindUser;
import com.sdkj.fixed.asset.api.system.in_vo.RoleCondition;
import com.sdkj.fixed.asset.api.system.out_vo.AuthTreeResultEntity;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.RoleManagement;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @ClassName RoleApi
 * @Description 角色管理
 * @Author 张欣
 * @Date 2020/7/20 14:40
 */
@RequestMapping({"/system/role/api"})
public interface RoleApi {
    /**
     * 角色分配用户
     * @param roleBindUser
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/bindUser", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo  bindUser(@RequestBody RoleBindUser roleBindUser) throws Exception;
    /**
     * 查询角色已分配用户
     * @param roleId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/queryHasBindUser", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo  queryHasBindUser(@RequestBody RoleManagement roleId) throws Exception;
    /**
     * 获取菜单树,用于角色设置权限
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/getAuthTree",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo<List<AuthTreeResultEntity>> getAuthTree(@RequestBody RoleManagement roleId) throws Exception;
    /**
     * 分配权限
     * @return
     */
    @RequestMapping(value = "/blindAuth",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo blindAuth(@RequestBody BindAuthParamEntity roleAuth) throws Exception;
    /**
     * 分页查询公司角色
     * @param params
     * @return
     */
    @RequestMapping(value = "/getPages",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo<PageBean<RoleManagement>> getPages(@RequestBody PageParams<RoleCondition> params) throws Exception;
    /**
     * 查询所有角色
     * @param params
     * @return
     */
    @RequestMapping(value = "/getAllRole",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo<PageBean<RoleManagement>> getAllRole(@RequestBody RoleCondition params) throws Exception ;

    }
