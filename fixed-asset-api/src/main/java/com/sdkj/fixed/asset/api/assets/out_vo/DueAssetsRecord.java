package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 到期资产记录
 * @Date 2020/8/12 9:47
 */
public class DueAssetsRecord {
    /**
     * 资产ID
     */
    private String assetId;
    /**
     * 资产条码
     */
    private String assetCode;
    /**
     * 资产名称
     */
    private String assetName;
    /**
     * 资产类别名称
     */
    private String className;

    /**
     * 规格型号
     */
    private String specificationModel;
    /**
     * 购入时间
     */
    private String purchaseTime;
    /**
     * 清理时间
     */
    private String clearTime;


    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getClearTime() {
        return clearTime;
    }

    public void setClearTime(String clearTime) {
        this.clearTime = clearTime;
    }
}
