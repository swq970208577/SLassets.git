package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * ReceiptInGetPage
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/21 14:49
 */
public class ReceiptOutGetPage {

    private Integer sign;

    private String id;
    /**
     * 单据类型
     */
    private Integer type;
    /**
     * 出库单号
     */
    private String number;
    /**
     * 仓库id
     */
    private String categoryId;
    /**
     * 仓库名称
     */
    private String categoryName;
    /**
     * 出库日期
     */
    private String bussinessDate;
    /**
     * 领用公司id
     */
    private String receiveOrg;
    /**
     * 领用公司名称
     */
    private String receiveOrgName;
    /**
     * 领用部门id
     */
    private String receiveDept;
    /**
     * 领用部门名称
     */
    private String receiveDeptName;
    /**
     * 领用人
     */
    private String receiveUser;
    /**
     * 领用人名称
     */
    private String receiveUserName;
    /**
     * 创建人id
     */
    private String cuser;
    /**
     * 创建人名称
     */
    private String cuserName;
    /**
     * 创建时间
     */
    private String ctime;
    /**
     * 备注
     */
    private String comment;
    /**
     * 物品编码
     */
    private String pCode;
    /**
     * 物品名称
     */
    private String pName;
    /**
     * 商品条码
     */
    private String pBarCode;
    /**
     * 规格型号
     */
    private String model;
    /**
     * 单位
     */
    private String unit;
    /**
     * 库存最小
     */
    private Integer safeMin;
    /**
     * 库存最大
     */
    private Integer safeMax;
    /**
     * 入库数量
     */
    private Integer num;
    /**
     * 入库单价
     */
    private String price;
    /**
     * 入库总价
     */
    private String totalPrice;

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpBarCode() {
        return pBarCode;
    }

    public void setpBarCode(String pBarCode) {
        this.pBarCode = pBarCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getSafeMin() {
        return safeMin;
    }

    public void setSafeMin(Integer safeMin) {
        this.safeMin = safeMin;
    }

    public Integer getSafeMax() {
        return safeMax;
    }

    public void setSafeMax(Integer safeMax) {
        this.safeMax = safeMax;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getReceiveOrg() {
        return receiveOrg;
    }

    public void setReceiveOrg(String receiveOrg) {
        this.receiveOrg = receiveOrg;
    }

    public String getReceiveOrgName() {
        return receiveOrgName;
    }

    public void setReceiveOrgName(String receiveOrgName) {
        this.receiveOrgName = receiveOrgName;
    }

    public String getReceiveDept() {
        return receiveDept;
    }

    public void setReceiveDept(String receiveDept) {
        this.receiveDept = receiveDept;
    }

    public String getReceiveDeptName() {
        return receiveDeptName;
    }

    public void setReceiveDeptName(String receiveDeptName) {
        this.receiveDeptName = receiveDeptName;
    }

    public String getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getReceiveUserName() {
        return receiveUserName;
    }

    public void setReceiveUserName(String receiveUserName) {
        this.receiveUserName = receiveUserName;
    }
}
