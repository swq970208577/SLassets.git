package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetSupplier;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author zhaozheyu
 * @Description //我的审批任务统计反参
 * @Date 2020/7/23 16:30
 */
public class Assnum implements Serializable {
    @ApiModelProperty(value = "资产领用数量")
    private Integer usenumber;
    @ApiModelProperty(value = "资产借用数量")
    private Integer borrownumber;
    @ApiModelProperty(value = "资产退还数量")
    private Integer returnnumber;
    @ApiModelProperty(value = "物品领用数量")
    private Integer collectnumber;
    @ApiModelProperty(value = "待审批数量")
    private Integer approvalnumber;


    @ApiModelProperty(value = "物品签字状态")
    private Integer confirm;

    @ApiModelProperty(value = "")
    private Integer number;


    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getUsenumber() {
        return usenumber;
    }

    public void setUsenumber(Integer usenumber) {
        this.usenumber = usenumber;
    }

    public Integer getBorrownumber() {
        return borrownumber;
    }

    public void setBorrownumber(Integer borrownumber) {
        this.borrownumber = borrownumber;
    }

    public Integer getReturnnumber() {
        return returnnumber;
    }

    public void setReturnnumber(Integer returnnumber) {
        this.returnnumber = returnnumber;
    }

    public Integer getCollectnumber() {
        return collectnumber;
    }

    public void setCollectnumber(Integer collectnumber) {
        this.collectnumber = collectnumber;
    }

    public Integer getApprovalnumber() {
        return approvalnumber;
    }

    public void setApprovalnumber(Integer approvalnumber) {
        this.approvalnumber = approvalnumber;
    }
}
