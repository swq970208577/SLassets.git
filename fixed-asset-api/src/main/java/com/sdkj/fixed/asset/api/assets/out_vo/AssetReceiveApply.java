package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetReceive;

/**
 * @Author zhangjinfei
 * @Description //TODO
 * @Date 2020/8/13 16:09
 */
public class AssetReceiveApply extends AssetReceive {

    private String assetName;
    private String assetCode;
    private String assetModel;
    private String unitMeasurement;
    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(String assetModel) {
        this.assetModel = assetModel;
    }

    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }
}
