package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.AppModelSelParam;
import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.SetEmployeeSelf;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.SetArea;
import com.sdkj.fixed.asset.pojo.assets.SetClass;
import com.sdkj.fixed.asset.pojo.assets.SetSupplier;
import com.sdkj.fixed.asset.pojo.assets.StandModel;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author niuliwei
 * @description 资产分类设置
 * @date 2020/7/21 9:03
 */
@RequestMapping("/assets/set/api")
public interface SetApi {

    /**
     * 资产分类-查询启用状态数据
     * @return
     */
    @RequestMapping(value = "/getAllEnClass", method = RequestMethod.POST)
    public BaseResultVo getAllEnClass();

    /**
     * 资产分类-列表
     * @return
     */
    @RequestMapping(value = "/getAllClass", method = RequestMethod.POST)
    public BaseResultVo getAllClass();

    /**
     * 分页查询资产分类
     * @param params
     * @return
     */
    @RequestMapping(value = "/getAllClassPage", method = RequestMethod.POST)
    public BaseResultVo getAllClassPage(@RequestBody PageParams<Params> params);
    /**
     * 新增顶级和同级分类
     * @param setClass
     * @return
     */
    @RequestMapping(value = "/addClass", method = RequestMethod.POST)
    public BaseResultVo addClass(@Validated @RequestBody SetClass setClass);

    /**
     * 新增下级分类
     * @param setClass
     * @return
     */
    @RequestMapping(value = "/addLowerClass", method = RequestMethod.POST)
    public BaseResultVo addLowerClass(@Validated @RequestBody SetClass setClass);

    /**
     * 根据id编辑资产分类
     * @param setClass
     * @return
     */
    @RequestMapping(value = "/updClass", method = RequestMethod.POST)
    public BaseResultVo updClass(@Validated @RequestBody SetClass setClass);

    /**
     *
     * 删除资产分类
     * @param params
     * @return
     */
    @RequestMapping(value = "/delClassById", method = RequestMethod.POST)
    public BaseResultVo delClassById(@Validated @RequestBody Params params);

    /**
     *
     * 根据主键id启用、禁用资产分类
     * 禁用分类及所有子类
     * 启用当前分类
     * @param setClass
     * @return
     */
    @RequestMapping(value = "/classEnOrdisable", method = RequestMethod.POST)
    public BaseResultVo classEnOrdisable(@RequestBody SetClass setClass);

    /**
     *
     * 根据主键id查询资产分类
     * @param params
     * @return
     */
    @RequestMapping(value = "/selClassById", method = RequestMethod.POST)
    public BaseResultVo selClassById(@Validated @RequestBody Params params);


    /**
     * 获取分类树
     * @return
     */
    @RequestMapping(value = "/getClassTree", method = RequestMethod.POST)
    public BaseResultVo getClassTree();

    /**
     * 查询指定分类查询其下及子类下的标准类型
     * @param params
     * @return
     */
    @RequestMapping(value = "/getAllStandBySetId", method = RequestMethod.POST)
    public BaseResultVo getAllStandBySetId(@RequestBody AppModelSelParam params);



    /**
     * 根据分类id分页查
     * 询标准类型
     * @param params
     * @return
     */
    @RequestMapping(value = "/getStandPageBySetId", method = RequestMethod.POST)
    public BaseResultVo getStandPageBySetId(@Validated @RequestBody PageParams<Params> params);


    /**
     *
     * 新增资产型号
     * @param standModel
     * @return
     */
    @RequestMapping(value = "/addStandModel", method = RequestMethod.POST)
    public BaseResultVo addStandModel(@Validated @RequestBody StandModel standModel);

    /**
     *
     * 删除资产型号
     * @param params
     * @return
     */
    @RequestMapping(value = "/delStandModelById", method = RequestMethod.POST)
    public BaseResultVo delStandModelById(@Validated @RequestBody Params params);

    /**
     *
     * 根据主键查询资产标准型号
     * @param params
     * @return
     */
    @RequestMapping(value = "/selStandModelById", method = RequestMethod.POST)
    public BaseResultVo selStandModelById(@Validated @RequestBody Params params);

    /**
     *
     * 根据主键id启用、禁用资产型号
     * @param standModel
     * @return
     */
    @RequestMapping(value = "/standModelEnOrdisable", method = RequestMethod.POST)
    public BaseResultVo standModelEnOrdisable(@RequestBody StandModel standModel);

    /**
     *
     * 根据主键id编辑资产型号
     * @param standModel
     * @return
     */
    @RequestMapping(value = "/updStandModel", method = RequestMethod.POST)
    public BaseResultVo updStandModel(@Validated @RequestBody StandModel standModel);

    /**
     * 区域-新增
     * @param area
     * @return
     */
    @RequestMapping(value = "/addArea", method = RequestMethod.POST)
    public BaseResultVo addArea(@Validated @RequestBody SetArea area);

    /**
     * 区域-启用或禁用
     * @param area
     * @return
     */
    @RequestMapping(value = "/enOrDisArea", method = RequestMethod.POST)
    public BaseResultVo enOrDisArea(@RequestBody SetArea area);

    /**
     * 区域-编辑
     * @param area
     * @return
     */
    @RequestMapping(value = "/updArea", method = RequestMethod.POST)
    public BaseResultVo updArea(@Validated @RequestBody SetArea area);

    /**
     * 区域-删除
     * @param params
     * @return
     */
    @RequestMapping(value = "/delArea", method = RequestMethod.POST)
    public BaseResultVo delArea(@Validated @RequestBody Params params);

    /**
     * 区域-分页查询
     * @param pageParams
     * @return
     */
    @RequestMapping(value = "/getAllPageArea", method = RequestMethod.POST)
    public BaseResultVo getAllPageArea(@RequestBody PageParams<SetArea> pageParams);

    /**
     * 区域-启用列表
     * @return
     */
    @RequestMapping(value = "/getAllArea", method = RequestMethod.POST)
    public BaseResultVo getAllArea();


    /**
     * 供应商管理-新增
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/addSupplier", method = RequestMethod.POST)
    public BaseResultVo addSupplier(@Validated @RequestBody SetSupplier supplier);

    /**
     * 供应商管理-启用或禁用
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/enOrDisSupplier", method = RequestMethod.POST)
    public BaseResultVo enOrDisSupplier(@RequestBody SetSupplier supplier);

    /**
     * 供应商管理-编辑
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/updSupplier", method = RequestMethod.POST)
    public BaseResultVo updSupplier(@Validated @RequestBody SetSupplier supplier);

    /**
     * 供应商管理-删除
     * @param params
     * @return
     */
    @RequestMapping(value = "/delSupplier", method = RequestMethod.POST)
    public BaseResultVo delSupplier(@Validated @RequestBody Params params);

    /**
     * 供应商管理-分页查询
     * @param pageParams
     * @return
     */
    @RequestMapping(value = "/getAllPageSupplier", method = RequestMethod.POST)
    public BaseResultVo getAllPageSupplier(@RequestBody PageParams<SetSupplier> pageParams);

    /**
     * 供应商-启用列表
     * @return
     */
    @RequestMapping(value = "/getAllSupplier", method = RequestMethod.POST)
    public BaseResultVo getAllSupplier();

    /**
     * 员工自助-设置
     * 0选中，1取消
     * type 1.领用、借用 2.交接
     * 如果type=1,根据所有数据receiveBorrow=1，再更新ids数据等于0
     * 如果type=2,根据所有数据handover=1，再更新ids数据等于0
     */
    @RequestMapping(value = "/setEmployeeSelf", method = RequestMethod.POST)
    public BaseResultVo setEmployeeSelf(@Validated @RequestBody SetEmployeeSelf ss);

    /**
     * 员工自助-删除
     * @param ss
     */
    @RequestMapping(value = "/delEmployeeSelf", method = RequestMethod.POST)
    public BaseResultVo delEmployeeSelf(@Validated @RequestBody SetEmployeeSelf ss);
    /**
     * 员工自助-查询
     */
    @RequestMapping(value = "/selEmployeeSelf", method = RequestMethod.POST)
    public BaseResultVo selEmployeeSelf();

}
