package com.sdkj.fixed.asset.api.assets.in_vo;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 分页查询入参
 * @Date 2020/7/21 13:25
 */
public class WarehousePagesParam extends SearchNameParam{

    /**
     * 公司或部门ID
     */
    private String companyDeptId;
    private List<String> companyList;
    private List<String> classList;
    private List<String> areaList;
    private String userId;
    private String orgId;
    private String treeCode;
    private String companyId;
    private String state;
    private String adminId;

    private List<String> useCompanyDeptList;
    private String purchaseTimeStart;
    private String purchaseTimeEnd;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getPurchaseTimeStart() {
        return purchaseTimeStart;
    }

    public void setPurchaseTimeStart(String purchaseTimeStart) {
        this.purchaseTimeStart = purchaseTimeStart;
    }

    public String getPurchaseTimeEnd() {
        return purchaseTimeEnd;
    }

    public void setPurchaseTimeEnd(String purchaseTimeEnd) {
        this.purchaseTimeEnd = purchaseTimeEnd;
    }

    public List<String> getUseCompanyDeptList() {
        return useCompanyDeptList;
    }

    public void setUseCompanyDeptList(List<String> useCompanyDeptList) {
        this.useCompanyDeptList = useCompanyDeptList;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCompanyDeptId() {
        return companyDeptId;
    }

    public void setCompanyDeptId(String companyDeptId) {
        this.companyDeptId = companyDeptId;
    }

    public List<String> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<String> companyList) {
        this.companyList = companyList;
    }

    public List<String> getClassList() {
        return classList;
    }

    public void setClassList(List<String> classList) {
        this.classList = classList;
    }

    public List<String> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<String> areaList) {
        this.areaList = areaList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTreeCode() {
        return treeCode;
    }

    public void setTreeCode(String treeCode) {
        this.treeCode = treeCode;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
