package com.sdkj.fixed.asset.api.system.in_vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 日志查询入参
 * 赵哲宇
 */
@ApiModel(value = "日志查询,查看入参")
public class LogEntity {

    /**
     * 开始时间
     */

    private String startTime;
    /**
     * 结束时间
     */

    private String endTime;
    /**
     * 机构id
     */
    @NotBlank(message = "公司id不能为空")
    private String orgId;
    /**
     * 日志id
     */
    @NotBlank(message = "id不能为空",groups = {LogIdGroup.class})
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
