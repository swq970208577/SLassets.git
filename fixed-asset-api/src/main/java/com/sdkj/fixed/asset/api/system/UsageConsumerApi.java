package com.sdkj.fixed.asset.api.system;

import com.sdkj.fixed.asset.common.base.BaseResultVo;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/14 10:26
 */
@Api
@RequestMapping({"/system/usageConsumer/api"})
public interface UsageConsumerApi {
    @ResponseBody
    @RequestMapping(value = "/getDxpireDayByCompanyId", method = RequestMethod.POST)
    public BaseResultVo getDxpireDayByCompanyId(@RequestParam("companyId") String companyId) throws Exception;
}
