package com.sdkj.fixed.asset.api.hc.vo.in;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sdkj.fixed.asset.pojo.hc.ReceiptApply;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

/**
 * ReceiptApplyParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/27 15:31
 */
public class ReceiptApplyParams extends ReceiptApply {
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    @Valid
    private List<ProductParams> productParamsList;

    public List<ProductParams> getProductParamsList() {
        return productParamsList;
    }

    public void setProductParamsList(List<ProductParams> productParamsList) {
        this.productParamsList = productParamsList;
    }
}
