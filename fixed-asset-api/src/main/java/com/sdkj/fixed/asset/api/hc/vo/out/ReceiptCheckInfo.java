package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * ReceiptCheckInfo
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 12:23
 */
public class ReceiptCheckInfo {

    private String name;

    private String categoryName;

    private String cuserName;

    private Integer finished;

    private Integer notFinished;

    private String executorName;

    private String executor;

    private Integer num;

    private String startTime;

    private String endTime;

    private Integer finish;

    public Integer getFinish() {
        return finish;
    }

    public void setFinish(Integer finish) {
        this.finish = finish;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public Integer getFinished() {
        return finished;
    }

    public void setFinished(Integer finished) {
        this.finished = finished;
    }

    public Integer getNotFinished() {
        return notFinished;
    }

    public void setNotFinished(Integer notFinished) {
        this.notFinished = notFinished;
    }
}
