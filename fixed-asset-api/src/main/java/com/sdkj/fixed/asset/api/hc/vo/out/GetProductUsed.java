package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * GetProductUsed
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/13 10:34
 */
public class GetProductUsed {
    private String id;
    @Excel(name = "耗材编码", orderNum = "1", width = 15)
    private String code;
    @Excel(name = "耗材名称", orderNum = "2", width = 15)
    private String name;
    @Excel(name = "耗材型号", orderNum = "3", width = 15)
    private String model;
    @Excel(name = "耗材单位", orderNum = "4", width = 15)
    private String unit;
    @ExcelCollection(name = "数量", orderNum = "5")
    private List<GetProductUsedNum> listNum;
    @ExcelCollection(name = "单价", orderNum = "6")
    private List<GetProductUsedPrice> listPrice;
    @ExcelCollection(name = "金额", orderNum = "7")
    private List<GetProductUsedTotalPrice> listTotalPrice;

    private GetProductUsedNum productUsedNums;
    private GetProductUsedPrice productUsedPrices;
    private GetProductUsedTotalPrice productUsedTotalPrices;

    public List<GetProductUsedNum> getListNum() {
        return listNum;
    }

    public void setListNum(List<GetProductUsedNum> listNum) {
        this.listNum = listNum;
    }

    public List<GetProductUsedPrice> getListPrice() {
        return listPrice;
    }

    public void setListPrice(List<GetProductUsedPrice> listPrice) {
        this.listPrice = listPrice;
    }

    public List<GetProductUsedTotalPrice> getListTotalPrice() {
        return listTotalPrice;
    }

    public void setListTotalPrice(List<GetProductUsedTotalPrice> listTotalPrice) {
        this.listTotalPrice = listTotalPrice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public GetProductUsedNum getProductUsedNums() {
        return productUsedNums;
    }

    public void setProductUsedNums(GetProductUsedNum productUsedNums) {
        this.productUsedNums = productUsedNums;
    }

    public GetProductUsedPrice getProductUsedPrices() {
        return productUsedPrices;
    }

    public void setProductUsedPrices(GetProductUsedPrice productUsedPrices) {
        this.productUsedPrices = productUsedPrices;
    }

    public GetProductUsedTotalPrice getProductUsedTotalPrices() {
        return productUsedTotalPrices;
    }

    public void setProductUsedTotalPrices(GetProductUsedTotalPrice productUsedTotalPrices) {
        this.productUsedTotalPrices = productUsedTotalPrices;
    }
}
