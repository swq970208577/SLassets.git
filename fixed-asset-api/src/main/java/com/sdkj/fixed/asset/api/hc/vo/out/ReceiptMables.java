package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import io.swagger.annotations.ApiModelProperty;
import org.apache.catalina.LifecycleState;

import java.util.List;

/**
 * ReceiptMables
 *
 * @author zhaozheyu
 * @Description 耗材领用查询
 * @date 2020/8/12 16:26
 */
public class ReceiptMables {
    @ApiModelProperty(value = "id")
    private String id;

    @Excel(name="耗材分类",orderNum = "1",width = 20, needMerge = true)
    private String flname;
    @Excel(name="耗材名称",orderNum = "2",width = 20, needMerge = true)
    private String hcname;

    @Excel(name="领用量",orderNum = "3",width = 20, needMerge = true)
    private Integer lynum;
    @Excel(name="金额",orderNum = "4",width = 20, needMerge = true)
    private String price;
    @ExcelCollection(name="分类下物品",orderNum = "7")
    private List<ReceiptCon> receiptConList;

    public List<ReceiptCon> getReceiptConList() {
        return receiptConList;
    }

    public void setReceiptConList(List<ReceiptCon> receiptConList) {
        this.receiptConList = receiptConList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlname() {
        return flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }

    public String getHcname() {
        return hcname;
    }

    public void setHcname(String hcname) {
        this.hcname = hcname;
    }

    public Integer getLynum() {
        return lynum;
    }

    public void setLynum(Integer lynum) {
        this.lynum = lynum;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}
