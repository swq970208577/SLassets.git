package com.sdkj.fixed.asset.api.system.out_vo;

import java.io.Serializable;

/**
 * @ClassName DictinnaryEntity
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 18:25
 */
public class DictionaryResult implements Serializable {
    private String value;
    private String name;
    private Integer sort;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
