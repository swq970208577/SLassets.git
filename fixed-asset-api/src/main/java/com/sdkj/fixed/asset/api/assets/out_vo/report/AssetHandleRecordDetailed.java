package com.sdkj.fixed.asset.api.assets.out_vo.report;

/**
 * @ClassName AssetHandleAllRecord
 * @Description 资产履历处理记录
 * @Author 张欣
 * @Date 2020/8/6 15:58
 */
public class AssetHandleRecordDetailed {
    /**
     * 单据类型
     */
    private String handType;
    /**
     * 处理时间
     */
    private String handleTime;
    /**
     * 处理人
     */
    private String handleUser;
    /**
     * 处理内容
     */
    private String content;

    public String getHandType() {
        return handType;
    }

    public void setHandType(String handType) {
        this.handType = handType == null ? "" : handType;
    }

    public String getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(String handleTime) {
        this.handleTime = handleTime  == null ? "" : handleTime;
    }

    public String getHandleUser() {
        return handleUser;
    }

    public void setHandleUser(String handleUser) {
        this.handleUser = handleUser == null ? "" : handleUser;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? "" : content;
    }
}
