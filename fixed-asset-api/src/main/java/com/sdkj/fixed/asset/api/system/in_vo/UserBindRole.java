package com.sdkj.fixed.asset.api.system.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @ClassName RoleBindUser
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 14:55
 */
public class UserBindRole {
    /**
     * 用户id
     */
    @NotBlank(message = "角色id不能为空")
    @Size(max = 32,message = "角色id长度不匹配")
    private String userId;
    /**
     *角色id
     */
    private List<String> roleIds;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }
}
