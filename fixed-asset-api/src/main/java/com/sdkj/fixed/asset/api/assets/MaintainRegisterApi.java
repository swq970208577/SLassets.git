package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.AppApplyMaintain;
import com.sdkj.fixed.asset.api.assets.in_vo.MaintainRegisterExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.SelPrams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @author niuliwei
 * @description 维修信息登记
 * @date 2020/7/27 11:21
 */
@RequestMapping("/assets/register/api")
public interface MaintainRegisterApi {
    /**
     * app申请报修
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/appApply", method = RequestMethod.POST)
    public BaseResultVo appApply (@Validated @RequestBody AppApplyMaintain params) throws Exception;
    /**
     * 维修信息登记-新增
     * @param register
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody MaintainRegisterExtend register);
    /**
     * 维修信息登记-分页查询
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    public BaseResultVo getAllPage(@RequestBody PageParams<SelPrams> param);

    /**
     * 维修信息登记-查看详情
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selById", method = RequestMethod.POST)
    public BaseResultVo selById(@Validated @RequestBody Params params);

    /**
     * 维修信息登记-导出
     * @param startDate
     * @param endDate
     * @return
     */
    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public void exportExcel(String startDate, String endDate, String orgId, String token) throws IOException;

    /**
     * 维修信息登记-编辑
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateInfo", method = RequestMethod.POST)
    public BaseResultVo updateInfo(@Validated @RequestBody MaintainRegisterExtend register);
    /**
     * 维修信息登记-开始维修
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/maintainStart", method = RequestMethod.POST)
    public BaseResultVo maintainStart(@Validated @RequestBody Params params);

    /**
     * 维修信息登记-维修完成
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/maintainEnd", method = RequestMethod.POST)
    public BaseResultVo maintainEnd(@Validated @RequestBody Params params);

    /**
     * 维修信息登记-打印
     * @return
     */
    @RequestMapping(value = "/print", method = RequestMethod.GET)
    public void print(@Param("id") String id) throws IOException;

    /**
     * 维修信息登记-删除（超管权限）
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delById", method = RequestMethod.POST)
    public BaseResultVo delById(@Validated @RequestBody Params params);
}
