package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ProductParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/27 15:33
 */
public class ProductParams {
    @Size(max = 32, message = "id-最大长度:32")
    @NotEmpty(message = "id-不能为空")
    private String id;
    @NotNull(message = "num-不能为空")
    private Integer num;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
