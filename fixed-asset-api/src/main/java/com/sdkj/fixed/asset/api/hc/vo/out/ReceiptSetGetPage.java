package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * ReceiptSetGetPage
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/23 17:20
 */
public class ReceiptSetGetPage {
    private String id;

    private String number;

    private String categoryId;

    private String categoryName;

    private String bussinessDate;

    private String cuser;

    private String cuserName;

    private String ctime;

    private String comment;

    private String pCode;

    private String pName;

    private String pBarCode;

    private String pModel;

    private String pUnit;

    private Integer numBefore;

    private String priceBefore;

    private String totalPriceBefore;

    private Integer num;

    private String price;

    private String totalPrice;

    private Integer numAfter;

    private String priceAfter;

    private String totalPriceAfter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpBarCode() {
        return pBarCode;
    }

    public void setpBarCode(String pBarCode) {
        this.pBarCode = pBarCode;
    }

    public String getpModel() {
        return pModel;
    }

    public void setpModel(String pModel) {
        this.pModel = pModel;
    }

    public String getpUnit() {
        return pUnit;
    }

    public void setpUnit(String pUnit) {
        this.pUnit = pUnit;
    }

    public Integer getNumBefore() {
        return numBefore;
    }

    public void setNumBefore(Integer numBefore) {
        this.numBefore = numBefore;
    }

    public String getPriceBefore() {
        return priceBefore;
    }

    public void setPriceBefore(String priceBefore) {
        this.priceBefore = priceBefore;
    }

    public String getTotalPriceBefore() {
        return totalPriceBefore;
    }

    public void setTotalPriceBefore(String totalPriceBefore) {
        this.totalPriceBefore = totalPriceBefore;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getNumAfter() {
        return numAfter;
    }

    public void setNumAfter(Integer numAfter) {
        this.numAfter = numAfter;
    }

    public String getPriceAfter() {
        return priceAfter;
    }

    public void setPriceAfter(String priceAfter) {
        this.priceAfter = priceAfter;
    }

    public String getTotalPriceAfter() {
        return totalPriceAfter;
    }

    public void setTotalPriceAfter(String totalPriceAfter) {
        this.totalPriceAfter = totalPriceAfter;
    }
}
