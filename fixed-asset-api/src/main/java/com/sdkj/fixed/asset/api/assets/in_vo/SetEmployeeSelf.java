package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author niuliwei
 * @description 员工自助类型
 * @date 2020/7/22 16:24
 */
public class SetEmployeeSelf {
    /**
     * 分类id集合
     */
    private List<String> ids;
    /**
     * 类型 1.领用、借用 2.交接
     */
    @NotNull
    @Digits(integer = 1, fraction = 2, message = "类型输入有误应为： 1.领用、借用 2.交接")
    private Integer type;
    /**
     * 分类id
     */
    private String id;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
