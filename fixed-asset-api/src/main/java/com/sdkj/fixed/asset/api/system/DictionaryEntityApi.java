package com.sdkj.fixed.asset.api.system;

import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.DictionaryEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName DictionaryEntityApi
 * @Description 数据字典
 * @Author 张欣
 * @Date 2020/7/21 16:23
 */
@RequestMapping({"/system/dic/api"})
public interface DictionaryEntityApi {
    /**
     * 删除数据字典
     * @param dicId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteDic", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo deleteDic(@RequestBody DictionaryEntity dicId) throws Exception;

    /**
     * 分页查询数据字典
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPage", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPage(@RequestBody PageParams<DictionaryEntity> param) throws Exception;
    /**
     * 获取数据字典详情
     * @param idParanEntity
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/queryDicInfo", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryDicInfo(@RequestBody DictionaryEntity idParanEntity) throws Exception;
}
