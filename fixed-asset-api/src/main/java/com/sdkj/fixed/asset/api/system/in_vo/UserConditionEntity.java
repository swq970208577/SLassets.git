package com.sdkj.fixed.asset.api.system.in_vo;

/**
 * @ClassName UserConditionEntity
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/24 10:36
 */
public class UserConditionEntity {
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 删除的员工数
     */
    private Integer deleted;
    /**
     * 状态
     */
    private Integer state;
    /**
     * 搜索框（姓名或工号）
     */
    private String  search;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search == null ? null : search.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
