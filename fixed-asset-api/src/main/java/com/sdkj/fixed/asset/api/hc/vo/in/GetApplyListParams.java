package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * GetAllParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/27 13:22
 */
public class GetApplyListParams {
    /**
     * 状态（1审批中2已批准3已驳回4已签收）
     */
    @NotNull(message = "状态-不能为空")
    private Integer state;
    /**
     * 时间（0一个月内1三个月内2半年内）
     */
    @NotNull(message = "时间-不能为空")
    private Integer time;

    private String startTime;

    private String endTime;

    private String cuser;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
