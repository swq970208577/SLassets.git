package com.sdkj.fixed.asset.api.system.in_vo;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName DicEntityId
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/22 10:33
 */
public class DicEntityId {
    @NotBlank(message = "数据字典id不能为空")
    private String dicEntityId;

    public String getDicEntityId() {
        return dicEntityId;
    }

    public void setDicEntityId(String dicEntityId) {
        this.dicEntityId = dicEntityId;
    }
}
