package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 调拨查询
 * @Date 2020/8/6 19:16
 */
public class TransferSearchParam extends SearchNameParam{

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
