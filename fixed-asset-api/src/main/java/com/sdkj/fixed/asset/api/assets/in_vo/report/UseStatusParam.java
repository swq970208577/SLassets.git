package com.sdkj.fixed.asset.api.assets.in_vo.report;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName HcUseStatusParam
 * @Description 首页使用情况参数
 * @Author 张欣
 * @Date 2020/8/11 16:04
 */
public class UseStatusParam {
    /**
     * 公司id
     */
    String companyId;
    /**
     * 近几年
     */
    @NotBlank(message = "时间条件不能为空")
    String yearNum;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getYearNum() {
        return yearNum;
    }

    public void setYearNum(String yearNum) {
        this.yearNum = yearNum;
    }
}
