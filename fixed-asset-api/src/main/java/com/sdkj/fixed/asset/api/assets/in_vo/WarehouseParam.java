package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.Warehouse;

import javax.validation.constraints.NotNull;

/**
 * @Author zhangjinfei
 * @Description //TODO 资产入库【添加、修改】的参数
 * @Date 2020/7/22 13:19
 */
public class WarehouseParam extends Warehouse {

    private String assetClassNo;
    private String companyNo;
    private String areaNo;
    private String deptNo;
    private String userNo;

    // -----维保信息------
    // 供应商
    private String supplierId;
    // 供应商名称
    private String supplierName;
    // 负责人ID
    private String supplierUserId;
    // 负责人
    private String supplierUser;
    // 维保到期日
    private String expireTime;
    // 维保说明
    private String explainText;

    public String getAssetClassNo() {
        return assetClassNo;
    }

    public void setAssetClassNo(String assetClassNo) {
        this.assetClassNo = assetClassNo;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public String getAreaNo() {
        return areaNo;
    }

    public void setAreaNo(String areaNo) {
        this.areaNo = areaNo;
    }

    public String getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierUserId() {
        return supplierUserId;
    }

    public void setSupplierUserId(String supplierUserId) {
        this.supplierUserId = supplierUserId;
    }

    public String getSupplierUser() {
        return supplierUser;
    }

    public void setSupplierUser(String supplierUser) {
        this.supplierUser = supplierUser;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getExplainText() {
        return explainText;
    }

    public void setExplainText(String explainText) {
        this.explainText = explainText;
    }
}
