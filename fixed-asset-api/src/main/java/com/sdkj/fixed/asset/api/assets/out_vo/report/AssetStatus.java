package com.sdkj.fixed.asset.api.assets.out_vo.report;

/**
 * @ClassName AssetStatus
 * @Description 首页资产状况
 * @Author 张欣
 * @Date 2020/8/11 9:57
 */
public class AssetStatus {
    /**
     * 总数
     */
   private String totalCount ;
    /**
     * 在用资产
     */
   private String usedCount;
    /**
     * 在用资产金额
     */
   private String usedAmount;
    /**
     * 闲置资产
     */
   private String freeCount;
    /**
     * 闲置金额
     */
   private String freeAmount;
    /**
     * 在用资产占用百分比
     */
   private String usePercent;
    /**
     * 闲置资产占用百分比
     */
   private String freePercent;

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getUsedCount() {
        return usedCount;
    }

    public void setUsedCount(String usedCount) {
        this.usedCount = usedCount;
    }

    public String getUsedAmount() {
        return usedAmount;
    }

    public void setUsedAmount(String usedAmount) {
        this.usedAmount = usedAmount;
    }

    public String getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(String freeCount) {
        this.freeCount = freeCount;
    }

    public String getFreeAmount() {
        return freeAmount;
    }

    public void setFreeAmount(String freeAmount) {
        this.freeAmount = freeAmount;
    }

    public String getUsePercent() {
        return usePercent;
    }

    public void setUsePercent(String usePercent) {
        this.usePercent = usePercent;
    }

    public String getFreePercent() {
        return freePercent;
    }

    public void setFreePercent(String freePercent) {
        this.freePercent = freePercent;
    }
}
