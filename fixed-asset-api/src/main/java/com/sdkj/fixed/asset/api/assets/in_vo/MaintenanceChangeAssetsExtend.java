package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.MaintenanceChangeAssets;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/27 11:19
 */
public class MaintenanceChangeAssetsExtend extends MaintenanceChangeAssets {

    /**
     * 供应商联系人
     */
    private String suppliercontact;
    /**
     * 供应商电话
     */
    private String supplierTel;
    /**
     * 资产名称
     */
    private String assetName;
    /**
     * 资产编码
     */
    private String assetCode;


    public String getSuppliercontact() {
        return suppliercontact;
    }

    public void setSuppliercontact(String suppliercontact) {
        this.suppliercontact = suppliercontact;
    }

    public String getSupplierTel() {
        return supplierTel;
    }

    public void setSupplierTel(String supplierTel) {
        this.supplierTel = supplierTel;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }
}
