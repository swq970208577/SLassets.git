package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * ReceiptMoveGetPageExtends
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/6 19:27
 */
public class ReceiptMoveGetPageExtends {

    @Excel(name = "物品编码", needMerge = true, orderNum = "0")
    private String code;

    @Excel(name = "物品名称", needMerge = true, orderNum = "1")
    private String name;

    @Excel(name = "商品条码", needMerge = true, orderNum = "2")
    private String barCode;

    @Excel(name = "规格型号", needMerge = true, orderNum = "3")
    private String model;

    @Excel(name = "单位", needMerge = true, orderNum = "4")
    private String unit;

    @Excel(name = "安全库存下限", needMerge = true, orderNum = "5")
    private Integer safeMin;

    @Excel(name = "安全库存上限", needMerge = true, orderNum = "6")
    private Integer safeMax;

    @Excel(name = "出库数量", needMerge = true, orderNum = "7")
    private Integer num;

    @Excel(name = "出库单价", needMerge = true, orderNum = "8")
    private String price;

    @Excel(name = "出库金额", needMerge = true, orderNum = "9")
    private String totalPrice;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getSafeMin() {
        return safeMin;
    }

    public void setSafeMin(Integer safeMin) {
        this.safeMin = safeMin;
    }

    public Integer getSafeMax() {
        return safeMax;
    }

    public void setSafeMax(Integer safeMax) {
        this.safeMax = safeMax;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
