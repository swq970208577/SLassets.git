package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.Withdrawal;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 退库查询
 * @Date 2020/7/28 16:21
 */
public class WithdrawalResult extends Withdrawal {

    private String withdrawalCompanyName;
    private String withdrawalAreaName;
    private List<WarehouseHistoryResult> warehouseList;

    public String getWithdrawalCompanyName() {
        return withdrawalCompanyName;
    }

    public void setWithdrawalCompanyName(String withdrawalCompanyName) {
        this.withdrawalCompanyName = withdrawalCompanyName;
    }

    public String getWithdrawalAreaName() {
        return withdrawalAreaName;
    }

    public void setWithdrawalAreaName(String withdrawalAreaName) {
        this.withdrawalAreaName = withdrawalAreaName;
    }

    public List<WarehouseHistoryResult> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<WarehouseHistoryResult> warehouseList) {
        this.warehouseList = warehouseList;
    }
}
