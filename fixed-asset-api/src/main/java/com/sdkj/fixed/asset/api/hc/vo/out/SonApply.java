package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * SonApply
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 16:48
 */
public class SonApply {

    private String sonId;

    private String name;

    private Integer num;

    private String unit;

    private String model;

    public String getSonId() {
        return sonId;
    }

    public void setSonId(String sonId) {
        this.sonId = sonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
