package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.Product;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;


/**
 * @author 史晨星
 * @ClassName: ProductApi
 * @Description: 物品档案
 * @date 2020年7月21日
 */
@RequestMapping("/hc/product/api")
public interface ProductApi {

    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody Product entity) throws Exception;

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody Product entity) throws Exception;

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody Product entity) throws Exception;

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo view(@RequestBody Product entity) throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody PageParams<Product> params) throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPagesIn", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPagesIn(@RequestBody PageParams<Product> params) throws Exception;

    /**
     * 根据分类查物品（分页查询）
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getProductByType", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getProductByType(@RequestBody Product params) throws Exception;


    /**
     * 根据分类和仓库查物品（分页查询）
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getProductByTypeAndCategory", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getProductByTypeAndCategory(@RequestBody PageParams<Product> params) throws Exception;

    /**
     * 根据分类和仓库查物品（分页查询）
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/saveImg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo saveImg(@RequestBody MultipartFile file) throws Exception;


    /**
     * 下载物品模板
     *
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/downTemp", method = RequestMethod.GET)
    @ResponseBody
    public void downTemp(HttpServletResponse response) throws Exception;

    /**
     * 导入
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/importProduct", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo importProduct(MultipartFile file) throws Exception;

    /**
     * 导出
     *
     * @param companyId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(String companyId, String type, HttpServletResponse response) throws Exception;

    /**
     * 报表
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/statistics", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo statistics(@RequestBody PageParams<StatisticsParams> params) throws Exception;

    /**
     * 仓库下物品
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getProductByCategory", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getProductByCategory(@RequestBody IdParams params) throws Exception;

    /**
     * 报表
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/statisticsDetail", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo statisticsDetail(@RequestBody PageParams<StatisticsParams> params) throws Exception;

    /**
     * 导出
     *
     * @param userId
     * @param categoryId
     * @param start
     * @param end
     * @param name
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/exportStatistics", method = RequestMethod.GET)
    @ResponseBody
    public void exportStatistics(String orgId, String userId, String categoryId, String start, String end, String name, HttpServletResponse response) throws Exception;

    /**
     * 导出
     *
     * @param userId
     * @param categoryId
     * @param start
     * @param end
     * @param productId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/exportStatisticsDetail", method = RequestMethod.GET)
    @ResponseBody
    public void exportStatisticsDetail(String orgId, String userId, String categoryId, String start, String end, String productId, HttpServletResponse response) throws Exception;
}

