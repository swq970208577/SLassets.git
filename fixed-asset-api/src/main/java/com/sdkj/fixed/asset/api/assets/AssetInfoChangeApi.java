package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.AssetInfoChangeExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.SelPrams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @author niuliwei
 * @description 实物信息变更
 * @date 2020/7/27 11:23
 */
@RequestMapping("/assets/assetInfoChange/api")
public interface AssetInfoChangeApi {

    /**
     * 实物信息变更-新增
     * @param change
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody AssetInfoChangeExtend change);

    /**
     * 实物信息变更-分页查询
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    public BaseResultVo getAllPage(@RequestBody PageParams<SelPrams> param);

    /**
     * 实物信息变更-查看详情
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selById", method = RequestMethod.POST)
    public BaseResultVo selById(@Validated @RequestBody Params params);

    /**
     * 实物信息变更-打印
     * @param id
     * @return
     */
    @RequestMapping(value = "/print", method = RequestMethod.GET)
    public void print(@Param("id") String id) throws IOException;
}
