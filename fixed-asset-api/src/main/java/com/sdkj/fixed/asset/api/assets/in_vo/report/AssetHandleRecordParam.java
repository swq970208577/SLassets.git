package com.sdkj.fixed.asset.api.assets.in_vo.report;

/**
 * @ClassName AssetListParam
 * @Description 资产履历参数
 * @Author 张欣
 * @Date 2020/7/30 16:47
 */
public class AssetHandleRecordParam {
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null?"":startTime.trim();
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime==null?"":endTime.trim();
    }
}
