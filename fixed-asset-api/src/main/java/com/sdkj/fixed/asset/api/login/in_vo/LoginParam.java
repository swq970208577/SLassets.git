package com.sdkj.fixed.asset.api.login.in_vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Pc端登录传参
 * @author 张欣
 */
@ApiModel
public class LoginParam {
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",required = true)
    @NotBlank(message = "账号不能为空")
    private String tel;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码,MD5加密",required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password.trim();
    }
}
