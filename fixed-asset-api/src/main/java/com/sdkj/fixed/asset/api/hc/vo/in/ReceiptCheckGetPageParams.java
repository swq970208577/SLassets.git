package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotEmpty;

/**
 * ReceiptCheckGetPageParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 10:29
 */
public class ReceiptCheckGetPageParams {

    private String orgId;

    private String categoryId;
    private String finish;

    private String name;

    private String userId;

    private Integer isAdmin;

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
