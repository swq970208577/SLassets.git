package com.sdkj.fixed.asset.api.login.out_vo;


import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 移动端登录返参
 * @author 张欣
 */
public class LoginUserResultMobile extends LoginUser implements Serializable {

    @ApiModelProperty(value = "用户电话")
    private String userTel;

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }
}
