package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 调拨明细
 * @Date 2020/8/4 18:43
 */
public class TransferDetail {

    private String assetId;

    private String transferInDept;
    private String transferInDeptTreecode;

    private String transferInArea;

    private String transferInAddress;

    private String useUserId;

    private String useUsername;

    private String transferInDate;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getTransferInDept() {
        return transferInDept;
    }

    public void setTransferInDept(String transferInDept) {
        this.transferInDept = transferInDept;
    }

    public String getTransferInDeptTreecode() {
        return transferInDeptTreecode;
    }

    public void setTransferInDeptTreecode(String transferInDeptTreecode) {
        this.transferInDeptTreecode = transferInDeptTreecode;
    }

    public String getTransferInArea() {
        return transferInArea;
    }

    public void setTransferInArea(String transferInArea) {
        this.transferInArea = transferInArea;
    }

    public String getTransferInAddress() {
        return transferInAddress;
    }

    public void setTransferInAddress(String transferInAddress) {
        this.transferInAddress = transferInAddress;
    }

    public String getUseUserId() {
        return useUserId;
    }

    public void setUseUserId(String useUserId) {
        this.useUserId = useUserId;
    }

    public String getUseUsername() {
        return useUsername;
    }

    public void setUseUsername(String useUsername) {
        this.useUsername = useUsername;
    }

    public String getTransferInDate() {
        return transferInDate;
    }

    public void setTransferInDate(String transferInDate) {
        this.transferInDate = transferInDate;
    }
}
