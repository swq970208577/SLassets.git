package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用导出
 * @Date 2020/7/28 14:47
 */
public class CollectExport  {


    @Excel(name = "办理状态",replace={"待签字_1","已签字_2"} ,orderNum = "0",needMerge = true)
    private Integer state;

    @Excel(name = "领用单号",orderNum = "1",needMerge = true)
    private String receiveNo;

    @Excel(name = "领用日期",orderNum = "2",needMerge = true)
    private String receiveDate;

    @Excel(name = "领用人",orderNum = "3",needMerge = true)
    private String receiveUser;

    @Excel(name = "领用后使用公司",orderNum = "4",needMerge = true)
    private String receiveCompanyName;

    @Excel(name = "领用后使用部门",orderNum = "5",needMerge = true)
    private String receiveDeptName;

    @Excel(name = "领用后区域",orderNum = "6",needMerge = true)
    private String receiveAreaName;

    @Excel(name = "领用后存放地点",orderNum = "7",needMerge = true)
    private String receiveAddress;

    @Excel(name = "领用备注",orderNum = "8",needMerge = true)
    private String receiveRemark;

    @Excel(name = "预计退库日期",orderNum = "9",needMerge = true)
    private String returnTime;

    @Excel(name = "处理人",orderNum = "10",needMerge = true)
    private String handlerUser;

    @ExcelCollection(name = "资产明细",orderNum = "11")
    private List<WarehouseBase> list;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getReceiveCompanyName() {
        return receiveCompanyName;
    }

    public void setReceiveCompanyName(String receiveCompanyName) {
        this.receiveCompanyName = receiveCompanyName;
    }

    public String getReceiveDeptName() {
        return receiveDeptName;
    }

    public void setReceiveDeptName(String receiveDeptName) {
        this.receiveDeptName = receiveDeptName;
    }

    public String getReceiveAreaName() {
        return receiveAreaName;
    }

    public void setReceiveAreaName(String receiveAreaName) {
        this.receiveAreaName = receiveAreaName;
    }

    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    public String getReceiveRemark() {
        return receiveRemark;
    }

    public void setReceiveRemark(String receiveRemark) {
        this.receiveRemark = receiveRemark;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser;
    }

    public List<WarehouseBase> getList() {
        return list;
    }

    public void setList(List<WarehouseBase> list) {
        this.list = list;
    }
}
