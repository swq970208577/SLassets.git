package com.sdkj.fixed.asset.api.system;

import com.sdkj.fixed.asset.api.system.in_vo.LogEntity;
import com.sdkj.fixed.asset.api.system.in_vo.UsageModifyParam;
import com.sdkj.fixed.asset.api.system.in_vo.UsageParam;
import com.sdkj.fixed.asset.api.system.out_vo.UsageResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/9 10:30
 */
@Api
@RequestMapping({"/system/usage/api"})
public interface UsageApi {

    @ApiOperation(value = "获取使用剩余天数")
    @ResponseBody
    @RequestMapping(value = "/expireDay", method = RequestMethod.POST)
    public BaseResultVo expireDay() throws Exception;
    @ApiOperation(value = "分页查询")
    @ResponseBody
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    public BaseResultVo<UsageResult> getPages(@RequestBody @Validated PageParams<UsageParam> params) throws Exception;
    @ApiOperation(value = "根据id修改")
    @ResponseBody
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public BaseResultVo modify(@RequestBody @Validated UsageModifyParam param) throws Exception;
    @ApiOperation(value = "根据id查询")
    @ResponseBody
    @RequestMapping(value = "/deatil", method = RequestMethod.GET)
    public BaseResultVo deatil(@RequestParam("id") String id) throws Exception;
    @ApiOperation(value = "根据id删除")
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public BaseResultVo delete(@NotBlank(message = "机构id不能为空")@RequestParam("companyId") String companyId) throws Exception;
    @ApiOperation(value = "根据companyId查询")
    @ResponseBody
    @RequestMapping(value = "/getUsage", method = RequestMethod.POST)
    public BaseResultVo getUsage() throws Exception;
}
