package com.sdkj.fixed.asset.api.hc.vo.out;

import java.util.List;

/**
 * ApplyResult
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/12 16:25
 */
public class ApplyResult {
    List<SonApply> sonApplies;
    List<ApplyDetail> applyDetails;

    public List<SonApply> getSonApplies() {
        return sonApplies;
    }

    public void setSonApplies(List<SonApply> sonApplies) {
        this.sonApplies = sonApplies;
    }

    public List<ApplyDetail> getApplyDetails() {
        return applyDetails;
    }

    public void setApplyDetails(List<ApplyDetail> applyDetails) {
        this.applyDetails = applyDetails;
    }
}
