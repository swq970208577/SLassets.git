package com.sdkj.fixed.asset.api.assets.in_vo;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 审批同意参数
 * @Date 2020/8/9 15:35
 */
public class WithdrawalApplyAgreeParam {

    /**
     * 子账单Id
     */
    private String id;
    private String useUserName;
    private List<String> assetIdList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUseUserName() {
        return useUserName;
    }

    public void setUseUserName(String useUserName) {
        this.useUserName = useUserName;
    }

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }
}
