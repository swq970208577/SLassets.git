package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.StatisticsParams;
import com.sdkj.fixed.asset.api.hc.vo.out.Statistics;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ProductType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * @author 史晨星
 * @ClassName: CategoryApi
 * @Description: 物品档案-分类
 * @date 2020年7月20日
 */
@RequestMapping("/hc/productType/api")
public interface ProductTypeApi {


    /**
     * 新增分类
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody ProductType entity) throws Exception;

    /**
     * 修改分类
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody ProductType entity) throws Exception;

    /**
     * 删除分类
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody ProductType entity) throws Exception;

    /**
     * 分类树状图
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getTree", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getTree() throws Exception;

    /**
     * 分类树状图App
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getTreeApp", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getTreeApp() throws Exception;


    /**
     * 导出
     *
     * @param companyId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(String companyId, HttpServletResponse response) throws Exception;

    /**
     * 导出报表
     *
     * @param orgId
     * @param categoryId
     * @param start
     * @param end
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/exportStatistics", method = RequestMethod.GET)
    @ResponseBody
    public void exportStatistics(String userId,String orgId, String categoryId, String start, String end, HttpServletResponse response) throws Exception;

    /**
     * 统计报表-物品
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/productList", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo productList(@RequestBody StatisticsParams params) throws Exception;

    /**
     * 统计报表-分类
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/productTypes", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo productTypes(@RequestBody StatisticsParams params) throws Exception;

    /**
     * 统计报表
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/statistics", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo statistics(@RequestBody StatisticsParams params) throws Exception;

}

