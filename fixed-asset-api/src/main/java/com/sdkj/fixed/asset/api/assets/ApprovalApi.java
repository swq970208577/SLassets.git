package com.sdkj.fixed.asset.api.assets;


import com.sdkj.fixed.asset.api.assets.in_vo.AppSignParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssMationParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroAppParam;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.AssApproval;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhaozheyu
 * @description 我的审批
 * @date 2020/7/27 11:22
 */
@RequestMapping("/assets/approval/api")
public interface ApprovalApi {
    /**
     * 我的审批前台查询
     */
    @PostMapping("/getAssAppList")
    @ResponseBody
    public BaseResultVo getAssAppList(@RequestBody PageParams<AssroParam> params);


    /**
     * 我的审批前台基本信息查询
     */
    @PostMapping("/getInformation")
    @ResponseBody
    public BaseResultVo getInformation(@RequestBody PageParams<AssMationParam> params);


    /**
     * 基本信息审批情况查询
     */
    @PostMapping("/getExamination")
    @ResponseBody
    public BaseResultVo getExamination(@RequestBody PageParams<AssMationParam> params);


    /**
     * 基本信息审批情况查询
     */
    @PostMapping("/getRelease")
    @ResponseBody
    public BaseResultVo getRelease(@RequestBody PageParams<AssMationParam> params);

    /**
     * 我的审批app查询
     */
    @PostMapping("/getAppList")
    @ResponseBody
    public BaseResultVo getAppList(@RequestBody PageParams<AssroAppParam> params);


    /**
     * 我的审批app基本信息查询
     */
    @PostMapping("/getAppInformation")
    @ResponseBody
    public BaseResultVo getAppInformation(@RequestBody PageParams<AssMationParam> params);

    /**
     *我的审批app待审批数量
     */
    @PostMapping("/getAppnum")
    @ResponseBody
    public BaseResultVo getAppnum(@RequestBody AssroAppParam assroAppParam);


    /**
     * 收货签字app
     */
    @PostMapping("/getAppsign")
    @ResponseBody
    public BaseResultVo getAppsign(@RequestBody PageParams<AppSignParam> params);
}
