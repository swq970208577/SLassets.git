package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotNull;

/**
 * ReceiptInGetPageParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/21 14:45
 */
public class ReceiptOutGetPageParams {
    private String categoryId;

    private String number;

    private String orgId;
    @NotNull(message = "类型不能为空")
    private Integer type;

    private String start;

    private String end;

    private String userId;

    private Integer isAdmin;

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start == null ? "" : start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end == null ? "" : end;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? "" : categoryId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? "" : number;
    }
}
