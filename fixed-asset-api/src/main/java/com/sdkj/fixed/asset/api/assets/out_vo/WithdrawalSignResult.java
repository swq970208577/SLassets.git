package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 签字
 * @Date 2020/8/14 11:24
 */
public class WithdrawalSignResult extends WithdrawalResult {

    private WithdrawalResult applyResult;

    public WithdrawalResult getApplyResult() {
        return applyResult;
    }

    public void setApplyResult(WithdrawalResult applyResult) {
        this.applyResult = applyResult;
    }
}
