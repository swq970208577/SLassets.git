package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.Inventory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/6 17:41
 */
public class AppInvEntity {

    private String assetId;
    private String assetCode;
    private String assetName;
    private String assetClassName;
    private String inventoryState;
    private String isManual;

    private List<Inventory> invList = new ArrayList<>();

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public List<Inventory> getInvList() {
        return invList;
    }

    public void setInvList(List<Inventory> invList) {
        this.invList = invList;
    }

    public String getInventoryState() {
        return inventoryState;
    }

    public void setInventoryState(String inventoryState) {
        this.inventoryState = inventoryState;
    }

    public String getIsManual() {
        return isManual;
    }

    public void setIsManual(String isManual) {
        this.isManual = isManual;
    }

    public boolean equals(Object obj) {
        AppInvEntity u = (AppInvEntity) obj;
        return assetCode.equals(u.assetCode);
    }

    public int hashCode() {
        String in = assetCode;
        return in.hashCode();
    }
}
