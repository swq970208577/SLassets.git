package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/6 11:26
 */
public class SubInvResultParam {
    //    盘点id
    @NotBlank(message = "盘点单id不能为空")
    private String inventoryId;
    //    盘盈资产自动新增入库,0是，1否
    @NotNull
    private Integer autoAddAsset;
    //    已盘资产修改内容自动更新资产信息,0是，1否
    @NotNull
    private Integer autoUpdateAsset;
    //    未盘、盘亏资产自动清理报废,0是，1否
    @NotNull
    private Integer autoScrap;

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Integer getAutoAddAsset() {
        return autoAddAsset;
    }

    public void setAutoAddAsset(Integer autoAddAsset) {
        this.autoAddAsset = autoAddAsset;
    }

    public Integer getAutoUpdateAsset() {
        return autoUpdateAsset;
    }

    public void setAutoUpdateAsset(Integer autoUpdateAsset) {
        this.autoUpdateAsset = autoUpdateAsset;
    }

    public Integer getAutoScrap() {
        return autoScrap;
    }

    public void setAutoScrap(Integer autoScrap) {
        this.autoScrap = autoScrap;
    }
}

