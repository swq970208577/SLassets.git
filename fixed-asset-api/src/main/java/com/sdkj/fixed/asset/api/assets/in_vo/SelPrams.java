package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/24 13:52
 */
public class SelPrams {

    private String startDate;

    private String endDate;

    private String orgId;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
