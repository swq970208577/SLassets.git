package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * GetProductUsedNum
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/13 10:35
 */
public class GetProductUsedNum {
    @Excel(name = "1月", orderNum = "1", width = 10)
    private Integer num1;
    @Excel(name = "2月", orderNum = "2", width = 10)
    private Integer num2;
    @Excel(name = "3月", orderNum = "3", width = 10)
    private Integer num3;
    @Excel(name = "4月", orderNum = "4", width = 10)
    private Integer num4;
    @Excel(name = "5月", orderNum = "5", width = 10)
    private Integer num5;
    @Excel(name = "6月", orderNum = "6", width = 10)
    private Integer num6;
    @Excel(name = "7月", orderNum = "7", width = 10)
    private Integer num7;
    @Excel(name = "8月", orderNum = "8", width = 10)
    private Integer num8;
    @Excel(name = "9月", orderNum = "9", width = 10)
    private Integer num9;
    @Excel(name = "10月", orderNum = "10", width = 10)
    private Integer num10;
    @Excel(name = "11月", orderNum = "11", width = 10)
    private Integer num11;
    @Excel(name = "12月", orderNum = "12", width = 10)
    private Integer num12;
    @Excel(name = "合计", orderNum = "13", width = 10)
    private Integer numTotal;

    public Integer getNumTotal() {
        return numTotal;
    }

    public void setNumTotal(Integer numTotal) {
        this.numTotal = numTotal;
    }

    public Integer getNum1() {
        return num1;
    }

    public void setNum1(Integer num1) {
        this.num1 = num1;
    }

    public Integer getNum2() {
        return num2;
    }

    public void setNum2(Integer num2) {
        this.num2 = num2;
    }

    public Integer getNum3() {
        return num3;
    }

    public void setNum3(Integer num3) {
        this.num3 = num3;
    }

    public Integer getNum4() {
        return num4;
    }

    public void setNum4(Integer num4) {
        this.num4 = num4;
    }

    public Integer getNum5() {
        return num5;
    }

    public void setNum5(Integer num5) {
        this.num5 = num5;
    }

    public Integer getNum6() {
        return num6;
    }

    public void setNum6(Integer num6) {
        this.num6 = num6;
    }

    public Integer getNum7() {
        return num7;
    }

    public void setNum7(Integer num7) {
        this.num7 = num7;
    }

    public Integer getNum8() {
        return num8;
    }

    public void setNum8(Integer num8) {
        this.num8 = num8;
    }

    public Integer getNum9() {
        return num9;
    }

    public void setNum9(Integer num9) {
        this.num9 = num9;
    }

    public Integer getNum10() {
        return num10;
    }

    public void setNum10(Integer num10) {
        this.num10 = num10;
    }

    public Integer getNum11() {
        return num11;
    }

    public void setNum11(Integer num11) {
        this.num11 = num11;
    }

    public Integer getNum12() {
        return num12;
    }

    public void setNum12(Integer num12) {
        this.num12 = num12;
    }
}
