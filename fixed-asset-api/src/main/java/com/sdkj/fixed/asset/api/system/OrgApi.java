package com.sdkj.fixed.asset.api.system;

import com.sdkj.fixed.asset.api.system.in_vo.EditState;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.pojo.system.OrgManagement;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName OrgApi
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/20 14:40
 */
@RequestMapping({"/system/org/api"})
public interface OrgApi {
    /**
     * 禁用机构
     * @param orgManagement
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/forbiddenOrOrgEnable", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo forbiddenOrOrgEnable(@RequestBody EditState orgManagement);

    @RequestMapping(value = "/queryOrgInfo", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryOrgInfo(@RequestBody OrgManagement orgManagement) throws Exception;

    /**
     * 获取所有组织架构
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAllOrg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAllOrg() throws Exception;
    /**
     * 获取所有启用状态的组织架构
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAllEnableOrg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAllEnableOrg() throws Exception;
    /**
     * 获取启用状态的公司
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getEnableCompany", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getEnableCompany() throws Exception;
    /**
     * 获取启用状态的公司
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAuthEnableCompany", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAuthEnableCompany() throws Exception;
    /**
     * 获取权限下的公司架构
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAuthOrg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAuthOrg() throws Exception;
    /**
     * 获取公司下启用状态的部门
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getEnableDeptByCompanyId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getEnableDeptByCompanyId(@RequestBody OrgManagement orgManagement) throws Exception;
    /**
     * 获取当前登录用户权限下公司下启用状态的部门
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAuthEnableDeptByCompanyId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAuthEnableDeptByCompanyId(@RequestBody OrgManagement orgManagement) throws Exception;
    /**
     * 下载部门模板
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/downDeptTemp", method = RequestMethod.GET)
    public void downDeptTemp(HttpServletResponse response) throws Exception;
    /**
     * 下载公司模板
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/downCompanyTemp", method = RequestMethod.GET)
    public void downCompanyTemp(HttpServletResponse response) throws Exception;
    /**
     * 导入公司
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/importOrg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo importOrg(MultipartFile file) throws Exception;

    /**
     * 导入部门
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/importDept", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo importDept(MultipartFile file) throws Exception;
}
