package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author niuliwei
 * @description app员工提交盘点结果
 * @date 2020/8/7 10:50
 */
public class AppUpdInvState {

    @NotBlank(message = "资产不能为空")
    private String assetCode;
    @NotBlank(message = "盘点单id不能为空")
    private String inventoryIds;
    @Size(max = 128, message = "备注超长")
    private String note;
    @NotBlank(message = "盘点状态不能为空")
    private String inventoryState;

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getInventoryIds() {
        return inventoryIds;
    }

    public void setInventoryIds(String inventoryIds) {
        this.inventoryIds = inventoryIds;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getInventoryState() {
        return inventoryState;
    }

    public void setInventoryState(String inventoryState) {
        this.inventoryState = inventoryState;
    }
}
