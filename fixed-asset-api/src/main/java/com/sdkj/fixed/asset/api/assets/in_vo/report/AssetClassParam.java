package com.sdkj.fixed.asset.api.assets.in_vo.report;

/**
 * @ClassName AssetClassParam
 * @Description 资产分类条件
 * @Author 张欣
 * @Date 2020/8/7 16:12
 */
public class AssetClassParam {
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 部门id
     */
    private String deptId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
}
