package com.sdkj.fixed.asset.api.hc.vo.in;

import io.swagger.annotations.ApiModelProperty;

/**
 * StockParams
 *
 * @author zhaozheyu
 * @Description
 * @date 2020/8/11 20:40
 */
public class StockParams {
    @ApiModelProperty(value = "物品编码或名称")
    private String name;

    @ApiModelProperty(value = "仓库id")
    private String ckid;

    @ApiModelProperty(value = "当前登录人id")
    private String userid;

    @ApiModelProperty(value = "当前机构id")
    private String orgid;

    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer ifadmin;



    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCkid() {
        return ckid;
    }

    public void setCkid(String ckid) {
        this.ckid = ckid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getIfadmin() {
        return ifadmin;
    }

    public void setIfadmin(Integer ifadmin) {
        this.ifadmin = ifadmin;
    }
}
