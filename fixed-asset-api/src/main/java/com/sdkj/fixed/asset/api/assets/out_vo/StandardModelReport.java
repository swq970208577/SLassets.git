package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zhangjinfei
 * @Description //TODO 标准型号报表
 * @Date 2020/8/11 9:53
 */
public class StandardModelReport {

//    资产分类编码
    @Excel(name="资产分类编码",orderNum = "1")
    private String  num;
//    资产分类名称
    @Excel(name="资产分类名称",orderNum = "2")
    private String  name;
//    规格型号ID
    private String  standardModel;
//    规格型号
    @Excel(name="规格型号",orderNum = "3")
    private String  standardModelName;
    //    闲置
    @Excel(name="闲置",orderNum = "6")
    private Integer idle;
    //    在用
    @Excel(name="在用",orderNum = "7")
    private Integer inUse;
    //    待发放
//    @Excel(name="待发放",orderNum = "8")
    private Integer give;
    //    借用
    @Excel(name="借用",orderNum = "8")
    private Integer borrow;
    //    调拨中
    @Excel(name="调拨中",orderNum = "9")
    private Integer transfer;
    //    维修中
    @Excel(name="维修中",orderNum = "10")
    private Integer repair;
    //    已报废
    @Excel(name="已报废",orderNum = "11")
    private Integer scrapped;
    //    总计
    @Excel(name="总计",orderNum = "12")
    private Integer total;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStandardModel() {
        return standardModel;
    }

    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel;
    }

    public String getStandardModelName() {
        return standardModelName;
    }

    public void setStandardModelName(String standardModelName) {
        this.standardModelName = standardModelName;
    }

    public Integer getIdle() {
        return idle;
    }

    public void setIdle(Integer idle) {
        this.idle = idle;
    }

    public Integer getInUse() {
        return inUse;
    }

    public void setInUse(Integer inUse) {
        this.inUse = inUse;
    }

    public Integer getGive() {
        return give;
    }

    public void setGive(Integer give) {
        this.give = give;
    }

    public Integer getBorrow() {
        return borrow;
    }

    public void setBorrow(Integer borrow) {
        this.borrow = borrow;
    }

    public Integer getTransfer() {
        return transfer;
    }

    public void setTransfer(Integer transfer) {
        this.transfer = transfer;
    }

    public Integer getRepair() {
        return repair;
    }

    public void setRepair(Integer repair) {
        this.repair = repair;
    }

    public Integer getScrapped() {
        return scrapped;
    }

    public void setScrapped(Integer scrapped) {
        this.scrapped = scrapped;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
