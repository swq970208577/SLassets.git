package com.sdkj.fixed.asset.api.hc.vo.out;

import com.sdkj.fixed.asset.pojo.hc.ReceiptMoveProduct;

import java.util.List;

/**
 * ReceiptMoveExtend
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/6 17:00
 */
public class ReceiptMoveExtend {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String number;

    private String outCategoryName;

    private String outTime;

    private String inCategoryName;

    private String outUser;

    private String outBussinessDate;

    private String outComment;

    private String inTime;

    private String inUser;

    private String inBussinessDate;

    private String inComment;

    private List<ReceiptMoveProductExtend> receiptMoveProductList;

    public List<ReceiptMoveProductExtend> getReceiptMoveProductList() {
        return receiptMoveProductList;
    }

    public void setReceiptMoveProductList(List<ReceiptMoveProductExtend> receiptMoveProductList) {
        this.receiptMoveProductList = receiptMoveProductList;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOutCategoryName() {
        return outCategoryName;
    }

    public void setOutCategoryName(String outCategoryName) {
        this.outCategoryName = outCategoryName;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getInCategoryName() {
        return inCategoryName;
    }

    public void setInCategoryName(String inCategoryName) {
        this.inCategoryName = inCategoryName;
    }

    public String getOutUser() {
        return outUser;
    }

    public void setOutUser(String outUser) {
        this.outUser = outUser;
    }

    public String getOutBussinessDate() {
        return outBussinessDate;
    }

    public void setOutBussinessDate(String outBussinessDate) {
        this.outBussinessDate = outBussinessDate;
    }

    public String getOutComment() {
        return outComment;
    }

    public void setOutComment(String outComment) {
        this.outComment = outComment;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getInUser() {
        return inUser;
    }

    public void setInUser(String inUser) {
        this.inUser = inUser;
    }

    public String getInBussinessDate() {
        return inBussinessDate;
    }

    public void setInBussinessDate(String inBussinessDate) {
        this.inBussinessDate = inBussinessDate;
    }

    public String getInComment() {
        return inComment;
    }

    public void setInComment(String inComment) {
        this.inComment = inComment;
    }
}
