package com.sdkj.fixed.asset.api.hc.vo.out;

import io.swagger.annotations.ApiModelProperty;

/**
 * ReceiptConsum
 *
 * @author zhaozheyu
 * @Description 耗材领用表list反参
 * @date 2020/8/12 20:21
 */
public class ReceiptConsum {
    @ApiModelProperty(value = "月份")
    private String month;

    @ApiModelProperty(value = "数额")
    private String amount;

    @ApiModelProperty(value = "领用量")
    private Integer lynum;


    public Integer getLynum() {
        return lynum;
    }

    public void setLynum(Integer lynum) {
        this.lynum = lynum;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
