package com.sdkj.fixed.asset.api.hc.vo.in;

/**
 * ReceiptSetGetPageParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/23 16:58
 */
public class ReceiptMoveGetPageParams {
    private String inCategoryId;

    private String outCategoryId;

    private String number;

    private String orgId;

    private String userId;

    private Integer isAdmin;

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getInCategoryId() {
        return inCategoryId;
    }

    public void setInCategoryId(String inCategoryId) {
        this.inCategoryId = inCategoryId;
    }

    public String getOutCategoryId() {
        return outCategoryId;
    }

    public void setOutCategoryId(String outCategoryId) {
        this.outCategoryId = outCategoryId;
    }
}
