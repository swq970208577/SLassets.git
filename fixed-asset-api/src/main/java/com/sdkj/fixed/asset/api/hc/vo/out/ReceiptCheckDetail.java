package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * ReceiptCheckDetail
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 10:51
 */
public class ReceiptCheckDetail {

    private String id;

    private Integer state;

    private String img;

    private String type;

    @Excel(name = "耗材分类", orderNum = "2", width = 20)
    private String typeName;

    @Excel(name = "耗材编码", orderNum = "3", width = 20)
    private String code;

    @Excel(name = "耗材名称", orderNum = "4", width = 20)
    private String name;

    @Excel(name = "商品条码", orderNum = "5", width = 20)
    private String barCode;

    @Excel(name = "规格型号", orderNum = "6", width = 20)
    private String model;

    @Excel(name = "单位", orderNum = "7", width = 20)
    private String unit;

    @Excel(name = "应盘数量", orderNum = "8", width = 20)
    private Integer num;

    @Excel(name = "单价", orderNum = "9", width = 20)
    private String price;

    @Excel(name = "金额", orderNum = "10", width = 20)
    private String totalPrice;

    @Excel(name = "实盘数量", orderNum = "11", width = 20)
    private Integer realNum;

    @Excel(name = "实盘金额", orderNum = "12", width = 20)
    private String realPrice;

    @Excel(name = "盘点备注", orderNum = "13", width = 20)
    private String comment;

    @Excel(name = "盘点时间", orderNum = "14", width = 20)
    private String checkTime;

    private String checkUser;

    @Excel(name = "盘点人", orderNum = "15", width = 20)
    private String checkUserName;

    @Excel(name = "盘点状态", orderNum = "1", width = 20)
    private String state1;

    private Integer surplus;

    public Integer getSurplus() {
        return surplus;
    }

    public void setSurplus(Integer surplus) {
        this.surplus = surplus;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getRealNum() {
        return realNum;
    }

    public void setRealNum(Integer realNum) {
        this.realNum = realNum;
    }

    public String getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(String realPrice) {
        this.realPrice = realPrice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }

    public String getCheckUser() {
        return checkUser;
    }

    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser;
    }

    public String getCheckUserName() {
        return checkUserName;
    }

    public void setCheckUserName(String checkUserName) {
        this.checkUserName = checkUserName;
    }
}
