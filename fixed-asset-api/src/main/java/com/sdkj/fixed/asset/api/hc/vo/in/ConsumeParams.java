package com.sdkj.fixed.asset.api.hc.vo.in;

import io.swagger.annotations.ApiModelProperty;

/**
 * ConsumeParams
 *
 * @author zhaozheyu
 * @Description 耗材领用表入参
 * @date 2020/8/12 20:27
 */
public class ConsumeParams {
    @ApiModelProperty(value = "当前登录人机构id")
    private String orgid;
    @ApiModelProperty(value = "当前登录人id")
    private String userid;


    @ApiModelProperty(value = "1.待签字 2已签字 默认展示已签字")
    private Integer processed;

    @ApiModelProperty(value = "年份")
    private String year;

    @ApiModelProperty(value = "部门id")
    private String sqdeptid;
    @ApiModelProperty(value = "机构id")
    private String sqorgid;

    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer ifadmin;



    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSqdeptid() {
        return sqdeptid;
    }

    public void setSqdeptid(String sqdeptid) {
        this.sqdeptid = sqdeptid;
    }

    public String getSqorgid() {
        return sqorgid;
    }

    public void setSqorgid(String sqorgid) {
        this.sqorgid = sqorgid;
    }

    public Integer getIfadmin() {
        return ifadmin;
    }

    public void setIfadmin(Integer ifadmin) {
        this.ifadmin = ifadmin;
    }
}
