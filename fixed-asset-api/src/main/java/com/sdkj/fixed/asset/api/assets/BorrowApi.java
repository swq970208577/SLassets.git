package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.BorrowIssueExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.SelPrams;
import com.sdkj.fixed.asset.api.assets.in_vo.SignatureParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.BorrowIssue;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author niuliwei
 * @description 资产借用与归还
 * @date 2020/7/23 16:25
 */
@RequestMapping("/assets/borrow/api")
public interface BorrowApi {

    /**
     * 新增
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody BorrowIssueExtend param);

    /**
     * 分页查询
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    public BaseResultVo getAllPage(@RequestBody PageParams<SelPrams> param);
    /**
     * 根据id查询
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selById", method = RequestMethod.POST)
    public BaseResultVo selById(@Validated @RequestBody Params param);

    /**
     * 编辑
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/udpById", method = RequestMethod.POST)
    public BaseResultVo udpById(@Validated @RequestBody BorrowIssueExtend param);

    /**
     * 归还
     * @param issue
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/backById", method = RequestMethod.POST)
    public BaseResultVo backById(@RequestBody BorrowIssue issue);
    /**
     * 删除
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delById", method = RequestMethod.POST)
    public BaseResultVo delById(@Validated @RequestBody Params param);

    /**
     * 修改状态为已签字，2已签字
     */
    @ResponseBody
    @RequestMapping(value = "/updState", method = RequestMethod.POST)
    public BaseResultVo updState(@Validated @RequestBody SignatureParam param);

    /**
     * 打印
     * @return
     */
    @RequestMapping(value = "/print", method = RequestMethod.GET)
    public void print(@Param("ids") String ids) throws Exception;

}
