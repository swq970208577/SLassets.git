package com.sdkj.fixed.asset.api.system;

import com.sdkj.fixed.asset.api.system.out_vo.MenuTreeEntity;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.pojo.system.AuthAction;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @ClassName AuthActionApi
 * @Description 菜单管理
 * @Author 张欣
 * @Date 2020/7/22 11:25
 */
@RequestMapping({"/system/auth/api"})
public interface AuthActionApi {
    /**
     * 删除菜单
     * @param idParanEntity
     * @return
     */
    @RequestMapping(value = "/deleteMenu",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo deleteMenu(@RequestBody AuthAction idParanEntity);
    /**
     *获取菜单详情
     * @param idParanEntity
     * @return
     */
    @RequestMapping(value = "/queryMenuInfo",method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryMenuInfo(@RequestBody AuthAction idParanEntity) throws Exception;

    /**
     * 获取菜单树
     * @return
     */
    @RequestMapping(value = "/getMenuTrees" ,method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo<List<MenuTreeEntity>> getMenuTrees(String name);
}
