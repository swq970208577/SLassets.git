package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zhangjinfei
 * @Description //TODO 公司部门汇总表
 * @Date 2020/8/12 15:03
 */
public class CompanyReport {
//    ID

    private String id;
//    编码
    @Excel(name="编码",orderNum = "0")
    private String num;
//    名称
    @Excel(name="名称",orderNum = "0")
    private String name;
//    资产数量
    @Excel(name="资产数量",orderNum = "0")
    private Integer count;
//    资产金额
    @Excel(name="资产金额",orderNum = "0")
    private String totalAmount;

    private String treecode;
//    父id
    private String pid;
//     资产等级
    private String level;
//    1 公司 2部门
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTreecode() {
        return treecode;
    }

    public void setTreecode(String treecode) {
        this.treecode = treecode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


}
