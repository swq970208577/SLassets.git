package com.sdkj.fixed.asset.api.system.in_vo;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName DicCodeParam
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/22 9:05
 */
public class DicCodeParam {
    @NotBlank(message = "code不能为空")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
