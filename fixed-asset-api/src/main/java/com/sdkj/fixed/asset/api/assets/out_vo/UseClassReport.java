package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zhangjinfei
 * @Description //TODO 分类使用报表
 * @Date 2020/8/12 17:45
 */
public class UseClassReport {

    //    资产分类编码
    @Excel(name="类别编码",orderNum = "0")
    private String  num;
    //    资产分类名称
    @Excel(name="类别名称",orderNum = "1")
    private String  name;
    //    在用
    @Excel(name="在用",orderNum = "2")
    private Integer inUse;
    //    闲置
    @Excel(name="闲置",orderNum = "3")
    private Integer idle;
    //    待发放
//    @Excel(name="待发放",orderNum = "4")
    private Integer give;
    //    借用
    @Excel(name="借出",orderNum = "4")
    private Integer borrow;
    //    已报废
    @Excel(name="已报废",orderNum = "5")
    private Integer scrapped;
    //    调拨中
    @Excel(name="调拨中",orderNum = "6")
    private Integer transfer;
    //    维修中
    @Excel(name="维修中",orderNum = "7")
    private Integer repair;
    //    总计
    @Excel(name="总计",orderNum = "8")
    private Integer total;

    private String id;

    private String treecode;
    //    父id
    private String pid;
    //     资产等级
    private String level;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInUse() {
        return inUse;
    }

    public void setInUse(Integer inUse) {
        this.inUse = inUse;
    }

    public Integer getIdle() {
        return idle;
    }

    public void setIdle(Integer idle) {
        this.idle = idle;
    }

    public Integer getGive() {
        return give;
    }

    public void setGive(Integer give) {
        this.give = give;
    }

    public Integer getBorrow() {
        return borrow;
    }

    public void setBorrow(Integer borrow) {
        this.borrow = borrow;
    }

    public Integer getScrapped() {
        return scrapped;
    }

    public void setScrapped(Integer scrapped) {
        this.scrapped = scrapped;
    }

    public Integer getTransfer() {
        return transfer;
    }

    public void setTransfer(Integer transfer) {
        this.transfer = transfer;
    }

    public Integer getRepair() {
        return repair;
    }

    public void setRepair(Integer repair) {
        this.repair = repair;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTreecode() {
        return treecode;
    }

    public void setTreecode(String treecode) {
        this.treecode = treecode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
