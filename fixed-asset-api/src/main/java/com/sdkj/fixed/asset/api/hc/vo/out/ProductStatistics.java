package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * ProductStatistics
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/10 19:29
 */
public class ProductStatistics {

    private String id;

    private String categoryId;
    @Excel(name = "照片", orderNum = "2", width = 30)
    private String img;
    @Excel(name = "仓库", orderNum = "1", width = 20)
    private String categoryName;
    @Excel(name = "物品编码", orderNum = "3", width = 20)
    private String name;
    @Excel(name = "物品名称", orderNum = "4", width = 20)
    private String code;
    @Excel(name = "物品条码", orderNum = "5", width = 20)
    private String barCode;
    @Excel(name = "物品型号", orderNum = "6", width = 20)
    private String model;
    @Excel(name = "物品单位", orderNum = "7", width = 10)
    private String unit;
    @Excel(name = "入库数量", orderNum = "8", width = 10)
    private Integer inNum;
    @Excel(name = "入库单价", orderNum = "9", width = 10)
    private String inPrice;
    @Excel(name = "入库总价", orderNum = "10", width = 10)
    private String inTotalPrice;
    @Excel(name = "出库数量", orderNum = "11", width = 10)
    private Integer outNum;
    @Excel(name = "出库单价", orderNum = "12", width = 10)
    private String outPrice;
    @Excel(name = "出库总价", orderNum = "13", width = 10)
    private String outTotalPrice;
    private Integer num;
    @Excel(name = "结存数量", orderNum = "14", width = 10)
    private Integer totalNum;
    @Excel(name = "结存单价", orderNum = "15", width = 10)
    private String price;
    @Excel(name = "结存总价", orderNum = "16", width = 10)
    private String totalPrice;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getInNum() {
        return inNum;
    }

    public void setInNum(Integer inNum) {
        this.inNum = inNum;
    }

    public String getInPrice() {
        return inPrice;
    }

    public void setInPrice(String inPrice) {
        this.inPrice = inPrice;
    }

    public String getInTotalPrice() {
        return inTotalPrice;
    }

    public void setInTotalPrice(String inTotalPrice) {
        this.inTotalPrice = inTotalPrice;
    }

    public Integer getOutNum() {
        return outNum;
    }

    public void setOutNum(Integer outNum) {
        this.outNum = outNum;
    }

    public String getOutPrice() {
        return outPrice;
    }

    public void setOutPrice(String outPrice) {
        this.outPrice = outPrice;
    }

    public String getOutTotalPrice() {
        return outTotalPrice;
    }

    public void setOutTotalPrice(String outTotalPrice) {
        this.outTotalPrice = outTotalPrice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
