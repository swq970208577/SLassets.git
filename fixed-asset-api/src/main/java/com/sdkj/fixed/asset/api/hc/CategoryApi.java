package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.assets.in_vo.AssroParam;
import com.sdkj.fixed.asset.api.hc.vo.in.ConsumeParams;
import com.sdkj.fixed.asset.api.hc.vo.in.MablesParams;
import com.sdkj.fixed.asset.api.hc.vo.in.StockParams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.Category;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author 史晨星
 * @ClassName: CategoryApi
 * @Description: 仓库管理
 * @date 2020年7月21日
 */
@RequestMapping("/hc/category/api")
public interface CategoryApi {


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody Category entity) throws Exception;

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody Category entity) throws Exception;


    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo view(@RequestBody Category entity) throws Exception;

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody Category entity) throws Exception;

    /**
     * 查询机构下可用仓库
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getAvailableCategoryByOrgId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAvailableCategoryByOrgId() throws Exception;


    /**
     * 查询用户权限下可用仓库
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getAvailableCategoryByUserId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getAvailableCategoryByUserId() throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody PageParams<Category> params) throws Exception;

    /**
     * 导出
     *
     * @param companyId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(String companyId, HttpServletResponse response) throws Exception;

    /**
     * 即时库存查询
     */
    @PostMapping("/getStockList")
    @ResponseBody
    public BaseResultVo getStockList(@RequestBody PageParams<StockParams> params);

    /**
     * 即时库存导出
     *
     * @param
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/StockExport", method = RequestMethod.GET)
    @ResponseBody
    public void Export(String userid,String orgid,String name,String ckid, HttpServletResponse response) throws Exception;

    /**
     * 耗材领用查询导出
     *
     * @param
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/MablExport", method = RequestMethod.GET)
    @ResponseBody
    public void MablExport(String userid,String orgid,String squserid,String sqdeptid,String sqorgid,String Starttime,String Endtime, HttpServletResponse response) throws Exception;

    /**
     * 耗材领用查询
     */
    @PostMapping("/getMablesList")
    @ResponseBody
    public BaseResultVo getMablesList(@RequestBody PageParams<MablesParams> params);

    /**
     * 耗材领用表
     */
    @PostMapping("/getConsumeList")
    @ResponseBody
    public BaseResultVo getConsumeList(@RequestBody PageParams<ConsumeParams> params);

    /**
     * 维修信息登记-打印
     * @return
     */
    @RequestMapping(value = "/MablPrint", method = RequestMethod.GET)
    public void MablPrint(MablesParams Params ) throws IOException;
}

