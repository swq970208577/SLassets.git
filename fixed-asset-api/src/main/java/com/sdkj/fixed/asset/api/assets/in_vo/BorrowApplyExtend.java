package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.BorrowApply;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/28 15:33
 */
public class BorrowApplyExtend extends BorrowApply {

    /**
     * 申请资产集合
     */
    @NotEmpty
    private List<BorrowApplyAssetsExtend> applyAssetList = new ArrayList<>();
    /**
     * 审批发放资产
     */
    private List<BorrowIssueExtend> issueExtendList = new ArrayList<>();

    public List<BorrowApplyAssetsExtend> getApplyAssetList() {
        return applyAssetList;
    }

    public void setApplyAssetList(List<BorrowApplyAssetsExtend> applyAssetList) {
        this.applyAssetList = applyAssetList;
    }

    public List<BorrowIssueExtend> getIssueExtendList() {
        return issueExtendList;
    }

    public void setIssueExtendList(List<BorrowIssueExtend> issueExtendList) {
        this.issueExtendList = issueExtendList;
    }
}
