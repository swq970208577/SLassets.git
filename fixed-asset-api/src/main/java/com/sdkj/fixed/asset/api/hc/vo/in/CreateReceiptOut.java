package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * CreateReceiptOut
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/31 9:55
 */
public class CreateReceiptOut {
    private String categoryId;
    private String productId;
    @Size(max = 32, message = "子单id-最大长度:32")
    @NotNull(message = "子单id-不能为空")
    private String id;
    private Integer num;

    private String orgId;

    private String cuser;

    private List<CreateReceiptOut> products;

    public List<CreateReceiptOut> getProducts() {
        return products;
    }

    public void setProducts(List<CreateReceiptOut> products) {
        this.products = products;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }
}
