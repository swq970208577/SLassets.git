package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.sdkj.fixed.asset.pojo.assets.AssetLog;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 资产履历
 * @Date 2020/7/27 17:44
 */
public class WarehouseLogResult implements Serializable {

    /**
     * 资产ID
     */
    private String assetId;

    /**
     * 资产条码
     */
    @Excel(name="资产条码",orderNum = "2",needMerge = true)
    private String assetCode;

    /**
     * 资产名称
     */
    @Excel(name="资产名称",orderNum = "3",needMerge = true)
    private String assetName;

    /**
     * 资产类别ID
     */
    private String assetClassId;

    /**
     * 标准型号=规格型号ID
     */
    private String standardModel;

    /**
     * 规格型号
     */
    @Excel(name="规格型号",orderNum = "5",needMerge = true)
    private String specificationModel;

    /**
     * 计量单位
     */
    @Excel(name="计量单位",orderNum = "7",needMerge = true)
    private String unitMeasurement;

    /**
     * SN号
     */
    @Excel(name="SN号",orderNum = "6",needMerge = true)
    private String snNumber;

    /**
     * 来源
     */
    @Excel(name="来源",orderNum = "19",replace={"购入_1","自建_2","租赁_3","捐赠_4","其他_5","内部购入_6"},needMerge = true)
    private String source;

    /**
     * 购入时间
     */
    @Excel(name="购入时间",orderNum = "16",needMerge = true)
    private String purchaseTime;

    /**
     * 所属公司ID
     */
    private String company;

    /**
     * 金额
     */
    @Excel(name="金额",orderNum = "8",needMerge = true)
    private String amount;

    /**
     * 管理员
     */
    @Excel(name="管理员",orderNum = "14",needMerge = true)
    private String admin;

    /**
     * 使用公司ID
     */
    private String useCompany;

    /**
     * 使用部门
     */
    private String useDepartment;

    /**
     * 使用人ID
     */
    private String handlerUserId;

    /**
     * 使用人
     */
    @Excel(name="使用人",orderNum = "11",needMerge = true)
    private String handlerUser;

    /**
     * 使用期限(月)
     */
    @Excel(name="使用期限",orderNum = "18",needMerge = true)
    private String serviceLife;

    /**
     * 区域
     */
    private String area;

    /**
     * 存放地点
     */
    @Excel(name="存放地点",orderNum = "13",needMerge = true)
    private String storageLocation;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态
     */
    @Excel(name="状态",orderNum = "0",needMerge = true)
    private String state;

    /**
     * 照片
     */
    @Excel(name="照片",orderNum = "1",needMerge = true)
    private String photo;

    /**
     * 创建时间
     */
    @Excel(name="创建时间",orderNum = "21",needMerge = true)
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改时间
     */

    private String updateTime;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 是否删除
     */
    private Integer isDelete;
    @Excel(name="资产类别",orderNum = "4",needMerge = true)
    private String assetClassName;
    @Excel(name="区域",orderNum = "12",needMerge = true)
    private String areaName;
    @Excel(name="所属公司",orderNum = "15",needMerge = true)
    private String companyName;
    @Excel(name="使用公司",orderNum = "9",needMerge = true)
    private String useCompanyName;
    @Excel(name="使用部门",orderNum = "10",needMerge = true)
    private String useDeptName;
    @Excel(name="供应商",orderNum = "17",needMerge = true)
    private String supplierName;
    @Excel(name="创建人",orderNum = "20" ,needMerge = true)
    private String createUserName;
    @ExcelCollection(name = "处理明细",orderNum = "22")
    private List<AssetLog> logList;
    private static final long serialVersionUID = 1L;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetClassId() {
        return assetClassId;
    }

    public void setAssetClassId(String assetClassId) {
        this.assetClassId = assetClassId;
    }

    public String getStandardModel() {
        return standardModel;
    }

    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel;
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }

    public String getSnNumber() {
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getUseCompany() {
        return useCompany;
    }

    public void setUseCompany(String useCompany) {
        this.useCompany = useCompany;
    }

    public String getUseDepartment() {
        return useDepartment;
    }

    public void setUseDepartment(String useDepartment) {
        this.useDepartment = useDepartment;
    }

    public String getHandlerUserId() {
        return handlerUserId;
    }

    public void setHandlerUserId(String handlerUserId) {
        this.handlerUserId = handlerUserId;
    }

    public String getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser;
    }

    public String getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(String serviceLife) {
        this.serviceLife = serviceLife;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public List<AssetLog> getLogList() {
        return logList;
    }

    public void setLogList(List<AssetLog> logList) {
        this.logList = logList;
    }
}
