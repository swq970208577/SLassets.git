package com.sdkj.fixed.asset.api.system;

import cn.jpush.api.common.resp.BaseResult;
import com.sdkj.fixed.asset.api.system.in_vo.RegisterParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/8 13:15
 */
@RequestMapping({"/system/register/api"})
public interface RegisterApi {

    /**
     * 注册
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public BaseResultVo register(@RequestBody RegisterParam registerParam) throws Exception;

    /**
     * 获取验证码
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/authCode", method = RequestMethod.GET)
    public BaseResultVo authCode(@RequestParam("email") @NotBlank(message = "邮箱不能为空") @Email(message = "邮箱格式不正确") String email) throws Exception;
}
