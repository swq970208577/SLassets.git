package com.sdkj.fixed.asset.api.assets.out_vo.report;

/**
 * @ClassName AssetStatus
 * @Description 首页资产状态占比
 * @Author 张欣
 * @Date 2020/8/11 9:57
 */
public class AssetAllStatus {
    /**
     * 总数
     */
   private String totalCount ;
    /**
     * 在用资产
     */
   private String usedCount;
    /**
     * 闲置资产
     */
   private String freeCount;
    /**
     * 借用
     */
   private String borrowCount;
    /**
     * 调拨中
     */
   private String transferCount;
    /**
     * 维修中
     */
   private String repairCount;
    /**
     * 清理报废
     */
    private String cleanCount;
    /**
     * 待发放
     */
    private String toBeIssuedCount;
    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getUsedCount() {
        return usedCount;
    }

    public void setUsedCount(String usedCount) {
        this.usedCount = usedCount;
    }

    public String getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(String freeCount) {
        this.freeCount = freeCount;
    }

    public String getBorrowCount() {
        return borrowCount;
    }

    public void setBorrowCount(String borrowCount) {
        this.borrowCount = borrowCount;
    }

    public String getTransferCount() {
        return transferCount;
    }

    public void setTransferCount(String transferCount) {
        this.transferCount = transferCount;
    }

    public String getRepairCount() {
        return repairCount;
    }

    public void setRepairCount(String repairCount) {
        this.repairCount = repairCount;
    }

    public String getCleanCount() {
        return cleanCount;
    }

    public void setCleanCount(String cleanCount) {
        this.cleanCount = cleanCount;
    }

    public String getToBeIssuedCount() {
        return toBeIssuedCount;
    }

    public void setToBeIssuedCount(String toBeIssuedCount) {
        this.toBeIssuedCount = toBeIssuedCount;
    }
}
