package com.sdkj.fixed.asset.api.assets.out_vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author zhaozheyu
 * @Description //我的审批基本信息反参
 * @Date 2020/7/23 16:30
 */
public class ApprovalMation implements Serializable {
    @ApiModelProperty(value = "审批订单类型")
    private String cut;
    @ApiModelProperty(value = "子单id")
    private String id;
    @ApiModelProperty(value = "主单id")
    private String order;
    @ApiModelProperty(value = "子单申请数量")
    private Integer mount;
    @ApiModelProperty(value = "子单审批状态1审批中2已批准3已驳回 ")
    private Integer state;
    @ApiModelProperty(value = "子单类型")
    private Integer dig;
    @ApiModelProperty(value = "物品/资产图片")
    private String pic;
    @ApiModelProperty(value = "物品/资产编码")
    private String code;
    @ApiModelProperty(value = "物品/资产名称")
    private String name;
    @ApiModelProperty(value = "物品/资产规格型号")
    private String model;
    @ApiModelProperty(value = "物品/资产计量单位")
    private String unit;
    @ApiModelProperty(value = "物品/资产金额")
    private String money;
    @ApiModelProperty(value = "申请人名字")
    private String person;
    @ApiModelProperty(value = "申请人id")
    private String personid;
    @ApiModelProperty(value = "资产/型号/耗材id")
    private String zcid;
    @ApiModelProperty(value = "1资产2型号")
    private String type;
    @ApiModelProperty(value = "耗材库存")
    private Integer stock;
    @ApiModelProperty(value = "备注")
    private String comment;






    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Integer getDig() {
        return dig;
    }

    public void setDig(Integer dig) {
        this.dig = dig;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getZcid() {
        return zcid;
    }

    public void setZcid(String zcid) {
        this.zcid = zcid;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getMount() {
        return mount;
    }

    public void setMount(Integer mount) {
        this.mount = mount;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
