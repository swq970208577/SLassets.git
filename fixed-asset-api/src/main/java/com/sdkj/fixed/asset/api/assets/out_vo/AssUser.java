package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * AssUser
 *
 * @author zhaozheyu
 * @Description
 * @date 2020/8/19 16:13
 */
public class AssUser {
    private String type;
    private Integer dia;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }
}
