package com.sdkj.fixed.asset.api.login.out_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 公司下拉框
 * @author 张欣
 */
public class CompanyBaseListResult {
    /**
      **公司id
      */
    @ApiModelProperty(value = "公司id")
    private String companyId;
    /**
     * 公司名称
     */
    @ApiModelProperty(value = "公司名称")
    private String companyName;
    /**
     * 父id
     */
    @ApiModelProperty(value = "父id")
    private String parentId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
