package com.sdkj.fixed.asset.api.assets.in_vo;

import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseHistoryResult;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseResult;
import com.sdkj.fixed.asset.pojo.assets.BorrowIssue;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/23 17:26
 */
public class BorrowIssueExtend extends BorrowIssue {

    /**
     * 资产id
     */

    @NotEmpty
    private List<String> assetIdList = new ArrayList<>();
    /**
     * 资产基本信息
     */
    @ExcelCollection(name = "资产明细", orderNum = "3")
    List<WarehouseHistoryResult> results = new ArrayList<>();

    List<WarehouseResult> res = new ArrayList<>();

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }

    public List<WarehouseHistoryResult> getResults() {
        return results;
    }

    public void setResults(List<WarehouseHistoryResult> results) {
        this.results = results;
    }

    public List<WarehouseResult> getRes() {
        return res;
    }

    public void setRes(List<WarehouseResult> res) {
        this.res = res;
    }
}
