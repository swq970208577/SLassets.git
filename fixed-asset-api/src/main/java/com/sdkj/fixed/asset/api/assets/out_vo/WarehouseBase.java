package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zhangjinfei
 * @Description //TODO 公共使用的类
 * @Date 2020/7/22 17:20
 */
public class WarehouseBase {

    private String photo;
    @Excel(name="资产条码",orderNum = "0",needMerge = true)
    private String assetCode;
    @Excel(name="资产类别",orderNum = "1",needMerge = true)
    private String assetClassName;
    @Excel(name="资产名称",orderNum = "2",needMerge = true)
    private String assetName;
    @Excel(name="规格型号",orderNum = "3",needMerge = true)
    private String specificationModel;
    @Excel(name="SN号",orderNum = "4",needMerge = true)
    private String snNumber;
    @Excel(name="金额",orderNum = "5",needMerge = true)
    private String amount;
    @Excel(name="使用公司",orderNum = "6",needMerge = true)
    private String useCompanyName;
    @Excel(name="使用部门",orderNum = "7",needMerge = true)
    private String useDeptName;
    @Excel(name="使用人",orderNum = "8",needMerge = true)
    private String handlerUser;
    @Excel(name="管理员",orderNum = "9",needMerge = true)
    private String admin;
    @Excel(name="所属公司",orderNum = "10",needMerge = true)
    private String companyName;
    @Excel(name="区域",orderNum = "11",needMerge = true)
    private String areaName;
    @Excel(name="存放地点",orderNum = "12",needMerge = true)
    private String storageLocation;
    private static final long serialVersionUID = 1L;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getSnNumber() {
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
