package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * Statistics
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/10 11:10
 */
public class Statistics {
    private String id;
    @Excel(name = "耗材分类", orderNum = "1", width = 40)
    private String typeName;
    private String type;
    @Excel(name = "入库数量", orderNum = "2", width = 15)
    private Integer inNum;
    @Excel(name = "入库金额", orderNum = "3", width = 15)
    private String inTotalPrice;
    @Excel(name = "出库数量", orderNum = "4", width = 15)
    private Integer outNum;
    @Excel(name = "出库金额", orderNum = "5", width = 15)
    private String outTotalPrice;
    @Excel(name = "结存数量", orderNum = "6", width = 15)
    private Integer totalNum;
    @Excel(name = "结存金额", orderNum = "7", width = 15)
    private String totalPrice;

    private String pid;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    private Integer num;

    private String tempTotalPrice;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getTempTotalPrice() {
        return tempTotalPrice;
    }

    public void setTempTotalPrice(String tempTotalPrice) {
        this.tempTotalPrice = tempTotalPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getInNum() {
        return inNum;
    }

    public void setInNum(Integer inNum) {
        this.inNum = inNum;
    }

    public String getInTotalPrice() {
        return inTotalPrice;
    }

    public void setInTotalPrice(String inTotalPrice) {
        this.inTotalPrice = inTotalPrice;
    }

    public Integer getOutNum() {
        return outNum;
    }

    public void setOutNum(Integer outNum) {
        this.outNum = outNum;
    }

    public String getOutTotalPrice() {
        return outTotalPrice;
    }

    public void setOutTotalPrice(String outTotalPrice) {
        this.outTotalPrice = outTotalPrice;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

}
