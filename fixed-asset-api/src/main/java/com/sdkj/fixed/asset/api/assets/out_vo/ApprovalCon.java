package com.sdkj.fixed.asset.api.assets.out_vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author zhaozheyu
 * @Description //我的审批基本信息审批情况反参
 * @Date 2020/7/23 16:30
 */
public class ApprovalCon implements Serializable {
    @ApiModelProperty(value = "审批订单类型")
    private String cut;
    @ApiModelProperty(value = "子单审批状态1审批中2已批准3已驳回 ")
    private Integer state;
    @ApiModelProperty(value = "物品/资产编码")
    private String code;
    @ApiModelProperty(value = "物品/资产名称")
    private String name;
    @ApiModelProperty(value = "审批人")
    private String person;
    @ApiModelProperty(value = "发放人")
    private String sqpersonid;
    @ApiModelProperty(value = "审批人")
    private String sqpersonname;

    @ApiModelProperty(value = "签字状态:1待签字  2 已签字 ")
    private Integer signstate;


    public String getSqpersonid() {
        return sqpersonid;
    }

    public void setSqpersonid(String sqpersonid) {
        this.sqpersonid = sqpersonid;
    }

    public String getSqpersonname() {
        return sqpersonname;
    }

    public void setSqpersonname(String sqpersonname) {
        this.sqpersonname = sqpersonname;
    }

    public Integer getSignstate() {
        return signstate;
    }

    public void setSignstate(Integer signstate) {
        this.signstate = signstate;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}
