package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetSupplier;

/**
 * @Author zhangjinfei
 * @Description //TODO 维保信息
 * @Date 2020/7/23 16:30
 */
public class AssetSupplierResult extends AssetSupplier {
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系方式
     */
    private String contactinfo;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactinfo() {
        return contactinfo;
    }

    public void setContactinfo(String contactinfo) {
        this.contactinfo = contactinfo;
    }
}
