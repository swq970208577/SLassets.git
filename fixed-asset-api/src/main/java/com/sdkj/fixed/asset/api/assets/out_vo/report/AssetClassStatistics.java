package com.sdkj.fixed.asset.api.assets.out_vo.report;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @ClassName AssetClassStatistics
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/7 16:43
 */
public class AssetClassStatistics {
    /**
     * 类别id
     */
    private String id;
    /**
     * 类别编码
     */
    @Excel(orderNum = "0",name="类别编码")
    private String code;
    /**
     * 类别名称
     */
    @Excel(orderNum = "1",name="类别名称")
    private String name;
    /**
     * 资产数量
     */
    @Excel(orderNum = "2",name="资产数量")
    private String total;
    /**
     * 资产金额
     */
    @Excel(orderNum = "3",name="资产金额")
    private String totalAmount;
    /**
     * 资产树结构
     */
    private String treecode;
    /**
     * 父id
     */
    private String pid;
    /**
     * 资产等级
     */
    @Excel(orderNum = "4",name="资产层级")
    private String level;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTreecode() {
        return treecode;
    }

    public void setTreecode(String treecode) {
        this.treecode = treecode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
