package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * GetProductUsedtotalPrice
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/13 10:35
 */
public class GetProductUsedTotalPrice {
    @Excel(name = "1月", orderNum = "1", width = 10)
    private String totalPrice1;
    @Excel(name = "2月", orderNum = "2", width = 10)
    private String totalPrice2;
    @Excel(name = "3月", orderNum = "3", width = 10)
    private String totalPrice3;
    @Excel(name = "4月", orderNum = "4", width = 10)
    private String totalPrice4;
    @Excel(name = "5月", orderNum = "5", width = 10)
    private String totalPrice5;
    @Excel(name = "6月", orderNum = "6", width = 10)
    private String totalPrice6;
    @Excel(name = "7月", orderNum = "7", width = 10)
    private String totalPrice7;
    @Excel(name = "8月", orderNum = "8", width = 10)
    private String totalPrice8;
    @Excel(name = "9月", orderNum = "9", width = 10)
    private String totalPrice9;
    @Excel(name = "10月", orderNum = "10", width = 10)
    private String totalPrice10;
    @Excel(name = "11月", orderNum = "11", width = 10)
    private String totalPrice11;
    @Excel(name = "12月", orderNum = "12", width = 10)
    private String totalPrice12;
    @Excel(name = "合计", orderNum = "13", width = 10)
    private String totalPriceTotal;

    public String getTotalPrice1() {
        return totalPrice1;
    }

    public void setTotalPrice1(String totalPrice1) {
        this.totalPrice1 = totalPrice1;
    }

    public String getTotalPrice2() {
        return totalPrice2;
    }

    public void setTotalPrice2(String totalPrice2) {
        this.totalPrice2 = totalPrice2;
    }

    public String getTotalPrice3() {
        return totalPrice3;
    }

    public void setTotalPrice3(String totalPrice3) {
        this.totalPrice3 = totalPrice3;
    }

    public String getTotalPrice4() {
        return totalPrice4;
    }

    public void setTotalPrice4(String totalPrice4) {
        this.totalPrice4 = totalPrice4;
    }

    public String getTotalPrice5() {
        return totalPrice5;
    }

    public void setTotalPrice5(String totalPrice5) {
        this.totalPrice5 = totalPrice5;
    }

    public String getTotalPrice6() {
        return totalPrice6;
    }

    public void setTotalPrice6(String totalPrice6) {
        this.totalPrice6 = totalPrice6;
    }

    public String getTotalPrice7() {
        return totalPrice7;
    }

    public void setTotalPrice7(String totalPrice7) {
        this.totalPrice7 = totalPrice7;
    }

    public String getTotalPrice8() {
        return totalPrice8;
    }

    public void setTotalPrice8(String totalPrice8) {
        this.totalPrice8 = totalPrice8;
    }

    public String getTotalPrice9() {
        return totalPrice9;
    }

    public void setTotalPrice9(String totalPrice9) {
        this.totalPrice9 = totalPrice9;
    }

    public String getTotalPrice10() {
        return totalPrice10;
    }

    public void setTotalPrice10(String totalPrice10) {
        this.totalPrice10 = totalPrice10;
    }

    public String getTotalPrice11() {
        return totalPrice11;
    }

    public void setTotalPrice11(String totalPrice11) {
        this.totalPrice11 = totalPrice11;
    }

    public String getTotalPrice12() {
        return totalPrice12;
    }

    public void setTotalPrice12(String totalPrice12) {
        this.totalPrice12 = totalPrice12;
    }

    public String getTotalPriceTotal() {
        return totalPriceTotal;
    }

    public void setTotalPriceTotal(String totalPriceTotal) {
        this.totalPriceTotal = totalPriceTotal;
    }
}
