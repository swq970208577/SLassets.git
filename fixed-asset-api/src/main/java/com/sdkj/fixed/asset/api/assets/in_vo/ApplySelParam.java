package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/29 10:44
 */
public class ApplySelParam {

    @NotBlank(message = "查询时间不能为空，1一个月内,2.3个月内,3.半年内")
    private String selTimeType;

    @NotNull(message = "审批状态不能为空，1审批中2已批准3已驳回4已发放")
    private Integer state;

    public String getSelTimeType() {
        return selTimeType;
    }

    public void setSelTimeType(String selTimeType) {
        this.selTimeType = selTimeType;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
