package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.Transfer;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 调拨
 * @Date 2020/7/30 14:01
 */
public class TransferResult extends Transfer {

    private String  transferOutAdminName;
    private String  transferInAdminName;
    private String  transferOutCompanyName;
    private String  transferInCompanyName;
    private String  transferInDeptName;
    private String  transferInAreaName;
    private List<WarehouseHistoryResult> warehouseList;

    public String getTransferInDeptName() {
        return transferInDeptName;
    }

    public void setTransferInDeptName(String transferInDeptName) {
        this.transferInDeptName = transferInDeptName;
    }

    public String getTransferInAreaName() {
        return transferInAreaName;
    }

    public void setTransferInAreaName(String transferInAreaName) {
        this.transferInAreaName = transferInAreaName;
    }

    public String getTransferOutAdminName() {
        return transferOutAdminName;
    }

    public void setTransferOutAdminName(String transferOutAdminName) {
        this.transferOutAdminName = transferOutAdminName;
    }

    public String getTransferInAdminName() {
        return transferInAdminName;
    }

    public void setTransferInAdminName(String transferInAdminName) {
        this.transferInAdminName = transferInAdminName;
    }

    public String getTransferOutCompanyName() {
        return transferOutCompanyName;
    }

    public void setTransferOutCompanyName(String transferOutCompanyName) {
        this.transferOutCompanyName = transferOutCompanyName;
    }

    public String getTransferInCompanyName() {
        return transferInCompanyName;
    }

    public void setTransferInCompanyName(String transferInCompanyName) {
        this.transferInCompanyName = transferInCompanyName;
    }

    public List<WarehouseHistoryResult> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<WarehouseHistoryResult> warehouseList) {
        this.warehouseList = warehouseList;
    }
}
