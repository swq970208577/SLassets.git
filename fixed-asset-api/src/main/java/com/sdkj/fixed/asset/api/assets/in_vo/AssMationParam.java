package com.sdkj.fixed.asset.api.assets.in_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * AssMationParam
 *
 * @author zhaozheyu
 * @Description     我的审批基本信息入参
 * @date 2020/7/31 9:40
 */
public class AssMationParam {
    @ApiModelProperty(value = "订单类型:1.申领资产2.申借3.申退4.申领物品")
    private Integer dig;
    @ApiModelProperty(value = "当前登录人id")
    private String userid;
    @ApiModelProperty(value = "主订单id")
    private String orderid;
    @ApiModelProperty(value = "处理状态(1.待处理2.已处理)")
    private Integer processed;
    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer ifadmin;




    public Integer getIfadmin() {
        return ifadmin;
    }

    public void setIfadmin(Integer ifadmin) {
        this.ifadmin = ifadmin;
    }

    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public Integer getDig() {
        return dig;
    }

    public void setDig(Integer dig) {
        this.dig = dig;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }
}
