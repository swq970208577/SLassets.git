package com.sdkj.fixed.asset.api.system.out_vo;


import com.sdkj.fixed.asset.pojo.system.OrgManagement;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @ClassName UserDataAuth
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/23 16:03
 */
public class UserHasDataAuthResult {


    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String userId;
    /**
     * 公司部门授权
     */
    private List<OrgDataAuth> orgList;
    /**
     * 资产类别
     */
    private List<AssestClass> assetClassList;
    /**
     * 区域
     */
    private List<String> regionList;
    /**
     * 仓库
     */
    private List<String> warehouseList;

    public List<OrgDataAuth> getOrgList() {
        return orgList;
    }

    public void setOrgList(List<OrgDataAuth> orgList) {
        this.orgList = orgList;
    }

    public List<AssestClass> getAssetClassList() {
        return assetClassList;
    }

    public void setAssetClassList(List<AssestClass> assetClassList) {
        this.assetClassList = assetClassList;
    }

    public List<String> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<String> regionList) {
        this.regionList = regionList;
    }

    public List<String> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<String> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
