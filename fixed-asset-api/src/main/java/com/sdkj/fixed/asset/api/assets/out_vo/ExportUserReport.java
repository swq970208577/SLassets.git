package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/12 13:05
 */
public class ExportUserReport extends UserInfo{
    
    @Excel(name="领用方式",orderNum = "6")
    private String useModel;
    @Excel(name="借用单号",orderNum = "7")
    private String number;
    @Excel(name="借用时间",orderNum = "8")
    private String lendDate;
    @Excel(name="资产条码",orderNum = "9")
    private String assetCode;
    @Excel(name="资产类别",orderNum = "10")
    private String assetClassName;
    @Excel(name="资产名称",orderNum = "11")
    private String assetName;
    @Excel(name="规格型号",orderNum = "12")
    private String specificationModel;
    @Excel(name="SN号",orderNum = "13")
    private String snNumber;
    @Excel(name="金额",orderNum = "14")
    private String amount;
    @Excel(name="使用公司",orderNum = "15")
    private String useCompanyName;
    @Excel(name="使用部门",orderNum = "16")
    private String useDeptName;
    @Excel(name="使用人",orderNum = "17")
    private String handlerUser;
    @Excel(name="管理员",orderNum = "18")
    private String admin;
    @Excel(name="所属公司",orderNum = "19")
    private String assetCompanyName;
    @Excel(name="区域",orderNum = "20")
    private String areaName;
    @Excel(name="存放地点",orderNum = "21")
    private String storageLocation;

    public String getUseModel() {
        return useModel;
    }

    public void setUseModel(String useModel) {
        this.useModel = useModel;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLendDate() {
        return lendDate;
    }

    public void setLendDate(String lendDate) {
        this.lendDate = lendDate;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getSnNumber() {
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getAssetCompanyName() {
        return assetCompanyName;
    }

    public void setAssetCompanyName(String assetCompanyName) {
        this.assetCompanyName = assetCompanyName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }
}
