package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.BorrowApplyAssets;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/28 15:33
 */
public class BorrowApplyAssetsExtend extends BorrowApplyAssets {

    private String assetName;
    private String assetCode;
    private String assetModel;
    private String unitMeasurement;

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(String assetModel) {
        this.assetModel = assetModel;
    }

    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }
}
