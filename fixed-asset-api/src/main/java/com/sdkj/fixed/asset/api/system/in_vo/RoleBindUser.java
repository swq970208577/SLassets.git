package com.sdkj.fixed.asset.api.system.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @ClassName RoleBindUser
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 14:55
 */
public class RoleBindUser {
    /**
     * 角色id
     */
    @NotBlank(message = "角色id不能为空")
    @Size(max = 32,message = "角色id长度不匹配")
    private String roleId;
    /**
     * 用户id
     */
    private List<String> userIds;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
