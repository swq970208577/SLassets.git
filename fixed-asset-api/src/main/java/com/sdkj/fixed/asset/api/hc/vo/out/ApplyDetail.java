package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * ApplyDetail
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/12 16:26
 */
public class ApplyDetail {

    private String productName;

    private String number;

    private String approveUser;

    private String productCode;

    private Integer sign;

    private String approveTime;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }
}
