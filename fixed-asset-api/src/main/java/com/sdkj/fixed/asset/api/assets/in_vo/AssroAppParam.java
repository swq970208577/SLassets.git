package com.sdkj.fixed.asset.api.assets.in_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * AssroParam
 *
 * @author zhaozheyu
 * @Description     我的审批app查询入参
 * @date 2020/7/31 9:40
 */
public class AssroAppParam {
    @ApiModelProperty(value = "处理状态(1.待处理2.已处理)")
    private Integer processed;
    @ApiModelProperty(value = "审批单号")
    private Integer dig;
    @ApiModelProperty(value = "当前登录人id")
    private String userid;
    @ApiModelProperty(value = "单据类型")
    private String cut;
    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer ifadmin;
    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer admin;
    @ApiModelProperty(value = "机构id")
    private String orgid;


    @ApiModelProperty(value = "开始时间")
    private String startdate;
    @ApiModelProperty(value = "开始时间")
    private String enddate;


    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public Integer getDig() {
        return dig;
    }

    public void setDig(Integer dig) {
        this.dig = dig;
    }

    public Integer getAdmin() {
        return admin;
    }

    public void setAdmin(Integer admin) {
        this.admin = admin;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public Integer getIfadmin() {
        return ifadmin;
    }

    public void setIfadmin(Integer ifadmin) {
        this.ifadmin = ifadmin;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }






    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
