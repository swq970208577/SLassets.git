package com.sdkj.fixed.asset.api.system.in_vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;

/**
 * @author niuliwei
 * @description 注册参数
 * @date 2021/4/8 13:20
 */
@ApiModel("注册参数")
public class RegisterParam {

    @Email(message = "邮箱格式不正确")
    @NotBlank(message = "邮箱不能为空")
    @ApiModelProperty(value = "邮箱")
    private String email;
    @Pattern(regexp = "^1(3|4|5|6|7|8|9)\\d{9}$",message = "手机号码格式不正确")
    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号")
    private String tel;
    @Pattern(regexp = "^\\d{4}$",message = "验证码格式不正确")
    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value = "验证码")
    private String authCode;
    @Size(max = 20, message = "公司名称最大长度20")
    @NotBlank(message = "公司名称不能为空")
    @ApiModelProperty(value = "公司名称")
    private String orgName;
//    @Pattern(regexp = "^[123]$", message = "版本不正确")
//    @NotNull(message = "版本不能为空")
//    @ApiModelProperty(value = "版本类型")
//    private Integer version;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}
