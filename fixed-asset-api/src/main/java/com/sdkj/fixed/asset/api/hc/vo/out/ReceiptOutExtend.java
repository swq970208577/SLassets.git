package com.sdkj.fixed.asset.api.hc.vo.out;

import java.util.List;

/**
 * ReceiptOutExtend
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/7 14:01
 */
public class ReceiptOutExtend {
    private String id;

    private Integer type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String number;

    private String categoryName;

    private String bussinessDate;

    private String receiveUser;

    private String receiveDept;

    private String receiveOrg;

    private String cuser;

    private String ctime;

    private String comment;

    private List<ReceiptMoveProductExtend> receiptMoveProductList;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<ReceiptMoveProductExtend> getReceiptMoveProductList() {
        return receiptMoveProductList;
    }

    public void setReceiptMoveProductList(List<ReceiptMoveProductExtend> receiptMoveProductList) {
        this.receiptMoveProductList = receiptMoveProductList;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getReceiveDept() {
        return receiveDept;
    }

    public void setReceiveDept(String receiveDept) {
        this.receiveDept = receiveDept;
    }

    public String getReceiveOrg() {
        return receiveOrg;
    }

    public void setReceiveOrg(String receiveOrg) {
        this.receiveOrg = receiveOrg;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
