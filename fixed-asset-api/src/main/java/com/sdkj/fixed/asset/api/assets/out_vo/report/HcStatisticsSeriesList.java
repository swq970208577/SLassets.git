package com.sdkj.fixed.asset.api.assets.out_vo.report;

import java.util.List;

/**
 * @ClassName HcStatisticsSeriesList
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/11 17:04
 */
public class HcStatisticsSeriesList {
    private String name;
    private String type;
    private String stack;
    private String color;
    private String[]  data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStack() {
        return stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }
}
