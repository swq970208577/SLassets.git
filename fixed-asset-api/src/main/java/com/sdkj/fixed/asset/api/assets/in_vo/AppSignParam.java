package com.sdkj.fixed.asset.api.assets.in_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * AppSignParam
 *
 * @author zhaozheyu
 * @Description APP签字入参
 * @date 2020/8/7 15:38
 */
public class AppSignParam {
    @ApiModelProperty(value = "当前登录人id")
    private String userid;

    @ApiModelProperty(value = "1.待签字 2已签字")
    private Integer processed;

    @ApiModelProperty(value = "1.次产领用2.资产借用3.资产退换4.办公品领用")
    private Integer dig;

    @ApiModelProperty(value = "1.近一月2.近三月3.近半年")
    private Integer date;






    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getDig() {
        return dig;
    }

    public void setDig(Integer dig) {
        this.dig = dig;
    }
}
