package com.sdkj.fixed.asset.api.system;


import com.sdkj.fixed.asset.api.system.in_vo.FedParam;
import com.sdkj.fixed.asset.api.system.in_vo.FededitParam;
import com.sdkj.fixed.asset.api.system.out_vo.FedEntity;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.FedManagement;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/**
 * zhaozheyu
 */
@Api
@RequestMapping(value = "/system/fed/api")
public interface FedApi {
    /**
     * 新增问题反馈
     *
     * @param entity
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody FedManagement entity, String userstate) throws Exception;

    /**
     * 问题反馈查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo<FedEntity> getAllPage(@RequestBody @Validated PageParams<FedParam> params) throws Exception;

    /**
     * 新增问题反馈
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public BaseResultVo edit(@Validated @RequestBody FededitParam param) throws Exception;

    /**
     * 查看问题反馈详情
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/see", method = RequestMethod.GET)
    public BaseResultVo see(@NotBlank(message = "id不能为空") @RequestParam("id") String id) throws Exception;
}



