package com.sdkj.fixed.asset.api.system.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @ClassName UserEditPassword
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/23 16:54
 */
public class UserEditPassword {
    /**
     * 旧密码
     */
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;
    /**
     * 新密码
     */
    @NotBlank(message = "新密码不能为空")
    @Size(max = 32,message = "新密码长度不匹配")
    private String newPassword;
    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String userId;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword == null ? null : oldPassword.trim();
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword== null ? null : newPassword.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId== null ? null : userId.trim();
    }
}
