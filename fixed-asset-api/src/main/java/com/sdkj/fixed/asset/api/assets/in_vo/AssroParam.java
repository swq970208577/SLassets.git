package com.sdkj.fixed.asset.api.assets.in_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * AssroParam
 *
 * @author zhaozheyu
 * @Description     我的审批查询入参
 * @date 2020/7/31 9:40
 */
public class AssroParam {
    @ApiModelProperty(value = "处理状态(1.待处理2.已处理)")
    private Integer processed;
    @ApiModelProperty(value = "当前登录人id")
    private String userid;
    @ApiModelProperty(value = "编号")
    private String number;
    @ApiModelProperty(value = "机构id")
    private String orgid;
    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer ifadmin;


    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public Integer getIfadmin() {
        return ifadmin;
    }

    public void setIfadmin(Integer ifadmin) {
        this.ifadmin = ifadmin;
    }

    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
