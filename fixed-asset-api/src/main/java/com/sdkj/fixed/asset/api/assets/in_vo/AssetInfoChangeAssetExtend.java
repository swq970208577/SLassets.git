package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetInfoChangeAsstes;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/27 11:19
 */
public class AssetInfoChangeAssetExtend extends AssetInfoChangeAsstes {

    /**
     * 使用公司
     */
    private String useCompanyName;
    /**
     * 使用部门
     */
    private String useDepartmentName;
    /**
     * 区域名称
     */
    private String areaName;

    private String assetName;
    private String assetCode;




    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDepartmentName() {
        return useDepartmentName;
    }

    public void setUseDepartmentName(String useDepartmentName) {
        this.useDepartmentName = useDepartmentName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Override
    public String getAssetName() {
        return assetName;
    }

    @Override
    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }
}
