package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.Collect;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用
 * @Date 2020/7/24 17:47
 */
public class CollectResult extends Collect {

    private String receiveAreaName;
    private String receiveCompanyName;
    private String receiveDeptName;
    private List<WarehouseHistoryResult> warehouseList;

    public String getReceiveAreaName() {
        return receiveAreaName;
    }

    public void setReceiveAreaName(String receiveAreaName) {
        this.receiveAreaName = receiveAreaName;
    }

    public String getReceiveCompanyName() {
        return receiveCompanyName;
    }

    public void setReceiveCompanyName(String receiveCompanyName) {
        this.receiveCompanyName = receiveCompanyName;
    }

    public String getReceiveDeptName() {
        return receiveDeptName;
    }

    public void setReceiveDeptName(String receiveDeptName) {
        this.receiveDeptName = receiveDeptName;
    }

    public List<WarehouseHistoryResult> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<WarehouseHistoryResult> warehouseList) {
        this.warehouseList = warehouseList;
    }
}
