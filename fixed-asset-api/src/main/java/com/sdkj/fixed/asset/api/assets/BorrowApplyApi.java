package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.ApplySelParam;
import com.sdkj.fixed.asset.api.assets.in_vo.ApprovalParams;
import com.sdkj.fixed.asset.api.assets.in_vo.BorrowApplyExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.SignSelParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/28 18:17
 */
@RequestMapping("/assets/borrowApply/api")
public interface BorrowApplyApi {
    /**
     * app借用申请-新增
     * @param extend
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody BorrowApplyExtend extend);

    /**
     * 分页查询
     * @param params
     * @retu
     */
    @ResponseBody
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    public BaseResultVo getAllPage(@Validated @RequestBody PageParams<ApplySelParam> params);

    /**
     * 更新审批状态
     * @param params
     */
    @ResponseBody
    @RequestMapping(value = "/updState", method = RequestMethod.POST)
    public BaseResultVo updState(@Validated @RequestBody ApprovalParams params);

    /**
     * 审批资产发放情况
     * @param params
     */
    @ResponseBody
    @RequestMapping(value = "/getApplyIssue", method = RequestMethod.POST)
    public BaseResultVo getApplyIssue(@Validated @RequestBody ApprovalParams params);

    /**
     * 签字后查看详情
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryDeail", method = RequestMethod.POST)
    public BaseResultVo queryDeail(@Validated @RequestBody SignSelParam params);


}
