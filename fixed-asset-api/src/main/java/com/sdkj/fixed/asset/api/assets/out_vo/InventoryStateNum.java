package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/5 10:38
 */
public class InventoryStateNum{

    private String inventoryStateNum;//盘点总数
    private String inventoryStateNum_0;//未盘
    private String inventoryStateNum_1;//已盘
    private String inventoryStateNum_2;//盘亏
    private String inventoryStateNum_3;//盘盈
    private String changeStateNum_4;//盘点时使用公司/使用部门/使用人变更的资产
    private String changeStateNum_5;//区域/存放地点变更的资产
    private String assetStateNum_6;//已盘资产中闲置
    private String assetStateNum_7;//已盘资产中在用
    private String assetStateNum_8;//已盘资产中借用
    private String changeStateNum_9;// 已盘资产中没有变动的资产


    public String getInventoryStateNum() {
        return inventoryStateNum;
    }

    public void setInventoryStateNum(String inventoryStateNum) {
        this.inventoryStateNum = inventoryStateNum;
    }

    public String getInventoryStateNum_0() {
        return inventoryStateNum_0;
    }

    public void setInventoryStateNum_0(String inventoryStateNum_0) {
        this.inventoryStateNum_0 = inventoryStateNum_0;
    }

    public String getInventoryStateNum_1() {
        return inventoryStateNum_1;
    }

    public void setInventoryStateNum_1(String inventoryStateNum_1) {
        this.inventoryStateNum_1 = inventoryStateNum_1;
    }

    public String getInventoryStateNum_2() {
        return inventoryStateNum_2;
    }

    public void setInventoryStateNum_2(String inventoryStateNum_2) {
        this.inventoryStateNum_2 = inventoryStateNum_2;
    }

    public String getInventoryStateNum_3() {
        return inventoryStateNum_3;
    }

    public void setInventoryStateNum_3(String inventoryStateNum_3) {
        this.inventoryStateNum_3 = inventoryStateNum_3;
    }

    public String getChangeStateNum_4() {
        return changeStateNum_4;
    }

    public void setChangeStateNum_4(String changeStateNum_4) {
        this.changeStateNum_4 = changeStateNum_4;
    }

    public String getChangeStateNum_5() {
        return changeStateNum_5;
    }

    public void setChangeStateNum_5(String changeStateNum_5) {
        this.changeStateNum_5 = changeStateNum_5;
    }

    public String getAssetStateNum_6() {
        return assetStateNum_6;
    }

    public void setAssetStateNum_6(String assetStateNum_6) {
        this.assetStateNum_6 = assetStateNum_6;
    }

    public String getAssetStateNum_7() {
        return assetStateNum_7;
    }

    public void setAssetStateNum_7(String assetStateNum_7) {
        this.assetStateNum_7 = assetStateNum_7;
    }

    public String getAssetStateNum_8() {
        return assetStateNum_8;
    }

    public void setAssetStateNum_8(String assetStateNum_8) {
        this.assetStateNum_8 = assetStateNum_8;
    }

    public String getChangeStateNum_9() {
        return changeStateNum_9;
    }

    public void setChangeStateNum_9(String changeStateNum_9) {
        this.changeStateNum_9 = changeStateNum_9;
    }
}
