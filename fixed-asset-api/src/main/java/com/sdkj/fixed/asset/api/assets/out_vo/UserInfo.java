package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.api.assets.in_vo.BorrowIssueExtend;

import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/5 18:52
 */
public class UserInfo {

    private String id;
    @Excel(name = "员工工号", orderNum = "0")
    private String employeeId;
    @Excel(name = "员工姓名", orderNum = "1")
    private String name;
    @Excel(name = "员工公司", orderNum = "2")
    private String companyName;
    @Excel(name = "员工部门", orderNum = "3")
    private String deptName;
    @Excel(name = "员工电话", orderNum = "4")
    private String tel;
    @Excel(name = "员工邮箱", orderNum = "5")
    private String email;
    private String isOnJob;
    private Integer dataIsAll;
    private String type;
    private String companyId;
    private String deptId;
    private String companyTreecode;
    private String deptTreecode;


    private List<WarehouseResult> userAssetList = new ArrayList<>();
    private List<BorrowIssueExtend> borrowList = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIsOnJob() {
        return isOnJob;
    }

    public void setIsOnJob(String isOnJob) {
        this.isOnJob = isOnJob;
    }

    public List<WarehouseResult> getUserAssetList() {
        return userAssetList;
    }

    public void setUserAssetList(List<WarehouseResult> userAssetList) {
        this.userAssetList = userAssetList;
    }

    public List<BorrowIssueExtend> getBorrowList() {
        return borrowList;
    }

    public void setBorrowList(List<BorrowIssueExtend> borrowList) {
        this.borrowList = borrowList;
    }

    public Integer getDataIsAll() {
        return dataIsAll;
    }

    public void setDataIsAll(Integer dataIsAll) {
        this.dataIsAll = dataIsAll;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getCompanyTreecode() {
        return companyTreecode;
    }

    public void setCompanyTreecode(String companyTreecode) {
        this.companyTreecode = companyTreecode;
    }

    public String getDeptTreecode() {
        return deptTreecode;
    }

    public void setDeptTreecode(String deptTreecode) {
        this.deptTreecode = deptTreecode;
    }
}
