package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/13 11:24
 */
public class AppApplyMaintain {
    @NotBlank(message = "id不能为空")
    @Size(max = 32, message = "资产id超长")
    private String id;
    @Size(max = 128, message = "备注/说明超长")
    private String note;
    @Size(max = 1000, message = "图片超长")
    private String imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
