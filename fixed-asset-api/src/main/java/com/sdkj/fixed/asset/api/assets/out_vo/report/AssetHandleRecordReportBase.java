package com.sdkj.fixed.asset.api.assets.out_vo.report;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zx
 * @Description //资产清单
 * @Date 2020/7/30 16:08
 */
public class AssetHandleRecordReportBase {
    /**
     * 资产ID
     */
    private String assetId;

    private String state;
    /**
     * 照片
     */
    private String photo;
    /**
     * 资产条码
     */

    private String assetCode;
    /**
     * 资产名称
     */

    private String assetName;


    private String assetClassName;

    /**
     * 规格型号
     */

    private String specificationModel;
    /**
     * SN号
     */

    private String snNumber;
    /**
     * 计量单位
     */

    private String unitMeasurement;
    /**
     * 金额
     */

    private String amount;



    private String useCompanyName;



    private String useDeptName;
    /**
     * 使用人
     */
    private String useUser;

    private String areaName;
    /**
     * 存放地点
     */

    private String storageLocation;
    /**
     * 管理员
     */

    private String admin;


    private String companyName;
    /**
     * 购入时间
     */

    private String purchaseTime;
    /**
     * 使用期限(月)
     */

    private String serviceLife;


    private String supplierName;

    private static final long serialVersionUID = 1L;

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取资产条码
     *
     * @return asset_code - 资产条码
     */
    public String getAssetCode() {
        return assetCode;
    }

    /**
     * 设置资产条码
     *
     * @param assetCode 资产条码
     */
    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode == null ? null : assetCode.trim();
    }

    /**
     * 获取资产名称
     *
     * @return asset_name - 资产名称
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * 设置资产名称
     *
     * @param assetName 资产名称
     */
    public void setAssetName(String assetName) {
        this.assetName = assetName == null ? null : assetName.trim();
    }



    /**
     * 获取规格型号
     *
     * @return specification_model - 规格型号
     */
    public String getSpecificationModel() {
        return specificationModel;
    }

    /**
     * 设置规格型号
     *
     * @param specificationModel 规格型号
     */
    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel == null ? null : specificationModel.trim();
    }

    /**
     * 获取计量单位
     *
     * @return unit_measurement - 计量单位
     */
    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    /**
     * 设置计量单位
     *
     * @param unitMeasurement 计量单位
     */
    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement == null ? null : unitMeasurement.trim();
    }

    /**
     * 获取SN号
     *
     * @return sn_number - SN号
     */
    public String getSnNumber() {
        return snNumber;
    }

    /**
     * 设置SN号
     *
     * @param snNumber SN号
     */
    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber == null ? null : snNumber.trim();
    }



    /**
     * 获取购入时间
     *
     * @return purchase_time - 购入时间
     */
    public String getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * 设置购入时间
     *
     * @param purchaseTime 购入时间
     */
    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime == null ? null : purchaseTime.trim();
    }



    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(String amount) {
        this.amount = amount == null ? null : amount.trim();
    }

    /**
     * 获取管理员
     *
     * @return admin - 管理员
     */
    public String getAdmin() {
        return admin;
    }

    /**
     * 设置管理员
     *
     * @param admin 管理员
     */
    public void setAdmin(String admin) {
        this.admin = admin == null ? null : admin.trim();
    }



    /**
     * 获取使用期限(月)
     *
     * @return service_life - 使用期限(月)
     */
    public String getServiceLife() {
        return serviceLife;
    }

    /**
     * 设置使用期限(月)
     *
     * @param serviceLife 使用期限(月)
     */
    public void setServiceLife(String serviceLife) {
        this.serviceLife = serviceLife == null ? null : serviceLife.trim();
    }



    /**
     * 获取存放地点
     *
     * @return storage_location - 存放地点
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * 设置存放地点
     *
     * @param storageLocation 存放地点
     */
    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation == null ? null : storageLocation.trim();
    }


    /**
     * 获取状态
     *
     * @return state - 状态
     */
    public String getState() {
        return state;
    }

    /**
     * 设置状态
     *
     * @param state 状态
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取照片
     *
     * @return photo - 照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 设置照片
     *
     * @param photo 照片
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }


    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getUseUser() {
        return useUser;
    }

    public void setUseUser(String useUser) {
        this.useUser = useUser;
    }
}
