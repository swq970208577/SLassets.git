package com.sdkj.fixed.asset.api.hc.vo.out;

import java.util.List;

/**
 * ReceiptMoveExtend
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/6 17:00
 */
public class ReceiptOutCheckExtend {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String number;

    private String categoryName;

    private String bussinessDate;

    private String cuser;

    private String ctime;

    private String comment;

    private List<ReceiptMoveProductExtend> receiptMoveProductList;

    public List<ReceiptMoveProductExtend> getReceiptMoveProductList() {
        return receiptMoveProductList;
    }

    public void setReceiptMoveProductList(List<ReceiptMoveProductExtend> receiptMoveProductList) {
        this.receiptMoveProductList = receiptMoveProductList;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
