package com.sdkj.fixed.asset.api.system.in_vo;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName CompanyId
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/23 13:59
 */
public class RoleCondition {
    @NotBlank(message = "公司id不能为空")
    private String companyId;
    /**
     * 名称
     */
    private String name;
    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
