package com.sdkj.fixed.asset.api.system.in_vo;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @ClassName EditState
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/22 17:25
 */
public class EditState {
    @Size(max = 32, message = "id长度不匹配")
    @NotBlank(message = "id不能为空")
    private String id;

    @NotNull(message = "状态不能为空")
    @Range(min = 1,max = 2,message = "状态只能输入1或2")
    private Integer state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
