package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptMoveGetPageParams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ReceiptMove;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * @author 史晨星
 * @ClassName: ReceiptInApi
 * @Description: 仓库管理
 * @date 2020年7月21日
 */
@RequestMapping("/hc/receiptMove/api")
public interface ReceiptMoveApi {


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody ReceiptMove entity) throws Exception;

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody ReceiptMove entity) throws Exception;

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody IdParams entity) throws Exception;

    /**
     * 确认
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo confirm(@RequestBody ReceiptMove entity) throws Exception;

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo view(@RequestBody IdParams entity) throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody PageParams<ReceiptMoveGetPageParams> params) throws Exception;


    /**
     * 导出
     *
     * @param number
     * @param companyId
     * @param inCategoryId
     * @param outCategoryId
     * @param userId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(@Param("number") String number, @Param("companyId") String companyId, @Param("inCategoryId") String inCategoryId, @Param("outCategoryId") String outCategoryId, @Param("userId") String userId, HttpServletResponse response) throws Exception;

    @RequestMapping(value = "/print", method = RequestMethod.GET)
    @ResponseBody
    public void print(String ids) throws Exception;

}

