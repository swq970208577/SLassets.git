package com.sdkj.fixed.asset.api.login.out_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 权限树返回结果
 * @author 张欣
 */
public class MenuResultEntity {
    /**
     * 对应权限id
     */
    @ApiModelProperty(value = "对应权限id,服务或权限")
    private String authId;
    /**
     * 父id
     */
    @ApiModelProperty(value = "父节点id")
    private String parentId;
    /**
     * 权限名称
     */
    @ApiModelProperty(value = "权限名称")
    private String name;
    @ApiModelProperty(value = "资源地址")
    private String sourceKey;
    @ApiModelProperty(value = "是否是最后一级菜单")
    private String isEndMenu;
    @ApiModelProperty(hidden = true)
    private String treeCode;
    private Integer sort;
    /**
     * 菜单等级， 层级类型：0：分类；1：菜单；2：表格外按钮；3：表格内按钮；4：Tab
     * 服务类型（0服务1服务组2按钮）
     */
    @ApiModelProperty(value = "菜单等级， (层级类型：0：分类；1：菜单；2：表格外按钮；3：表格内按钮；4：Tab)，服务类型（0服务1服务组2按钮）")
    private String level;
    @ApiModelProperty(value = "试用标识，1试用期不可用")
    private Integer lifeFlag;

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getIsEndMenu() {
        return isEndMenu;
    }

    public void setIsEndMenu(String isEndMenu) {
        this.isEndMenu = isEndMenu;
    }

    public String getTreeCode() {
        return treeCode;
    }

    public void setTreeCode(String treeCode) {
        this.treeCode = treeCode;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getLifeFlag() {
        return lifeFlag;
    }

    public void setLifeFlag(Integer lifeFlag) {
        this.lifeFlag = lifeFlag;
    }
}
