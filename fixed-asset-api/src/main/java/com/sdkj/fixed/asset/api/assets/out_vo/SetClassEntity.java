package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/21 14:09
 */
public class SetClassEntity {

    private String id;

    private String orgId;

    private String name;

    private String pid;

    private Integer level;

    private String num;

    private String years;

    private Integer state;

    private String pName;

    private String treecode;
    private Integer ifShow;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getTreecode() {
        return treecode;
    }

    public void setTreecode(String treecode) {
        this.treecode = treecode;
    }

    public Integer getIfShow() {
        return ifShow;
    }

    public void setIfShow(Integer ifShow) {
        this.ifShow = ifShow;
    }
}
