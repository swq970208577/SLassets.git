package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetIntermediate;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/6 11:18
 */
public class AssetIntermediateExtend extends AssetIntermediate {

    private String assetClassNo;
    private String companyNo;
    private String areaNo;

    public String getAssetClassNo() {
        return assetClassNo;
    }

    public void setAssetClassNo(String assetClassNo) {
        this.assetClassNo = assetClassNo;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public String getAreaNo() {
        return areaNo;
    }

    public void setAreaNo(String areaNo) {
        this.areaNo = areaNo;
    }
}
