package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 盘点状态查询
 * @Date 2020/7/21 18:35
 */
public class StateParam {

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
