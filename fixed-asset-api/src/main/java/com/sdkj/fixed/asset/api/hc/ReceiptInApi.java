package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptInGetPageParams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ReceiptIn;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * @author 史晨星
 * @ClassName: ReceiptInApi
 * @Description: 仓库管理
 * @date 2020年7月21日
 */
@RequestMapping("/hc/receiptIn/api")
public interface ReceiptInApi {


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody ReceiptIn entity) throws Exception;

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody ReceiptIn entity) throws Exception;

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo view(@RequestBody ReceiptIn entity) throws Exception;

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody ReceiptIn entity) throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody PageParams<ReceiptInGetPageParams> params) throws Exception;

    /**
     * 打印盘亏出库
     *
     * @param ids
     * @throws Exception
     */
    @RequestMapping(value = "/print", method = RequestMethod.GET)
    @ResponseBody
    public void print(String ids) throws Exception;

    /**
     * 导出
     *
     * @param number
     * @param companyId
     * @param type
     * @param categoryId
     * @param userId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(Integer type, String number, String companyId, String categoryId, String userId, HttpServletResponse response) throws Exception;
}

