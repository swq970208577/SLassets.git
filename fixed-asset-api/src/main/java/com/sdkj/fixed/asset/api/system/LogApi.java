package com.sdkj.fixed.asset.api.system;


import com.sdkj.fixed.asset.api.system.in_vo.LogEntity;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.LogManagement;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@RequestMapping(value = "/system/log/api")
public interface LogApi {
    /**
     * 日志列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody @Validated PageParams<LogEntity> params)throws Exception;
    /**
     * 查看日志详情
     *
     * @param logEntity
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/viewLog", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo viewLog(@RequestBody @Validated LogEntity logEntity)throws Exception;
    /**
     * 记录操作日志
     * @param log
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/insertLog", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo insertLog(@RequestBody @Validated LogManagement log)throws Exception;
}



