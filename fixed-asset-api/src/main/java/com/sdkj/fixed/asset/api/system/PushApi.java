package com.sdkj.fixed.asset.api.system;


import com.sdkj.fixed.asset.api.system.in_vo.UserConditionEntity;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName OrgApi
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/20 14:40
 */
@RequestMapping({"/system/push/api"})
public interface PushApi {

    /**
     * 分页查询用户
     *
     * @return
     */
    @RequestMapping(value = "/getPagePush", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPagePush(@RequestBody PageParams params)  throws Exception;

}
