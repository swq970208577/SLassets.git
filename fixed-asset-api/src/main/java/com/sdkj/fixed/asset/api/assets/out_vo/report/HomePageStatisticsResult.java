package com.sdkj.fixed.asset.api.assets.out_vo.report;

/**
 * @ClassName HomePageStatisticsResult
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/11 14:24
 */
public class HomePageStatisticsResult {
    /**
     * x坐标
     */
    private String xAxis[];
    /**
     * y坐标
     */
    private String yAxirs[];
    /**
     * 数量
     */
    private String series[];
    /**
     * 金额
     */
    private String series1[];

    public String[] getxAxis() {
        return xAxis;
    }

    public void setxAxis(String[] xAxis) {
        this.xAxis = xAxis == null ? new String[0] : xAxis;
    }

    public String[] getyAxirs() {
        return yAxirs;
    }

    public void setyAxirs(String[] yAxirs) {
        this.yAxirs = yAxirs == null ? new String[0] : yAxirs;
    }

    public String[] getSeries() {
        return series;
    }

    public void setSeries(String[] series) {
        this.series = series == null ? new String[0] : series;
    }

    public String[] getSeries1() {
        return series1;
    }

    public void setSeries1(String[] series1) {
        this.series1 = series1 == null ? new String[0] : series1;
    }
}
