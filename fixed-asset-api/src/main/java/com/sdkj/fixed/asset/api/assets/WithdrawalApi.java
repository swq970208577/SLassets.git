package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.WithdrawalResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author zhangjinfei
 * @Date 2020/7/29 13:25
 */
@RequestMapping("/assets/withdrawal/api")
public interface WithdrawalApi {

    /**
     * 分页查询
     * @param params
     * @return
     */
    @PostMapping("/queryPages")
    public BaseResultVo<WithdrawalResult> queryPages(@RequestBody PageParams<SearchNameParam> params);

    /**
     * 查看
     * @param withdrawalId
     * @return
     */
    @GetMapping("/queryWithdrawal")
    public BaseResultVo<WithdrawalResult> queryWithdrawal(@RequestParam("withdrawalId") String  withdrawalId);

    /**
     * 添加退库
     * @param withdrawal
     * @return
     */
    @PostMapping("/insertWithdrawal")
    public BaseResultVo insertWithdrawal(@RequestBody WithdrawalParam withdrawal);



    /**
     * 修改退库
     * @param withdrawal
     * @return
     */
    @PostMapping("/updateWithdrawal")
    public BaseResultVo updateWithdrawal(@RequestBody WithdrawalParam withdrawal) throws Exception;

    /**
     * 退库签字
     * @param param
     * @return
     */
    @PostMapping("/updateSign")
    public BaseResultVo updateSign(@RequestBody SignatureParam param) throws Exception;

    /**
     * 删除退库
     * @param withdrawalId
     * @return
     */
    @GetMapping("/deleteWithdrawal")
    public BaseResultVo deleteWithdrawal(@RequestParam("withdrawalId") String withdrawalId) throws Exception;

    /**
     * 退库导出
     */
    @GetMapping("/exportWithdrawal")
    public void exportWithdrawal(HttpServletResponse response,@RequestParam("token")String token,@RequestParam(value = "searchName",required = false)String searchName) throws Exception;

    /**
     * 退库打印
     */
    @GetMapping("/printWithdrawal")
    public void printWithdrawal(HttpServletResponse response,@RequestParam("withdrawalIds") String withdrawalIds) throws Exception;

    /**
     * 添加退库申请
     * @param withdrawal
     * @return
     */
    @PostMapping("/insertWithdrawalApply")
    public BaseResultVo insertWithdrawalApply(@RequestBody WithdrawalApplyParam withdrawal);

    /**
     * 退库申请同意
     * @param param
     * @return
     */
    @PostMapping("/withdrawalApplyAgree")
    public BaseResultVo withdrawalApplyAgree(@RequestBody WithdrawalApplyAgreeParam param);


    /**
     * 退库申请拒绝
     * @param param
     * @return
     */
    @PostMapping("/withdrawalApplyRefuse")
    public BaseResultVo withdrawalApplyRefuse(@RequestBody WithdrawalApplyAgreeParam param);

    /**
     * 審批查詢
     * @return
     */
    @PostMapping("/getAllWithdrawalApply")
    BaseResultVo getAllWithdrawalApply(@RequestBody ApplySelParam params);

    /**
     * 签字查询查詢
     * @return
     */
    @GetMapping("/getWithdrawalApplyById")
    BaseResultVo getWithdrawalApplyById(@RequestParam("id") String id,@RequestParam("state") Integer state);
}
