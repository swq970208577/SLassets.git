package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.assets.Inventory;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/4 13:43
 */
public class InventoryExtend extends Inventory {

    /**
     * 创建人名称
     */
    @Excel(name = "创建人", orderNum = "2")
    private String createUserName;

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
}
