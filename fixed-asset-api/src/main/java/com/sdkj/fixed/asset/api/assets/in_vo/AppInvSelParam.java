package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/6 20:40
 */
public class AppInvSelParam {

    private String search;
    @NotBlank(message = "盘点状态不能为空")
    private String inventoryState;
    private String inventoryId;


    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getInventoryState() {
        return inventoryState;
    }

    public void setInventoryState(String inventoryState) {
        this.inventoryState = inventoryState;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }
}
