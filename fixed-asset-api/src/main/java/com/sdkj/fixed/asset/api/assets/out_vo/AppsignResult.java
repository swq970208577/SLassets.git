package com.sdkj.fixed.asset.api.assets.out_vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * AppsignResult
 *
 * @author 赵哲宇
 * @Description
 * @date 2020/8/7 13:34
 */
public class AppsignResult implements Serializable {
    @ApiModelProperty(value = "物品/分类名称")
    private String name;

    @ApiModelProperty(value = "物品/分类编码")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
