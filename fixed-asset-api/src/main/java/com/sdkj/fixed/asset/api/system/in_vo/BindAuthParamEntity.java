package com.sdkj.fixed.asset.api.system.in_vo;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 绑定权限参数
 * @author 张欣
 */
public class BindAuthParamEntity {
    /**
     * 角色id
     */

    @NotBlank(message = "角色id不能为空")
    private String roleId;
    /**
     * 菜单id集合
     */
    private List<String> menus;


    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public List<String> getMenus() {
        return menus;
    }

    public void setMenus(List<String> menus) {
        this.menus = menus;
    }

}
