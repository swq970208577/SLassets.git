package com.sdkj.fixed.asset.api.assets.out_vo.report;

import java.util.List;

/**
 * @ClassName AssetHandleRecords
 * @Description 资产履历查询结果
 * @Author 张欣
 * @Date 2020/8/6 16:10
 */
public class AssetHandleRecords extends AssetHandleRecordReportBase{
    /**
     * 处理明细
     */
    List<AssetHandleRecordDetailed> assetHandleRecordDetaileds;

    public List<AssetHandleRecordDetailed> getAssetHandleRecordDetaileds() {
        return assetHandleRecordDetaileds;
    }

    public void setAssetHandleRecordDetaileds(List<AssetHandleRecordDetailed> assetHandleRecordDetaileds) {
        this.assetHandleRecordDetaileds = assetHandleRecordDetaileds;
    }
}
