package com.sdkj.fixed.asset.api.hc.vo.in;

import io.swagger.annotations.ApiModelProperty;

/**
 * MablesParams
 *
 * @author zhaozheyu
 * @Description 耗材领用查询入参
 * @date 2020/8/12 17:33
 */
public class MablesParams {
    @ApiModelProperty(value = "当前登录人机构id")
    private String orgid;
    @ApiModelProperty(value = "当前登录人id")
    private String userid;

    @ApiModelProperty(value = "员工id")
    private String squserid;
    @ApiModelProperty(value = "部门id")
    private String sqdeptid;
    @ApiModelProperty(value = "机构id")
    private String sqorgid;



    @ApiModelProperty(value = "开始时间")
    private String startdate;
    @ApiModelProperty(value = "开始时间")
    private String enddate;


    @ApiModelProperty(value = "1.是管理员2.不是管理员")
    private Integer ifadmin;


    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getSquserid() {
        return squserid;
    }

    public void setSquserid(String squserid) {
        this.squserid = squserid;
    }

    public String getSqdeptid() {
        return sqdeptid;
    }

    public void setSqdeptid(String sqdeptid) {
        this.sqdeptid = sqdeptid;
    }

    public String getSqorgid() {
        return sqorgid;
    }

    public void setSqorgid(String sqorgid) {
        this.sqorgid = sqorgid;
    }



    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public Integer getIfadmin() {
        return ifadmin;
    }

    public void setIfadmin(Integer ifadmin) {
        this.ifadmin = ifadmin;
    }
}
