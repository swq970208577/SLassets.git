package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 搜索框
 * @Date 2020/7/21 13:29
 */
public class SearchNameParam {

    /**
     * 搜索输入框
     */
    private String searchName;

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
