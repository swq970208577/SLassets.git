package com.sdkj.fixed.asset.api.hc.vo.out;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * ReceiptConsume
 *
 * @author zhaozheyu
 * @Description 耗材领用表反参
 * @date 2020/8/12 20:14
 */
public class ReceiptConsume {
    @ApiModelProperty(value = "耗材id")
    private String id;
    @ApiModelProperty(value = "耗材名称")
    private String hcname;
    @ApiModelProperty(value = "耗材编码")
    private String hccode;
    @ApiModelProperty(value = "规格型号")
    private String model;
    @ApiModelProperty(value = "单位")
    private String unit;
    @ApiModelProperty(value = "年份(必填)")
    private String year;
    @ApiModelProperty(value = "数量合计")
    private Integer numtotal;
    @ApiModelProperty(value = "单价合计")
    private String pricetotal;
    @ApiModelProperty(value = "金额合计")
    private String tpricetotal;

    @ApiModelProperty(value = "数量list")
    private List<ReceiptConsum> numlist;
    @ApiModelProperty(value = "单价list")
    private List<ReceiptConsum> pricelist;
    @ApiModelProperty(value = "金额list")
    private List<ReceiptConsum> tpricelist;




    public Integer getNumtotal() {
        return numtotal;
    }

    public void setNumtotal(Integer numtotal) {
        this.numtotal = numtotal;
    }

    public String getPricetotal() {
        return pricetotal;
    }

    public void setPricetotal(String pricetotal) {
        this.pricetotal = pricetotal;
    }

    public String getTpricetotal() {
        return tpricetotal;
    }

    public void setTpricetotal(String tpricetotal) {
        this.tpricetotal = tpricetotal;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHcname() {
        return hcname;
    }

    public void setHcname(String hcname) {
        this.hcname = hcname;
    }

    public String getHccode() {
        return hccode;
    }

    public void setHccode(String hccode) {
        this.hccode = hccode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<ReceiptConsum> getNumlist() {
        return numlist;
    }

    public void setNumlist(List<ReceiptConsum> numlist) {
        this.numlist = numlist;
    }

    public List<ReceiptConsum> getPricelist() {
        return pricelist;
    }

    public void setPricelist(List<ReceiptConsum> pricelist) {
        this.pricelist = pricelist;
    }

    public List<ReceiptConsum> getTpricelist() {
        return tpricelist;
    }

    public void setTpricelist(List<ReceiptConsum> tpricelist) {
        this.tpricelist = tpricelist;
    }
}
