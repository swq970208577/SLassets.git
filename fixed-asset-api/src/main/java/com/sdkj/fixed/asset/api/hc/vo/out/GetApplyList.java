package com.sdkj.fixed.asset.api.hc.vo.out;

import java.util.List;

/**
 * GetApplyList
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 16:47
 */
public class GetApplyList {

    private String id;

    private String applyTime;

    private Integer num;

    private String number;

    private List<SonApply> sonApplyList;

    public List<SonApply> getSonApplyList() {
        return sonApplyList;
    }

    public void setSonApplyList(List<SonApply> sonApplyList) {
        this.sonApplyList = sonApplyList;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
