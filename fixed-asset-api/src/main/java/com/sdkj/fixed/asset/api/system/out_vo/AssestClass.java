package com.sdkj.fixed.asset.api.system.out_vo;

/**
 * @ClassName AssestClass
 * @Description 资产类别
 * @Author 张欣
 * @Date 2020/7/31 13:45
 */
public class AssestClass {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
