package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.AssetIntermediate;
import com.sdkj.fixed.asset.pojo.assets.Inventory;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author niuliwei
 * @description 资产盘点管理
 * @date 2020/8/3 16:47
 */
@RequestMapping("/assets/inventory/api")
public interface InventoryApi {

    /**
     * 新增盘点任务
     * @param inventory
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody Inventory inventory);

    /**
     * 删除盘点任务
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public BaseResultVo delete(@Validated @RequestBody Params params);

    /**
     * 导出盘点任务
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void export(@RequestParam("orgId") String orgId, @RequestParam("state")String state, @RequestParam("token")String token) throws IOException;

    /**
     * 查看盘点信息
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryInvInfo", method = RequestMethod.POST)
    public BaseResultVo queryInvInfo(@Validated @RequestBody Params params);

    /**
     * 下载盘点报告
     * @param inventoryId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void download(@Param("inventoryId") String inventoryId) throws IOException;

    /**
     * 分页查询盘点管理列表
     * @param pageParams
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    public BaseResultVo getAllPage(@RequestBody PageParams<StateParam> pageParams);
    /**
     * 盘点管理列表-查询数量
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/stateNum", method = RequestMethod.POST)
    public BaseResultVo stateNum();

    /**
     * 分配用户
     * @param inventory
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/allotUser", method = RequestMethod.POST)
    public BaseResultVo allotUser(@RequestBody Inventory inventory);

    /**
     * 分页查询盘点结果
     * @param pageParams
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryInvResult", method = RequestMethod.POST)
    public BaseResultVo queryInvResult(@RequestBody PageParams<AssetIntermediate> pageParams);

    /**
     * 盘点结果-统计数量
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/resultStatistical", method = RequestMethod.POST)
    public BaseResultVo resultStatistical(@Validated @RequestBody Params params);

    /**
     * 导出盘点结果
     * @param inventoryId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/exportInvResult", method = RequestMethod.GET)
    public void exportInvResult(@Param("inventoryId") String inventoryId) throws IOException;

    /**
     * 提交盘点结果
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/submitInvResult", method = RequestMethod.POST)
    public BaseResultVo submitInvResult(@Validated @RequestBody SubInvResultParam param);

    /**
     * 查询未盘点的用户
     * @param pageParams
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryNoInvUser", method = RequestMethod.POST)
    public BaseResultVo queryNoInvUser(@RequestBody PageParams<Search> pageParams);

    /**
     * 新增盘盈
     * @param intermediate
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addInvSurplus", method = RequestMethod.POST)
    public BaseResultVo addInvSurplus(@RequestBody AssetIntermediateExtend intermediate);

    /**
     * 删除盘盈
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delInvSurplus", method = RequestMethod.POST)
    public BaseResultVo delInvSurplus(@Validated @RequestBody Params params);

    /**
     * 编辑盘点
     * @param intermediate
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/saveUpdInvResultByIds", method = RequestMethod.POST)
    public BaseResultVo saveUpdInvResultByIds(@RequestBody AssetIntermediate intermediate);

    /**
     * 给未盘点的用户推送消息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/pushNoInvUser", method = RequestMethod.POST)
    public BaseResultVo pushNoInvUser(@RequestBody Params params);

    /**
     * app
     * 查询盘点任务
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/appInvList", method = RequestMethod.POST)
    public BaseResultVo appInvList(@RequestBody AppInvSelParam params);

    /**
     * app
     * 根据盘点单查询用户盘点任务
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/appInvOrder", method = RequestMethod.POST)
    public BaseResultVo appInvOrder(@Validated @RequestBody AppInvSelParam param);
    /**
     * app
     * 员工提交盘点结果
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/appUpdInvState", method = RequestMethod.POST)
    public BaseResultVo appUpdInvState(@Validated @RequestBody AppUpdInvState params);

    /**
     * app
     * 员工提交盘点结果,扫码枪盘点
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/appSweepGunInv", method = RequestMethod.POST)
    public BaseResultVo appSweepGunInv(@Validated @RequestBody SweepGunInvParam param);

}
