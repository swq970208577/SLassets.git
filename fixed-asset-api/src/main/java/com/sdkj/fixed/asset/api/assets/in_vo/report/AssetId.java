package com.sdkj.fixed.asset.api.assets.in_vo.report;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName AssetId
 * @Description 资产id
 * @Author 张欣
 * @Date 2020/8/3 11:05
 */
public class AssetId {
    /**
     * 资产id
     */
    @NotBlank(message = "资产id不能为空")
    private String assetId;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }
}
