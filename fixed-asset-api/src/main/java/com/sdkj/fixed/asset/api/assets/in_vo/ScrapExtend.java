package com.sdkj.fixed.asset.api.assets.in_vo;

import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseBase;
import com.sdkj.fixed.asset.pojo.assets.Scrap;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/27 9:38
 */
public class ScrapExtend extends Scrap {

    /**
     * 资产id
     */

    @NotEmpty
    private List<String> assetIdList;
    /**
     * 资产基本信息
     */
    @ExcelCollection(name = "资产明细", orderNum = "5")
    List<WarehouseBase> results = new ArrayList<>();

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }

    public List<WarehouseBase> getResults() {
        return results;
    }

    public void setResults(List<WarehouseBase> results) {
        this.results = results;
    }
}
