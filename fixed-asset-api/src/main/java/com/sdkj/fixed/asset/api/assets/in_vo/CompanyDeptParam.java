package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 公司 、部门
 * @Date 2020/8/12 17:47
 */
public class CompanyDeptParam {

    private String companyId;
    private String deptId;
    private String userId;
    private String orgId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
