package com.sdkj.fixed.asset.api.system.out_vo;

/**
 * @ClassName EnableUserResult
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/24 9:10
 */
public class EnableUserResult {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
