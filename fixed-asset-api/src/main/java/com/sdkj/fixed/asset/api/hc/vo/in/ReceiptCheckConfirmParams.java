package com.sdkj.fixed.asset.api.hc.vo.in;

import com.sdkj.fixed.asset.pojo.hc.group.EditCheckProduct;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ReceiptCheckConfirmParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 12:59
 */
public class ReceiptCheckConfirmParams {
    @Size(max = 32, message = "盘点单id-最大长度:32")
    @NotNull(message = "盘点单id-不能为空")
    private String recepitId;

    private Integer createProduct;
    /**
     * 是否自动生成盘盈入库单（1是2否）
     */
    @NotNull(message = "是否自动生成盘盈入库单-不能为空")
    private Integer createReceiptIn;
    /**
     * 是否自动生成盘亏出库单（1是2否）
     */
    @NotNull(message = "是否自动生成盘亏出库单-不能为空")
    private Integer createReceiptOut;

    private String orgId;

    private String userId;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRecepitId() {
        return recepitId;
    }

    public void setRecepitId(String recepitId) {
        this.recepitId = recepitId;
    }

    public Integer getCreateProduct() {
        return createProduct;
    }

    public void setCreateProduct(Integer createProduct) {
        this.createProduct = createProduct;
    }

    public Integer getCreateReceiptIn() {
        return createReceiptIn;
    }

    public void setCreateReceiptIn(Integer createReceiptIn) {
        this.createReceiptIn = createReceiptIn;
    }

    public Integer getCreateReceiptOut() {
        return createReceiptOut;
    }

    public void setCreateReceiptOut(Integer createReceiptOut) {
        this.createReceiptOut = createReceiptOut;
    }
}
