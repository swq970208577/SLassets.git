package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetReceive;
import com.sdkj.fixed.asset.pojo.assets.Collect;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用审批
 * @Date 2020/8/7 16:20
 */
public class CollectApplyParam extends Collect {

    private List<AssetReceive> assetReceiveList;

    public List<AssetReceive> getAssetReceiveList() {
        return assetReceiveList;
    }

    public void setAssetReceiveList(List<AssetReceive> assetReceiveList) {
        this.assetReceiveList = assetReceiveList;
    }
}
