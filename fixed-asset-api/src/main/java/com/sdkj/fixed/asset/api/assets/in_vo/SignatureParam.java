package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/24 17:39
 */
public class SignatureParam {

    @NotBlank(message = "id不能为空")
    private String id;
    @NotBlank(message = "签名照不能为空")
    private String signPic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSignPic() {
        return signPic;
    }

    public void setSignPic(String signPic) {
        this.signPic = signPic;
    }
}
