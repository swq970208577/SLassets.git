package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * ReceiptOutExport
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/7 17:07
 */
public class ReceiptOutExport {

    private String id;

    @Excel(name = "单据类型", needMerge = true, orderNum = "1")
    private String type1;

    private Integer type;

    @Excel(name = "出库单号", needMerge = true, orderNum = "2")
    private String number;

    @Excel(name = "出库仓库", needMerge = true, orderNum = "3")
    private String categoryName;

    @Excel(name = "出库日期", needMerge = true, orderNum = "4")
    private String bussinessDate;

    @Excel(name = "领用公司", needMerge = true, orderNum = "5")
    private String receiveOrg;

    @Excel(name = "领用部门", needMerge = true, orderNum = "6")
    private String receiveDept;

    @Excel(name = "领用人", needMerge = true, orderNum = "7")
    private String receiveUser;

    @Excel(name = "经办人", needMerge = true, orderNum = "8")
    private String cuserName;

    @Excel(name = "经办日期", needMerge = true, orderNum = "9")
    private String ctime;

    @Excel(name = "出库备注", needMerge = true, orderNum = "10")
    private String comment;

    @ExcelCollection(name = "出库明细", orderNum = "11")
    private List<ReceipOutGetPageExtends> list;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getReceiveOrg() {
        return receiveOrg;
    }

    public void setReceiveOrg(String receiveOrg) {
        this.receiveOrg = receiveOrg;
    }

    public String getReceiveDept() {
        return receiveDept;
    }

    public void setReceiveDept(String receiveDept) {
        this.receiveDept = receiveDept;
    }

    public String getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public List<ReceipOutGetPageExtends> getList() {
        return list;
    }

    public void setList(List<ReceipOutGetPageExtends> list) {
        this.list = list;
    }
}
