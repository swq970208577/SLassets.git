package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * ReceiptInGetPage
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/21 14:49
 */
public class ReceiptInGetPage {

    @Excel(name = "单据类型", needMerge = true, orderNum = "1")
    private String type1;

    private Integer type;
    /**
     * id
     */
    private String id;
    /**
     * 入库单号
     */
    @Excel(name = "入库单号", needMerge = true, orderNum = "2")
    private String number;
    /**
     * 仓库id
     */
    private String categoryId;
    /**
     * 仓库名称
     */
    @Excel(name = "入库仓库", needMerge = true, orderNum = "3")
    private String categoryName;
    /**
     * 入库日期
     */
    @Excel(name = "入库日期", needMerge = true, orderNum = "4")
    private String bussinessDate;

    private String provider;

    @Excel(name = "供应商", needMerge = true, orderNum = "5")
    private String providerName;
    /**
     * 创建人id
     */
    private String cuser;
    /**
     * 创建人名称
     */
    @Excel(name = "经办人", needMerge = true, orderNum = "6")
    private String cuserName;
    /**
     * 创建时间
     */
    @Excel(name = "经办日期", needMerge = true, orderNum = "7")
    private String ctime;
    /**
     * 备注
     */
    @Excel(name = "入库备注", needMerge = true, orderNum = "8")
    private String comment;






    /**
     * 物品编码
     */
    private String pCode;
    /**
     * 物品名称
     */
    private String pName;
    /**
     * 商品条码
     */
    private String pBarCode;
    /**
     * 规格型号
     */
    private String model;
    /**
     * 单位
     */
    private String unit;
    /**
     * 库存最小
     */
    private Integer safeMin;
    /**
     * 库存最大
     */
    private Integer safeMax;
    /**
     * 入库数量
     */
    private Integer num;
    /**
     * 入库单价
     */
    private String price;
    /**
     * 入库总价
     */
    private String totalPrice;

    @ExcelCollection(name = "入库明细", orderNum = "9")
    private List<ReceiptInGetPageExtends> list;

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public List<ReceiptInGetPageExtends> getList() {
        return list;
    }

    public void setList(List<ReceiptInGetPageExtends> list) {
        this.list = list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpBarCode() {
        return pBarCode;
    }

    public void setpBarCode(String pBarCode) {
        this.pBarCode = pBarCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getSafeMin() {
        return safeMin;
    }

    public void setSafeMin(Integer safeMin) {
        this.safeMin = safeMin;
    }

    public Integer getSafeMax() {
        return safeMax;
    }

    public void setSafeMax(Integer safeMax) {
        this.safeMax = safeMax;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
