package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * ReceiptUser
 *
 * @author zhaozheyu
 * @Description 用户状态反参
 * @date 2020/8/19 14:32
 */
public class ReceiptUser {
    private String type;
    private Integer dia;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }
}
