package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/5 18:51
 */
public class Search {

    private String search;
    private String inventoryId;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }
}
