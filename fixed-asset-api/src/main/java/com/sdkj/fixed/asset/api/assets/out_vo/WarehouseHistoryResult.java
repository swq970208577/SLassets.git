package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.assets.AssetLog;
import com.sdkj.fixed.asset.pojo.assets.WarehouseHistory;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 查询结果
 * @Date 2020/7/22 17:20
 */
public class WarehouseHistoryResult extends WarehouseHistory {

    @Excel(name="资产类别",orderNum = "4")
    private String assetClassName;
    @Excel(name="区域",orderNum = "12")
    private String areaName;
    @Excel(name="所属公司",orderNum = "15")
    private String companyName;
    @Excel(name="使用公司",orderNum = "9")
    private String useCompanyName;
    @Excel(name="使用部门",orderNum = "10")
    private String useDeptName;
    @Excel(name="供应商",orderNum = "17")
    private String supplierName;
    // 联系人
    private String contact;
    // 联系方式
    private String contactInfo;
    // 供应商ID
    private String supplierId;
    // 负责人ID
    private String supplierUserId;
    // 负责人
    private String supplierUser;
    // 维保到期日
    private String expireTime;
    // 维保说明
    private String explainText;



    @Excel(name="创建人",orderNum = "20")
    private String createUserName;
    private Integer transferState;
    private String transferInDate;
    private List<AssetLog> logList;

    public String getTransferInDate() {
        return transferInDate;
    }

    public void setTransferInDate(String transferInDate) {
        this.transferInDate = transferInDate;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierUserId() {
        return supplierUserId;
    }

    public void setSupplierUserId(String supplierUserId) {
        this.supplierUserId = supplierUserId;
    }

    public String getSupplierUser() {
        return supplierUser;
    }

    public void setSupplierUser(String supplierUser) {
        this.supplierUser = supplierUser;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getExplainText() {
        return explainText;
    }

    public void setExplainText(String explainText) {
        this.explainText = explainText;
    }

    public Integer getTransferState() {
        return transferState;
    }

    public void setTransferState(Integer transferState) {
        this.transferState = transferState;
    }

    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public List<AssetLog> getLogList() {
        return logList;
    }

    public void setLogList(List<AssetLog> logList) {
        this.logList = logList;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
}
