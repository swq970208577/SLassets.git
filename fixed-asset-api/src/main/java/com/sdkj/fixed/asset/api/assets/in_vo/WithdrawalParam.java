package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.Withdrawal;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 退库
 * @Date 2020/7/28 17:59
 */
public class WithdrawalParam extends Withdrawal {
    @NotEmpty
    private List<String> assetIdList;

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }
}
