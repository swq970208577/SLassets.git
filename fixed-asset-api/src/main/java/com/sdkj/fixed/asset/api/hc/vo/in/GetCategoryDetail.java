package com.sdkj.fixed.asset.api.hc.vo.in;

/**
 * GetCategoryDetail
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/31 9:34
 */
public class GetCategoryDetail {

    private String productId;

    private String productName;

    private Integer num;

    private String categoryId;

    private String categoryName;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
