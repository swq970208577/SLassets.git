package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 退库导出
 * @Date 2020/7/28 14:47
 */
public class WithdrawalExport {

    @Excel(name = "办理状态",replace={"待签字_1","已签字_2"} ,orderNum = "0",needMerge = true)
    private Integer state;

    @Excel(name = "退库单号",orderNum = "1",needMerge = true)
    private String withdrawalNo;

    @Excel(name = "退库日期",orderNum = "2",needMerge = true)
    private String withdrawalDate;

    @Excel(name = "退库后使用公司",orderNum = "3",needMerge = true)
    private String withdrawalCompanyName;

    @Excel(name = "退库后区域",orderNum = "4",needMerge = true)
    private String withdrawalAreaName;

    @Excel(name = "退库后存放地点",orderNum = "5",needMerge = true)
    private String withdrawalAddress;

    @Excel(name = "退库备注",orderNum = "6",needMerge = true)
    private String withdrawalRemark;

    @Excel(name = "处理人",orderNum = "7",needMerge = true)
    private String handlerUser;

    @ExcelCollection(name = "资产明细",orderNum = "8")
    private List<WarehouseBase> list;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getWithdrawalNo() {
        return withdrawalNo;
    }

    public void setWithdrawalNo(String withdrawalNo) {
        this.withdrawalNo = withdrawalNo;
    }

    public String getWithdrawalDate() {
        return withdrawalDate;
    }

    public void setWithdrawalDate(String withdrawalDate) {
        this.withdrawalDate = withdrawalDate;
    }

    public String getWithdrawalCompanyName() {
        return withdrawalCompanyName;
    }

    public void setWithdrawalCompanyName(String withdrawalCompanyName) {
        this.withdrawalCompanyName = withdrawalCompanyName;
    }

    public String getWithdrawalAreaName() {
        return withdrawalAreaName;
    }

    public void setWithdrawalAreaName(String withdrawalAreaName) {
        this.withdrawalAreaName = withdrawalAreaName;
    }

    public String getWithdrawalAddress() {
        return withdrawalAddress;
    }

    public void setWithdrawalAddress(String withdrawalAddress) {
        this.withdrawalAddress = withdrawalAddress;
    }

    public String getWithdrawalRemark() {
        return withdrawalRemark;
    }

    public void setWithdrawalRemark(String withdrawalRemark) {
        this.withdrawalRemark = withdrawalRemark;
    }

    public String getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser;
    }

    public List<WarehouseBase> getList() {
        return list;
    }

    public void setList(List<WarehouseBase> list) {
        this.list = list;
    }
}
