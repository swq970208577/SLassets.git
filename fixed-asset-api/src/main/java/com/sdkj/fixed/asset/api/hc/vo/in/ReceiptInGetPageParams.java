package com.sdkj.fixed.asset.api.hc.vo.in;

import com.sdkj.fixed.asset.api.hc.vo.out.ReceiptInGetPage;

import javax.validation.constraints.NotNull;

/**
 * ReceiptInGetPageParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/21 14:45
 */
public class ReceiptInGetPageParams {
    private String categoryId;

    private String number;

    private String orgId;
    @NotNull(message = "类型-不能为空", groups = {ReceiptInGetPage.class})
    private Integer type;

    private String userId;

    private Integer isAdmin;

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? "" : categoryId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? "" : number;
    }
}
