package com.sdkj.fixed.asset.api.login.pojo;


import com.sdkj.fixed.asset.api.login.out_vo.CompanyBaseListResult;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 登录用户信息，用于存储当前登录用户基础信息
 * @author 张欣
 */
public class LoginUser implements  Serializable, LoginUserBase {
    /**
     * 登录用户id
     */
    @ApiModelProperty(value = "登录用户id")
    public  String userId;
    /**
     * 登录用户姓名
     */
    @ApiModelProperty(value = "登录用户姓名")
    public String userName;
    /**
     * token,用与调用接口权限验证
     */
    @ApiModelProperty(value = "token,用与调用接口权限验证")
    public String token;
    /**
     * 角色信息
     */
    @ApiModelProperty(value = "角色信息",hidden = true)
    public List<LoginRole> loginRoles;
    /**
     * 所属公司id
     */
    private String belongToCompanyId;
    /**
     * 使用版本1:免费版;2：标准版;3:专业版
     */
    @ApiModelProperty(value = "使用版本1:免费版;2：标准版;3:专业版")
    private Integer version;

    public void setLoginRoles(List<LoginRole> loginRoles) {
        this.loginRoles = loginRoles;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    @Override
    public List<LoginRole> getLoginRoles() {
        return loginRoles;
    }
    @Override
    public String getUserId() {
        return userId;
    }
    @Override
    public String getUserName() {
        return userName;
    }
    /**
     * 公司信息
     */
    @ApiModelProperty(value = "公司信息")
    private List<CompanyBaseListResult> companys;

    public List<CompanyBaseListResult> getCompanys() {
        return companys;
    }

    public void setCompanys(List<CompanyBaseListResult> companys) {
        this.companys = companys;
    }

    public String getBelongToCompanyId() {
        return belongToCompanyId;
    }

    public void setBelongToCompanyId(String belongToCompanyId) {
        this.belongToCompanyId = belongToCompanyId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
