package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/5 17:38
 */
public class StateNum {

    private String stateNum;
    private String stateNum_0;
    private String stateNum_1;

    public String getStateNum() {
        return stateNum;
    }

    public void setStateNum(String stateNum) {
        this.stateNum = stateNum;
    }

    public String getStateNum_0() {
        return stateNum_0;
    }

    public void setStateNum_0(String stateNum_0) {
        this.stateNum_0 = stateNum_0;
    }

    public String getStateNum_1() {
        return stateNum_1;
    }

    public void setStateNum_1(String stateNum_1) {
        this.stateNum_1 = stateNum_1;
    }
}
