package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.CollectResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author zhangjinfei
 * @Date 2020/7/27 9:58
 */
@RequestMapping("/assets/collect/api")
public interface CollectApi {

    /**
     * 分页查询
     * @param params
     * @return
     */
    @PostMapping("/queryPages")
    public BaseResultVo<CollectResult> queryPages(@RequestBody PageParams<SearchNameParam> params);

    /**
     * 查看
     * @param collectId
     * @return
     */
    @GetMapping("/queryCollect")
    public BaseResultVo<CollectResult> queryCollect(@RequestParam("collectId") String  collectId);

    /**
     * 添加领用
     * @param collect
     * @return
     */
    @PostMapping("/insertCollect")
    public BaseResultVo insertCollect(@RequestBody CollectParam collect);



    /**
     * 修改领用
     * @param collect
     * @return
     */
    @PostMapping("/updateCollect")
    public BaseResultVo updateCollect(@RequestBody CollectParam collect) throws Exception;

    /**
     * 领用签字
     * @param param
     * @return
     */
    @PostMapping(value = "/updState")
    public BaseResultVo updateSign(@RequestBody SignatureParam param);
    /**
     * 删除领用
     * @param receiveId
     * @return
     */
    @GetMapping("/deleteCollect")
    public BaseResultVo deleteCollect(@RequestParam("receiveId") String receiveId) throws Exception;

    /**
     * 领用导出
     */
    @GetMapping("/exportCollect")
    public void exportCollect(HttpServletResponse response,@RequestParam("token")String token,@RequestParam(value = "searchName",required = false)String searchName) throws Exception;
    /**
     * 领用打印
     */
    @GetMapping("/printCollect")
    public void printCollect(HttpServletResponse response,@RequestParam("collectIds")String collectIds) throws Exception;
// ============================================

    /**
     * 添加领用申请
     * @param collect
     * @return
     */
    @PostMapping("/insertCollectApply")
    public BaseResultVo insertCollectApply(@RequestBody CollectApplyParam collect);

    /**
     * 同意审批
     * @param collect
     * @return
     */
    @PostMapping("/collectApplyAgree")
    public BaseResultVo collectApplyAgree(@RequestBody CollectApplyAgreeParam collect);

    /**
     * 拒绝审批
     * @param collect
     * @return
     */
    @PostMapping("/collectApplyRefuse")
    public BaseResultVo collectApplyRefuse(@RequestBody CollectApplyAgreeParam collect);

    /**
     * 分页查询
     * @param param
     * @retu
     */
    @PostMapping("/getAllCollectApply")
    public BaseResultVo getAllCollectApply(@RequestBody ApplySelParam param);
    /**
     * 签字查询
     * @param id
     * @retu
     */
    @GetMapping("/getCollectApplyById")
    public BaseResultVo getCollectApplyById(@RequestParam("id") String id,@RequestParam("state") Integer state);
}
