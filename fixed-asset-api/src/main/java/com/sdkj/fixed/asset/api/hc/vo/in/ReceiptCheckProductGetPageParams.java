package com.sdkj.fixed.asset.api.hc.vo.in;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ReceiptCheckGetPageParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 10:29
 */
public class ReceiptCheckProductGetPageParams {
    @Size(max = 32, message = "盘点单id-最大长度:32")
    @NotNull(message = "盘点单id-不能为空")
    private String receiptId;

    private String name;
    /**
     * 状态（1已盘2未盘）
     */
    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }
}
