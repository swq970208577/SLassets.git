package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zhangjinfei
 * @Description //TODO 到期资产报表 //月增加对账表
 * @Date 2020/8/12 0:26
 */
public class AssetsReportResult {

    /**
     * 资产ID
     */
    private String assetId;
    /**
     * 领用日期
     */
//    @Excel(name="照片",orderNum = "0")
    private String photo;
    /**
     * 资产条码
     */
    @Excel(name="资产条码",orderNum = "1")
    private String assetCode;

    /**
     * 资产名称
     */
    @Excel(name="资产名称",orderNum = "2")
    private String assetName;

    /**
     * 资产类别ID
     */
    private String assetClassId;

    /**
     * 资产类别编码
     */
    @Excel(name="资产类别编码",orderNum = "3")
    private String classNum;

    /**
     * 资产类别名称
     */
    @Excel(name="资产类别",orderNum = "4")
    private String className;

    /**
     * 金额
     */
    @Excel(name="金额",orderNum = "5")
    private String amount;

    /**
     * 标准型号=规格型号ID
     */
    private String standardModel;

    /**
     * 规格型号
     */
    @Excel(name="规格型号",orderNum = "6")
    private String specificationModel;

    /**
     * 计量单位
     */
    @Excel(name="计量单位",orderNum = "7")
    private String unitMeasurement;

    /**
     * 使用公司编码
     */
    @Excel(name="使用公司编码",orderNum = "8")
    private String useCompanyNum;
    /**
     * 使用公司
     */
    @Excel(name="使用公司",orderNum = "9")
    private String useCompanyName;

    /**
     * 使用部门编码
     */
    @Excel(name="使用部门编码",orderNum = "10")
    private String useDeptNum;
    /**
     * 使用部门
     */
    @Excel(name="使用部门",orderNum = "11")
    private String useDeptName;

    /**
     * 使用人
     */
    @Excel(name="使用人",orderNum = "12")
    private String useUserName;

    /**
     * 领用日期
     */
//    @Excel(name="领用日期",orderNum = "13")
    private String collectDate;

    /**
     * 区域编码
     */
    @Excel(name="区域编码",orderNum = "15")
    private String areaNum;
    /**
     * 区域
     */
    @Excel(name="区域",orderNum = "16")
    private String areaName;
    /**
     * 存放地点
     */
    @Excel(name="存放地点",orderNum = "17")
    private String address;

    /**
     * 管理员
     */
    @Excel(name="管理员",orderNum = "18")
    private String adminName;

    /**
     * 所属公司编码
     */
    @Excel(name="所属公司编码",orderNum = "19")
    private String companyNum;

    /**
     * 所属公司
     */
    @Excel(name="所属公司",orderNum = "20")
    private String companyName;


    /**
     * 购入时间
     */
    @Excel(name="购入时间",orderNum = "21")
    private String purchaseTime;


    /**
     * 供应商
     */
    @Excel(name="供应商",orderNum = "22")
    private String suppelier;

    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "23")
    private String remark;

    /**
     * 存放地点
     */
    private String storageLocation;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetClassId() {
        return assetClassId;
    }

    public void setAssetClassId(String assetClassId) {
        this.assetClassId = assetClassId;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStandardModel() {
        return standardModel;
    }

    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel;
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }

    public String getUseCompanyNum() {
        return useCompanyNum;
    }

    public void setUseCompanyNum(String useCompanyNum) {
        this.useCompanyNum = useCompanyNum;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptNum() {
        return useDeptNum;
    }

    public void setUseDeptNum(String useDeptNum) {
        this.useDeptNum = useDeptNum;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getUseUserName() {
        return useUserName;
    }

    public void setUseUserName(String useUserName) {
        this.useUserName = useUserName;
    }

    public String getCollectDate() {
        return collectDate;
    }

    public void setCollectDate(String collectDate) {
        this.collectDate = collectDate;
    }

    public String getAreaNum() {
        return areaNum;
    }

    public void setAreaNum(String areaNum) {
        this.areaNum = areaNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getCompanyNum() {
        return companyNum;
    }

    public void setCompanyNum(String companyNum) {
        this.companyNum = companyNum;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getSuppelier() {
        return suppelier;
    }

    public void setSuppelier(String suppelier) {
        this.suppelier = suppelier;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }
}
