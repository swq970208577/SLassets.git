package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.TransferParam;
import com.sdkj.fixed.asset.api.assets.in_vo.TransferSearchParam;
import com.sdkj.fixed.asset.api.assets.out_vo.TransferResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangjinfei
 * @Date 2020/7/30 16:37
 */
@RequestMapping("/assets/transfer/api")
public interface TransferApi {


    /**
     * 分页查询
     * @param params
     * @return
     */
    @PostMapping("/queryPages")
    public BaseResultVo<TransferResult> queryPages(@RequestBody PageParams<TransferSearchParam> params);

    /**
     * 查看
     * @param transferId
     * @return
     */
    @GetMapping("/queryTransfer")
    public BaseResultVo<TransferResult> queryTransfer(@RequestParam("transferId") String  transferId);

    /**
     * 添加调拨
     * @param transfer
     * @return
     */
    @PostMapping("/insertTransfer")
    public BaseResultVo insertTransfer(@RequestBody @Validated TransferParam transfer);

    /**
     * 调拨调入
     * @param transfer
     * @return
     */
    @PostMapping("/transferSuccess")
    public BaseResultVo transferSuccess(@RequestBody  TransferParam transfer) throws Exception;

    /**
     * 调拨打回
     * @param transfer
     * @return
     */
    @PostMapping("/transferBack")
    public BaseResultVo transferBack(@RequestBody TransferParam transfer) throws Exception;

    /**
     * 调拨取消
     * @param transferId
     * @return
     */
    @GetMapping("/updateCancel")
    public BaseResultVo updateCancel(@RequestParam("transferId") String transferId) throws Exception;

    /**
     * 调拨打印
     */
    @GetMapping("/printTransfer")
    public void printTransfer(HttpServletResponse response,@RequestParam("transferIds") String transferIds) throws IOException;

    /**
     * 查询调入公司管理员
     */
    @GetMapping("/transferInAdmin")
    public BaseResultVo<UserManagement> transferInAdmin(@RequestParam("companyId") String companyId) throws IOException;
}
