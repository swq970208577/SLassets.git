package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * ReceiptOutCheck
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/7 16:38
 */
public class ReceiptOutCheck {
    private String id;

    @Excel(name = "出库单号", needMerge = true, orderNum = "1")
    private String number;

    @Excel(name = "出库仓库", needMerge = true, orderNum = "2")
    private String categoryName;

    private String categoryId;

    @Excel(name = "出库日期", needMerge = true, orderNum = "3")
    private String bussinessDate;

    @Excel(name = "经办时间", needMerge = true, orderNum = "4")
    private String ctime;

    private String cuser;

    @Excel(name = "经办人", needMerge = true, orderNum = "5")
    private String cuserName;

    @Excel(name = "出库备注", needMerge = true, orderNum = "6")
    private String comment;

    @ExcelCollection(name = "出库明细", orderNum = "7")
    private List<ReceipOutGetPageExtends> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<ReceipOutGetPageExtends> getList() {
        return list;
    }

    public void setList(List<ReceipOutGetPageExtends> list) {
        this.list = list;
    }
}
