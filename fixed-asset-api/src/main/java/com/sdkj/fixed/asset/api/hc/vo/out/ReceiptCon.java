package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ReceiptCon
 *
 * @author zhaozheyu
 * @Description 耗材领用LIst
 * @date 2020/8/12 16:34
 */
public class ReceiptCon {

    @Excel(name="耗材分类",orderNum = "1",width = 20)
    private String flname;

    @Excel(name="耗材名称",orderNum = "2",width = 20)
    private String hcname;

    @Excel(name="领用单号",orderNum = "3",width = 20)
    private String number;

    @Excel(name="领用日期",orderNum = "4",width = 20)
    private String date;

    @Excel(name="领用量",orderNum = "5",width = 20)
    private Integer lynum;

    @Excel(name="金额",orderNum = "6",width = 20)
    private String price;


    public String getFlname() {
        return flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }

    public String getHcname() {
        return hcname;
    }

    public void setHcname(String hcname) {
        this.hcname = hcname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getLynum() {
        return lynum;
    }

    public void setLynum(Integer lynum) {
        this.lynum = lynum;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
