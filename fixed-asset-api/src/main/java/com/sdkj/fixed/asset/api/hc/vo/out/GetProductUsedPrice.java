package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * GetProductUsedprice
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/13 10:35
 */
public class GetProductUsedPrice {
    @Excel(name = "1月", orderNum = "1", width = 10)
    private String price1;
    @Excel(name = "2月", orderNum = "2", width = 10)
    private String price2;
    @Excel(name = "3月", orderNum = "3", width = 10)
    private String price3;
    @Excel(name = "4月", orderNum = "4", width = 10)
    private String price4;
    @Excel(name = "5月", orderNum = "5", width = 10)
    private String price5;
    @Excel(name = "6月", orderNum = "6", width = 10)
    private String price6;
    @Excel(name = "7月", orderNum = "7", width = 10)
    private String price7;
    @Excel(name = "8月", orderNum = "8", width = 10)
    private String price8;
    @Excel(name = "9月", orderNum = "9", width = 10)
    private String price9;
    @Excel(name = "10月", orderNum = "10", width = 10)
    private String price10;
    @Excel(name = "11月", orderNum = "11", width = 10)
    private String price11;
    @Excel(name = "12月", orderNum = "12", width = 10)
    private String price12;
    @Excel(name = "平均", orderNum = "13", width = 10)
    private String priceAve;

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getPrice2() {
        return price2;
    }

    public void setPrice2(String price2) {
        this.price2 = price2;
    }

    public String getPrice3() {
        return price3;
    }

    public void setPrice3(String price3) {
        this.price3 = price3;
    }

    public String getPrice4() {
        return price4;
    }

    public void setPrice4(String price4) {
        this.price4 = price4;
    }

    public String getPrice5() {
        return price5;
    }

    public void setPrice5(String price5) {
        this.price5 = price5;
    }

    public String getPrice6() {
        return price6;
    }

    public void setPrice6(String price6) {
        this.price6 = price6;
    }

    public String getPrice7() {
        return price7;
    }

    public void setPrice7(String price7) {
        this.price7 = price7;
    }

    public String getPrice8() {
        return price8;
    }

    public void setPrice8(String price8) {
        this.price8 = price8;
    }

    public String getPrice9() {
        return price9;
    }

    public void setPrice9(String price9) {
        this.price9 = price9;
    }

    public String getPrice10() {
        return price10;
    }

    public void setPrice10(String price10) {
        this.price10 = price10;
    }

    public String getPrice11() {
        return price11;
    }

    public void setPrice11(String price11) {
        this.price11 = price11;
    }

    public String getPrice12() {
        return price12;
    }

    public void setPrice12(String price12) {
        this.price12 = price12;
    }

    public String getPriceAve() {
        return priceAve;
    }

    public void setPriceAve(String priceAve) {
        this.priceAve = priceAve;
    }
}
