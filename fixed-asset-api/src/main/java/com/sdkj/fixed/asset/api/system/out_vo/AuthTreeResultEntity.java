package com.sdkj.fixed.asset.api.system.out_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 权限树返回结果
 * @author 张欣
 */
public class AuthTreeResultEntity {
    /**
     * 对应权限id
     */
    @ApiModelProperty(value = "对应权限id")
    private String authId;
    /**
     * 父id
     */
    @ApiModelProperty(value = "父id")
    private String parentId;
    /**
     * 权限名称
     */
    @ApiModelProperty(value = "权限名称")
    private String name;
    /**
     * 菜单等级， 层级类型：0：分类；1：菜单；2：表格外按钮；3：表格内按钮；4：Tab
     * 服务类型（0服务1服务组2按钮）
     */
    @ApiModelProperty(value = "服务类型（0服务1服务组2按钮）/n 菜单等级， 层级类型：0：分类；1：菜单；2：表格外按钮；3：表格内按钮；4：Tab")
    private String level;
    /**
     * 是否已绑定 0：否；1：是
     */
    private Integer isChecked;
    @ApiModelProperty(hidden = true)
    private String treeCode;
    private Integer sort;
    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Integer isChecked) {
        this.isChecked = isChecked;
    }

    public String getTreeCode() {
        return treeCode;
    }

    public void setTreeCode(String treeCode) {
        this.treeCode = treeCode;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
