package com.sdkj.fixed.asset.api.system.in_vo;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/9 15:32
 */
public class UsageModifyParam {
    @NotBlank(message = "主键id")
    @ApiModelProperty(value = "主键id")
    private String id;
    @NotNull
    @ApiModelProperty(value = "类型")
    private Integer type;
    @NotBlank(message = "购买资产数量")
    @ApiModelProperty(value = "购买资产数量")
    private String number;
    @NotBlank(message = "")
    @ApiModelProperty(value = "到期时间")
    private String expireDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }
}
