package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @ClassName PrintLableParam
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/3 17:19
 */
public class PrintLabelParam {
    /**
     * ip
     */
    @NotBlank(message = "ip不能为空")
    private String ip;
    /**
     * 端口号
     */
    @NotBlank(message = "端口号不能为空")
    private String port;
    /**
     * 资产ids
     */
    @NotNull(message = "资产ids不能为空")
    private List<String> assatIds;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public List<String> getAssatIds() {
        return assatIds;
    }

    public void setAssatIds(List<String> assatIds) {
        this.assatIds = assatIds;
    }
}
