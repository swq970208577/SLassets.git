package com.sdkj.fixed.asset.api.login.in_vo;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 移动端登录参数
 * @author 张欣
 */
public class LoginParamMobile {
    /**
     * 微信唯一识别码
     */
    @ApiModelProperty(value = "微信唯一识别码")
    @NotBlank(message = "账号不能为空")
    private String weChat;

    public String getWeChat() {
        return weChat;
    }

    public void setWeChat(String weChat) {
        this.weChat = weChat.trim();
    }
}
