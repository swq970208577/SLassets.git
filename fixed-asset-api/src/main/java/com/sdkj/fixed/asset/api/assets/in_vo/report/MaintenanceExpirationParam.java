package com.sdkj.fixed.asset.api.assets.in_vo.report;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName AssetListParam
 * @Description 资产履历参数
 * @Author 张欣
 * @Date 2020/7/30 16:47
 */
public class MaintenanceExpirationParam {
    /**
     * 开始时间
     */
    @NotBlank(message = "开始时间不能为空")
    private String startTime;
    /**
     * 结束时间
     */
    @NotBlank(message = "结束时间不能为空")
    private String endTime;
    /**
     * 搜索框
     */
    private String search;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null?"":startTime.trim();
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime==null?"":endTime.trim();
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
