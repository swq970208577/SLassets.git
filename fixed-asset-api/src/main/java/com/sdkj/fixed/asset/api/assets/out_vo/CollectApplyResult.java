package com.sdkj.fixed.asset.api.assets.out_vo;

import com.sdkj.fixed.asset.pojo.assets.Collect;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用审批
 * @Date 2020/8/13 15:28
 */
public class CollectApplyResult extends Collect {

    /**
     * 申请资产集合 申请明细
     */
    private List<AssetReceiveApply> assetReceiveList;
    /**
     * 审批发放资产  发放明细
     */
    private List<CollectResult> collectResultList;

    public List<AssetReceiveApply> getAssetReceiveList() {
        return assetReceiveList;
    }

    public void setAssetReceiveList(List<AssetReceiveApply> assetReceiveList) {
        this.assetReceiveList = assetReceiveList;
    }

    public List<CollectResult> getCollectResultList() {
        return collectResultList;
    }

    public void setCollectResultList(List<CollectResult> collectResultList) {
        this.collectResultList = collectResultList;
    }
}
