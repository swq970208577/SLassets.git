package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用签字
 * @Date 2020/8/15 11:07
 */
public class CollectSignResult extends CollectResult {

    private CollectResult applyResult;

    public CollectResult getApplyResult() {
        return applyResult;
    }

    public void setApplyResult(CollectResult applyResult) {
        this.applyResult = applyResult;
    }
}
