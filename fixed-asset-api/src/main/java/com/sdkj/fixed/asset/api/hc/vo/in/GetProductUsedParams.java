package com.sdkj.fixed.asset.api.hc.vo.in;

/**
 * GetProductUsed
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/13 10:32
 */
public class GetProductUsedParams {
    private String productId;

    private String orgId;

    private String deptId;

    private String year;

    private String userId;

    private String start;

    private String end;

    private String date1s;

    private String date1e;

    private String date2s;

    private String date2e;

    private String date3s;

    private String date3e;

    private String date4s;

    private String date4e;

    private String date5s;

    private String date5e;

    private String date6s;

    private String date6e;

    private String date7s;

    private String date7e;

    private String date8s;

    private String date8e;

    private String date9s;

    private String date9e;

    private String date10s;

    private String date10e;

    private String date11s;

    private String date11e;

    private String date12s;

    private String date12e;

    private String dateAlls;

    private String dateAlle;

    private Integer isAdmin;

    private String orgId1;

    public String getOrgId1() {
        return orgId1;
    }

    public void setOrgId1(String orgId1) {
        this.orgId1 = orgId1;
    }

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getDateAlls() {
        return dateAlls;
    }

    public void setDateAlls(String dateAlls) {
        this.dateAlls = dateAlls;
    }

    public String getDateAlle() {
        return dateAlle;
    }

    public void setDateAlle(String dateAlle) {
        this.dateAlle = dateAlle;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDate1s() {
        return date1s;
    }

    public void setDate1s(String date1s) {
        this.date1s = date1s;
    }

    public String getDate1e() {
        return date1e;
    }

    public void setDate1e(String date1e) {
        this.date1e = date1e;
    }

    public String getDate2s() {
        return date2s;
    }

    public void setDate2s(String date2s) {
        this.date2s = date2s;
    }

    public String getDate2e() {
        return date2e;
    }

    public void setDate2e(String date2e) {
        this.date2e = date2e;
    }

    public String getDate3s() {
        return date3s;
    }

    public void setDate3s(String date3s) {
        this.date3s = date3s;
    }

    public String getDate3e() {
        return date3e;
    }

    public void setDate3e(String date3e) {
        this.date3e = date3e;
    }

    public String getDate4s() {
        return date4s;
    }

    public void setDate4s(String date4s) {
        this.date4s = date4s;
    }

    public String getDate4e() {
        return date4e;
    }

    public void setDate4e(String date4e) {
        this.date4e = date4e;
    }

    public String getDate5s() {
        return date5s;
    }

    public void setDate5s(String date5s) {
        this.date5s = date5s;
    }

    public String getDate5e() {
        return date5e;
    }

    public void setDate5e(String date5e) {
        this.date5e = date5e;
    }

    public String getDate6s() {
        return date6s;
    }

    public void setDate6s(String date6s) {
        this.date6s = date6s;
    }

    public String getDate6e() {
        return date6e;
    }

    public void setDate6e(String date6e) {
        this.date6e = date6e;
    }

    public String getDate7s() {
        return date7s;
    }

    public void setDate7s(String date7s) {
        this.date7s = date7s;
    }

    public String getDate7e() {
        return date7e;
    }

    public void setDate7e(String date7e) {
        this.date7e = date7e;
    }

    public String getDate8s() {
        return date8s;
    }

    public void setDate8s(String date8s) {
        this.date8s = date8s;
    }

    public String getDate8e() {
        return date8e;
    }

    public void setDate8e(String date8e) {
        this.date8e = date8e;
    }

    public String getDate9s() {
        return date9s;
    }

    public void setDate9s(String date9s) {
        this.date9s = date9s;
    }

    public String getDate9e() {
        return date9e;
    }

    public void setDate9e(String date9e) {
        this.date9e = date9e;
    }

    public String getDate10s() {
        return date10s;
    }

    public void setDate10s(String date10s) {
        this.date10s = date10s;
    }

    public String getDate10e() {
        return date10e;
    }

    public void setDate10e(String date10e) {
        this.date10e = date10e;
    }

    public String getDate11s() {
        return date11s;
    }

    public void setDate11s(String date11s) {
        this.date11s = date11s;
    }

    public String getDate11e() {
        return date11e;
    }

    public void setDate11e(String date11e) {
        this.date11e = date11e;
    }

    public String getDate12s() {
        return date12s;
    }

    public void setDate12s(String date12s) {
        this.date12s = date12s;
    }

    public String getDate12e() {
        return date12e;
    }

    public void setDate12e(String date12e) {
        this.date12e = date12e;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
