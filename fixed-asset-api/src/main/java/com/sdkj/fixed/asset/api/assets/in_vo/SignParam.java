package com.sdkj.fixed.asset.api.assets.in_vo;

/**
 * @Author zhangjinfei
 * @Description //TODO 签名参数
 * @Date 2020/7/30 11:24
 */
public class SignParam {

    private String id;
    private Integer sign;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }
}
