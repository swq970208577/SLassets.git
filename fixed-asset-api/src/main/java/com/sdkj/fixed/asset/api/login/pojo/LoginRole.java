package com.sdkj.fixed.asset.api.login.pojo;

import java.io.Serializable;

/**
 * 存储当前登录用户角色信息
 * @author 张欣
 */
public class LoginRole implements Serializable {
    /**
     * 角色id
     */
    private String roleId;
    /**
     * 1:超管；2：管理员；3普通用户
     */
    private String roleLevel;
    /**
     * 角色所属公司
     */
    private String companyId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleLevel() {
        return roleLevel;
    }

    public void setRoleLevel(String roleLevel) {
        this.roleLevel = roleLevel;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
