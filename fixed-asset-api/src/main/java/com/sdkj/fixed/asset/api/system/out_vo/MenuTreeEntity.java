package com.sdkj.fixed.asset.api.system.out_vo;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单树返回结果
 * @author 张欣
 */
public class MenuTreeEntity {
    /**
     * 对应权限id
     */

    private String value;
    /**
     * 父id
     */

    private String parentId;
    /**
     * 权限名称
     */

    private String label;
    /**
     * 对应权限id
     */

    private String authId;
    /**
     * 菜单等级， 层级类型：1：分类；2：菜单；3：其他
     *
     */

    private String level;

    private String sourceKey;
    private List<MenuTreeEntity> children = new ArrayList<MenuTreeEntity>();

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<MenuTreeEntity> getChildren() {
        return children;
    }

    public void setChildren(List<MenuTreeEntity> children) {
        this.children = children;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }
}
