package com.sdkj.fixed.asset.api.system.in_vo;


import com.sdkj.fixed.asset.api.system.out_vo.AssestClass;
import com.sdkj.fixed.asset.pojo.system.OrgManagement;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @ClassName UserDataAuth
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/23 16:03
 */
public class UserDataAuth {
    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String userId;
    /**
     * 公司部门授权
     */
    @Valid
    private List< @NotBlank(message = "公司/部门id不能为空") String> orgList;
    /**
     * 资产类别
     */
    @Valid
    private List<@NotBlank(message = "资产类别id不能为空")String> assetClassList;
    /**
     * 区域
     */
    @Valid
    private List<@NotBlank(message = "区域id不能为空") String> regionList;
    /**
     * 仓库
     */
    @Valid
    private List<@NotBlank(message = "仓库id不能为空") String> warehouseList;

    public List<String> getOrgList() {
        return orgList;
    }

    public void setOrgList(List<String> orgList) {
        this.orgList = orgList;
    }

    public List<String> getAssetClassList() {
        return assetClassList;
    }

    public void setAssetClassList(List<String> assetClassList) {
        this.assetClassList = assetClassList;
    }

    public List<String> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<String> regionList) {
        this.regionList = regionList;
    }

    public List<String> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<String> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
