package com.sdkj.fixed.asset.api.system.in_vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/9 10:34
 */
public class UsageParam {

    //手机号和公司名称
    @ApiModelProperty(value = "模糊查询条件")
    private String search;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
