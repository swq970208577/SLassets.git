package com.sdkj.fixed.asset.api.assets.in_vo;

import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseHistoryResult;
import com.sdkj.fixed.asset.pojo.assets.AssetInfoChange;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/27 11:19
 */
public class AssetInfoChangeExtend extends AssetInfoChange {

    /**
     * 使用公司
     */
    private String useCompanyName;
    /**
     * 使用部门
     */
    private String useDepartmentName;
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 分类名称名称
     */
    private String className;

    private String supplierName;

    private List<AssetInfoChangeAssetExtend> infoChangeAssetExtendList = new ArrayList<>();

    /**
     * 资产id
     */

    @NotEmpty
    private List<String> assetIdList = new ArrayList<>();
    /**
     * 资产基本信息
     */
    @ExcelCollection(name = "资产明细", orderNum = "3")
    List<WarehouseHistoryResult> results = new ArrayList<>();

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }

    public List<WarehouseHistoryResult> getResults() {
        return results;
    }

    public void setResults(List<WarehouseHistoryResult> results) {
        this.results = results;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDepartmentName() {
        return useDepartmentName;
    }

    public void setUseDepartmentName(String useDepartmentName) {
        this.useDepartmentName = useDepartmentName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public List<AssetInfoChangeAssetExtend> getInfoChangeAssetExtendList() {
        return infoChangeAssetExtendList;
    }

    public void setInfoChangeAssetExtendList(List<AssetInfoChangeAssetExtend> infoChangeAssetExtendList) {
        this.infoChangeAssetExtendList = infoChangeAssetExtendList;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
}
