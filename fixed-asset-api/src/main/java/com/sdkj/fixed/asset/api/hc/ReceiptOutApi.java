package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.IdParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptOutGetPageParams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ReceiptOut;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * @author 史晨星
 * @ClassName: ReceiptInApi
 * @Description: 仓库管理
 * @date 2020年7月21日
 */
@RequestMapping("/hc/receiptOut/api")
public interface ReceiptOutApi {


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody ReceiptOut entity) throws Exception;

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody ReceiptOut entity) throws Exception;

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody IdParams entity) throws Exception;

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo view(@RequestBody IdParams entity) throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody PageParams<ReceiptOutGetPageParams> params) throws Exception;

    /**
     * 签字
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/sign", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo sign(@RequestBody ReceiptOut entity) throws Exception;

    /**
     * 打印盘亏出库
     *
     * @param ids
     * @throws Exception
     */
    @RequestMapping(value = "/printCheck", method = RequestMethod.GET)
    @ResponseBody
    public void printCheck(String ids) throws Exception;


    /**
     * 打印出库
     *
     * @param ids
     * @throws Exception
     */
    @RequestMapping(value = "/print", method = RequestMethod.GET)
    @ResponseBody
    public void print(String ids) throws Exception;

    /**
     * 盘亏导出
     *
     * @param number
     * @param userId
     * @param companyId
     * @param categoryId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/exportCheck", method = RequestMethod.GET)
    @ResponseBody
    public void exportCheck(@Param("number") String number, @Param("userId") String userId, @Param("companyId") String companyId, @Param("categoryId") String categoryId, HttpServletResponse response) throws Exception;

    /**
     * 导出
     *
     * @param number
     * @param userId
     * @param companyId
     * @param categoryId
     * @param start
     * @param end
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    @ResponseBody
    public void export(@Param("number") String number, @Param("userId") String userId, @Param("companyId") String companyId, @Param("categoryId") String categoryId, @Param("start") String start, @Param("end") String end, HttpServletResponse response) throws Exception;
}

