package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.PrintLabelParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName PrintLableApi
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/4 10:29
 */
@RequestMapping("/assets/print/api")
public interface PrintLableApi {
    /**
     * 打印标签
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/printLabel",method = RequestMethod.POST)
    public BaseResultVo printLabel(@RequestBody  PrintLabelParam param ) throws Exception;
}
