package com.sdkj.fixed.asset.api.assets.out_vo;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author zhangjinfei
 * @Description //TODO 分类增减
 * @Date 2020/8/13 1:08
 */
public class ClassIncreaseDecrease {

    @Excel(name="资产分类",orderNum = "0")
    private String name;
    @Excel(name="初始数量",orderNum = "1")
    private String initSum;
    @Excel(name="初始金额",orderNum = "2")
    private String initAmount;
    @Excel(name="增加数量",orderNum = "3")
    private String addSum;
    @Excel(name="增加金额",orderNum = "4")
    private String addAmount;
    @Excel(name="减少数量",orderNum = "5")
    private String reduceSum;
    @Excel(name="减少金额",orderNum = "6")
    private String reduceAmount;
    @Excel(name="结存数量",orderNum = "7")
    private String balanceSum;
    @Excel(name="结存金额",orderNum = "8")
    private String balanceAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitSum() {
        return initSum;
    }

    public void setInitSum(String initSum) {
        this.initSum = initSum;
    }

    public String getInitAmount() {
        return initAmount;
    }

    public void setInitAmount(String initAmount) {
        this.initAmount = initAmount;
    }

    public String getAddSum() {
        return addSum;
    }

    public void setAddSum(String addSum) {
        this.addSum = addSum;
    }

    public String getAddAmount() {
        return addAmount;
    }

    public void setAddAmount(String addAmount) {
        this.addAmount = addAmount;
    }

    public String getReduceSum() {
        return reduceSum;
    }

    public void setReduceSum(String reduceSum) {
        this.reduceSum = reduceSum;
    }

    public String getReduceAmount() {
        return reduceAmount;
    }

    public void setReduceAmount(String reduceAmount) {
        this.reduceAmount = reduceAmount;
    }

    public String getBalanceSum() {
        return balanceSum;
    }

    public void setBalanceSum(String balanceSum) {
        this.balanceSum = balanceSum;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
}
