package com.sdkj.fixed.asset.api.system.out_vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName 问题反馈查询反参
 * @Description TODO
 * @Author ZHAOZHEYU
 * @Date 2020/7/21 18:25
 */
@ApiModel(value = "问题反馈查询反参")
public class FedEntity {

    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "问题描述")
    private String content;
    @ApiModelProperty(value = "创建时间")
    private String ctime;
    @ApiModelProperty(value = "机构名称")
    private String orgname;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "解决方案")
    private String solution;
    @ApiModelProperty(value = "手机号")
    private String tel;
    @ApiModelProperty(value = "状态1.已处理2未处理")
    private Integer state;
    @ApiModelProperty(value = "反馈类型1:BUG;2：咨询;3:建议")
    private Integer type;
    @ApiModelProperty(value = "图片")
    private String photo;


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
