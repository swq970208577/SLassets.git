package com.sdkj.fixed.asset.api.assets.out_vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhaozheyu
 * @Description //APP收货签字反参
 * @Date 2020/7/23 16:30
 */
public class AppSignRes implements Serializable {
    @ApiModelProperty(value = "签字类型")
    private String cut;

    @ApiModelProperty(value = "签字时间")
    private String atime;

    @ApiModelProperty(value = "签字编号")
    private String number;

    @ApiModelProperty(value = "申请编号")
    private String sqnumber;

    @ApiModelProperty(value = "发放人")
    private String cluser;

    @ApiModelProperty(value = "签字类型")
    private Integer dig;

    @ApiModelProperty(value = "订单id")
    private String id;

    @ApiModelProperty(value = "申请id")
    private String sqid;

    @ApiModelProperty(value = "详情页文字")
    private String details;

    @ApiModelProperty(value = "物品list")
    private List<AppsignResult> AppsignResult;



    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getSqid() {
        return sqid;
    }

    public void setSqid(String sqid) {
        this.sqid = sqid;
    }

    public String getSqnumber() {
        return sqnumber;
    }

    public void setSqnumber(String sqnumber) {
        this.sqnumber = sqnumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public String getAtime() {
        return atime;
    }

    public void setAtime(String atime) {
        this.atime = atime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCluser() {
        return cluser;
    }

    public void setCluser(String cluser) {
        this.cluser = cluser;
    }

    public Integer getDig() {
        return dig;
    }

    public void setDig(Integer dig) {
        this.dig = dig;
    }

    public List<com.sdkj.fixed.asset.api.assets.out_vo.AppsignResult> getAppsignResult() {
        return AppsignResult;
    }

    public void setAppsignResult(List<com.sdkj.fixed.asset.api.assets.out_vo.AppsignResult> appsignResult) {
        AppsignResult = appsignResult;
    }
}
