package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.*;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ProductType;
import com.sdkj.fixed.asset.pojo.hc.ReceiptApply;
import com.sdkj.fixed.asset.pojo.hc.ReceiptApplySon;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * @author 史晨星
 * @ClassName: CategoryApi
 * @Description: 物品档案-分类
 * @date 2020年7月20日
 */
@RequestMapping("/hc/receiptApply/api")
public interface ReceiptApplyApi {


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody ReceiptApplyParams entity) throws Exception;


    /**
     * 确认
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo confirm(@RequestBody ReceiptApplySon entity) throws Exception;


    /**
     * 确认
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo approve(@RequestBody ReceiptApplySon entity) throws Exception;

    /**
     * 全部领用单
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/getApplyList", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getApplyList(@RequestBody PageParams<GetApplyListParams> params) throws Exception;

    /**
     * 查询库存记录
     *
     * @param receiptApplySon
     * @return
     */
    @RequestMapping(value = "/getCategoryDetail", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getCategoryDetail(@RequestBody ReceiptApplySon receiptApplySon) throws Exception;

    /**
     * 生成出库单
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/createReceiptOut", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo createReceiptOut(@RequestBody CreateReceiptOut entity) throws Exception;


    /**
     * 拒绝
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/refuse", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo refuse(@RequestBody CreateReceiptOut entity) throws Exception;

    /**
     * 查看发放详情
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/getSendDetail", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getSendDetail(@RequestBody PageParams<IdParams> params) throws Exception;


    /**
     * 申请详情
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/getSonApply", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getSonApply(@RequestBody IdParams params) throws Exception;

    /**
     * 申请详情
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/statistic", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo statistic(@RequestBody PageParams<GetProductUsedParams> params) throws Exception;

    /**
     * 导出
     *
     * @param orgId
     * @param deptId
     * @param year
     * @param token
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(String orgId, String deptId, String year, String token, HttpServletResponse response) throws Exception;
}

