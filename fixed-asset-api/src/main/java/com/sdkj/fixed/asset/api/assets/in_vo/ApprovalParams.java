package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author niuliwei
 * @description 审批参数
 * @date 2020/7/29 14:28
 */
public class ApprovalParams {

    @NotBlank(message = "资产借用单明细id不能为空")
    private String id;
    @NotNull(message = "状态不能为空，1审批中2已批准3已驳回")
    private Integer state;
    @NotEmpty(message = "assetId不能为空")
    private List<String> assetIdList;
    @NotBlank(message = "applyUser不能为空")
    private String applyUser;
    @NotBlank(message = "applyUserId不能为空")
    private String applyUserId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }

    public String getApplyUser() {
        return applyUser;
    }

    public void setApplyUser(String applyUser) {
        this.applyUser = applyUser;
    }

    public String getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(String applyUserId) {
        this.applyUserId = applyUserId;
    }
}
