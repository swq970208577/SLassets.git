package com.sdkj.fixed.asset.api.assets.in_vo;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 审批同意参数
 * @Date 2020/8/9 15:35
 */
public class CollectApplyAgreeParam {

    /**
     * 子账单Id
     */
    private String id;
    private String useUserId;
    private String useUserName;
    private String companyId;
    private String companyTreecode;
    private String deptId;
    private String deptTreecode;
    private List<String> assetIdList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUseUserId() {
        return useUserId;
    }

    public void setUseUserId(String useUserId) {
        this.useUserId = useUserId;
    }

    public String getUseUserName() {
        return useUserName;
    }

    public void setUseUserName(String useUserName) {
        this.useUserName = useUserName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyTreecode() {
        return companyTreecode;
    }

    public void setCompanyTreecode(String companyTreecode) {
        this.companyTreecode = companyTreecode;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptTreecode() {
        return deptTreecode;
    }

    public void setDeptTreecode(String deptTreecode) {
        this.deptTreecode = deptTreecode;
    }

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }
}
