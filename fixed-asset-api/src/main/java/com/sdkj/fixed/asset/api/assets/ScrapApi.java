package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.Params;
import com.sdkj.fixed.asset.api.assets.in_vo.ScrapExtend;
import com.sdkj.fixed.asset.api.assets.in_vo.SelPrams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @author niuliwei
 * @description 清理报废
 * @date 2020/7/27 11:22
 */
@RequestMapping("/assets/scrap/api")
public interface ScrapApi {

    /**
     * 清理报废-新增
     * @param scrap
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResultVo add(@Validated @RequestBody ScrapExtend scrap);

    /**
     * 清理报废-分页查询
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAllPage", method = RequestMethod.POST)
    public BaseResultVo getAllPage(@RequestBody PageParams<SelPrams> param);

    /**
     * 清理报废-还原
     * @param params
     */
    @ResponseBody
    @RequestMapping(value = "/reduction", method = RequestMethod.POST)
    public BaseResultVo reduction(@Validated @RequestBody Params params);

    /**
     * 清理报废-查看详情
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selById", method = RequestMethod.POST)
    public BaseResultVo selById(@Validated @RequestBody Params params);

    /**
     * 清理报废-导出
     * @param startDate
     * @param endDate
     * @return
     */
    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public void exportExcel(String startDate, String endDate, String orgId, String token) throws IOException;

    /**
     * 清理报废-打印
     * @param id
     * @return
     */
    @RequestMapping(value = "/print", method = RequestMethod.GET)
    public void print(@Param("id") String id) throws IOException;
}
