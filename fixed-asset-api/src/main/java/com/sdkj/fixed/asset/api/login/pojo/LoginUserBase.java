package com.sdkj.fixed.asset.api.login.pojo;

import java.util.List;

/**
 * @ClassName LoginUserInter
 * @Description TODO
 * @Author 张欣
 * @Date 2020/5/7 16:09
 */
public interface LoginUserBase {
    public List<LoginRole> getLoginRoles();
    public String getUserId();
    public String getUserName();
}
