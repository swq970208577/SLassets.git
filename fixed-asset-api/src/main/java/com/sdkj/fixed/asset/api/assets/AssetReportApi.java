package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.*;
import com.sdkj.fixed.asset.api.assets.out_vo.UserAssetSelParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


/**
 * @Author zhangjinfei
 * @Description //TODO 资产报表
 * @Date 2020/8/11 9:40
 */
@RequestMapping("/assets/assetReport/api")
public interface AssetReportApi {

    /**
     * 清理报报表-分页查询
     * @param param
     * @return
     */
    @RequestMapping(value = "/queryScrapPage", method = RequestMethod.POST)
    public BaseResultVo queryScrapPage(@RequestBody PageParams<SelPrams> param);
    /**
     * 清理报报表-导出
     * @return
     */
    @RequestMapping(value = "/exportScrap", method = RequestMethod.GET)
    public void exportScrap(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, @RequestParam("orgId") String orgId, @RequestParam("token") String token) throws IOException;

    /**
     * 员工资产统计-分页查询
     * @return
     */
    @RequestMapping(value = "/userAssetInfo", method = RequestMethod.POST)
    public BaseResultVo userAssetInfo(@RequestBody PageParams<UserAssetSelParam> params);
    /**
     * 员工资产统计-导出
     * @return
     */
    @RequestMapping(value = "/exportUserAssetInfo", method = RequestMethod.GET)
    public void exportUserAssetInfo(@RequestParam("search") String search, @RequestParam("isOnJob") String isOnJob, @RequestParam("orgId") String orgId, @RequestParam("token") String token) throws IOException;

    /**
     * 标准资产型号统计
     * @param name
     * @return
     */
    @GetMapping("/standardModelReport")
    public BaseResultVo standardModelReport(@RequestParam("name") String name);

    /**
     * 标准资产型号统计导出
     * @param token
     * @param name
     * @throws IOException
     */
    @GetMapping("/exportStandardModelReport")
    public void exportStandardModelReport(@RequestParam("token") String token,@RequestParam(value = "name" ,required = false) String name) throws IOException;

    /**
     * 到期资产
     * @param param
     * @return
     */
    @PostMapping("/dueAssetsReport")
    public BaseResultVo dueAssetsReport(@RequestBody PageParams<WarehouseReportParam > param);


    @GetMapping("/dueAssetsReportRecord")
    public BaseResultVo dueAssetsReportRecord(@RequestParam("assetId") String assetId);

    @PostMapping("/dueAssetsReportRecordLog")
    public BaseResultVo dueAssetsReportRecordLog(@RequestBody PageParams<AssetIdParam> params);

    /**
     * 到期资产导出
     * @param token
     * @param dueDate
     * @param contain
     * @throws IOException
     */
    @GetMapping("/exportDueAssetsReport")
    public void exportDueAssetsReport(@RequestParam("token") String token,@RequestParam(value = "dueDate") String dueDate,@RequestParam(value = "contain") Integer contain) throws IOException;

    /**
     * 月增加对账
     * @param param
     * @return
     */
    @PostMapping("/monthlyIncreasReport")
    public BaseResultVo monthlyIncreasReport(@RequestBody PageParams<WarehouseReportParam > param);

    /**
     * 月增加对账导出
     * @param token
     * @param purchaseMonth
     * @param company
     * @throws IOException
     */
    @GetMapping("/exportMonthlyIncreasReport")
    public void exportMonthlyIncreasReport(@RequestParam("token") String token,@RequestParam(value = "purchaseMonth") String purchaseMonth,@RequestParam(value = "company") String company) throws IOException;

    /**
     * 公司部门汇总表
     * @param param
     * @return
     */
    @PostMapping("/companyDeptReport")
    public BaseResultVo companyDeptReport(@RequestBody ClassIdParam  param);

    /**
     * 公司部门汇总表导出
     * @param token
     * @param classId
     * @throws IOException
     */
    @GetMapping("/exportCompanyDeptReport")
    public void exportCompanyDeptReport(@RequestParam("token") String token,@RequestParam(value = "classId") String classId) throws IOException;

    /**
     * 分类使用情况表
     * @param param
     * @return
     */
    @PostMapping("/useClassReport")
    public BaseResultVo useClassReport(@RequestBody CompanyDeptParam  param);

    /**
     * 分类使用情况表导出
     * @param token
     * @param companyId
     * @param deptId
     * @throws IOException
     */
    @GetMapping("/exportUseClassReport")
    public void exportUseClassReport(@RequestParam("token") String token,@RequestParam("companyId")String companyId, @RequestParam("deptId")String deptId) throws IOException;

    /**
     * 分类增减表
     * @param param
     * @return
     */
    @PostMapping("/classIncreaseDecrease")
    public BaseResultVo classIncreaseDecrease(@RequestBody ClassInDeParam  param);

    /**
     * 分类增减表导出
     * @param token
     * @param companyId
     * @param startTime
     * @param endTime
     */
    @GetMapping("/exportClassIncreaseDecreaset")
    public void exportClassIncreaseDecreaset(@RequestParam("token") String token,@RequestParam("companyId")String companyId,@RequestParam("startTime")String startTime,@RequestParam("endTime")String endTime) throws IOException;

}
