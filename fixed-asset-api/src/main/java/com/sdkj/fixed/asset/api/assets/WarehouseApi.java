package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.WarehousePagesParam;
import com.sdkj.fixed.asset.api.assets.in_vo.WarehouseParam;
import com.sdkj.fixed.asset.api.assets.out_vo.AssetSupplierResult;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.assets.Warehouse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author zhangjinfei
 * @Date 2020/7/21 13:19
 */
@RequestMapping("/assets/warehouse/api")
public interface WarehouseApi {

    /**
     * 分页查询资产
     * @param params
     * @return
     */
    @PostMapping("/queryPages")
    public BaseResultVo<WarehouseResult> queryPages(@RequestBody  PageParams<WarehousePagesParam> params);


    /**
     * 选择资产分页查询资产
     * @param params
     * @return
     */
    @PostMapping("/queryStatePages")
    public BaseResultVo<WarehouseResult> queryStatePages(@RequestBody PageParams<WarehousePagesParam> params);

    /**
     * 退还资产 userId + state
     * @param param
     * @return
     */
    @PostMapping("/queryWithdrawal")
    public BaseResultVo<WarehouseResult> queryWithdrawal(@RequestBody WarehousePagesParam param);

    /**
     * 查询资产
     * @param assetId
     * @return
     */
    @GetMapping("/queryByAssetId")
    public BaseResultVo<WarehouseResult> queryByAssetId(@RequestParam("assetId") String assetId);

    /**
     * 通过审批查询资产
     * @param classId
     * @param areaId
     * @return
     */
    @GetMapping("/queryByAreaId")
    public BaseResultVo<Warehouse> queryByClassIdAndAreaId(@RequestParam("classId") String classId,@RequestParam("areaId") String areaId);

    /**
     * 查询资产
     * @param userId
     * @return
     */
    @GetMapping("/queryCountByUserId")
    public BaseResultVo<Integer> queryCountByUserId(@RequestParam("userId") String userId);

    /**
     * 所属公司查询资产
     * @param companyId
     * @return
     */
    @GetMapping("/queryByCompanyId")
    public BaseResultVo<WarehouseResult> queryByCompanyId(@RequestParam("companyId") String companyId);

    /**
     * 员工的查询资产 App
     * @param userId
     * @return
     */
    @GetMapping("/queryByUserId")
    public BaseResultVo<WarehouseResult> queryByUserId(@RequestParam("userId") String userId,@RequestParam(value = "searchName",required = false) String searchName);

    /**
     * 分类查询资产 App
     * @param classId
     * @return
     */
    @GetMapping("/queryByClassId")
    public BaseResultVo<WarehouseResult> queryByClassId(@RequestParam("classId") String classId,@RequestParam("searchName") String searchName);

    /**
     * 处理记录
     * @param assetId
     * @return
     */
    @GetMapping("/queryLogByAssetId")
    public BaseResultVo<WarehouseResult> queryLogByAssetId(@RequestParam("assetId") String assetId);

    /**
     * 维保信息
     * @param assetId
     * @return
     */
    @GetMapping("/queryMaintainByAssetId")
    public BaseResultVo<AssetSupplierResult> queryMaintainByAssetId(@RequestParam("assetId") String assetId);


    /**
     * 添加资产
     * @param warehouse
     * @return
     */
    @PostMapping("/insertWarehouse")
    public BaseResultVo<WarehouseParam> insertWarehouse(@RequestBody @Validated WarehouseParam warehouse);

    /**
     * 修改资产
     * @param warehouse
     * @return
     */
    @PostMapping("/updateWarehouse")
    public BaseResultVo<WarehouseParam> updateWarehouse(@RequestBody @Validated WarehouseParam warehouse);

    /**
     * 删除资产
     * @param assetId
     * @return
     * @throws Exception
     */
    @GetMapping("/deleteWarehouse")
    public BaseResultVo<Warehouse> deleteWarehouse(@RequestParam("assetId") String assetId) throws Exception;

    /**
     * 导入模板下载
     * @param response
     * @throws Exception
     */
    @GetMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws Exception;

    /**
     * 批量导入资产
     * @throws Exception
     */
    @PostMapping("/importAssetData")
    public BaseResultVo importAssetData(MultipartFile file) throws Exception;

    /**
     * 导出资产
     * @throws Exception
     */
    @GetMapping("/exportWarehouse")
    public void exportWarehouse(HttpServletResponse response,@RequestParam("token") String token,@RequestParam(value = "companyDeptId",required = false) String companyDeptId ,@RequestParam(value = "searchName",required = false) String searchName) throws Exception;

    /**
     * 导出资产履历
     * @throws Exception
     */
    @GetMapping("/exportWarehouseResume")
    public void exportWarehouseResume(HttpServletResponse response,@RequestParam("token") String token,@RequestParam(value = "companyDeptId",required = false) String companyDeptId ,@RequestParam(value = "searchName",required = false) String searchName) throws Exception;
    /**
     * 导出资产卡片
     * @throws Exception
     */
    @GetMapping("/printCard")
    public void printCard(HttpServletResponse response, @RequestParam("assetIds") String assetIds)throws Exception;

    /**
     * 打印入库单
     * @param response
     * @param assetIds
     * @throws Exception
     */
    @GetMapping("/printWarehousing")
    public void printWarehousing(HttpServletResponse response, @RequestParam("assetIds") String assetIds)throws Exception;


}
