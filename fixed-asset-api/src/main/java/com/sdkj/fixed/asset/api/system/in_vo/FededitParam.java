package com.sdkj.fixed.asset.api.system.in_vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "问题反馈查看入参")
public class FededitParam {



    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "解决方案")
    private String solution;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }
}
