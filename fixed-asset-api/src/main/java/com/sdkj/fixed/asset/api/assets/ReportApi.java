package com.sdkj.fixed.asset.api.assets;

import com.sdkj.fixed.asset.api.assets.in_vo.report.*;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName ReportApi
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/30 15:57
 */
@RequestMapping("/assets/report/api")
public interface ReportApi {
    /**
     * 查询资产清单
     * @param pageParams
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPageAssetList", method = RequestMethod.POST)
    public BaseResultVo getPageAssetList(@RequestBody PageParams<AssetListParam> pageParams) throws Exception;
    /**
     * 查看资产处理记录
     * @param assetId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/viewAssetHandleRecord", method = RequestMethod.POST)
    public BaseResultVo viewAssetHandleRecord(@RequestBody AssetId assetId) throws Exception;
    /**
     * 导出资产清单
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/exportAssetList", method = RequestMethod.GET)
    public void exportAssetList( String useCompanyId,String search,String loginUserId, HttpServletResponse response) throws Exception;
    /**
     * 查询资产履历
     * @param pageParams
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPageAssetHandleRecord", method = RequestMethod.POST)
    public BaseResultVo getPageAssetHandleRecord(@RequestBody PageParams<AssetHandleRecordParam> pageParams) throws Exception;

    /**
     * 导出资产履历
     * @param loginUserId
     * @param startTime
     * @param endTime
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportAssetHandleRecord", method = RequestMethod.GET)
    public void exportAssetHandleRecord(String loginUserId,String startTime,String endTime, HttpServletResponse response)throws Exception;
    /**
     * 资产分类汇总表
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAssetClassStatistics", method = RequestMethod.POST)
    public BaseResultVo getAssetClassStatistics(@RequestBody AssetClassParam param) throws Exception;

    /**
     * 导出资产分类汇总表
     * @param token
     * @param companyId
     * @param useDeptId
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/exportAssetClassStatistics", method = RequestMethod.GET)
    public void exportAssetClassStatistics(String token,String companyId,String useDeptId, HttpServletResponse response)throws Exception;
    /**
     * 维保到期统计表
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/StatisticsOfMaintenanceExpiration", method = RequestMethod.POST)
    public BaseResultVo StatisticsOfMaintenanceExpiration(@RequestBody  PageParams<MaintenanceExpirationParam> param) throws Exception;

    /**
     * 导出维保到期统计表
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/exportStatisticsOfMaintenanceExpiration", method = RequestMethod.GET)
    public void exportStatisticsOfMaintenanceExpiration(String startTime, String endTime,String search, String token, HttpServletResponse response) throws Exception;
    /**
     * 查询首页头部统计信息（待审批、待签字、维保到期、报修资产数、待确认挑拨单）
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getHeaderData", method = RequestMethod.GET)
    public BaseResultVo getHeaderData() throws Exception;

    /**
     * 查询资产状况
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getAssetStatus", method = RequestMethod.GET)
    public BaseResultVo getAssetStatus() throws Exception;
    /**
     * 查询资产状况占比
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getAssetStatPer", method = RequestMethod.GET)
    public BaseResultVo getAssetStatPer() throws Exception;
    /**
     * 资产分类统计
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getAssetTypeSum", method = RequestMethod.GET)
    public BaseResultVo getAssetTypeSum(String companyId) throws Exception;

    /**
     * 耗材领用情况
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getHcUseStatus", method = RequestMethod.POST)
    public BaseResultVo getHcUseStatus(@RequestBody   UseStatusParam param) throws Exception;
    /**
     * 固资使用情况
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getAssetUseStatus", method = RequestMethod.POST)
    public BaseResultVo getAssetUseStatus(@RequestBody   UseStatusParam param) throws Exception;
}
