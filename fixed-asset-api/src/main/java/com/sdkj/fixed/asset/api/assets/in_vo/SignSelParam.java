package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/26 13:53
 */
public class SignSelParam {

    @NotBlank(message = "id不能为空")
    private String id;
    @NotNull(message = "签字状态不能为空,2待签字，4已签字")
    private Integer state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
