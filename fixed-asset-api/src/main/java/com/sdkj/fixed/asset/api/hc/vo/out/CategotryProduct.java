package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * CategotryProduct
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/23 16:21
 */
public class CategotryProduct {
    private String id;

    private String img;

    private String type;

    private String typeCode;

    private String typeName;

    private String code;

    private String name;

    private String barCode;

    private String model;

    private String unit;

    private Integer num;

    private String price;

    private String totalPrice;

    private String categoryName;

    private Integer safeMin;

    private Integer safeMax;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getSafeMin() {
        return safeMin;
    }

    public void setSafeMin(Integer safeMin) {
        this.safeMin = safeMin;
    }

    public Integer getSafeMax() {
        return safeMax;
    }

    public void setSafeMax(Integer safeMax) {
        this.safeMax = safeMax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
