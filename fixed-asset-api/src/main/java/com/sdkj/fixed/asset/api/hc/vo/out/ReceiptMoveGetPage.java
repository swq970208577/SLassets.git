package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * ReceiptSetGetPage
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/23 17:20
 */
public class ReceiptMoveGetPage {
    private String id;

    @Excel(name = "调拨单号", needMerge = true, orderNum = "1")
    private String number;

    @Excel(name = "状态", needMerge = true, orderNum = "2")
    private String confirm1;

    private Integer confirm;

    private String outCategoryId;

    @Excel(name = "调出仓库", needMerge = true, orderNum = "3")
    private String outCategoryName;

    private String outUser;

    @Excel(name = "调出人", needMerge = true, orderNum = "5")
    private String outUserName;

    @Excel(name = "调出日期", needMerge = true, orderNum = "6")
    private String outTime;

    @Excel(name = "调出备注", needMerge = true, orderNum = "7")
    private String outComment;

    private String inCategoryId;

    @Excel(name = "调入仓库", needMerge = true, orderNum = "4")
    private String inCategoryName;

    private String confirmUser;

    @Excel(name = "调入确认人", needMerge = true, orderNum = "8")
    private String confirmUserName;

    @Excel(name = "调入确认日期", needMerge = true, orderNum = "9")
    private String confirmTime;

    @Excel(name = "调入备注", needMerge = true, orderNum = "10")
    private String confirmComment;

    @ExcelCollection(name = "调拨明细", orderNum = "11")
    private List<ReceiptMoveGetPageExtends> list;

    private String pCode;

    private String pName;

    private String pBarCode;

    private String pModel;

    private String pUnit;

    private Integer pSafeMin;

    private Integer pSafeMax;

    private Integer num;

    private String price;

    private String totalPrice;

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpBarCode() {
        return pBarCode;
    }

    public void setpBarCode(String pBarCode) {
        this.pBarCode = pBarCode;
    }

    public String getpModel() {
        return pModel;
    }

    public void setpModel(String pModel) {
        this.pModel = pModel;
    }

    public String getpUnit() {
        return pUnit;
    }

    public void setpUnit(String pUnit) {
        this.pUnit = pUnit;
    }

    public Integer getpSafeMin() {
        return pSafeMin;
    }

    public void setpSafeMin(Integer pSafeMin) {
        this.pSafeMin = pSafeMin;
    }

    public Integer getpSafeMax() {
        return pSafeMax;
    }

    public void setpSafeMax(Integer pSafeMax) {
        this.pSafeMax = pSafeMax;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getConfirm1() {
        return confirm1;
    }

    public void setConfirm1(String confirm1) {
        this.confirm1 = confirm1;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public String getOutCategoryId() {
        return outCategoryId;
    }

    public void setOutCategoryId(String outCategoryId) {
        this.outCategoryId = outCategoryId;
    }

    public String getOutCategoryName() {
        return outCategoryName;
    }

    public void setOutCategoryName(String outCategoryName) {
        this.outCategoryName = outCategoryName;
    }

    public String getOutUser() {
        return outUser;
    }

    public void setOutUser(String outUser) {
        this.outUser = outUser;
    }

    public String getOutUserName() {
        return outUserName;
    }

    public void setOutUserName(String outUserName) {
        this.outUserName = outUserName;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getOutComment() {
        return outComment;
    }

    public void setOutComment(String outComment) {
        this.outComment = outComment;
    }

    public String getInCategoryId() {
        return inCategoryId;
    }

    public void setInCategoryId(String inCategoryId) {
        this.inCategoryId = inCategoryId;
    }

    public String getInCategoryName() {
        return inCategoryName;
    }

    public void setInCategoryName(String inCategoryName) {
        this.inCategoryName = inCategoryName;
    }

    public String getConfirmUser() {
        return confirmUser;
    }

    public void setConfirmUser(String confirmUser) {
        this.confirmUser = confirmUser;
    }

    public String getConfirmUserName() {
        return confirmUserName;
    }

    public void setConfirmUserName(String confirmUserName) {
        this.confirmUserName = confirmUserName;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getConfirmComment() {
        return confirmComment;
    }

    public void setConfirmComment(String confirmComment) {
        this.confirmComment = confirmComment;
    }

    public List<ReceiptMoveGetPageExtends> getList() {
        return list;
    }

    public void setList(List<ReceiptMoveGetPageExtends> list) {
        this.list = list;
    }
}
