package com.sdkj.fixed.asset.api.hc.vo.in;

/**
 * ReceiptSetGetPageParams
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/23 16:58
 */
public class ReceiptSetGetPageParams {
    private String number;

    private String orgId;

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    private Integer isAdmin;

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }
}
