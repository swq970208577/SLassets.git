package com.sdkj.fixed.asset.api.hc;

import com.sdkj.fixed.asset.api.hc.vo.in.ProductExtends;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckConfirmParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckGetPageParams;
import com.sdkj.fixed.asset.api.hc.vo.in.ReceiptCheckProductGetPageParams;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.hc.ReceiptCheck;
import com.sdkj.fixed.asset.pojo.hc.ReceiptCheckProduct;
import com.sdkj.fixed.asset.pojo.hc.group.DelCheckProduct;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;


/**
 * @author 史晨星
 * @ClassName: ReceiptCheckApi
 * @Description: 仓库管理
 * @date 2020年7月21日
 */
@RequestMapping("/hc/receiptCheck/api")
public interface ReceiptCheckApi {


    /**
     * 新增
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo add(@RequestBody ReceiptCheck entity) throws Exception;

    /**
     * 查看
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo view(@RequestBody ReceiptCheck entity) throws Exception;

    /**
     * 重新分配用户
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo edit(@RequestBody ReceiptCheck entity) throws Exception;

    /**
     * 详情分页查询
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/getPageDetail", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPageDetail(@RequestBody PageParams<ReceiptCheckProductGetPageParams> params) throws Exception;

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo del(@RequestBody ReceiptCheck entity) throws Exception;

    /**
     * 列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPages", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPages(@RequestBody PageParams<ReceiptCheckGetPageParams> params) throws Exception;

    /**
     * 新增盘盈
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/surplus", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo surplus(@RequestBody ProductExtends entity) throws Exception;

    /**
     * 编辑（手动盘点）
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/editCheckProduct", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo editCheckProduct(@RequestBody ReceiptCheckProduct entity) throws Exception;

    /**
     * 没盘到
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/checkNone", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo checkNone(@RequestBody ReceiptCheckProduct entity) throws Exception;

    /**
     * 删除（只能删除盘盈物品）
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/delCheckProduct", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo delCheckProduct(@RequestBody ReceiptCheckProduct entity) throws Exception;

    /**
     * 确认盘点结果
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo confirm(@RequestBody ReceiptCheckConfirmParams entity) throws Exception;

    /**
     * 列表查询App
     *
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPagesApp", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPagesApp(@RequestBody PageParams<ReceiptCheckGetPageParams> params) throws Exception;

    /**
     * 详情分页查询APP
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/getPageDetailApp", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPageDetailApp(@RequestBody PageParams<ReceiptCheckProductGetPageParams> params) throws Exception;

    /**
     * 导出
     *
     * @param companyId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @ResponseBody
    public void export(String companyId, String categoryId, String userId, HttpServletResponse response) throws Exception;

    /**
     * 导出明细
     *
     * @param receiptId
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/exportDetail", method = RequestMethod.GET)
    @ResponseBody
    public void exportDetail(String receiptId, HttpServletResponse response) throws Exception;
}

