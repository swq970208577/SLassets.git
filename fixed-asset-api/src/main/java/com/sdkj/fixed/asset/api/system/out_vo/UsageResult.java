package com.sdkj.fixed.asset.api.system.out_vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/9 9:58
 */
public class UsageResult{

    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "类型")
    private int type;
    @ApiModelProperty(value = "类型名称")
    private String typeName;
    @ApiModelProperty(value = "公司名称")
    private String companyName;
    @ApiModelProperty(value = "购买资产数")
    private String buyAssetNum;
    @ApiModelProperty(value = "公司耗材总数")
    private int hcNum;
    @ApiModelProperty(value = "公司资产总数")
    private int assetsNum;
    @ApiModelProperty(value = "公司人员数")
    private int personNum;
    @ApiModelProperty(value = "手机号")
    private String tel;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ApiModelProperty(value = "到期时间")
    private String expireDate;
    @ApiModelProperty(value = "开始时间")
    private String ctime;
    @ApiModelProperty(value = "公司companyId")
    private String companyId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBuyAssetNum() {
        return buyAssetNum;
    }

    public void setBuyAssetNum(String buyAssetNum) {
        this.buyAssetNum = buyAssetNum;
    }

    public int getHcNum() {
        return hcNum;
    }

    public void setHcNum(int hcNum) {
        this.hcNum = hcNum;
    }

    public int getAssetsNum() {
        return assetsNum;
    }

    public void setAssetsNum(int assetsNum) {
        this.assetsNum = assetsNum;
    }

    public int getPersonNum() {
        return personNum;
    }

    public void setPersonNum(int personNum) {
        this.personNum = personNum;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
