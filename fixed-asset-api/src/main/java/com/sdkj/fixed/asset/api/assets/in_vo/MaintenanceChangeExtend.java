package com.sdkj.fixed.asset.api.assets.in_vo;

import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseHistoryResult;
import com.sdkj.fixed.asset.pojo.assets.MaintenanceChange;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/27 11:19
 */
public class MaintenanceChangeExtend extends MaintenanceChange {
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 供应商联系人
     */
    private String suppliercontact;
    /**
     * 供应商电话
     */
    private String supplierTel;

    /**
     * 资产明细
     */
    private List<MaintenanceChangeAssetsExtend> maintenanceChangeAssetsExtendist = new ArrayList<>();
    /**
     * 资产id
     */

    @NotEmpty(message = "资产明细不能为空")
    private List<String> assetIdList = new ArrayList<>();
    /**
     * 资产基本信息
     */
    @ExcelCollection(name = "资产明细", orderNum = "3")
    List<WarehouseHistoryResult> results = new ArrayList<>();

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }

    public List<WarehouseHistoryResult> getResults() {
        return results;
    }

    public void setResults(List<WarehouseHistoryResult> results) {
        this.results = results;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSuppliercontact() {
        return suppliercontact;
    }

    public void setSuppliercontact(String suppliercontact) {
        this.suppliercontact = suppliercontact;
    }

    public String getSupplierTel() {
        return supplierTel;
    }

    public void setSupplierTel(String supplierTel) {
        this.supplierTel = supplierTel;
    }

    public List<MaintenanceChangeAssetsExtend> getMaintenanceChangeAssetsExtendist() {
        return maintenanceChangeAssetsExtendist;
    }

    public void setMaintenanceChangeAssetsExtendist(List<MaintenanceChangeAssetsExtend> maintenanceChangeAssetsExtendist) {
        this.maintenanceChangeAssetsExtendist = maintenanceChangeAssetsExtendist;
    }
}
