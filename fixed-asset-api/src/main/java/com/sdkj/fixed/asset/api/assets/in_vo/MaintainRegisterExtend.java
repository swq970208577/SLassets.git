package com.sdkj.fixed.asset.api.assets.in_vo;

import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.sdkj.fixed.asset.api.assets.out_vo.WarehouseBase;
import com.sdkj.fixed.asset.pojo.assets.MaintainRegister;

import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/27 11:18
 */
public class MaintainRegisterExtend extends MaintainRegister {
    private String stateName;

    /**
     * 资产id
     */
    private List<String> assetIdList = new ArrayList<>();
    /**
     * 资产基本信息
     */
    @ExcelCollection(name = "资产明细", orderNum = "9")
    List<WarehouseBase> results = new ArrayList<>();

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }

    public List<WarehouseBase> getResults() {
        return results;
    }

    public void setResults(List<WarehouseBase> results) {
        this.results = results;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
