package com.sdkj.fixed.asset.api.assets.out_vo.report;

/**
 * @ClassName HomePageStatistics
 * @Description 首页头部统计值
 * @Author 张欣
 * @Date 2020/8/10 15:49
 */
public class HomePageStatistics {
    /**
     * 待审批
     */
    private String pendingApprovalCount;
    /**
     * 待签字
     */
    private String toBeSignedCount;
    /**
     * 维保到期
     */
    private String maintenanceExpiredCount;
    /**
     * 报修资产
     */
    private String reportRepairCount;
    /**
     * 待确认调拨单
     */
    private String pendingTransferCount;

    public String getPendingApprovalCount() {
        return pendingApprovalCount;
    }

    public void setPendingApprovalCount(String pendingApprovalCount) {
        this.pendingApprovalCount = pendingApprovalCount;
    }

    public String getToBeSignedCount() {
        return toBeSignedCount;
    }

    public void setToBeSignedCount(String toBeSignedCount) {
        this.toBeSignedCount = toBeSignedCount;
    }

    public String getMaintenanceExpiredCount() {
        return maintenanceExpiredCount;
    }

    public void setMaintenanceExpiredCount(String maintenanceExpiredCount) {
        this.maintenanceExpiredCount = maintenanceExpiredCount;
    }

    public String getReportRepairCount() {
        return reportRepairCount;
    }

    public void setReportRepairCount(String reportRepairCount) {
        this.reportRepairCount = reportRepairCount;
    }

    public String getPendingTransferCount() {
        return pendingTransferCount;
    }

    public void setPendingTransferCount(String pendingTransferCount) {
        this.pendingTransferCount = pendingTransferCount;
    }
}
