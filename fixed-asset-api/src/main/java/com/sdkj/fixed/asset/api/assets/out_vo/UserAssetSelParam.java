package com.sdkj.fixed.asset.api.assets.out_vo;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/11 16:07
 */
public class UserAssetSelParam {

    private String search;

    private String isOnJob;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getIsOnJob() {
        return isOnJob;
    }

    public void setIsOnJob(String isOnJob) {
        this.isOnJob = isOnJob;
    }
}
