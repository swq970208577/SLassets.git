package com.sdkj.fixed.asset.api.assets.out_vo.report;

/**
 * @ClassName AssetHandleRecord
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/3 11:12
 */
public class AssetHandleRecord {
    /**
     * 资产条码
     */
    private String barCode;
    /**
     * 资产名称
     */
    private String assetName;
    /**
     * 资产类别名称
     */
    private String assetTypeName;
    /**
     * 购入日期
     */
    private String buyDate;
    /**
     * 规格型号
     */
    private String specs;
    /**
     * 清理时间
     */
    private String clearDate;

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public String getClearDate() {
        return clearDate;
    }

    public void setClearDate(String clearDate) {
        this.clearDate = clearDate;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }
}
