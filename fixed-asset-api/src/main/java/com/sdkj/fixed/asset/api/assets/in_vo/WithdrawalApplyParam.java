package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.AssetWithdrawal;
import com.sdkj.fixed.asset.pojo.assets.Withdrawal;

import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 领用审批
 * @Date 2020/8/7 16:20
 */
public class WithdrawalApplyParam extends Withdrawal {

    private List<AssetWithdrawal> assetWithdrawalList;

    public List<AssetWithdrawal> getAssetWithdrawalList() {
        return assetWithdrawalList;
    }

    public void setAssetWithdrawalList(List<AssetWithdrawal> assetWithdrawalList) {
        this.assetWithdrawalList = assetWithdrawalList;
    }
}
