package com.sdkj.fixed.asset.api.assets.out_vo.report;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.assets.AssetLog;
import com.sdkj.fixed.asset.pojo.assets.Warehouse;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.List;

/**
 * @Author zx
 * @Description //资产清单
 * @Date 2020/7/30 16:08
 */
public class AssetListReport {
    /**
     * 资产ID
     */
    private String assetId;
    @Excel(name="状态",orderNum = "0")
    private String state;
    /**
     * 照片
     */
    private String photo;
    /**
     * 资产条码
     */
    @Excel(name="资产条码",orderNum = "1")
    private String assetCode;
    /**
     * 资产名称
     */
    @Excel(name="资产名称",orderNum = "2")
    private String assetName;

    @Excel(name="资产类别编码",orderNum = "3")
    private String assetClassCode;

    @Excel(name="资产类别",orderNum = "4")
    private String assetClassName;

    /**
     * 规格型号
     */
    @Excel(name="规格型号",orderNum = "5")
    private String specificationModel;
    /**
     * SN号
     */
    @Excel(name="SN号",orderNum = "6")
    private String snNumber;
    /**
     * 计量单位
     */
    @Excel(name="计量单位",orderNum = "7")
    private String unitMeasurement;
    /**
     * 金额
     */
    @Excel(name="金额",orderNum = "8")
    private String amount;
    @Excel(name="使用公司编码",orderNum = "9")
    private String useCompanyCode;
    @Excel(name="使用公司",orderNum = "10")
    private String useCompanyName;
    @Excel(name="使用部门编码",orderNum = "11")
    private String useDeptCode;
    @Excel(name="使用部门",orderNum = "12")
    private String useDeptName;
    /**
     * 使用人
     */
    @Excel(name="使用人",orderNum = "13")
    private String handlerUser;
    @Excel(name="区域编码",orderNum = "14")
    private String areaCode;
    @Excel(name="区域",orderNum = "15")
    private String areaName;
    /**
     * 存放地点
     */
    @Excel(name="存放地点",orderNum = "16")
    private String storageLocation;
    /**
     * 管理员
     */
    @Excel(name="管理员",orderNum = "17")
    private String admin;
    @Excel(name="所属公司编码",orderNum = "18")
    private String companyCode;
    @Excel(name="所属公司",orderNum = "19")
    private String companyName;
    /**
     * 购入时间
     */
    @Excel(name="购入时间",orderNum = "20")
    private String purchaseTime;
    /**
     * 使用期限(月)
     */
    @Excel(name="使用期限",orderNum = "21")
    private String serviceLife;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "22")
    private String remark;

    @Excel(name="创建人",orderNum = "23")
    private String createUserName;

    /**
     * 创建时间
     */
    @Excel(name="创建时间",orderNum = "24")
    private String createTime;

    @Excel(name="供应商",orderNum = "26")
    private String supplierName;
    @Excel(name="联系人",orderNum = "27")
    private String contact;
    @Excel(name="联系方式",orderNum = "28")
    private String contactinfo;
    @Excel(name="负责人",orderNum = "29")
    private String supplierUser;
    @Excel(name="维保到期日",orderNum = "30")
    private String expireTime;
    @Excel(name="维保说明",orderNum = "31")
    private String explainText;

    /**
     * 来源
     */
    private String source;




    private static final long serialVersionUID = 1L;

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取资产条码
     *
     * @return asset_code - 资产条码
     */
    public String getAssetCode() {
        return assetCode;
    }

    /**
     * 设置资产条码
     *
     * @param assetCode 资产条码
     */
    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode == null ? null : assetCode.trim();
    }

    /**
     * 获取资产名称
     *
     * @return asset_name - 资产名称
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * 设置资产名称
     *
     * @param assetName 资产名称
     */
    public void setAssetName(String assetName) {
        this.assetName = assetName == null ? null : assetName.trim();
    }



    /**
     * 获取规格型号
     *
     * @return specification_model - 规格型号
     */
    public String getSpecificationModel() {
        return specificationModel;
    }

    /**
     * 设置规格型号
     *
     * @param specificationModel 规格型号
     */
    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel == null ? null : specificationModel.trim();
    }

    /**
     * 获取计量单位
     *
     * @return unit_measurement - 计量单位
     */
    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    /**
     * 设置计量单位
     *
     * @param unitMeasurement 计量单位
     */
    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement == null ? null : unitMeasurement.trim();
    }

    /**
     * 获取SN号
     *
     * @return sn_number - SN号
     */
    public String getSnNumber() {
        return snNumber;
    }

    /**
     * 设置SN号
     *
     * @param snNumber SN号
     */
    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber == null ? null : snNumber.trim();
    }

    /**
     * 获取来源
     *
     * @return source - 来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 设置来源
     *
     * @param source 来源
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * 获取购入时间
     *
     * @return purchase_time - 购入时间
     */
    public String getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * 设置购入时间
     *
     * @param purchaseTime 购入时间
     */
    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime == null ? null : purchaseTime.trim();
    }



    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(String amount) {
        this.amount = amount == null ? null : amount.trim();
    }

    /**
     * 获取管理员
     *
     * @return admin - 管理员
     */
    public String getAdmin() {
        return admin;
    }

    /**
     * 设置管理员
     *
     * @param admin 管理员
     */
    public void setAdmin(String admin) {
        this.admin = admin == null ? null : admin.trim();
    }


    /**
     * 获取使用人
     *
     * @return handler_user - 使用人
     */
    public String getHandlerUser() {
        return handlerUser;
    }

    /**
     * 设置使用人
     *
     * @param handlerUser 使用人
     */
    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser == null ? null : handlerUser.trim();
    }

    /**
     * 获取使用期限(月)
     *
     * @return service_life - 使用期限(月)
     */
    public String getServiceLife() {
        return serviceLife;
    }

    /**
     * 设置使用期限(月)
     *
     * @param serviceLife 使用期限(月)
     */
    public void setServiceLife(String serviceLife) {
        this.serviceLife = serviceLife == null ? null : serviceLife.trim();
    }



    /**
     * 获取存放地点
     *
     * @return storage_location - 存放地点
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * 设置存放地点
     *
     * @param storageLocation 存放地点
     */
    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation == null ? null : storageLocation.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取状态
     *
     * @return state - 状态
     */
    public String getState() {
        return state;
    }

    /**
     * 设置状态
     *
     * @param state 状态
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取照片
     *
     * @return photo - 照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 设置照片
     *
     * @param photo 照片
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }


    public String getAssetClassName() {
        return assetClassName;
    }

    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDeptName() {
        return useDeptName;
    }

    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getAssetClassCode() {
        return assetClassCode;
    }

    public void setAssetClassCode(String assetClassCode) {
        this.assetClassCode = assetClassCode;
    }

    public String getUseCompanyCode() {
        return useCompanyCode;
    }

    public void setUseCompanyCode(String useCompanyCode) {
        this.useCompanyCode = useCompanyCode;
    }

    public String getUseDeptCode() {
        return useDeptCode;
    }

    public void setUseDeptCode(String useDeptCode) {
        this.useDeptCode = useDeptCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactinfo() {
        return contactinfo;
    }

    public void setContactinfo(String contactinfo) {
        this.contactinfo = contactinfo;
    }

    public String getSupplierUser() {
        return supplierUser;
    }

    public void setSupplierUser(String supplierUser) {
        this.supplierUser = supplierUser;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getExplainText() {
        return explainText;
    }

    public void setExplainText(String explainText) {
        this.explainText = explainText;
    }
}
