package com.sdkj.fixed.asset.api.assets.in_vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/21 13:11
 */
public class Params extends StateParam{

    @NotBlank(message = "id不能为空")
    private String id;

    @Size(max = 128, message = "备注/说明超长")
    private String note;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
