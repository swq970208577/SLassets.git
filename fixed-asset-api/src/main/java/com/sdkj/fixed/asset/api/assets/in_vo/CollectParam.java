package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.Collect;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 添加修改领用
 * @Date 2020/7/27 10:12
 */
public class CollectParam extends Collect {
    @NotEmpty
    private List<String> assetIdList;

    public List<String> getAssetIdList() {
        return assetIdList;
    }

    public void setAssetIdList(List<String> assetIdList) {
        this.assetIdList = assetIdList;
    }
}
