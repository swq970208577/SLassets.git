package com.sdkj.fixed.asset.api.hc.vo.in;

import com.sdkj.fixed.asset.pojo.hc.Product;
import com.sdkj.fixed.asset.pojo.hc.group.AddProduct;
import com.sdkj.fixed.asset.pojo.hc.group.EditProduct;
import com.sdkj.fixed.asset.pojo.hc.group.GetProductByTypeAndCategory;
import com.sdkj.fixed.asset.pojo.hc.group.Surplus;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ProductExtends
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/28 12:42
 */
public class ProductExtends extends Product {

    @Transient
    @Size(max = 32, message = "盘点单id-最大长度:32")
    @NotNull(message = "盘点单id-不能为空", groups = {Surplus.class})
    private String recepitId;

    @Transient
    @NotNull(message = "实盘数量-不能为空", groups = {Surplus.class})
    private Integer realNum;

    @Transient
    @NotNull(message = "实盘金额-不能为空", groups = {Surplus.class})
    private String realTotalPrice;

    public String getRecepitId() {
        return recepitId;
    }

    public void setRecepitId(String recepitId) {
        this.recepitId = recepitId;
    }

    public Integer getRealNum() {
        return realNum;
    }

    public void setRealNum(Integer realNum) {
        this.realNum = realNum;
    }

    public String getRealTotalPrice() {
        return realTotalPrice;
    }

    public void setRealTotalPrice(String realTotalPrice) {
        this.realTotalPrice = realTotalPrice;
    }
}
