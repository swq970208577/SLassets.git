package com.sdkj.fixed.asset.api.login;


import com.sdkj.fixed.asset.api.login.in_vo.LoginParamMobile;
import com.sdkj.fixed.asset.api.login.in_vo.LoginParam;
import com.sdkj.fixed.asset.api.login.out_vo.CompanyBaseListResult;
import com.sdkj.fixed.asset.api.login.out_vo.MenuResultEntity;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.springframework.web.bind.annotation.*;
/**
 * 登录控制类
 * @author zx
 */
@RequestMapping("/login/login/api")
public interface LoginApi {
    /**
     * PC端登录
     * @param loginParam
     * @return
     */

    @ResponseBody
    @RequestMapping(value = "/loginPC",method = RequestMethod.POST)
    public BaseResultVo loginPC(@RequestBody LoginParam loginParam
                               ) throws Exception;
   /**
     * 获取菜单
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getMenus",method = RequestMethod.POST)
    public BaseResultVo<MenuResultEntity> getMenus() throws Exception;

    /**
     * 移动端登录
     * @param paramMobile
     * @return
     */

    @ResponseBody
    @RequestMapping(value = "/mobile",method = RequestMethod.POST)
    public BaseResultVo loginMobile(@RequestBody LoginParam paramMobile) throws Exception;

    /**
     * 退出登录
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "loginOut",method = RequestMethod.GET)
    public BaseResultVo LogOut() throws Exception;
    /**
     * 获取头部公司信息
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "getCompanyOfUser",method = RequestMethod.GET)
    public BaseResultVo<CompanyBaseListResult> getCompanyOfUser() throws Exception;
}
