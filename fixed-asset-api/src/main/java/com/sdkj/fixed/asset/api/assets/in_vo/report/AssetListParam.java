package com.sdkj.fixed.asset.api.assets.in_vo.report;

/**
 * @ClassName AssetListParam
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/30 16:47
 */
public class AssetListParam {
    /**
     * 公司id
     */
    private String useCompanyId;
    /**
     * 搜索框
     */
    private String search;
    public String getUseCompanyId() {
        return useCompanyId;
    }

    public void setUseCompanyId(String useCompanyId) {
        this.useCompanyId = useCompanyId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
