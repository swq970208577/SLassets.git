package com.sdkj.fixed.asset.api.assets.out_vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2020/8/7 10:13
 */
public class AppInvOrder {

    private String inventoryId;
    private String inventoryName;
    private String createTime;
    private String overTime;
    private String count;
    private String count0;
    private String count1;
    private List<AppInvEntity> appInvEntityList = new ArrayList<>();
    private String username;
    private String isManual;

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOverTime() {
        return overTime;
    }

    public void setOverTime(String overTime) {
        this.overTime = overTime;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCount0() {
        return count0;
    }

    public void setCount0(String count0) {
        this.count0 = count0;
    }

    public String getCount1() {
        return count1;
    }

    public void setCount1(String count1) {
        this.count1 = count1;
    }

    public List<AppInvEntity> getAppInvEntityList() {
        return appInvEntityList;
    }

    public void setAppInvEntityList(List<AppInvEntity> appInvEntityList) {
        this.appInvEntityList = appInvEntityList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIsManual() {
        return isManual;
    }

    public void setIsManual(String isManual) {
        this.isManual = isManual;
    }
}
