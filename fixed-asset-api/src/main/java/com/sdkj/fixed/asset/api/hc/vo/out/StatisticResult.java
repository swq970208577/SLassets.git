package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * StatisticResult
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/13 11:35
 */
public class StatisticResult {
    private Integer num;

    private String price;

    private String totalPrice;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
