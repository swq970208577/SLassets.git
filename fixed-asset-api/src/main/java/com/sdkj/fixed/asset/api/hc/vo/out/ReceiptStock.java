package com.sdkj.fixed.asset.api.hc.vo.out;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ReceiptStock
 *
 * @author zhaozheyu
 * @Description
 * @date 2020/8/11 20:23
 */
public class ReceiptStock {
    @ApiModelProperty(value = "分类id")
    private String id;

    @Excel(name="分类编码",orderNum = "1",width = 20)
    private String flcode;

    @Excel(name="分类编码",orderNum = "2",width = 20)
    private String flname;

    @Excel(name="物品编码",orderNum = "3",width = 20)
    private String code;

    @Excel(name="物品名称",orderNum = "4",width = 20)
    private String name;

    @Excel(name="规格型号",orderNum = "6",width = 20)
    private String model;

    @Excel(name="耗材商品条码",orderNum = "5",width = 20)
    private String bcode;

    @Excel(name="单位",orderNum = "7",width = 10)
    private String unit;

    @Excel(name="安全库存下限",orderNum = "12",width = 20)
    private Integer min;

    @Excel(name="安全库存上限",orderNum = "13",width = 20)
    private Integer max;

    @Excel(name="数量",orderNum = "9",width = 15)
    private Integer num;

    @Excel(name="单价",orderNum = "10",width = 15)
    private String price;

    @Excel(name="金额",orderNum = "11",width = 15)
    private String zjprice;

    @Excel(name="仓库名称",orderNum = "8",width = 20)
    private String ckname;

    @ApiModelProperty(value = "耗材图片")
    private String img;
    @ApiModelProperty(value = "仓库id")
    private String ckid;
    @ApiModelProperty(value = "耗材机构id")
    private String orgid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlcode() {
        return flcode;
    }

    public void setFlcode(String flcode) {
        this.flcode = flcode;
    }

    public String getFlname() {
        return flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBcode() {
        return bcode;
    }

    public void setBcode(String bcode) {
        this.bcode = bcode;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getZjprice() {
        return zjprice;
    }

    public void setZjprice(String zjprice) {
        this.zjprice = zjprice;
    }

    public String getCkname() {
        return ckname;
    }

    public void setCkname(String ckname) {
        this.ckname = ckname;
    }

    public String getCkid() {
        return ckid;
    }

    public void setCkid(String ckid) {
        this.ckid = ckid;
    }
}
