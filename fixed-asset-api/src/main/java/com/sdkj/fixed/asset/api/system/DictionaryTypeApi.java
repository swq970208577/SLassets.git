package com.sdkj.fixed.asset.api.system;

import com.sdkj.fixed.asset.api.system.in_vo.DicCodeParam;
import com.sdkj.fixed.asset.api.system.in_vo.DicEntityId;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.DictionaryType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName DictionaryEntityApi
 * @Description 数据字典项
 * @Author 张欣
 * @Date 2020/7/21 16:23
 */
@RequestMapping({"/system/dicType/api"})
public interface DictionaryTypeApi {
    /**
     * 删除数据字典
     * @param dicId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteDic", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo deleteDic(@RequestBody DictionaryType dicId) throws Exception;

    /**
     * 分页查询数据字典
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPage", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getPage(@RequestBody PageParams<DicEntityId> param) throws Exception;
    /**
     *根据code查询数据字典项
     * @param dicCode
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getDicByCode", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo getDicByCode(@RequestBody DicCodeParam dicCode ) throws Exception;

    /**
     * 获取数据字典项详情
     * @param idParanEntity
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/queryDicTypeInfo", method = RequestMethod.POST)
    @ResponseBody
    public BaseResultVo queryDicTypeInfo(@RequestBody DictionaryType idParanEntity) throws Exception;
}
