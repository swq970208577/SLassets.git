package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * SendDetail
 *
 * @author shichenxing
 * @Description
 * @date 2020/8/5 17:45
 */
public class SendDetail {

    private String number;

    private String productName;

    private String approveUser;

    private String productCode;

    private String approveTime;

    private Integer sign;

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }
}
