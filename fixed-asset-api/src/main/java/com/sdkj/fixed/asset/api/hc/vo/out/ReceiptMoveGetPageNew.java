package com.sdkj.fixed.asset.api.hc.vo.out;

/**
 * ReceiptSetGetPage
 *
 * @author shichenxing
 * @Description
 * @date 2020/7/23 17:20
 */
public class ReceiptMoveGetPageNew {
    private String id;

    private String number;

    private Integer confirm;

    private String outCategoryName;

    private String outUserName;

    private String outTime;

    private String outComment;

    private String inCategoryName;

    private String confirmUserName;

    private String confirmTime;

    private String confirmComment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public String getOutCategoryName() {
        return outCategoryName;
    }

    public void setOutCategoryName(String outCategoryName) {
        this.outCategoryName = outCategoryName;
    }

    public String getOutUserName() {
        return outUserName;
    }

    public void setOutUserName(String outUserName) {
        this.outUserName = outUserName;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getOutComment() {
        return outComment;
    }

    public void setOutComment(String outComment) {
        this.outComment = outComment;
    }

    public String getInCategoryName() {
        return inCategoryName;
    }

    public void setInCategoryName(String inCategoryName) {
        this.inCategoryName = inCategoryName;
    }

    public String getConfirmUserName() {
        return confirmUserName;
    }

    public void setConfirmUserName(String confirmUserName) {
        this.confirmUserName = confirmUserName;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getConfirmComment() {
        return confirmComment;
    }

    public void setConfirmComment(String confirmComment) {
        this.confirmComment = confirmComment;
    }
}
