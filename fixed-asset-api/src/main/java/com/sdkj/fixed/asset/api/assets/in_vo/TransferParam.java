package com.sdkj.fixed.asset.api.assets.in_vo;

import com.sdkj.fixed.asset.pojo.assets.Transfer;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author zhangjinfei
 * @Description //TODO 添加修改领用
 * @Date 2020/7/27 10:12
 */
public class TransferParam extends Transfer {
    @NotEmpty
    private List<TransferDetail> detailList;

    public List<TransferDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<TransferDetail> detailList) {
        this.detailList = detailList;
    }
}
