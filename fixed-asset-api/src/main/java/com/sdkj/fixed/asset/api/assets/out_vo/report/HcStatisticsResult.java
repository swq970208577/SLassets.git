package com.sdkj.fixed.asset.api.assets.out_vo.report;

import java.util.List;

/**
 * @ClassName HcStatisticsResult
 * @Description TODO
 * @Author 张欣
 * @Date 2020/8/11 17:00
 */
public class HcStatisticsResult extends HomePageStatisticsResult{
    /**
     * 数量
     */
    private List<HcStatisticsSeriesList> seriesList;
    /**
     * 金额
     */
    private List<HcStatisticsSeriesList> seriesList1;

    public List<HcStatisticsSeriesList> getSeriesList() {
        return seriesList;
    }

    public void setSeriesList(List<HcStatisticsSeriesList> seriesList) {
        this.seriesList = seriesList;
    }

    public List<HcStatisticsSeriesList> getSeriesList1() {
        return seriesList1;
    }

    public void setSeriesList1(List<HcStatisticsSeriesList> seriesList1) {
        this.seriesList1 = seriesList1;
    }
}
