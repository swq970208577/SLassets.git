package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-07-23 : 02:30:30
 */
@Table(name = "as_asset_log")
public class AssetLog implements Serializable {

    public AssetLog() {
        this.logType ="";
        this.handlerUser ="";
        this.createTime ="";
        this.handlerContext ="";
    }

    /**
     * ID
     */
    @Id
    @Column(name = "log_id")
    @KeySql(genId = UUIdGenId.class)
    private String logId;

    /**
     * 资产ID
     */
    @Column(name = "asset_id")
    private String assetId;
    /**
     * 单据类型
     */
    @Column(name = "log_type")
    @Excel(name = "单据类型",orderNum ="0" )
    private String logType;
    /**
     * 处理方式ID
     */
    @Column(name = "handler_id")
    private String handlerId;
    /**
     * 处理人
     */
    @Excel(name = "处理人",orderNum ="2" )
    @Column(name = "handler_user")
    private String handlerUser;
    /**
     * 创建时间
     */
    @Excel(name = "处理时间",orderNum ="1" )
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 处理记录
     */
    @Excel(name = "处理内容",orderNum ="3" )
    @Column(name = "handler_context")
    private String handlerContext;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return log_id - ID
     */
    public String getLogId() {
        return logId;
    }

    /**
     * 设置ID
     *
     * @param logId ID
     */
    public void setLogId(String logId) {
        this.logId = logId == null ? null : logId.trim();
    }

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取日志类型
     *
     * @return log_type - 日志类型
     */
    public String getLogType() {
        return logType;
    }

    /**
     * 设置日志类型
     *
     * @param logType 日志类型
     */
    public void setLogType(String logType) {
        this.logType = logType == null ? null : logType.trim();
    }

    /**
     * 获取处理方式ID
     *
     * @return handler_id - 处理方式ID
     */
    public String getHandlerId() {
        return handlerId;
    }

    /**
     * 设置处理方式ID
     *
     * @param handlerId 处理方式ID
     */
    public void setHandlerId(String handlerId) {
        this.handlerId = handlerId == null ? null : handlerId.trim();
    }

    /**
     * 获取处理人
     *
     * @return handler_user - 处理人
     */
    public String getHandlerUser() {
        return handlerUser;
    }

    /**
     * 设置处理人
     *
     * @param handlerUser 处理人
     */
    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser == null ? null : handlerUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取处理记录
     *
     * @return handler_context - 处理记录
     */
    public String getHandlerContext() {
        return handlerContext;
    }

    /**
     * 设置处理记录
     *
     * @param handlerContext 处理记录
     */
    public void setHandlerContext(String handlerContext) {
        this.handlerContext = handlerContext == null ? null : handlerContext.trim();
    }

}