package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-08-10 : 11:07:05
 */
@Table(name = "as_asset_report")
public class AssetReport implements Serializable {
    /**
     * Id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 年
     */
    @Column(name = "ar_year")
    private String arYear;

    /**
     * 月
     */
    @Column(name = "ar_month")
    private String arMonth;

    /**
     * 日
     */
    @Column(name = "ar_day")
    private String arDay;

    /**
     * 资产ID
     */
    @Column(name = "asset_id")
    private String assetId;

    /**
     * 使用人用户ID
     */
    @Column(name = "use_user_id")
    private String useUserId;

    /**
     * 金额
     */
    private String amount;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 使用状态 0 在用 1 领用 2 借用
     */
    @Column(name = "use_type")
    private Integer useType;

    @Column(name = "org_id")
    private String orgId;

    @Column(name = "use_company")
    private String useCompany;

    private static final long serialVersionUID = 1L;

    public String getUseCompany() {
        return useCompany;
    }

    public void setUseCompany(String useCompany) {
        this.useCompany = useCompany;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取Id
     *
     * @return id - Id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置Id
     *
     * @param id Id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取年
     *
     * @return ar_year - 年
     */
    public String getArYear() {
        return arYear;
    }

    /**
     * 设置年
     *
     * @param arYear 年
     */
    public void setArYear(String arYear) {
        this.arYear = arYear == null ? null : arYear.trim();
    }

    /**
     * 获取月
     *
     * @return ar_month - 月
     */
    public String getArMonth() {
        return arMonth;
    }

    /**
     * 设置月
     *
     * @param arMonth 月
     */
    public void setArMonth(String arMonth) {
        this.arMonth = arMonth == null ? null : arMonth.trim();
    }

    /**
     * 获取日
     *
     * @return ar_day - 日
     */
    public String getArDay() {
        return arDay;
    }

    /**
     * 设置日
     *
     * @param arDay 日
     */
    public void setArDay(String arDay) {
        this.arDay = arDay == null ? null : arDay.trim();
    }

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取使用人用户ID
     *
     * @return use_user_id - 使用人用户ID
     */
    public String getUseUserId() {
        return useUserId;
    }

    /**
     * 设置使用人用户ID
     *
     * @param useUserId 使用人用户ID
     */
    public void setUseUserId(String useUserId) {
        this.useUserId = useUserId == null ? null : useUserId.trim();
    }

    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(String amount) {
        this.amount = amount == null ? null : amount.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取使用状态 0 在用 1 领用 2 借用
     *
     * @return use_type - 使用状态 0 在用 1 领用 2 借用
     */
    public Integer getUseType() {
        return useType;
    }

    /**
     * 设置使用状态 0 在用 1 领用 2 借用
     *
     * @param useType 使用状态 0 在用 1 领用 2 借用
     */
    public void setUseType(Integer useType) {
        this.useType = useType;
    }
}