package com.sdkj.fixed.asset.pojo.system;

import cn.afterturn.easypoi.excel.annotation.Excel;

import cn.afterturn.easypoi.handler.inter.IExcelModel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.*;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 用户
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
public class UserImportManagement implements Serializable, IExcelModel {
    /**
     * 行号
     */
    @Transient
    private int rowNum;

    /**
     * 错误消息
     */
    @Transient
    private String errorMsg;


    @NotBlank(message = "员工名称不能为空")
    @Size(max = 16,message = "姓名最大长度为12")
    @Excel(name="员工名称(必填)",orderNum = "2",width = 20)
    private String name;


    @Excel(name="手机号码(必填)")
    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1(3|4|5|6|7|8|9)\\d{9}$",message = "请输入正确的手机号码")
    private String tel;

    @Email(message = "邮箱格式不正确")
    @Excel(name="电子邮箱")
    private String email;

    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 公司编码
     */
    @Excel(name="所属公司编码(必填)")
    @NotBlank(message = "所属公司编码不能为空")
    @Transient
    private String companyCode;
    /**
     * 部门名称
     */

    private String deptName;
    /**
     * 部门编码
     */
    @Excel(name="所属部门编码(必填)")
    @NotBlank(message = "所属部门编码不能为空")
    private String deptCode;


    public int getRowNum() {
        return rowNum;
    }


    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }
}