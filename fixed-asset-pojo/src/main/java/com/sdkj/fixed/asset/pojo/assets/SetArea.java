package com.sdkj.fixed.asset.pojo.assets;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.validated.assets.EnOrDisArea;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_set_area")
public class SetArea implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @NotBlank(message = "id不能为空", groups = EnOrDisArea.class)
    private String id;

    /**
     * 公司ID
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 区域编码
     */
    @NotBlank(message = "区域编码不能为空")
    @Pattern(regexp = "^[0-9A-Za-z]{4}$", message = "区域编码只能是四位数字或字母")
    private String type;

    /**
     * 区域名称
     */
    @NotBlank(message = "区域名称不能为空")
    @Size(max = 32, message = "区域名称超长")
    private String name;

    /**
     * 状态（0启用1禁用）
     */
    @NotNull(message = "状态（0启用1禁用）不能为空", groups = EnOrDisArea.class)
    private Integer state;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除,0未删除1.已删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取公司ID
     *
     * @return org_id - 公司ID
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司ID
     *
     * @param orgId 公司ID
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取区域编码
     *
     * @return type - 区域编码
     */
    public String getType() {
        return type;
    }

    /**
     * 设置区域编码
     *
     * @param type 区域编码
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取区域名称
     *
     * @return name - 区域名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置区域名称
     *
     * @param name 区域名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取状态（0启用1禁用）
     *
     * @return state - 状态（0启用1禁用）
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态（0启用1禁用）
     *
     * @param state 状态（0启用1禁用）
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除,0未删除1.已删除
     *
     * @return is_deleted - 是否删除,0未删除1.已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除,0未删除1.已删除
     *
     * @param isDeleted 是否删除,0未删除1.已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }
}