package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.IdMustOrg;
import com.sdkj.fixed.asset.pojo.system.group.OrgEdit;
import org.hibernate.validator.constraints.Range;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 组织架构实体
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name = "sys_org_management")
public class OrgManagement implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id长度不匹配",groups = {OrgEdit.class})
    @NotBlank(message = "id不能为空",groups = {IdMustOrg.class})
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    /**
     *状态  1:可用；2：禁用
     */

    private Integer state;
    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @Size(max = 20, message = "名称长度不匹配")
    private String name;

    /**
     * 父id
     */
    @Size(max = 32, message = "父id长度不匹配")
    private String pid;
    /**
     * 编码
     */
    @Size(min = 4,max = 4, message = "编码长度为4")
    @NotBlank(message = "编码不能为空")
    private String code;

    /**
     *类型  1:机构；2：部门
     */
    @NotNull(message = "类型不能为空")
    @Range(min = 1,max = 2,message = "类型只能为1或2")
    private Integer type;

    /**
     * 前端使用 最多8级，用逗号隔开,如果没有父级应为空串
     */
    @Size(max = 263, message = "")
    private String treecode;

    private String reserved;
    /**
     * 父名称
     */
    @Transient
    private String pName;
    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取1:可用；2：禁用
     *
     * @return state - 1:可用；2：禁用
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：禁用
     *
     * @param state 1:可用；2：禁用
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * @param pid
     */
    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取1:机构；2：部门
     *
     * @return type - 1:机构；2：部门
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置1:机构；2：部门
     *
     * @param type 1:机构；2：部门
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取最多8级，用逗号隔开
     *
     * @return treecode - 最多8级，用逗号隔开
     */
    public String getTreecode() {
        return treecode;
    }

    /**
     * 设置最多8级，用逗号隔开
     *
     * @param treecode 最多8级，用逗号隔开
     */
    public void setTreecode(String treecode) {
        this.treecode = treecode == null ? null : treecode.trim();
    }

    /**
     * @return reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }
}