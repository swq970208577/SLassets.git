package com.sdkj.fixed.asset.pojo.assets;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_set_currency")
public class SetCurrency implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 资产类别id
     */
    @Column(name = "set_id")
    private String setId;

    /**
     * 公司ID
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 1.自助领用/借用2.自助交接
     */
    private Integer cate;

    /**
     * 是否删除,0未删除1.已删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取资产类别id
     *
     * @return set_id - 资产类别id
     */
    public String getSetId() {
        return setId;
    }

    /**
     * 设置资产类别id
     *
     * @param setId 资产类别id
     */
    public void setSetId(String setId) {
        this.setId = setId == null ? null : setId.trim();
    }

    /**
     * 获取公司ID
     *
     * @return org_id - 公司ID
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司ID
     *
     * @param orgId 公司ID
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取1.自助领用/借用2.自助交接
     *
     * @return cate - 1.自助领用/借用2.自助交接
     */
    public Integer getCate() {
        return cate;
    }

    /**
     * 设置1.自助领用/借用2.自助交接
     *
     * @param cate 1.自助领用/借用2.自助交接
     */
    public void setCate(Integer cate) {
        this.cate = cate;
    }

    /**
     * 获取是否删除,0未删除1.已删除
     *
     * @return is_deleted - 是否删除,0未删除1.已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除,0未删除1.已删除
     *
     * @param isDeleted 是否删除,0未删除1.已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }
}