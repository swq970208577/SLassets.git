package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-08-04 : 04:30:50
 */
@Table(name="as_asset_intermediate")
public class AssetIntermediate implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 资产ID
     */
    @Column(name = "asset_id")
    private String assetId;

    /**
     * 盘点id
     */
    @Column(name = "inventory_id")
    private String inventoryId;

    /**
     * 盘点状态，0未盘1已盘2盘盈3盘亏
     */
    @Column(name = "inventory_state")
    @Excel(name = "盘点状态", orderNum = "0", replace = {"未盘_0","已盘_1","盘盈_2","盘亏_3"})
    private String inventoryState;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 修改后区域
     */
    @Column(name = "change_area")
    private String changeArea;
    /**
     * 修改后区域名称
     */
    @Column(name = "change_area_name")
    @Excel(name = "修改后区域", orderNum = "13")
    private String changeAreaName;

    /**
     * 修改后存放地点
     */
    @Column(name = "change_storage_location")
    @Excel(name = "修改后存放地点", orderNum = "14")
    private String changeStorageLocation;

    /**
     * 修改后使用公司id
     */
    @Column(name = "change_use_company")
    private String changeUseCompany;

    /**
     * 修改后使用公司名称
     */
    @Column(name = "change_use_company_name")
    @Excel(name = "修改后使用公司", orderNum = "15")
    private String changeUseCompanyName;

    /**
     * 修改后使用部门id
     */
    @Column(name = "change_use_department")
    private String changeUseDepartment;

    /**
     * 修改后使用部门名称
     */
    @Column(name = "change_use_department_name")
    @Excel(name = "修改后使用部门", orderNum = "16")
    private String changeUseDepartmentName;

    /**
     * 修改后使用人id
     */
    @Column(name = "change_handler_user")
    private String changeHandlerUser;

    /**
     * 修改后使用名称
     */
    @Column(name = "change_handler_user_name")
    @Excel(name = "修改后使用人", orderNum = "17")
    private String changeHandlerUserName;
    /**
     *
     */
    @Column(name = "change_use_company_treecode")
    private String changeUseCompanyTreecode;
     /**
     *
     */
    @Column(name = "change_use_dept_treecode")
    private String changeUseDeptTreecode;
    /**
     *
     */
    @Column(name = "change_company_treecode")
    private String changeCompanyTreecode;

    /**
     * 资产条码
     */
    @Column(name = "asset_code")
    @Excel(name = "条形码", orderNum = "3")
    private String assetCode;

    /**
     * 资产名称
     */
    @Column(name = "asset_name")
    @Excel(name = "固定资产名称", orderNum = "4")
    private String assetName;

    /**
     * 资产类别ID
     */
    @Column(name = "asset_class_id")
    private String assetClassId;

    /**
     * 资产类别名称
     */
    @Column(name = "asset_class_name")
    @Excel(name = "固定资产类别", orderNum = "1")
    private String assetClassName;

    /**
     * 标准型号=规格型号ID
     */
    @Column(name = "standard_model")
    private String standardModel;
    /**
     * 规格型号
     */
    @Column(name = "specification_model")
    @Excel(name = "规格型号", orderNum = "5")
    private String specificationModel;

    /**
     * 计量单位
     */
    @Column(name = "unit_measurement")
    @Excel(name = "计量单位", orderNum = "6")
    private String unitMeasurement;

    /**
     * SN号
     */
    @Column(name = "sn_number")
    @Excel(name = "SN号", orderNum = "2")
    private String snNumber;

    /**
     * 所属公司ID
     */
    private String company;

    /**
     * 所属公司ID
     */
    @Column(name = "company_name")
    @Excel(name = "所属公司", orderNum = "11")
    private String companyName;

    /**
     * 使用公司ID
     */
    @Column(name = "use_company")
    private String useCompany;

    /**
     * 使用公司名称
     */
    @Column(name = "use_company_name")
    @Excel(name = "使用公司", orderNum = "9")
    private String useCompanyName;

    /**
     * 使用部门
     */
    @Column(name = "use_department")
    private String useDepartment;

    /**
     * 使用部门名称
     */
    @Column(name = "use_department_name")
    @Excel(name = "使用部门", orderNum = "10")
    private String useDepartmentName;

    /**
     * 使用人ID
     */
    @Column(name = "handler_user_id")
    private String handlerUserId;

    /**
     * 使用人
     */
    @Column(name = "handler_user")
    @Excel(name = "使用人", orderNum = "12")
    private String handlerUser;

    /**
     * 区域
     */
    private String area;
    /**
     * 区域
     */
    @Column(name = "area_name")
    @Excel(name = "区域", orderNum = "7")
    private String areaName;

    /**
     * 存放地点
     */
    @Column(name = "storage_location")
    @Excel(name = "存放地点", orderNum = "8")
    private String storageLocation;

    /**
     * 盘点人名称
     */
    @Column(name = "inventory_user")
    @Excel(name = "盘点人", orderNum = "18")
    private String inventoryUser;

    /**
     * 照片
     */
    private String photo;

    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "19")
    private String remark;
    /**
     * 资产状态
     */
    @Column(name = "asset_state")
    private String assetState;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取盘点id
     *
     * @return inventory_id - 盘点id
     */
    public String getInventoryId() {
        return inventoryId;
    }

    /**
     * 设置盘点id
     *
     * @param inventoryId 盘点id
     */
    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId == null ? null : inventoryId.trim();
    }

    /**
     * 获取盘点状态，0未盘1已盘2盘盈3盘亏
     *
     * @return inventory_state - 盘点状态，0未盘1已盘2盘盈3盘亏
     */
    public String getInventoryState() {
        return inventoryState;
    }

    /**
     * 设置盘点状态，0未盘1已盘2盘盈3盘亏
     *
     * @param inventoryState 盘点状态，0未盘1已盘2盘盈3盘亏
     */
    public void setInventoryState(String inventoryState) {
        this.inventoryState = inventoryState == null ? null : inventoryState.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取修改后区域
     *
     * @return change_area - 修改后区域
     */
    public String getChangeArea() {
        return changeArea;
    }

    /**
     * 设置修改后区域
     *
     * @param changeArea 修改后区域
     */
    public void setChangeArea(String changeArea) {
        this.changeArea = changeArea == null ? null : changeArea.trim();
    }

    /**
     * 获取修改后存放地点
     *
     * @return change_storage_location - 修改后存放地点
     */
    public String getChangeStorageLocation() {
        return changeStorageLocation;
    }

    /**
     * 设置修改后存放地点
     *
     * @param changeStorageLocation 修改后存放地点
     */
    public void setChangeStorageLocation(String changeStorageLocation) {
        this.changeStorageLocation = changeStorageLocation == null ? null : changeStorageLocation.trim();
    }

    /**
     * 获取修改后使用公司id
     *
     * @return change_use_company - 修改后使用公司id
     */
    public String getChangeUseCompany() {
        return changeUseCompany;
    }

    /**
     * 设置修改后使用公司id
     *
     * @param changeUseCompany 修改后使用公司id
     */
    public void setChangeUseCompany(String changeUseCompany) {
        this.changeUseCompany = changeUseCompany == null ? null : changeUseCompany.trim();
    }

    /**
     * 获取修改后使用公司名称
     *
     * @return change_use_company_name - 修改后使用公司名称
     */
    public String getChangeUseCompanyName() {
        return changeUseCompanyName;
    }

    /**
     * 设置修改后使用公司名称
     *
     * @param changeUseCompanyName 修改后使用公司名称
     */
    public void setChangeUseCompanyName(String changeUseCompanyName) {
        this.changeUseCompanyName = changeUseCompanyName == null ? null : changeUseCompanyName.trim();
    }

    /**
     * 获取修改后使用部门id
     *
     * @return change_use_department - 修改后使用部门id
     */
    public String getChangeUseDepartment() {
        return changeUseDepartment;
    }

    /**
     * 设置修改后使用部门id
     *
     * @param changeUseDepartment 修改后使用部门id
     */
    public void setChangeUseDepartment(String changeUseDepartment) {
        this.changeUseDepartment = changeUseDepartment == null ? null : changeUseDepartment.trim();
    }

    /**
     * 获取修改后使用部门名称
     *
     * @return change_use_department_name - 修改后使用部门名称
     */
    public String getChangeUseDepartmentName() {
        return changeUseDepartmentName;
    }

    /**
     * 设置修改后使用部门名称
     *
     * @param changeUseDepartmentName 修改后使用部门名称
     */
    public void setChangeUseDepartmentName(String changeUseDepartmentName) {
        this.changeUseDepartmentName = changeUseDepartmentName == null ? null : changeUseDepartmentName.trim();
    }

    /**
     * 获取修改后使用人id
     *
     * @return change_handler_user - 修改后使用人id
     */
    public String getChangeHandlerUser() {
        return changeHandlerUser;
    }

    /**
     * 设置修改后使用人id
     *
     * @param changeHandlerUser 修改后使用人id
     */
    public void setChangeHandlerUser(String changeHandlerUser) {
        this.changeHandlerUser = changeHandlerUser == null ? null : changeHandlerUser.trim();
    }

    /**
     * 获取修改后使用名称
     *
     * @return change_handler_user_name - 修改后使用名称
     */
    public String getChangeHandlerUserName() {
        return changeHandlerUserName;
    }

    /**
     * 设置修改后使用名称
     *
     * @param changeHandlerUserName 修改后使用名称
     */
    public void setChangeHandlerUserName(String changeHandlerUserName) {
        this.changeHandlerUserName = changeHandlerUserName == null ? null : changeHandlerUserName.trim();
    }

    /**
     * 获取资产条码
     *
     * @return asset_code - 资产条码
     */
    public String getAssetCode() {
        return assetCode;
    }

    /**
     * 设置资产条码
     *
     * @param assetCode 资产条码
     */
    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode == null ? null : assetCode.trim();
    }

    /**
     * 获取资产名称
     *
     * @return asset_name - 资产名称
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * 设置资产名称
     *
     * @param assetName 资产名称
     */
    public void setAssetName(String assetName) {
        this.assetName = assetName == null ? null : assetName.trim();
    }

    /**
     * 获取资产类别ID
     *
     * @return asset_class_id - 资产类别ID
     */
    public String getAssetClassId() {
        return assetClassId;
    }

    /**
     * 设置资产类别ID
     *
     * @param assetClassId 资产类别ID
     */
    public void setAssetClassId(String assetClassId) {
        this.assetClassId = assetClassId == null ? null : assetClassId.trim();
    }

    /**
     * 获取资产类别名称
     *
     * @return asset_class_name - 资产类别名称
     */
    public String getAssetClassName() {
        return assetClassName;
    }

    /**
     * 设置资产类别名称
     *
     * @param assetClassName 资产类别名称
     */
    public void setAssetClassName(String assetClassName) {
        this.assetClassName = assetClassName == null ? null : assetClassName.trim();
    }

    /**
     * 获取标准型号=规格型号ID
     *
     * @return standard_model - 标准型号=规格型号ID
     */
    public String getStandardModel() {
        return standardModel;
    }

    /**
     * 设置标准型号=规格型号ID
     *
     * @param standardModel 标准型号=规格型号ID
     */
    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel == null ? null : standardModel.trim();
    }

    /**
     * 获取计量单位
     *
     * @return unit_measurement - 计量单位
     */
    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    /**
     * 设置计量单位
     *
     * @param unitMeasurement 计量单位
     */
    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement == null ? null : unitMeasurement.trim();
    }

    /**
     * 获取SN号
     *
     * @return sn_number - SN号
     */
    public String getSnNumber() {
        return snNumber;
    }

    /**
     * 设置SN号
     *
     * @param snNumber SN号
     */
    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber == null ? null : snNumber.trim();
    }

    /**
     * 获取所属公司ID
     *
     * @return company - 所属公司ID
     */
    public String getCompany() {
        return company;
    }

    /**
     * 设置所属公司ID
     *
     * @param company 所属公司ID
     */
    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }

    /**
     * 获取所属公司ID
     *
     * @return company_name - 所属公司ID
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 设置所属公司ID
     *
     * @param companyName 所属公司ID
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    /**
     * 获取使用公司ID
     *
     * @return use_company - 使用公司ID
     */
    public String getUseCompany() {
        return useCompany;
    }

    /**
     * 设置使用公司ID
     *
     * @param useCompany 使用公司ID
     */
    public void setUseCompany(String useCompany) {
        this.useCompany = useCompany == null ? null : useCompany.trim();
    }

    /**
     * 获取使用公司名称
     *
     * @return use_company_name - 使用公司名称
     */
    public String getUseCompanyName() {
        return useCompanyName;
    }

    /**
     * 设置使用公司名称
     *
     * @param useCompanyName 使用公司名称
     */
    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName == null ? null : useCompanyName.trim();
    }

    /**
     * 获取使用部门
     *
     * @return use_department - 使用部门
     */
    public String getUseDepartment() {
        return useDepartment;
    }

    /**
     * 设置使用部门
     *
     * @param useDepartment 使用部门
     */
    public void setUseDepartment(String useDepartment) {
        this.useDepartment = useDepartment == null ? null : useDepartment.trim();
    }

    /**
     * 获取使用部门名称
     *
     * @return use_department_name - 使用部门名称
     */
    public String getUseDepartmentName() {
        return useDepartmentName;
    }

    /**
     * 设置使用部门名称
     *
     * @param useDepartmentName 使用部门名称
     */
    public void setUseDepartmentName(String useDepartmentName) {
        this.useDepartmentName = useDepartmentName == null ? null : useDepartmentName.trim();
    }

    /**
     * 获取使用人ID
     *
     * @return handler_user_id - 使用人ID
     */
    public String getHandlerUserId() {
        return handlerUserId;
    }

    /**
     * 设置使用人ID
     *
     * @param handlerUserId 使用人ID
     */
    public void setHandlerUserId(String handlerUserId) {
        this.handlerUserId = handlerUserId == null ? null : handlerUserId.trim();
    }

    /**
     * 获取使用人
     *
     * @return handler_user - 使用人
     */
    public String getHandlerUser() {
        return handlerUser;
    }

    /**
     * 设置使用人
     *
     * @param handlerUser 使用人
     */
    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser == null ? null : handlerUser.trim();
    }

    /**
     * 获取区域
     *
     * @return area - 区域
     */
    public String getArea() {
        return area;
    }

    /**
     * 设置区域
     *
     * @param area 区域
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    /**
     * 获取存放地点
     *
     * @return storage_location - 存放地点
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * 设置存放地点
     *
     * @param storageLocation 存放地点
     */
    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation == null ? null : storageLocation.trim();
    }

    public String getInventoryUser() {
        return inventoryUser;
    }

    public void setInventoryUser(String inventoryUser) {
        this.inventoryUser = inventoryUser;
    }

    /**
     * 获取照片
     *
     * @return photo - 照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 设置照片
     *
     * @param photo 照片
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getChangeAreaName() {
        return changeAreaName;
    }

    public void setChangeAreaName(String changeAreaName) {
        this.changeAreaName = changeAreaName;
    }

    public String getChangeUseCompanyTreecode() {
        return changeUseCompanyTreecode;
    }

    public void setChangeUseCompanyTreecode(String changeUseCompanyTreecode) {
        this.changeUseCompanyTreecode = changeUseCompanyTreecode;
    }

    public String getChangeUseDeptTreecode() {
        return changeUseDeptTreecode;
    }

    public void setChangeUseDeptTreecode(String changeUseDeptTreecode) {
        this.changeUseDeptTreecode = changeUseDeptTreecode;
    }

    public String getChangeCompanyTreecode() {
        return changeCompanyTreecode;
    }

    public void setChangeCompanyTreecode(String changeCompanyTreecode) {
        this.changeCompanyTreecode = changeCompanyTreecode;
    }

    public String getAssetState() {
        return assetState;
    }

    public void setAssetState(String assetState) {
        this.assetState = assetState;
    }
}