package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录
 *
 * @author 史晨星
 * @date 2020-07-21 : 11:22:55
 */
@Table(name = "pm_category_product_config")
public class CategoryProduct implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 仓库id
     */
    @Column(name = "category_id")
    private String categoryId;

    /**
     * 耗材id
     */
    @Column(name = "product_id")
    private String productId;

    /**
     * 耗材数量
     */
    @Column(name = "product_num")
    private Integer productNum;

    private String price;

    private String totalPrice;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取仓库id
     *
     * @return category_id - 仓库id
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 设置仓库id
     *
     * @param categoryId 仓库id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? null : categoryId.trim();
    }

    /**
     * 获取耗材id
     *
     * @return product_id - 耗材id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 设置耗材id
     *
     * @param productId 耗材id
     */
    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    /**
     * 获取耗材数量
     *
     * @return product_num - 耗材数量
     */
    public Integer getProductNum() {
        return productNum;
    }

    /**
     * 设置耗材数量
     *
     * @param productNum 耗材数量
     */
    public void setProductNum(Integer productNum) {
        this.productNum = productNum;
    }
}