package com.sdkj.fixed.asset.pojo.assets;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * AssApproval
 *
 * @author zhaohzeyu
 * @Description 我的审批实体
 * @date 2020/7/30 18:23
 */
public class AssApproval implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "申领类型")
    private String cut;
    @ApiModelProperty(value = "申请类型编号")
    private Integer dig;
    @ApiModelProperty(value = "申领状态")
    private Integer state;
    @ApiModelProperty(value = "申领订单号")
    private String number;
    @ApiModelProperty(value = "申请人")
    private String poser;
    @ApiModelProperty(value = "申请人公司")
    private String company;
    @ApiModelProperty(value = "申请人部门")
    private String ment;
    @ApiModelProperty(value = "申请时间")
    private String atime;
    @ApiModelProperty(value = "申请人id")
    private String sqpersonid;
    @ApiModelProperty(value = "申请人名字")
    private String sqpersonname;
    @ApiModelProperty(value = "备注")
    private String comment;


    public Integer getDig() {
        return dig;
    }

    public void setDig(Integer dig) {
        this.dig = dig;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPoser() {
        return poser;
    }

    public void setPoser(String poser) {
        this.poser = poser;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMent() {
        return ment;
    }

    public void setMent(String ment) {
        this.ment = ment;
    }

    public String getAtime() {
        return atime;
    }

    public void setAtime(String atime) {
        this.atime = atime;
    }

    public String getSqpersonid() {
        return sqpersonid;
    }

    public void setSqpersonid(String sqpersonid) {
        this.sqpersonid = sqpersonid;
    }

    public String getSqpersonname() {
        return sqpersonname;
    }

    public void setSqpersonname(String sqpersonname) {
        this.sqpersonname = sqpersonname;
    }
}
