package com.sdkj.fixed.asset.pojo.assets;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_stand_model")
public class StandModel implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 资产名称
     */
    @NotBlank(message = "资产名称不能为空")
    @Size(max = 32, message = "资产名称超长")
    private String name;

    /**
     * 规格型号
     */
    @NotBlank(message = "规格型号不能为空")
    @Size(max = 32, message = "规格型号超长")
    private String model;

    /**
     * 计量单位
     */
    @Size(max = 32, message = "计量单位超长")
    private String type;

    /**
     * 原值
     */
    @Size(max = 32, message = "原值超长")
    private String value;

    /**
     * 照片
     */
    @Size(max = 500, message = "照片超长")
    private String pic;

    /**
     * 资产状态（0可用1禁用）
     */
    @Digits (integer = 0, fraction = 1, message = "资产状态（0可用1禁用）")
    private Integer state;

    /**
     * 资产类别id
     */
    @Column(name = "set_id")
    @NotBlank(message = "资产类别不能为空")
    @Size(max = 32, message = "资产类别超长")
    private String setId;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除,0未删除1.已删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取资产名称
     *
     * @return name - 资产名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置资产名称
     *
     * @param name 资产名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取规格型号
     *
     * @return model - 规格型号
     */
    public String getModel() {
        return model;
    }

    /**
     * 设置规格型号
     *
     * @param model 规格型号
     */
    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    /**
     * 获取计量单位
     *
     * @return type - 计量单位
     */
    public String getType() {
        return type;
    }

    /**
     * 设置计量单位
     *
     * @param type 计量单位
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取原值
     *
     * @return value - 原值
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置原值
     *
     * @param value 原值
     */
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    /**
     * 获取照片
     *
     * @return pic - 照片
     */
    public String getPic() {
        return pic;
    }

    /**
     * 设置照片
     *
     * @param pic 照片
     */
    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    /**
     * 获取资产状态（0可用1禁用）
     *
     * @return state - 资产状态（0可用1禁用）
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置资产状态（0可用1禁用）
     *
     * @param state 资产状态（0可用1禁用）
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取资产类别id
     *
     * @return set_id - 资产类别id
     */
    public String getSetId() {
        return setId;
    }

    /**
     * 设置资产类别id
     *
     * @param setId 资产类别id
     */
    public void setSetId(String setId) {
        this.setId = setId == null ? null : setId.trim();
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除,0未删除1.已删除
     *
     * @return is_deleted - 是否删除,0未删除1.已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除,0未删除1.已删除
     *
     * @param isDeleted 是否删除,0未删除1.已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }
}