package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.EditDicType;
import com.sdkj.fixed.asset.pojo.system.group.IdMustDicType;
import com.sdkj.fixed.asset.pojo.system.group.addDicType;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name = "sys_dictionary_type")
public class DictionaryType implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @NotBlank(message = "id不能为空",groups = {IdMustDicType.class})
    @Size(max = 32,message = "id长度不匹配",groups = {EditDicType.class})
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    @NotBlank(message = "名称不能为空")
    @Size(max = 32,message = "名称长度不匹配")
    private String name;

    @NotBlank(message = "值不能为空")
    @Size(max = 32,message = "值长度不匹配")
    private String value;

    @NotBlank(message = "排序不能为空")
    @Size(max = 3,message = "排序长度不匹配")
    @Pattern(regexp = "^\\d{1,3}$",message = "排序只能为数字")
    private String sort;

    @NotBlank(message = "数据字典id不能为空",groups = {addDicType.class})
    @Size(max = 32,message = "排序长度不匹配",groups = {addDicType.class})
    @Column(name = "sde_id")
    private String sdeId;

    @Size(max = 128,message = "备注长度不匹配")
    private String comments;

    /**
     * 1:可用；2：删除
     */
    private Integer state;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     */
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    /**
     * @return sort
     */
    public String getSort() {
        return sort;
    }

    /**
     * @param sort
     */
    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * @return sde_id
     */
    public String getSdeId() {
        return sdeId;
    }

    /**
     * @param sdeId
     */
    public void setSdeId(String sdeId) {
        this.sdeId = sdeId == null ? null : sdeId.trim();
    }

    /**
     * @return comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    /**
     * 获取1:可用；2：删除
     *
     * @return state - 1:可用；2：删除
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：删除
     *
     * @param state 1:可用；2：删除
     */
    public void setState(Integer state) {
        this.state = state;
    }
}