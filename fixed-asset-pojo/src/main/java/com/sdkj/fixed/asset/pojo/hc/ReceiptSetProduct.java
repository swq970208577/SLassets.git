package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptSet;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptSet;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 调整单-耗材关联
 *
 * @author 史晨星
 * @date 2020-07-23 : 09:44:09
 */
@Table(name = "pm_product_set_receipt_config")
public class ReceiptSetProduct implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 单号id
     */
    @Column(name = "receipt_id")
    private String receiptId;

    /**
     * 耗材id
     */
    @Column(name = "product_id")
    @Size(max = 32, message = "耗材id-最大长度:32")
    @NotNull(message = "耗材id-不能为空", groups = {EditReceiptSet.class, AddReceiptSet.class})
    private String productId;

    /**
     * 耗材数量
     */
    @NotNull(message = "耗材数量-不能为空", groups = {EditReceiptSet.class, AddReceiptSet.class})
    private Integer num;

    /**
     * 耗材单价
     */
    @NotNull(message = "耗材单价-不能为空", groups = {EditReceiptSet.class, AddReceiptSet.class})
    @Size(max = 32, message = "耗材单价-最大长度:32")
    private String price;

    /**
     * 耗材金额
     */
    @Column(name = "total_price")
    @NotNull(message = "耗材金额-不能为空", groups = {EditReceiptSet.class, AddReceiptSet.class})
    @Size(max = 32, message = "耗材金额-最大长度:32")
    private String totalPrice;

    /**
     * 原耗材数量
     */
    private Integer numBefore;

    /**
     * 原耗材单价
     */
    private String priceBefore;

    /**
     * 原耗材金额
     */
    @Column(name = "total_price_before")
    private String totalPriceBefore;

    /**
     * 耗材数量
     */
    private Integer numAfter;

    /**
     * 耗材单价
     */
    private String priceAfter;

    /**
     * 耗材金额
     */
    @Column(name = "total_price_after")
    private String totalPriceAfter;

    @Transient
    private String number;

    @Transient
    private String cuser;


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    @Transient
    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getNumBefore() {
        return numBefore;
    }

    public void setNumBefore(Integer numBefore) {
        this.numBefore = numBefore;
    }

    public String getPriceBefore() {
        return priceBefore;
    }

    public void setPriceBefore(String priceBefore) {
        this.priceBefore = priceBefore;
    }

    public String getTotalPriceBefore() {
        return totalPriceBefore;
    }

    public void setTotalPriceBefore(String totalPriceBefore) {
        this.totalPriceBefore = totalPriceBefore;
    }

    public Integer getNumAfter() {
        return numAfter;
    }

    public void setNumAfter(Integer numAfter) {
        this.numAfter = numAfter;
    }

    public String getPriceAfter() {
        return priceAfter;
    }

    public void setPriceAfter(String priceAfter) {
        this.priceAfter = priceAfter;
    }

    public String getTotalPriceAfter() {
        return totalPriceAfter;
    }

    public void setTotalPriceAfter(String totalPriceAfter) {
        this.totalPriceAfter = totalPriceAfter;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取单号id
     *
     * @return receipt_id - 单号id
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * 设置单号id
     *
     * @param receiptId 单号id
     */
    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId == null ? null : receiptId.trim();
    }

    /**
     * 获取耗材id
     *
     * @return product_id - 耗材id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 设置耗材id
     *
     * @param productId 耗材id
     */
    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    /**
     * 获取耗材数量
     *
     * @return num - 耗材数量
     */
    public Integer getNum() {
        return num;
    }

    /**
     * 设置耗材数量
     *
     * @param num 耗材数量
     */
    public void setNum(Integer num) {
        this.num = num;
    }

    /**
     * 获取耗材单价
     *
     * @return price - 耗材单价
     */
    public String getPrice() {
        return price;
    }

    /**
     * 设置耗材单价
     *
     * @param price 耗材单价
     */
    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    /**
     * 获取耗材金额
     *
     * @return total_price - 耗材金额
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * 设置耗材金额
     *
     * @param totalPrice 耗材金额
     */
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice == null ? null : totalPrice.trim();
    }
}