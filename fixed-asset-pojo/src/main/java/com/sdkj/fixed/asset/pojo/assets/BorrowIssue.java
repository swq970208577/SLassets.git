package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_borrow_issue")
public class BorrowIssue implements Serializable {
    /**
     * 序号
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Excel(name = "序号", orderNum = "0", width = 20, needMerge = true)
    private String id;

    /**
     * 借用人
     */
    @Column(name = "borrow_user")
    @Size(max = 32, message = "借用人超长")
    @NotBlank(message = "借用人不能为空")
    @Excel(name = "借用人", orderNum = "1", width = 20, needMerge = true)
    private String borrowUser;
    /**
     * 借用人
     */
    @Column(name = "borrow_user_id")
    @Size(max = 32, message = "借用人超长")
    @NotBlank(message = "借用人id不能为空")
    private String borrowUserId;

    /**
     * 借出时间
     */
    @Column(name = "lend_date")
    @NotBlank(message = "借用时间不能为空")
    @Size(max = 32, message = "借出时间超长")
    @Excel(name = "借出时间", orderNum = "2", width = 20, needMerge = true)
    private String lendDate;

    /**
     * 预计归还时间
     */
    @Column(name = "expect_back_date")
    @NotBlank(message = "预计归还时间不能为空")
    private String expectBackDate;

    /**
     * 实际归还时间
     */
    @Column(name = "actual_back_date")
    private String actualBackDate;

    /**
     * 借出处理人
     */
    @Column(name = "lend_user")
    @NotBlank(message = "借出处理人不能为空")
    @Size(max = 32, message = "借用人超长")
    private String lendUser;

    /**
     * 归还处理人
     */
    @Column(name = "back_user")
    @Size(max = 32, message = "借用人超长")
    private String backUser;

    /**
     * 状态，1.待签字2已签字3取消
     */
    private Integer state;

    /**
     * 备注
     */
    @Size(max = 128, message = "备注超长")
    private String note;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_date")
    private String updateDate;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 公司id
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 归还借用状态，1借用2归还
     */
    @Column(name = "borrow_back_state")
    private Integer borrowBackState;
    /**
     * 签名照
     */
    @Column(name = "sign_pic")
    private String signPic;

    /**
     * 编号
     */
    private String number;
    /**
     * 申请单据编号
     */
    @Column(name = "apply_id")
    private String applyId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public String getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取借用人
     *
     * @return borrow_user - 借用人
     */
    public String getBorrowUser() {
        return borrowUser;
    }

    /**
     * 设置借用人
     *
     * @param borrowUser 借用人
     */
    public void setBorrowUser(String borrowUser) {
        this.borrowUser = borrowUser == null ? null : borrowUser.trim();
    }

    /**
     * 获取借出时间
     *
     * @return lend_date - 借出时间
     */
    public String getLendDate() {
        return lendDate;
    }

    /**
     * 设置借出时间
     *
     * @param lendDate 借出时间
     */
    public void setLendDate(String lendDate) {
        this.lendDate = lendDate == null ? null : lendDate.trim();
    }

    /**
     * 获取预计归还时间
     *
     * @return expect_back_date - 预计归还时间
     */
    public String getExpectBackDate() {
        return expectBackDate;
    }

    /**
     * 设置预计归还时间
     *
     * @param expectBackDate 预计归还时间
     */
    public void setExpectBackDate(String expectBackDate) {
        this.expectBackDate = expectBackDate == null ? null : expectBackDate.trim();
    }

    /**
     * 获取实际归还时间
     *
     * @return actual_back_date - 实际归还时间
     */
    public String getActualBackDate() {
        return actualBackDate;
    }

    /**
     * 设置实际归还时间
     *
     * @param actualBackDate 实际归还时间
     */
    public void setActualBackDate(String actualBackDate) {
        this.actualBackDate = actualBackDate == null ? null : actualBackDate.trim();
    }

    /**
     * 获取借出处理人
     *
     * @return lend_user - 借出处理人
     */
    public String getLendUser() {
        return lendUser;
    }

    /**
     * 设置借出处理人
     *
     * @param lendUser 借出处理人
     */
    public void setLendUser(String lendUser) {
        this.lendUser = lendUser == null ? null : lendUser.trim();
    }

    /**
     * 获取归还处理人
     *
     * @return back_user - 归还处理人
     */
    public String getBackUser() {
        return backUser;
    }

    /**
     * 设置归还处理人
     *
     * @param backUser 归还处理人
     */
    public void setBackUser(String backUser) {
        this.backUser = backUser == null ? null : backUser.trim();
    }

    /**
     * 获取状态，1.待签字2已签字3取消
     *
     * @return state - 状态，1.待签字2已签字3取消
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态，1.待签字2已签字3取消
     *
     * @param state 状态，1.待签字2已签字3取消
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_date - 修改时间
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_deleted - 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除
     *
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取公司id
     *
     * @return org_id - 公司id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司id
     *
     * @param orgId 公司id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public Integer getBorrowBackState() {
        return borrowBackState;
    }

    public void setBorrowBackState(Integer borrowBackState) {
        this.borrowBackState = borrowBackState;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSignPic() {
        return signPic;
    }

    public void setSignPic(String signPic) {
        this.signPic = signPic;
    }

    public String getBorrowUserId() {
        return borrowUserId;
    }

    public void setBorrowUserId(String borrowUserId) {
        this.borrowUserId = borrowUserId;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }
}