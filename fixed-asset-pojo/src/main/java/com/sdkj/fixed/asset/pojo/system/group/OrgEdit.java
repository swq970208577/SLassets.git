package com.sdkj.fixed.asset.pojo.system.group;

import javax.validation.groups.Default;

/**
 * @ClassName OrgEdit
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/22 17:22
 */
public interface OrgEdit extends Default,IdMustOrg {
}
