package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 盘点单-分配用户
 *
 * @author 史晨星
 * @date 2020-08-11 : 04:42:48
 */
@Table(name = "pm_qwert")
public class Qwert implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;
    @Excel(name = "单据号", orderNum = "1", width = 20)
    private String number;
    @Excel(name = "经办日期", orderNum = "6", width = 15)
    private String ctime;

    private String cuser;

    @Column(name = "before_num")
    @Excel(name = "初始数量", orderNum = "7", width = 10)
    private Integer beforeNum;

    @Column(name = "before_price")
    @Excel(name = "初始单价", orderNum = "8", width = 10)
    private String beforePrice;

    @Column(name = "before_total_price")
    @Excel(name = "初始金额", orderNum = "9", width = 10)
    private String beforeTotalPrice;

    private Integer num;

    private String price;

    @Column(name = "total_price")
    private String totalPrice;

    @Column(name = "after_num")
    @Excel(name = "结存数量", orderNum = "16", width = 10)
    private Integer afterNum;

    @Column(name = "after_price")
    @Excel(name = "结存单价", orderNum = "17", width = 10)
    private String afterPrice;

    @Column(name = "after_total_price")
    @Excel(name = "结存总价", orderNum = "18", width = 10)
    private String afterTotalPrice;

    private String productId;

    private String categoryId;
    @Transient
    @Excel(name = "单据类型", orderNum = "2", width = 15)
    private String type;
    @Transient
    @Excel(name = "入库数量", orderNum = "10", width = 10)
    private Integer inNum;
    @Transient
    @Excel(name = "入库单价", orderNum = "11", width = 10)
    private String inPrice;
    @Transient
    @Excel(name = "入库总价", orderNum = "12", width = 10)
    private String inTotalPrice;
    @Transient
    @Excel(name = "出库单价", orderNum = "15", width = 10)
    private String outPrice;
    @Transient
    @Excel(name = "出库总价", orderNum = "14", width = 10)
    private String outTotalPrice;
    @Transient
    @Excel(name = "出库数量", orderNum = "13", width = 10)
    private Integer outNum;

    @Excel(name = "业务日期", orderNum = "3", width = 15)
    private String bussinessDate;

    @Transient
    @Excel(name = "经办人", orderNum = "5", width = 15)
    private String cuserName;
    @Transient
    private String ctime1;
    @Transient
    @Excel(name = "单据日期", orderNum = "4", width = 15)
    private String ctime2;

    private String categoryId1;

    public String getCategoryId1() {
        return categoryId1;
    }

    public void setCategoryId1(String categoryId1) {
        this.categoryId1 = categoryId1;
    }

    public String getBussinessDate() {
        return bussinessDate;
    }

    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate;
    }

    public String getCtime1() {
        return ctime1;
    }

    public void setCtime1(String ctime1) {
        this.ctime1 = ctime1;
    }

    public String getCtime2() {
        return ctime2;
    }

    public void setCtime2(String ctime2) {
        this.ctime2 = ctime2;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public Integer getInNum() {
        return inNum;
    }

    public void setInNum(Integer inNum) {
        this.inNum = inNum;
    }

    public String getInPrice() {
        return inPrice;
    }

    public void setInPrice(String inPrice) {
        this.inPrice = inPrice;
    }

    public String getInTotalPrice() {
        return inTotalPrice;
    }

    public void setInTotalPrice(String inTotalPrice) {
        this.inTotalPrice = inTotalPrice;
    }

    public String getOutPrice() {
        return outPrice;
    }

    public void setOutPrice(String outPrice) {
        this.outPrice = outPrice;
    }

    public String getOutTotalPrice() {
        return outTotalPrice;
    }

    public void setOutTotalPrice(String outTotalPrice) {
        this.outTotalPrice = outTotalPrice;
    }

    public Integer getOutNum() {
        return outNum;
    }

    public void setOutNum(Integer outNum) {
        this.outNum = outNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return before_num
     */
    public Integer getBeforeNum() {
        return beforeNum;
    }

    /**
     * @param beforeNum
     */
    public void setBeforeNum(Integer beforeNum) {
        this.beforeNum = beforeNum;
    }

    /**
     * @return before_price
     */
    public String getBeforePrice() {
        return beforePrice;
    }

    /**
     * @param beforePrice
     */
    public void setBeforePrice(String beforePrice) {
        this.beforePrice = beforePrice == null ? null : beforePrice.trim();
    }

    /**
     * @return before_total_price
     */
    public String getBeforeTotalPrice() {
        return beforeTotalPrice;
    }

    /**
     * @param beforeTotalPrice
     */
    public void setBeforeTotalPrice(String beforeTotalPrice) {
        this.beforeTotalPrice = beforeTotalPrice == null ? null : beforeTotalPrice.trim();
    }

    /**
     * @return num
     */
    public Integer getNum() {
        return num;
    }

    /**
     * @param num
     */
    public void setNum(Integer num) {
        this.num = num;
    }

    /**
     * @return price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price
     */
    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    /**
     * @return total_price
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     */
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice == null ? null : totalPrice.trim();
    }

    /**
     * @return after_num
     */
    public Integer getAfterNum() {
        return afterNum;
    }

    /**
     * @param afterNum
     */
    public void setAfterNum(Integer afterNum) {
        this.afterNum = afterNum;
    }

    /**
     * @return after_price
     */
    public String getAfterPrice() {
        return afterPrice;
    }

    /**
     * @param afterPrice
     */
    public void setAfterPrice(String afterPrice) {
        this.afterPrice = afterPrice == null ? null : afterPrice.trim();
    }

    /**
     * @return after_total_price
     */
    public String getAfterTotalPrice() {
        return afterTotalPrice;
    }

    /**
     * @param afterTotalPrice
     */
    public void setAfterTotalPrice(String afterTotalPrice) {
        this.afterTotalPrice = afterTotalPrice == null ? null : afterTotalPrice.trim();
    }
}