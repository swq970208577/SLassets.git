package com.sdkj.fixed.asset.pojo.system;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2021-04-08 : 11:58:50
 */
@Table(name = "sys_usage_management")
public class UsageManagement implements Serializable {
    /**
     * 主键
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 管理员id
     */
    private String cuser;

    /**
     * 创建时间
     */
    private String ctime;

    /**
     * 修改人id
     */
    private String euser;

    /**
     * 修改时间
     */
    private String etime;

    /**
     * 公司id
     */
    @Column(name = "company_id")
    private String companyId;

    /**
     * 1:免费版;2：标准版;3:专业版
     */
    private Integer type;

    /**
     * 可用资产条数
     */
    private String number;

    /**
     * 到期时间
     */
    private String date;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取管理员id
     *
     * @return cuser - 管理员id
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置管理员id
     *
     * @param cuser 管理员id
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取修改人id
     *
     * @return euser - 修改人id
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改人id
     *
     * @param euser 修改人id
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return etime - 修改时间
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改时间
     *
     * @param etime 修改时间
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取公司id
     *
     * @return company_id - 公司id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 设置公司id
     *
     * @param companyId 公司id
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    /**
     * 获取1:免费版;2：标准版;3:专业版
     *
     * @return type - 1:免费版;2：标准版;3:专业版
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置1:免费版;2：标准版;3:专业版
     *
     * @param type 1:免费版;2：标准版;3:专业版
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取可用资产条数
     *
     * @return number - 可用资产条数
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置可用资产条数
     *
     * @param number 可用资产条数
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取到期时间
     *
     * @return date - 到期时间
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置到期时间
     *
     * @param date 到期时间
     */
    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }
}