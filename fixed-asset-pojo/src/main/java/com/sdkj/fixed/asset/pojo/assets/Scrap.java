package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_scrap")
public class Scrap implements Serializable {
    /**
     * 序号
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 清理日期
     */
    @NotBlank(message = "清理日期不能为空")
    @Size(max = 10, message = "清理日期超长，格式：2020-07-29")
    @Excel(name = "清理日期", orderNum = "2", needMerge = true)
    private String date;

    /**
     * 清理人
     */
    @Column(name = "scrap_user")
    @NotBlank(message = "清理人不能为空")
    @Size(max = 32, message = "清理人超长")
    @Excel(name = "清理人", orderNum = "3", needMerge = true)
    private String scrapUser;

    /**
     * 备注
     */
    @Size(max = 128, message = "清理说明超长")
    @Excel(name = "清理说明", orderNum = "4", needMerge = true)
    private String note;

    /**
     * 还原日期
     */
    @Column(name = "restore_date")
    private String restoreDate;

    /**
     * 还原处理人
     */
    @Column(name = "restore_user")
    private String restoreUser;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_date")
    private String updateDate;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 公司id
     */
    @Column(name = "org_id")
    private String orgId;
    /**
     * 单据编号
     */
    @Excel(name = "清理单号", orderNum = "1", needMerge = true)
    private String number;

    private static final long serialVersionUID = 1L;

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public String getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(String id) {
        this.id = id == null ? "" : id.trim();
    }

    /**
     * 获取清理日期
     *
     * @return date - 清理日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置清理日期
     *
     * @param date 清理日期
     */
    public void setDate(String date) {
        this.date = date == null ? "" : date.trim();
    }

    /**
     * 获取清理人
     *
     * @return scrap_user - 清理人
     */
    public String getScrapUser() {
        return scrapUser;
    }

    /**
     * 设置清理人
     *
     * @param scrapUser 清理人
     */
    public void setScrapUser(String scrapUser) {
        this.scrapUser = scrapUser == null ? "" : scrapUser.trim();
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note == null ? "" : note.trim();
    }

    /**
     * 获取还原日期
     *
     * @return restore_date - 还原日期
     */
    public String getRestoreDate() {
        return restoreDate;
    }

    /**
     * 设置还原日期
     *
     * @param restoreDate 还原日期
     */
    public void setRestoreDate(String restoreDate) {
        this.restoreDate = restoreDate == null ? "" : restoreDate.trim();
    }

    /**
     * 获取还原处理人
     *
     * @return restore_user - 还原处理人
     */
    public String getRestoreUser() {
        return restoreUser;
    }

    /**
     * 设置还原处理人
     *
     * @param restoreUser 还原处理人
     */
    public void setRestoreUser(String restoreUser) {
        this.restoreUser = restoreUser == null ? "" : restoreUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? "" : createDate.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? "" : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_date - 修改时间
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? "" : updateDate.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? "" : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_deleted - 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除
     *
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? "" : reserved.trim();
    }

    /**
     * 获取公司id
     *
     * @return org_id - 公司id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司id
     *
     * @param orgId 公司id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? "" : orgId.trim();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}