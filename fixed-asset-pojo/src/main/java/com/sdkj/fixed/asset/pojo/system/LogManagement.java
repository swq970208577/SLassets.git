package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 日志
 * @author 张欣
 * @date  2020-07-27 : 10:23:49
 */
@Table(name = "sys_log_management")
public class LogManagement implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    /**
     * 1:可用；2：删除
     */
    private Integer state;

    private String reserved;

    private String url;

    @Column(name = "user_id")
    private String userId;

    private String ip;

    private String content;

    @Column(name = "org_id")
    private String orgId;

    private String time;
    /**
     * 连接对应操作
     */
    @Transient
    private String bname;
    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取1:可用；2：删除
     *
     * @return state - 1:可用；2：删除
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：删除
     *
     * @param state 1:可用；2：删除
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * @return org_id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * @return time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time
     */
    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }
}