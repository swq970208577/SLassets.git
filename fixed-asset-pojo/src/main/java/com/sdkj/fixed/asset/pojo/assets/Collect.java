package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-07-28 : 01:34:01
 */
@Table(name="as_collect")
public class Collect implements Serializable {
    /**
     * 领用ID
     */
    @Id
    @Column(name = "receive_id")
    @KeySql(genId = UUIdGenId.class)
    private String receiveId;

    /**
     * 领用单号
     */
    @Column(name = "receive_no")
    private String receiveNo;

    /**
     * 领用日期
     */
    @Column(name = "receive_date")
    private String receiveDate;

    /**
     * 领用人ID
     */
    @Column(name = "receive_user_id")
    private String receiveUserId;

    /**
     * 领用人姓名
     */
    @Column(name = "receive_user")
    private String receiveUser;

    /**
     * 领用后使用公司
     */
    @Column(name = "receive_company")
    private String receiveCompany;

    /**
     * 领用后使用部门
     */
    @Column(name = "receive_dept")
    private String receiveDept;

    /**
     * 领用后区域
     */
    @Column(name = "receive_area")
    private String receiveArea;

    /**
     * 领用后存放地点
     */
    @Column(name = "receive_address")
    private String receiveAddress;

    /**
     * 领用备注
     */
    @Column(name = "receive_remark")
    private String receiveRemark;

    /**
     * 预计退库时间
     */
    @Column(name = "return_time")
    private String returnTime;

    /**
     * 处理人姓名
     */
    @Column(name = "handler_user")
    private String handlerUser;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 办理状态 0 待签字  1 已签字
     */
    private Integer state;

    /**
     * 是否审批 1待审批 2同意 3驳回
     */
    private Integer approval;

    /**
     * 0 管理员发放 1 员工申请
     */
    @Column(name = "collect_type")
    private Integer collectType;

    /**
     * 总公司ID
     */
    @Column(name = "org_id")
    private String orgId;

    @Column(name = "asset_num")
    private Integer assetNum;

    /**
     * 领用后公司treecode
     */
    @Column(name = "receive_company_treecode")
    private String receiveCompanyTreecode;

    /**
     * 领用后部门treecode
     */
    @Column(name = "receive_dept_treecode")
    private String receiveDeptTreecode;

    @Column(name = "sign_pic")
    private String signPic;

    @Column(name = "apply_id")
    private String applyId;

    private static final long serialVersionUID = 1L;

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public String getSignPic() {
        return signPic;
    }

    public void setSignPic(String signPic) {
        this.signPic = signPic;
    }

    /**
     * 获取领用ID
     *
     * @return receive_id - 领用ID
     */
    public String getReceiveId() {
        return receiveId;
    }

    /**
     * 设置领用ID
     *
     * @param receiveId 领用ID
     */
    public void setReceiveId(String receiveId) {
        this.receiveId = receiveId == null ? null : receiveId.trim();
    }

    /**
     * 获取领用单号
     *
     * @return receive_no - 领用单号
     */
    public String getReceiveNo() {
        return receiveNo;
    }

    /**
     * 设置领用单号
     *
     * @param receiveNo 领用单号
     */
    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo == null ? null : receiveNo.trim();
    }

    /**
     * 获取领用日期
     *
     * @return receive_date - 领用日期
     */
    public String getReceiveDate() {
        return receiveDate;
    }

    /**
     * 设置领用日期
     *
     * @param receiveDate 领用日期
     */
    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate == null ? null : receiveDate.trim();
    }

    /**
     * 获取领用人ID
     *
     * @return receive_user_id - 领用人ID
     */
    public String getReceiveUserId() {
        return receiveUserId;
    }

    /**
     * 设置领用人ID
     *
     * @param receiveUserId 领用人ID
     */
    public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId == null ? null : receiveUserId.trim();
    }

    /**
     * 获取领用人姓名
     *
     * @return receive_user - 领用人姓名
     */
    public String getReceiveUser() {
        return receiveUser;
    }

    /**
     * 设置领用人姓名
     *
     * @param receiveUser 领用人姓名
     */
    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser == null ? null : receiveUser.trim();
    }

    /**
     * 获取领用后使用公司
     *
     * @return receive_company - 领用后使用公司
     */
    public String getReceiveCompany() {
        return receiveCompany;
    }

    /**
     * 设置领用后使用公司
     *
     * @param receiveCompany 领用后使用公司
     */
    public void setReceiveCompany(String receiveCompany) {
        this.receiveCompany = receiveCompany == null ? null : receiveCompany.trim();
    }

    /**
     * 获取领用后使用部门
     *
     * @return receive_dept - 领用后使用部门
     */
    public String getReceiveDept() {
        return receiveDept;
    }

    /**
     * 设置领用后使用部门
     *
     * @param receiveDept 领用后使用部门
     */
    public void setReceiveDept(String receiveDept) {
        this.receiveDept = receiveDept == null ? null : receiveDept.trim();
    }

    /**
     * 获取领用后区域
     *
     * @return receive_area - 领用后区域
     */
    public String getReceiveArea() {
        return receiveArea;
    }

    /**
     * 设置领用后区域
     *
     * @param receiveArea 领用后区域
     */
    public void setReceiveArea(String receiveArea) {
        this.receiveArea = receiveArea == null ? null : receiveArea.trim();
    }

    /**
     * 获取领用后存放地点
     *
     * @return receive_address - 领用后存放地点
     */
    public String getReceiveAddress() {
        return receiveAddress;
    }

    /**
     * 设置领用后存放地点
     *
     * @param receiveAddress 领用后存放地点
     */
    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress == null ? null : receiveAddress.trim();
    }

    /**
     * 获取领用备注
     *
     * @return receive_remark - 领用备注
     */
    public String getReceiveRemark() {
        return receiveRemark;
    }

    /**
     * 设置领用备注
     *
     * @param receiveRemark 领用备注
     */
    public void setReceiveRemark(String receiveRemark) {
        this.receiveRemark = receiveRemark == null ? null : receiveRemark.trim();
    }

    /**
     * 获取预计退库时间
     *
     * @return return_time - 预计退库时间
     */
    public String getReturnTime() {
        return returnTime;
    }

    /**
     * 设置预计退库时间
     *
     * @param returnTime 预计退库时间
     */
    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime == null ? null : returnTime.trim();
    }

    /**
     * 获取处理人姓名
     *
     * @return handler_user - 处理人姓名
     */
    public String getHandlerUser() {
        return handlerUser;
    }

    /**
     * 设置处理人姓名
     *
     * @param handlerUser 处理人姓名
     */
    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser == null ? null : handlerUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取办理状态 0 待签字  1 已签字
     *
     * @return state - 办理状态 0 待签字  1 已签字
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置办理状态 0 待签字  1 已签字
     *
     * @param state 办理状态 0 待签字  1 已签字
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取是否审批 1待审批 2同意 3驳回
     *
     * @return approval - 是否审批 1待审批 2同意 3驳回
     */
    public Integer getApproval() {
        return approval;
    }

    /**
     * 设置是否审批 1待审批 2同意 3驳回
     *
     * @param approval 是否审批 1待审批 2同意 3驳回
     */
    public void setApproval(Integer approval) {
        this.approval = approval;
    }

    /**
     * 获取0 管理员发放 1 员工申请
     *
     * @return collect_type - 0 管理员发放 1 员工申请
     */
    public Integer getCollectType() {
        return collectType;
    }

    /**
     * 设置0 管理员发放 1 员工申请
     *
     * @param collectType 0 管理员发放 1 员工申请
     */
    public void setCollectType(Integer collectType) {
        this.collectType = collectType;
    }

    /**
     * 获取总公司ID
     *
     * @return org_id - 总公司ID
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置总公司ID
     *
     * @param orgId 总公司ID
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * @return asset_num
     */
    public Integer getAssetNum() {
        return assetNum;
    }

    /**
     * @param assetNum
     */
    public void setAssetNum(Integer assetNum) {
        this.assetNum = assetNum;
    }

    /**
     * 获取领用后公司treecode
     *
     * @return receive_company_treecode - 领用后公司treecode
     */
    public String getReceiveCompanyTreecode() {
        return receiveCompanyTreecode;
    }

    /**
     * 设置领用后公司treecode
     *
     * @param receiveCompanyTreecode 领用后公司treecode
     */
    public void setReceiveCompanyTreecode(String receiveCompanyTreecode) {
        this.receiveCompanyTreecode = receiveCompanyTreecode == null ? null : receiveCompanyTreecode.trim();
    }

    /**
     * 获取领用后部门treecode
     *
     * @return receive_dept_treecode - 领用后部门treecode
     */
    public String getReceiveDeptTreecode() {
        return receiveDeptTreecode;
    }

    /**
     * 设置领用后部门treecode
     *
     * @param receiveDeptTreecode 领用后部门treecode
     */
    public void setReceiveDeptTreecode(String receiveDeptTreecode) {
        this.receiveDeptTreecode = receiveDeptTreecode == null ? null : receiveDeptTreecode.trim();
    }
}