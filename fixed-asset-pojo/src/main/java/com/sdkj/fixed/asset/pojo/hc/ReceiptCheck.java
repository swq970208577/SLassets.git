package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.*;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 盘点单
 *
 * @author 史晨星
 * @date 2020-07-28 : 09:56:06
 */
@Table(name = "pm_check_receipt")
public class ReceiptCheck implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {DelReceiptCheck.class, EditReceiptCheck.class})
    private String id;

    /**
     * 盘点仓库
     */
    @Column(name = "category_id")
    @Size(max = 32, message = "盘点仓库-最大长度:32")
    @NotNull(message = "盘点仓库-不能为空", groups = {AddReceiptCheck.class})
    private String categoryId;

    /**
     * 盘点名称
     */
    @Size(max = 32, message = "盘点名称-最大长度:32")
    @NotNull(message = "盘点名称-不能为空", groups = {AddReceiptCheck.class})
    @Excel(name = "盘点名称", orderNum = "1", width = 20)
    private String name;

    /**
     * 分配用户
     */
    @Size(max = 256, message = "分配用户-最大长度:256")
    @NotNull(message = "分配用户-不能为空", groups = {AddReceiptCheck.class, EditReceiptCheck.class})
    private String executor;

    /**
     * 允许手工盘点(1是2否)
     */
    @Column(name = "is_manual")
    private Integer isManual;

    /**
     * 开始盘点时间
     */
    @Excel(name = "开始盘点时间", orderNum = "4", width = 20)
    private String starttime;

    /**
     * 完成盘点时间
     */
    @Excel(name = "完成盘点时间", orderNum = "5", width = 20)
    private String endtime;

    /**
     * 创建人
     */
    private String cuser;

    /**
     * 创建时间
     */
    private String ctime;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    /**
     * 状态(1可用2禁用)
     */
    private Integer state;

    /**
     * 机构id
     */
    @Column(name = "org_id")
    private String orgId;


    @Transient
    @Excel(name = "是否完成", orderNum = "6", width = 20)
    private String ifFinish;

    public String getIfFinish() {
        return ifFinish;
    }

    public void setIfFinish(String ifFinish) {
        this.ifFinish = ifFinish;
    }


    private Integer finish;

    public Integer getFinish() {
        return finish;
    }

    public void setFinish(Integer finish) {
        this.finish = finish;
    }

    @Transient
    @Excel(name = "盘点仓库", orderNum = "3", width = 20)
    private String categoryName;

    @Transient
    private String categoryCode;

    @Transient
    private String executorName;

    @Transient
    @Excel(name = "盘点负责人", orderNum = "2", width = 20)
    private String cuserName;

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取盘点仓库
     *
     * @return category_id - 盘点仓库
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 设置盘点仓库
     *
     * @param categoryId 盘点仓库
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? null : categoryId.trim();
    }

    /**
     * 获取盘点名称
     *
     * @return name - 盘点名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置盘点名称
     *
     * @param name 盘点名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取分配用户
     *
     * @return executor - 分配用户
     */
    public String getExecutor() {
        return executor;
    }

    /**
     * 设置分配用户
     *
     * @param executor 分配用户
     */
    public void setExecutor(String executor) {
        this.executor = executor == null ? null : executor.trim();
    }

    /**
     * 获取允许手工盘点(1是2否)
     *
     * @return is_manual - 允许手工盘点(1是2否)
     */
    public Integer getIsManual() {
        return isManual;
    }

    /**
     * 设置允许手工盘点(1是2否)
     *
     * @param isManual 允许手工盘点(1是2否)
     */
    public void setIsManual(Integer isManual) {
        this.isManual = isManual;
    }

    /**
     * 获取开始盘点时间
     *
     * @return starttime - 开始盘点时间
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * 设置开始盘点时间
     *
     * @param starttime 开始盘点时间
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime == null ? null : starttime.trim();
    }

    /**
     * 获取完成盘点时间
     *
     * @return endtime - 完成盘点时间
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     * 设置完成盘点时间
     *
     * @param endtime 完成盘点时间
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime == null ? null : endtime.trim();
    }

    /**
     * 获取创建人
     *
     * @return cuser - 创建人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置创建人
     *
     * @param cuser 创建人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取机构id
     *
     * @return org_id - 机构id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置机构id
     *
     * @param orgId 机构id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }
}