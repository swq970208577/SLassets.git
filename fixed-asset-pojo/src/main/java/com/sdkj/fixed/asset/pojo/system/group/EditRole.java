package com.sdkj.fixed.asset.pojo.system.group;

import javax.validation.groups.Default;

/**
 * @ClassName EditRole
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/23 11:09
 */
public interface EditRole extends Default,IdMustRole {
}
