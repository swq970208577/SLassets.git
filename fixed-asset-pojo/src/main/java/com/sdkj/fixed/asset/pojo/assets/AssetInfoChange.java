package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_material_info_change")
public class AssetInfoChange implements Serializable {
    /**
     * 序号
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 业务日期
     */
    private String date;

    /**
     * 变更单号
     */
    private String number;
    /**
     * 处理人名称
     */
    @Column(name = "handle_user_name")
    private String handleUserName;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_date")
    private String updateDate;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 公司id
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 资产分类id
     */
    @Column(name = "asset_class_id")
    private String assetClassId;

    /**
     * 资产型号ID
     */
    @Column(name = "asset_model_id")
    private String assetModelId;

    /**
     * 标准型号
     */
    @Column(name = "standard_model")
    private String standardModel;

    /**
     * 规格型号
     */
    @Column(name = "specification_model")
    private String specificationModel;

    /**
     * 计量单位
     */
    @Column(name = "unit_measurement")
    private String unitMeasurement;

    /**
     * SN号
     */
    @Column(name = "sn_number")
    private String snNumber;

    /**
     * 来源
     */
    private String source;

    /**
     * 购入时间
     */
    @Column(name = "purchase_time")
    private String purchaseTime;

    /**
     * 使用公司
     */
    @Column(name = "use_company")
    private String useCompany;

    /**
     * 使用部门
     */
    @Column(name = "use_department")
    private String useDepartment;

    /**
     * 使用人
     */
    private String user;
    /**
     * 使用人名称
     */
    @Column(name = "user_Name")
    private String userName;

    /**
     * 使用期限(月)
     */
    @Column(name = "service_life")
    private String serviceLife;

    /**
     * 区域
     */
    private String area;

    /**
     * 存放地点
     */
    @Column(name = "storage_location")
    private String storageLocation;

    /**
     * 备注
     */
    private String remark;

    /**
     * 照片
     */
    private String photo;

    /**
     * 资产名称
     */
    @Column(name = "asset_name")
    private String assetName;

    /**
     * 供应商id
     */
    @Column(name = "supplier_id")
    private String supplierId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public String getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取业务日期
     *
     * @return date - 业务日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置业务日期
     *
     * @param date 业务日期
     */
    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    /**
     * 获取变更单号
     *
     * @return number - 变更单号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置变更单号
     *
     * @param number 变更单号
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_date - 修改时间
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_deleted - 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除
     *
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取公司id
     *
     * @return org_id - 公司id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司id
     *
     * @param orgId 公司id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取资产型号ID
     *
     * @return asset_model_id - 资产型号ID
     */
    public String getAssetModelId() {
        return assetModelId;
    }

    /**
     * 设置资产型号ID
     *
     * @param assetModelId 资产型号ID
     */
    public void setAssetModelId(String assetModelId) {
        this.assetModelId = assetModelId == null ? null : assetModelId.trim();
    }

    /**
     * 获取标准型号
     *
     * @return standard_model - 标准型号
     */
    public String getStandardModel() {
        return standardModel;
    }

    /**
     * 设置标准型号
     *
     * @param standardModel 标准型号
     */
    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel == null ? null : standardModel.trim();
    }

    /**
     * 获取规格型号
     *
     * @return specification_model - 规格型号
     */
    public String getSpecificationModel() {
        return specificationModel;
    }

    /**
     * 设置规格型号
     *
     * @param specificationModel 规格型号
     */
    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel == null ? null : specificationModel.trim();
    }

    /**
     * 获取计量单位
     *
     * @return unit_measurement - 计量单位
     */
    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    /**
     * 设置计量单位
     *
     * @param unitMeasurement 计量单位
     */
    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement == null ? null : unitMeasurement.trim();
    }

    /**
     * 获取SN号
     *
     * @return sn_number - SN号
     */
    public String getSnNumber() {
        return snNumber;
    }

    /**
     * 设置SN号
     *
     * @param snNumber SN号
     */
    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber == null ? null : snNumber.trim();
    }

    /**
     * 获取来源
     *
     * @return source - 来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 设置来源
     *
     * @param source 来源
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * 获取购入时间
     *
     * @return purchase_time - 购入时间
     */
    public String getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * 设置购入时间
     *
     * @param purchaseTime 购入时间
     */
    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime == null ? null : purchaseTime.trim();
    }

    /**
     * 获取使用公司
     *
     * @return use_company - 使用公司
     */
    public String getUseCompany() {
        return useCompany;
    }

    /**
     * 设置使用公司
     *
     * @param useCompany 使用公司
     */
    public void setUseCompany(String useCompany) {
        this.useCompany = useCompany == null ? null : useCompany.trim();
    }

    /**
     * 获取使用部门
     *
     * @return use_department - 使用部门
     */
    public String getUseDepartment() {
        return useDepartment;
    }

    /**
     * 设置使用部门
     *
     * @param useDepartment 使用部门
     */
    public void setUseDepartment(String useDepartment) {
        this.useDepartment = useDepartment == null ? null : useDepartment.trim();
    }

    /**
     * 获取使用人
     *
     * @return user - 使用人
     */
    public String getUser() {
        return user;
    }

    /**
     * 设置使用人
     *
     * @param user 使用人
     */
    public void setUser(String user) {
        this.user = user == null ? null : user.trim();
    }

    /**
     * 获取使用期限(月)
     *
     * @return service_life - 使用期限(月)
     */
    public String getServiceLife() {
        return serviceLife;
    }

    /**
     * 设置使用期限(月)
     *
     * @param serviceLife 使用期限(月)
     */
    public void setServiceLife(String serviceLife) {
        this.serviceLife = serviceLife == null ? null : serviceLife.trim();
    }

    /**
     * 获取区域
     *
     * @return area - 区域
     */
    public String getArea() {
        return area;
    }

    /**
     * 设置区域
     *
     * @param area 区域
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    /**
     * 获取存放地点
     *
     * @return storage_location - 存放地点
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * 设置存放地点
     *
     * @param storageLocation 存放地点
     */
    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation == null ? null : storageLocation.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取照片
     *
     * @return photo - 照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 设置照片
     *
     * @param photo 照片
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * 获取资产名称
     *
     * @return asset_name - 资产名称
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * 设置资产名称
     *
     * @param assetName 资产名称
     */
    public void setAssetName(String assetName) {
        this.assetName = assetName == null ? null : assetName.trim();
    }

    /**
     * 获取供应商id
     *
     * @return supplier_id - 供应商id
     */
    public String getSupplierId() {
        return supplierId;
    }

    /**
     * 设置供应商id
     *
     * @param supplierId 供应商id
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId == null ? null : supplierId.trim();
    }

    public String getHandleUserName() {
        return handleUserName;
    }

    public void setHandleUserName(String handleUserName) {
        this.handleUserName = handleUserName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAssetClassId() {
        return assetClassId;
    }

    public void setAssetClassId(String assetClassId) {
        this.assetClassId = assetClassId;
    }
}