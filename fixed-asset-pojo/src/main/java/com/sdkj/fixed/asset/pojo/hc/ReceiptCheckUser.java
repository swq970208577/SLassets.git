package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 盘点单-分配用户 
 * @author 史晨星
 * @date  2020-08-05 : 03:51:27
 */
@Table(name = "pm_check_user")
public class ReceiptCheckUser implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "receipt_id")
    private String receiptId;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * @return receipt_id
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * @param receiptId
     */
    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId == null ? null : receiptId.trim();
    }
}