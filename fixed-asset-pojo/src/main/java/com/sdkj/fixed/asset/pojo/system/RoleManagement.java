package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.IdMustRole;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name = "sys_role_management")
public class RoleManagement implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @NotBlank(message = "id不能为空",groups = {IdMustRole.class})
    @Size(max = 32,message = "id长度不匹配",groups = {IdMustRole.class})
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    /**
     * 1:可用；2：删除
     */
    private Integer state;

    private String reserved;
    @NotBlank(message = "名称不能为空")
    @Size(max = 32,message = "长度不匹配")
    private String name;

    /**
     * 1:超管；2：管理员；3普通用户
     */
    private String level;

    @Size(max = 128,message = "长度不匹配")
    private String comments;

    private String orgId;

    /**
     * 是否默认 1:是；2：否
     */

    @Column(name = "is_default")
    private Integer isDefault;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取1:可用；2：删除
     *
     * @return state - 1:可用；2：删除
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：删除
     *
     * @param state 1:可用；2：删除
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取1:超管；2：管理员；3普通用户
     *
     * @return level - 1:超管；2：管理员；3普通用户
     */
    public String getLevel() {
        return level;
    }

    /**
     * 设置1:超管；2：管理员；3普通用户
     *
     * @param level 1:超管；2：管理员；3普通用户
     */
    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }

    /**
     * @return comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    /**
     * @return org_id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取1:是；2：否
     *
     * @return is_default - 1:是；2：否
     */
    public Integer getIsDefault() {
        return isDefault;
    }

    /**
     * 设置1:是；2：否
     *
     * @param isDefault 1:是；2：否
     */
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }
}