package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_set_class")
public class SetClass implements Serializable {
    /**
     * 序号ID
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 公司ID
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @Size(max = 32, message = "资产名称超长")
    private String name;

    /**
     * 父级ID
     */
    private String pid;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 编号
     */
    @NotBlank(message = "资产分类编号不能为空")
    @Pattern(regexp = "^[0-9A-Za-z]{4}$", message = "资产分类编号只能是四位数字或字母")
    private String num;

    /**
     * 年限(月)
     */
    @NotBlank(message = "年限不能为空")
    @Pattern(regexp = "^[0-9]+$", message = "资产分类年限只能为数字")
    private String years;

    /**
     * 状态（0可用1禁用）
     */
    @Digits(integer = 0, fraction = 1, message = "状态取值有误（0可用1禁用）")
    private Integer state;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除,0未删除1.已删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 备注
     */
    private String comment;

    /**
     * 是否展示(0展示1不展示)
     */
    @Column(name = "if_show")
    private Integer ifShow;

    /**
     * 结构树
     */
    private String treecode;
    /**
     * 员工自助领用借用(0选中1取消)
     */
    @Column(name = "receive_borrow")
    private Integer receiveBorrow;
    /**
     * 员工自助交接(0选中1取消)
     */
    private Integer handover;


    private List<SetClass> list = new ArrayList<>();

    private static final long serialVersionUID = 1L;

    /**
     * 获取序号ID
     *
     * @return id - 序号ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置序号ID
     *
     * @param id 序号ID
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取公司ID
     *
     * @return org_id - 公司ID
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司ID
     *
     * @param orgId 公司ID
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取父级ID
     *
     * @return pid - 父级ID
     */
    public String getPid() {
        return pid;
    }

    /**
     * 设置父级ID
     *
     * @param pid 父级ID
     */
    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    /**
     * 获取层级
     *
     * @return level - 层级
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 设置层级
     *
     * @param level 层级
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * 获取编号
     *
     * @return num - 编号
     */
    public String getNum() {
        return num;
    }

    /**
     * 设置编号
     *
     * @param num 编号
     */
    public void setNum(String num) {
        this.num = num == null ? null : num.trim();
    }

    /**
     * 获取年限(月)
     *
     * @return years - 年限(月)
     */
    public String getYears() {
        return years;
    }

    /**
     * 设置年限(月)
     *
     * @param years 年限(月)
     */
    public void setYears(String years) {
        this.years = years == null ? null : years.trim();
    }

    /**
     * 获取状态（0可用1禁用）
     *
     * @return state - 状态（0可用1禁用）
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态（0可用1禁用）
     *
     * @param state 状态（0可用1禁用）
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除,0未删除1.已删除
     *
     * @return is_deleted - 是否删除,0未删除1.已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除,0未删除1.已删除
     *
     * @param isDeleted 是否删除,0未删除1.已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取备注
     *
     * @return comment - 备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置备注
     *
     * @param comment 备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取是否展示(0展示1不展示)
     *
     * @return if_show - 是否展示(0展示1不展示)
     */
    public Integer getIfShow() {
        return ifShow;
    }

    /**
     * 设置是否展示(0展示1不展示)
     *
     * @param ifShow 是否展示(0展示1不展示)
     */
    public void setIfShow(Integer ifShow) {
        this.ifShow = ifShow;
    }

    /**
     * 获取结构树
     *
     * @return treecode - 结构树
     */
    public String getTreecode() {
        return treecode;
    }

    /**
     * 设置结构树
     *
     * @param treecode 结构树
     */
    public void setTreecode(String treecode) {
        this.treecode = treecode == null ? null : treecode.trim();
    }

    public Integer getReceiveBorrow() {
        return receiveBorrow;
    }

    public void setReceiveBorrow(Integer receiveBorrow) {
        this.receiveBorrow = receiveBorrow;
    }

    public Integer getHandover() {
        return handover;
    }

    public void setHandover(Integer handover) {
        this.handover = handover;
    }

    public List<SetClass> getList() {
        return list;
    }

    public void setList(List<SetClass> list) {
        this.list = list;
    }
}