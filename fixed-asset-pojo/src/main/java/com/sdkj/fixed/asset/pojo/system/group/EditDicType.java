package com.sdkj.fixed.asset.pojo.system.group;

import javax.validation.groups.Default;

/**
 * @ClassName EditDicType
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/22 9:49
 */
public interface EditDicType extends Default,IdMustDicType {
}
