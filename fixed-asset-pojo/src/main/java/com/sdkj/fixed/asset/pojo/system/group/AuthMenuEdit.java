package com.sdkj.fixed.asset.pojo.system.group;

import javax.validation.groups.Default;

/**
 * @ClassName AuthMenuEdit
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/22 13:41
 */
public interface AuthMenuEdit extends Default,IdMustAuthEntity {
}
