package com.sdkj.fixed.asset.pojo.system.group;

import javax.validation.groups.Default;

/**
 * @ClassName UserEdit
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/23 10:07
 */
public interface UserEdit extends Default,IdMustUser {
}
