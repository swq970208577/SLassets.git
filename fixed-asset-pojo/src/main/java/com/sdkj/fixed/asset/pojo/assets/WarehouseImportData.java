package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Author zhangjinfei
 * @Description //TODO 资产入库导入
 * @Date 2020/8/6 11:02
 */
public class WarehouseImportData  implements Serializable, IExcelModel  {
    /**
     * 行号
     */
    @Transient
    private int rowNum;

    /**
     * 错误消息
     */
    @Transient
    private String errorMsg;

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    // ------------------------------------

    public static final String DATE_REGEXP = "\\d{4}-\\d{2}-\\d{2}";

    private String assetId;

    private String assetCode;
    /**
     * 资产名称*
     */
    @Excel(name="资产名称*")
    @NotBlank(message = "[资产名称]不能为空")
    private String assetName;
    /**
     * 资产类别编码*
     */
    @Excel(name="资产类别编码*")
    @NotBlank(message = "[资产类别编码]不能为空")
    private String assetClassNo;
    /**
     * 资产类别ID
     */
    private String assetClassId;
    /**
     * 资产类别treeCode
     */
    private String classTreecode;

    /**
     * 金额
     */
    @Excel(name="金额" )
    @Pattern(regexp = "^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$", message = "[金额]金额输入错误")
    private String amount;

    /**
     * 标准型号
     */
    private String standardModel;
    /**
     *  规格型号
     */
    @Excel(name="规格型号")
    private String specificationModel;

    /**
     * 计量单位
     */
    @Excel(name="计量单位")
    private String unitMeasurement;

    /**
     * 使用公司编码*
     */
    private String useCompany;
    @Excel(name="使用公司编码*")
    @NotBlank(message = "[使用公司编码]不能为空")
    private String useCompanyNo;
    @Excel(name="使用公司名称")
    private String useCompanyName;
    private String useCompanyTreecode;

    /**
     * 使用部门
     */
    private String useDepartment;
    private String useDeptTreecode;
    @Excel(name="使用部门编码")
    private String deptNo;
    @Excel(name="使用部门名称")
    private String deptName;

    /**
     * 使用人
     */

    @Excel(name="员工姓名（使用人）")
    private String handlerUser;
    @Excel(name="员工编号")
    private String userNo;
    private String handlerUserId;

    /**
     * 区域
     */
    private String area;
    @Excel(name = "区域编码*")
    @NotBlank(message = "[区域编码]不能为空")
    private String areaNo;
    private String areaName;

    /**
     * 存放地点
     */
    @Excel(name="存放地点" )
    private String storageLocation;

    /**
     * 管理员
     */
    @Excel(name="管理员姓名*")
    @NotBlank(message = "[管理员姓名]不能为空")
    private String admin;

    private String adminId;

    /**
     * 所属公司ID
     */
    private String company;
    @Excel(name = "所属公司编码*")
    @NotBlank(message = "[所属公司编码]不能为空")
    private String companyNo;
    /**
     * 所属公司treecode
     */
    private String companyTreecode;

    /**
     * 购入时间
     */
    @Excel(name="购入时间*")
    @NotBlank(message = "[购入时间]不能为空")
    @Pattern(regexp = DATE_REGEXP, message = "[购入时间]时间格式错误")
    private String purchaseTime;

    /**
     * 供应商
     */
    private String supplierId;
    @Excel(name="供应商")
    private String supplierName;
    /**
     * 维保负责人
     */

    private String supplierUserId;
    @Excel(name = "维保负责人")
    private String supplierUser;
    /**
     * 维保到期日
     */
    @Excel(name="维保到期日")
    private String  expireTime;
    /**
     * 备注
     */
    @Excel(name="备注")
    private String remark;
    /**
     * SN号
     */
    @Excel(name="SN号")
    private String snNumber;

    /**
     * 使用期限(月)
     */
    @Excel(name="使用期限(月)")
    private String serviceLife;

    /**
     * 来源 来源（下拉框）1"购入",2"自建",3"租赁",4"捐赠",5"其他",6"内部购入"
     */
    @Excel(name="来源",replace = {"购入_1", "自建_2", "租赁_3", "捐赠_4", "其他_5", "内部购入_6"})
    @Pattern(regexp = "[123456]", message = "来源信息错误")
    private String source;

    private String state;
    private String photo;

    private String createTime;
    private String createUser;
    private String updateTime;
    private String updateUser;

    // -----维保信息------
    // 维保说明
    private String explainText;

    private Integer isDelete;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getExplainText() {
        return explainText;
    }

    public void setExplainText(String explainText) {
        this.explainText = explainText;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getStandardModel() {
        return standardModel;
    }

    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel;
    }

    public String getUseCompanyTreecode() {
        return useCompanyTreecode;
    }

    public void setUseCompanyTreecode(String useCompanyTreecode) {
        this.useCompanyTreecode = useCompanyTreecode;
    }

    public String getUseDeptTreecode() {
        return useDeptTreecode;
    }

    public void setUseDeptTreecode(String useDeptTreecode) {
        this.useDeptTreecode = useDeptTreecode;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetClassNo() {
        return assetClassNo;
    }

    public void setAssetClassNo(String assetClassNo) {
        this.assetClassNo = assetClassNo;
    }

    public String getAssetClassId() {
        return assetClassId;
    }

    public void setAssetClassId(String assetClassId) {
        this.assetClassId = assetClassId;
    }

    public String getClassTreecode() {
        return classTreecode;
    }

    public void setClassTreecode(String classTreecode) {
        this.classTreecode = classTreecode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSpecificationModel() {
        return specificationModel;
    }

    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel;
    }

    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }

    public String getUseCompany() {
        return useCompany;
    }

    public void setUseCompany(String useCompany) {
        this.useCompany = useCompany;
    }

    public String getUseCompanyNo() {
        return useCompanyNo;
    }

    public void setUseCompanyNo(String useCompanyNo) {
        this.useCompanyNo = useCompanyNo;
    }

    public String getUseCompanyName() {
        return useCompanyName;
    }

    public void setUseCompanyName(String useCompanyName) {
        this.useCompanyName = useCompanyName;
    }

    public String getUseDepartment() {
        return useDepartment;
    }

    public void setUseDepartment(String useDepartment) {
        this.useDepartment = useDepartment;
    }

    public String getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getHandlerUserId() {
        return handlerUserId;
    }

    public void setHandlerUserId(String handlerUserId) {
        this.handlerUserId = handlerUserId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaNo() {
        return areaNo;
    }

    public void setAreaNo(String areaNo) {
        this.areaNo = areaNo;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public String getCompanyTreecode() {
        return companyTreecode;
    }

    public void setCompanyTreecode(String companyTreecode) {
        this.companyTreecode = companyTreecode;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierUserId() {
        return supplierUserId;
    }

    public void setSupplierUserId(String supplierUserId) {
        this.supplierUserId = supplierUserId;
    }

    public String getSupplierUser() {
        return supplierUser;
    }

    public void setSupplierUser(String supplierUser) {
        this.supplierUser = supplierUser;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSnNumber() {
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    public String getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(String serviceLife) {
        this.serviceLife = serviceLife;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
