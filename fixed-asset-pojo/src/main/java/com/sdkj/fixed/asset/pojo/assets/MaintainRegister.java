package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_maintain_info_register")
public class MaintainRegister implements Serializable {
    /**
     * 序号
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 业务日期
     */
    @NotBlank(message = "业务日期不能为空")
    @Excel(name = "业务日期", needMerge = true, orderNum = "3")
    private String date;
    /**
     * 编号
     */
    @Excel(name = "维修单号", needMerge = true, orderNum = "2")
    private String number;

    /**
     * 处理人
     */
    @Excel(name = "处理人", needMerge = true, orderNum = "4")
    private String operator;

    /**
     * 维修花费,保留两位小数
     */
    @Size(max = 10, message = "维修花费超长")
    @Excel(name = "维修花费", needMerge = true, orderNum = "6")
    private String cost;

    /**
     * 状态，1保修中2维修中3已完成
     */
    @Excel(name = "状态", needMerge = true, orderNum = "1", replace = {"保修中_1","维修中_2","已完成_3"})
    private Integer state;

    /**
     * 报修人
     */
    @Column(name = "repairs_user")
    private String repairsUser;
    /**
     * 报修人名称
     */
    @Column(name = "repairs_user_name")
    @Excel(name = "报修人", needMerge = true, orderNum = "5")
    private String repairsUserName;

    /**
     * 维修内容
     */
    @Column(name = "repairs_content")
    @NotBlank(message = "维修内容不能为空")
    @Size(max = 128, message = "维修内容超长")
    @Excel(name = "维修内容", needMerge = true, orderNum = "7")
    private String repairsContent;

    /**
     * 备注
     */
    @Size(max = 128, message = "说明超长")
    @Excel(name = "备注", needMerge = true, orderNum = "8")
    private String note;

    /**
     * 照片
     */
    @Column(name = "image_url")
    private String imageUrl;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_date")
    private String updateDate;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 公司id
     */
    @Column(name = "org_id")
    private String orgId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public String getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取业务日期
     *
     * @return date - 业务日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置业务日期
     *
     * @param date 业务日期
     */
    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    /**
     * 获取处理人
     *
     * @return operator - 处理人
     */
    public String getOperator() {
        return operator;
    }

    /**
     * 设置处理人
     *
     * @param operator 处理人
     */
    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    /**
     * 获取维修花费,保留两位小数
     *
     * @return cost - 维修花费,保留两位小数
     */
    public String getCost() {
        return cost;
    }

    /**
     * 设置维修花费,保留两位小数
     *
     * @param cost 维修花费,保留两位小数
     */
    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }

    /**
     * 获取状态，1保修中2维修中3已完成
     *
     * @return state - 状态，1保修中2维修中3已完成
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态，1保修中2维修中3已完成
     *
     * @param state 状态，1保修中2维修中3已完成
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取报修人
     *
     * @return repairs_user - 报修人
     */
    public String getRepairsUser() {
        return repairsUser;
    }

    /**
     * 设置报修人
     *
     * @param repairsUser 报修人
     */
    public void setRepairsUser(String repairsUser) {
        this.repairsUser = repairsUser == null ? null : repairsUser.trim();
    }

    /**
     * 获取维修内容
     *
     * @return repairs_content - 维修内容
     */
    public String getRepairsContent() {
        return repairsContent;
    }

    /**
     * 设置维修内容
     *
     * @param repairsContent 维修内容
     */
    public void setRepairsContent(String repairsContent) {
        this.repairsContent = repairsContent == null ? null : repairsContent.trim();
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * 获取照片
     *
     * @return image_url - 照片
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 设置照片
     *
     * @param imageUrl 照片
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_date - 修改时间
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_deleted - 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除
     *
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取公司id
     *
     * @return org_id - 公司id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置公司id
     *
     * @param orgId 公司id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRepairsUserName() {
        return repairsUserName;
    }

    public void setRepairsUserName(String repairsUserName) {
        this.repairsUserName = repairsUserName;
    }
}