package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name="sys_role_menu")
public class RoleAndMenu implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    @Column(name = "auth_id")
    private String authId;

    @Column(name = "role_id")
    private String roleId;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return auth_id
     */
    public String getAuthId() {
        return authId;
    }

    /**
     * @param authId
     */
    public void setAuthId(String authId) {
        this.authId = authId == null ? null : authId.trim();
    }

    /**
     * @return role_id
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
}