package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.*;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name = "sys_user_management")
public class UserManagement implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @NotBlank(message = "id不能为空",groups = {IdMustUser.class})
    @Size(max = 32 ,message = "id最大长度为32",groups = {SetUserAdmin.class, UserEdit.class})
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    /**
     * 1:可用；2：删除;2:禁用
     */
    @NotNull(message = "状态不能为空",groups = {ForbiddenOrEnableUser.class})
    private Integer state;

    private String reserved;
    /**
     * 工号
     */
//    @NotBlank(message = "工号不能为空")
//    @Size(max = 12,message = "工号最大长度为12")
    @Column(name = "employeeId")
    @Excel(name="员工编号",orderNum = "1",width = 20)
    private String employeeid;

    @NotBlank(message = "姓名不能为空")
    @Size(max = 16,message = "姓名最大长度为12")
    @Excel(name="员工姓名",orderNum = "2",width = 20)
    private String name;

    @NotBlank(message = "公司不能为空")
    @Size(max = 32,message = "公司最大长度为32")
    @Column(name = "company_id")
    private String companyId;

    @NotBlank(message = "部门不能为空")
    @Size(max = 32,message = "部门最大长度为32")
    @Column(name = "dept_id")
    private String deptId;

    @Excel(name="手机号码",orderNum = "7",width = 20)
    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1(3|4|5|6|7|8|9)\\d{9}$",message = "请输入正确的手机号码")
    private String tel;

    @Email(message = "邮箱格式不正确")
    @Excel(name="电子邮箱",orderNum = "8",width = 20)
    private String email;

    /**
     * 1:在职；2：离职
     */

    @NotBlank(message = "在职状态不能为空")
    @Size(max = 32,message = "在职状态最大长度为32")
    @Column(name = "is_on_job")
    private String isOnJob;

    @Size(max = 32,message = "昵称最大长度为32")
    private String nickname;

    @NotBlank(message = "密码不能为空",groups = {addUser.class})
    @Size(max = 32,message = "密码最大长度为32",groups = {addUser.class})
    private String password;

    /**
     * 1：系统用户；2：员工
     */
    @NotBlank(message = "员工类型不能为空",groups = {SetUserAdmin.class})
    @Pattern(regexp = "^[1|2]$",message = "只能为1或2",groups = {SetUserAdmin.class})
    private String type;

    /**
     * 1：是；2：否
     */
    @Column(name = "data_is_all")
    private Integer dataIsAll;
    /**
     * 总公司
     */
    @Column(name = "top_company_id")
    private String topCompanyId;
    /**
     * 公司名称
     */
    @Excel(name="所属公司",orderNum = "4",width = 20)
    @Transient
    private String companyName;
    /**
     * 公司名称
     */
    @Excel(name="所属公司编码",orderNum = "3",width = 15)
    @Transient
    private String companyCode;
    /**
     * 部门名称
     */
    @Excel(name="所属部门",orderNum = "6",width = 20)
    @Transient
    private String deptName;
    /**
     * 部门编码
     */
    @Excel(name="所属部门编码",orderNum = "5",width = 15)
    @Transient
    private String deptCode;
    /**
     * 在职状态翻译字段
     */
    @Excel(name="在职状态",orderNum = "9")
    @Transient
    private String isOnJobName;

    @Transient
    private String companyTreeCode;
    @Transient
    private String deptTreeCode;
    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取1:可用；2：删除;2:禁用
     *
     * @return state - 1:可用；2：删除;2:禁用
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：删除;2:禁用
     *
     * @param state 1:可用；2：删除;2:禁用
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * @return employeeId
     */
    public String getEmployeeid() {
        return employeeid;
    }

    /**
     * @param employeeid
     */
    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid == null ? null : employeeid.trim();
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return company_id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    /**
     * @return dept_id
     */
    public String getDeptId() {
        return deptId;
    }

    /**
     * @param deptId
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId == null ? null : deptId.trim();
    }

    /**
     * @return tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel
     */
    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    /**
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * 获取1:在职；2：离职
     *
     * @return is_on_job - 1:在职；2：离职
     */
    public String getIsOnJob() {
        return isOnJob;
    }

    /**
     * 设置1:在职；2：离职
     *
     * @param isOnJob 1:在职；2：离职
     */
    public void setIsOnJob(String isOnJob) {
        this.isOnJob = isOnJob == null ? null : isOnJob.trim();
    }

    /**
     * @return nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取1：系统用户；2：员工
     *
     * @return type - 1：系统用户；2：员工
     */
    public String getType() {
        return type;
    }

    /**
     * 设置1：系统用户；2：员工
     *
     * @param type 1：系统用户；2：员工
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取1：是；2：否
     *
     * @return data_is_all - 1：是；2：否
     */
    public Integer getDataIsAll() {
        return dataIsAll;
    }

    /**
     * 设置1：是；2：否
     *
     * @param dataIsAll 1：是；2：否
     */
    public void setDataIsAll(Integer dataIsAll) {
        this.dataIsAll = dataIsAll;
    }



    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getIsOnJobName() {
        return isOnJobName;
    }

    public void setIsOnJobName(String isOnJobName) {
        this.isOnJobName = isOnJobName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getTopCompanyId() {
        return topCompanyId;
    }

    public void setTopCompanyId(String topCompanyId) {
        this.topCompanyId = topCompanyId;
    }

    public String getCompanyTreeCode() {
        return companyTreeCode;
    }

    public void setCompanyTreeCode(String companyTreeCode) {
        this.companyTreeCode = companyTreeCode;
    }

    public String getDeptTreeCode() {
        return deptTreeCode;
    }

    public void setDeptTreeCode(String deptTreeCode) {
        this.deptTreeCode = deptTreeCode;
    }
}