package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.DelCheckProduct;
import com.sdkj.fixed.asset.pojo.hc.group.EditCheckProduct;
import com.sdkj.fixed.asset.pojo.hc.group.Surplus;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 盘点单-物品关联
 *
 * @author 史晨星
 * @date 2020-07-28 : 10:00:03
 */
@Table(name = "pm_product_check_receipt_detail")
public class ReceiptCheckProduct implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Column(name = "id")
    @NotEmpty(message = "id-不能为空", groups = {DelCheckProduct.class})
    private String id;

    /**
     * 单号id
     */
    @Column(name = "receipt_id")
    private String receiptId;

    /**
     * 耗材id
     */
    @Column(name = "product_id")
    private String productId;

    /**
     * 实盘数量
     */
    @Column(name = "real_num")
    @NotNull(message = "实盘数量-不能为空", groups = {EditCheckProduct.class})
    private Integer realNum;

    /**
     * 实盘金额
     */
    @Column(name = "real_total_price")
    @Size(max = 32, message = "实盘金额-最大长度:32")
    @NotNull(message = "实盘金额-不能为空", groups = {EditCheckProduct.class})
    private String realTotalPrice;

    /**
     * 盘点备注
     */
    @Size(max = 128, message = "盘点备注-最大长度:128")
    private String comment;

    /**
     * 盘点时间
     */
    @Column(name = "check_time")
    private String checkTime;

    /**
     * 盘点人
     */
    @Column(name = "check_user")
    private String checkUser;

    /**
     * 状态(1未盘2已盘3盘盈4盘亏)
     */
    private Integer state;

    private Integer num;

    private String price;

    private String totalPrice;

    private Integer surplus;

    public Integer getSurplus() {
        return surplus;
    }

    public void setSurplus(Integer surplus) {
        this.surplus = surplus;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取单号id
     *
     * @return receipt_id - 单号id
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * 设置单号id
     *
     * @param receiptId 单号id
     */
    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId == null ? null : receiptId.trim();
    }

    /**
     * 获取耗材id
     *
     * @return product_id - 耗材id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 设置耗材id
     *
     * @param productId 耗材id
     */
    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    /**
     * 获取实盘数量
     *
     * @return real_num - 实盘数量
     */
    public Integer getRealNum() {
        return realNum;
    }

    /**
     * 设置实盘数量
     *
     * @param realNum 实盘数量
     */
    public void setRealNum(Integer realNum) {
        this.realNum = realNum;
    }

    public String getRealTotalPrice() {
        return realTotalPrice;
    }

    public void setRealTotalPrice(String realTotalPrice) {
        this.realTotalPrice = realTotalPrice;
    }

    /**
     * 获取盘点备注
     *
     * @return comment - 盘点备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置盘点备注
     *
     * @param comment 盘点备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取盘点时间
     *
     * @return check_time - 盘点时间
     */
    public String getCheckTime() {
        return checkTime;
    }

    /**
     * 设置盘点时间
     *
     * @param checkTime 盘点时间
     */
    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime == null ? null : checkTime.trim();
    }

    /**
     * 获取盘点人
     *
     * @return check_user - 盘点人
     */
    public String getCheckUser() {
        return checkUser;
    }

    /**
     * 设置盘点人
     *
     * @param checkUser 盘点人
     */
    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser == null ? null : checkUser.trim();
    }

    /**
     * 获取状态(1未盘2已盘3盘盈4盘亏)
     *
     * @return state - 状态(1未盘2已盘3盘盈4盘亏)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1未盘2已盘3盘盈4盘亏)
     *
     * @param state 状态(1未盘2已盘3盘盈4盘亏)
     */
    public void setState(Integer state) {
        this.state = state;
    }
}