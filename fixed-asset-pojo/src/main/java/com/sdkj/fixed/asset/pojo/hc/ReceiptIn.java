package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptIn;
import com.sdkj.fixed.asset.pojo.hc.group.DelReceiptIn;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 入库单
 *
 * @author 史晨星
 * @date 2020-07-21 : 01:42:19
 */
@Table(name = "pm_in_receipt")
public class ReceiptIn implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {DelReceiptIn.class})
    private String id;

    /**
     * 入库单号
     */
    private String number;

    /**
     * 入库仓库
     */
    @Size(max = 32, message = "入库仓库id-最大长度:32")
    @NotNull(message = "入库仓库id-不能为空", groups = {AddReceiptIn.class})
    @Column(name = "category_id")
    private String categoryId;

    /**
     * 业务日期
     */
    @Size(max = 20, message = "业务日期-最大长度:20")
    @NotNull(message = "业务日期-不能为空", groups = {AddReceiptIn.class})
    @Column(name = "bussiness_date")
    private String bussinessDate;

    /**
     * 供应商
     */
    private String provider;

    /**
     * 经办人
     */
    @Size(max = 32, message = "经办人-最大长度:32")
    @NotNull(message = "经办人-不能为空", groups = {AddReceiptIn.class})
    private String cuser;

    /**
     * 经办时间
     */
    @Size(max = 20, message = "经办时间-最大长度:20")
    @NotNull(message = "经办时间-不能为空", groups = {AddReceiptIn.class})
    private String ctime;

    /**
     * 入库备注
     */
    private String comment;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    /**
     * 状态(1可用2禁用)
     */
    private Integer state;

    /**
     * 类型（1入库2盘盈）
     */
    @NotNull(message = "类型-不能为空", groups = {AddReceiptIn.class})
    private Integer type;

    /**
     * 机构id
     */
    @Column(name = "org_id")
    private String orgId;
    /**
     * 入库单-物品关联明细
     */
    @Transient
    @Valid
    private List<ReceiptInProduct> receiptInProduct;
    /**
     * 仓库名称
     */
    @Transient
    private String categoryName;

    @Transient
    private String cuserName;

    @Transient
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<ReceiptInProduct> getReceiptInProduct() {
        return receiptInProduct;
    }

    public void setReceiptInProduct(List<ReceiptInProduct> receiptInProduct) {
        this.receiptInProduct = receiptInProduct;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取入库单号
     *
     * @return number - 入库单号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置入库单号
     *
     * @param number 入库单号
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取入库仓库
     *
     * @return category_id - 入库仓库
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 设置入库仓库
     *
     * @param categoryId 入库仓库
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? null : categoryId.trim();
    }

    /**
     * 获取业务日期
     *
     * @return bussiness_date - 业务日期
     */
    public String getBussinessDate() {
        return bussinessDate;
    }

    /**
     * 设置业务日期
     *
     * @param bussinessDate 业务日期
     */
    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate == null ? null : bussinessDate.trim();
    }

    /**
     * 获取供应商
     *
     * @return provider - 供应商
     */
    public String getProvider() {
        return provider;
    }

    /**
     * 设置供应商
     *
     * @param provider 供应商
     */
    public void setProvider(String provider) {
        this.provider = provider == null ? null : provider.trim();
    }

    /**
     * 获取经办人
     *
     * @return cuser - 经办人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置经办人
     *
     * @param cuser 经办人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取经办时间
     *
     * @return ctime - 经办时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置经办时间
     *
     * @param ctime 经办时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取入库备注
     *
     * @return comment - 入库备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置入库备注
     *
     * @param comment 入库备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取类型（1入库2盘盈）
     *
     * @return type - 类型（1入库2盘盈）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置类型（1入库2盘盈）
     *
     * @param type 类型（1入库2盘盈）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取机构id
     *
     * @return org_id - 机构id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置机构id
     *
     * @param orgId 机构id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }
}