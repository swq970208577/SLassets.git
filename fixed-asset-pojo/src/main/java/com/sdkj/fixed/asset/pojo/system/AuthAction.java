package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.AuthMenuEdit;
import com.sdkj.fixed.asset.pojo.system.group.IdMustAuthEntity;
import tk.mybatis.mapper.annotation.KeySql;

/**
 *
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name = "sys_auth_action")
public class AuthAction implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @NotBlank(message = "id不能为空",groups = {IdMustAuthEntity.class})
    @Size(max = 32,message = "id长度不匹配" ,groups = {AuthMenuEdit.class})
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    /**
     * 1:可用；2：删除
     */
    private Integer state;

    @Size(max = 32,message = "父id长度不匹配")
    private String pid;

    @NotBlank(message = "名称不能为空")
    @Size(max = 32,message = "名称长度不匹配")
    private String name;

    @NotBlank(message = "资源路径不能为空")
    @Size(max = 32,message = "资源路径长度不匹配")
    @Column(name = "source_key")
    private String sourceKey;

    @NotBlank(message = "菜单等级不能为空")
    @Size(max = 2,message = "菜单等级长度不匹配")
    private String level;

    @NotBlank(message = "排序不能为空")
    @Size(max = 4,message = "排序长度不匹配")
    @Pattern(regexp = "^\\d{1,4}$",message = "排序只能为数字")
    private String sort;

    @Size(max = 255,message = "图标长度不匹配")
    private String icon;

    @NotBlank(message = "请求地址不能为空")
    @Size(max = 273,message = "请求地址长度不匹配")
    private String url;

    @NotBlank(message = "是否是最后一级菜单不能为空")
    @Size(max = 1,message = "是否是最后一级菜单长度不匹配")
    @Pattern(regexp = "^\\d{1}$",message = "是否是最后一级菜单只能为数字")
    @Column(name = "is_end_menu")
    private String isEndMenu;

    private String reserved;

    @Size(max = 128,message = "备注长度不匹配")
    private String comments;

    /**
     * 1：是；2：否
     */
    @NotNull(message = "是否角色初始化使用不能为空")
    @Column(name = "is_init_use")
    private Integer isInitUse;

    /**
     * 父级结构
     */
    @Size(max = 273,message = "父级长度不匹配")
    private String treecode;

    /**
     * 1:管理管；2：员工端；3：共用
     */
    @NotBlank(message = "菜单类型不能为空")
    @Size(max = 1,message = "菜单类型长度不匹配")
    @Pattern(regexp = "^\\d{1,2}$",message = "菜单类型只能为数字")
    private String type;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取1:可用；2：删除
     *
     * @return state - 1:可用；2：删除
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：删除
     *
     * @param state 1:可用；2：删除
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * @param pid
     */
    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return source_key
     */
    public String getSourceKey() {
        return sourceKey;
    }

    /**
     * @param sourceKey
     */
    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey == null ? null : sourceKey.trim();
    }

    /**
     * @return level
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param level
     */
    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }

    /**
     * @return sort
     */
    public String getSort() {
        return sort;
    }

    /**
     * @param sort
     */
    public void setSort(String sort) {
        this.sort = sort == null ? null : sort.trim();
    }

    /**
     * @return icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon
     */
    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * @return is_end_menu
     */
    public String getIsEndMenu() {
        return isEndMenu;
    }

    /**
     * @param isEndMenu
     */
    public void setIsEndMenu(String isEndMenu) {
        this.isEndMenu = isEndMenu == null ? null : isEndMenu.trim();
    }

    /**
     * @return reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * @return comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    /**
     * 获取1：是；2：否
     *
     * @return is_init_use - 1：是；2：否
     */
    public Integer getIsInitUse() {
        return isInitUse;
    }

    /**
     * 设置1：是；2：否
     *
     * @param isInitUse 1：是；2：否
     */
    public void setIsInitUse(Integer isInitUse) {
        this.isInitUse = isInitUse;
    }

    /**
     * 获取父级结构
     *
     * @return treecode - 父级结构
     */
    public String getTreecode() {
        return treecode;
    }

    /**
     * 设置父级结构
     *
     * @param treecode 父级结构
     */
    public void setTreecode(String treecode) {
        this.treecode = treecode == null ? null : treecode.trim();
    }

    /**
     * 获取1:管理管；2：员工端；3：共用
     *
     * @return type - 1:管理管；2：员工端；3：共用
     */
    public String getType() {
        return type;
    }

    /**
     * 设置1:管理管；2：员工端；3：共用
     *
     * @param type 1:管理管；2：员工端；3：共用
     */
    public void setType(String type) {
        this.type = type;
    }
}