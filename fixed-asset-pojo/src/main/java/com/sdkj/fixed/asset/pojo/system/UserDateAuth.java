package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name= "sys_user_data_auth")
public class UserDateAuth implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 1:公司部门；2：资产类别；3：区域授权；4：仓库授权
     */
    private Integer type;

    @Column(name = "auth_id")
    private String authId;

    @Column(name = "user_id")
    private String userId;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取1:公司部门；2：资产类别；3：区域授权；4：仓库授权
     *
     * @return type - 1:公司部门；2：资产类别；3：区域授权；4：仓库授权
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置1:公司部门；2：资产类别；3：区域授权；4：仓库授权
     *
     * @param type 1:公司部门；2：资产类别；3：区域授权；4：仓库授权
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return auth_id
     */
    public String getAuthId() {
        return authId;
    }

    /**
     * @param authId
     */
    public void setAuthId(String authId) {
        this.authId = authId == null ? null : authId.trim();
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
}