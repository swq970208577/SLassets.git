package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddCategory;
import com.sdkj.fixed.asset.pojo.hc.group.DelCategory;
import com.sdkj.fixed.asset.pojo.hc.group.EditCategory;
import com.sdkj.fixed.asset.pojo.hc.group.ViewCategory;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录
 *
 * @author 史晨星
 * @date 2020-07-20 : 01:45:18
 */
@Table(name = "pm_category_management")
public class Category implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {EditCategory.class, ViewCategory.class, DelCategory.class})
    private String id;

    /**
     * 编码
     */
    @NotNull(message = "编码-不能为空", groups = {AddCategory.class})
    @Size(max = 32, message = "编码-最大长度:32")
    @Excel(name = "仓库编码", orderNum = "1", width = 20)
    private String code;

    /**
     * 名称
     */
    @NotNull(message = "名称-不能为空", groups = {AddCategory.class})
    @Size(max = 32, message = "名称-最大长度:32")
    @Excel(name = "仓库名称", orderNum = "1", width = 20)
    private String name;

    /**
     * 所属公司
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 状态(1可用2禁用)
     */
    @NotNull(message = "状态-不能为空", groups = {DelCategory.class})
    private Integer state;

    /**
     * 创建人
     */
    private String cuser;

    /**
     * 创建时间
     */
    private String ctime;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    private Integer isCheck;

    public Integer getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    @Transient
    @Excel(name = "所属公司", orderNum = "1", width = 20)
    private String orgName;

    @Transient
    private String orgTreeCode;

    @Transient
    @Excel(name = "状态", orderNum = "1", width = 20)
    private String state1;

    @Transient
    private String orgId1;

    public String getOrgId1() {
        return orgId1;
    }

    public void setOrgId1(String orgId1) {
        this.orgId1 = orgId1;
    }

    public String getOrgTreeCode() {
        return orgTreeCode;
    }

    public void setOrgTreeCode(String orgTreeCode) {
        this.orgTreeCode = orgTreeCode;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取编码
     *
     * @return code - 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置编码
     *
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取所属公司
     *
     * @return org_id - 所属公司
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置所属公司
     *
     * @param orgId 所属公司
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取创建人
     *
     * @return cuser - 创建人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置创建人
     *
     * @param cuser 创建人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }
}