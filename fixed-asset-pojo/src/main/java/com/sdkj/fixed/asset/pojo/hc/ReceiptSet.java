package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptSet;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptSet;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 调整单
 *
 * @author 史晨星
 * @date 2020-07-23 : 09:26:06
 */
@Table(name = "pm_set_receipt")
public class ReceiptSet implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {EditReceiptSet.class})
    private String id;

    /**
     * 单号
     */
    private String number;

    /**
     * 调整仓库
     */
    @Column(name = "category_id")
    @Size(max = 32, message = "调整仓库id-最大长度:32")
    @NotNull(message = "调整仓库id-不能为空", groups = {EditReceiptSet.class, AddReceiptSet.class})
    private String categoryId;

    /**
     * 业务日期
     */
    @Column(name = "bussiness_date")
    @Size(max = 32, message = "业务日期-最大长度:32")
    @NotNull(message = "业务日期-不能为空", groups = {EditReceiptSet.class, AddReceiptSet.class})
    private String bussinessDate;

    /**
     * 创建人
     */
    private String cuser;

    /**
     * 创建时间
     */
    private String ctime;

    /**
     * 调整说明
     */
    private String comment;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    /**
     * 状态(1可用2禁用)
     */
    private Integer state;

    /**
     * 机构id
     */
    @Column(name = "org_id")
    private String orgId;

    @Transient
    @Valid
    private List<ReceiptSetProduct> receiptSetProductList;

    @Transient
    private String categoryName;

    @Transient
    private String cuserName;

    @Transient
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<ReceiptSetProduct> getReceiptSetProductList() {
        return receiptSetProductList;
    }

    public void setReceiptSetProductList(List<ReceiptSetProduct> receiptSetProductList) {
        this.receiptSetProductList = receiptSetProductList;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取单号
     *
     * @return number - 单号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置单号
     *
     * @param number 单号
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取调整仓库
     *
     * @return category_id - 调整仓库
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 设置调整仓库
     *
     * @param categoryId 调整仓库
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? null : categoryId.trim();
    }

    /**
     * 获取业务日期
     *
     * @return bussiness_date - 业务日期
     */
    public String getBussinessDate() {
        return bussinessDate;
    }

    /**
     * 设置业务日期
     *
     * @param bussinessDate 业务日期
     */
    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate == null ? null : bussinessDate.trim();
    }

    /**
     * 获取创建人
     *
     * @return cuser - 创建人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置创建人
     *
     * @param cuser 创建人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取调整说明
     *
     * @return comment - 调整说明
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置调整说明
     *
     * @param comment 调整说明
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取机构id
     *
     * @return org_id - 机构id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置机构id
     *
     * @param orgId 机构id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }
}