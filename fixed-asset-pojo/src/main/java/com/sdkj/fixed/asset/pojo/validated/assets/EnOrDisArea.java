package com.sdkj.fixed.asset.pojo.validated.assets;

import javax.validation.groups.Default;

/**
 * @author niuliwei
 * @description
 * @date 2020/7/22 13:38
 */
public interface EnOrDisArea extends Default {
}
