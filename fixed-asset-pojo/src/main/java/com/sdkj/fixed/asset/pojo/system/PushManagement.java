package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 推送实体
 * @author 张欣
 * @date  2020-07-29 : 05:05:18
 */
@Table(name = "pm_app_push")
public class PushManagement implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 推送对象
     */
    private String sender;

    /**
     * 内容
     */
    private String content;

    /**
     * 推送时间
     */
    private String ctime;

    /**
     * 创建人
     */
    private String cuser;

    /**
     * 推送结果（0成功1失败）
     */
    private Integer result;
    @Transient
    private String resultName;
    /**
     * 所属顶级公司
     */
    @Column(name = "company_id")
    private String companyId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取推送对象
     *
     * @return sender - 推送对象
     */
    public String getSender() {
        return sender;
    }

    /**
     * 设置推送对象
     *
     * @param sender 推送对象
     */
    public void setSender(String sender) {
        this.sender = sender == null ? null : sender.trim();
    }

    /**
     * 获取内容
     *
     * @return content - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取推送时间
     *
     * @return ctime - 推送时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置推送时间
     *
     * @param ctime 推送时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取创建人
     *
     * @return cuser - 创建人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置创建人
     *
     * @param cuser 创建人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取推送结果（0成功1失败）
     *
     * @return result - 推送结果（0成功1失败）
     */
    public Integer getResult() {
        return result;
    }

    /**
     * 设置推送结果（0成功1失败）
     *
     * @param result 推送结果（0成功1失败）
     */
    public void setResult(Integer result) {
        this.result = result;
    }

    /**
     * 获取所属顶级公司
     *
     * @return company_id - 所属顶级公司
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 设置所属顶级公司
     *
     * @param companyId 所属顶级公司
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }
}