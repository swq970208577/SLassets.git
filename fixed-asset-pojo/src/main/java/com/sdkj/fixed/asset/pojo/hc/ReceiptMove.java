package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptMove;
import com.sdkj.fixed.asset.pojo.hc.group.ConfirmReceiptMove;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptMove;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 调拨单
 *
 * @author 史晨星
 * @date 2020-07-23 : 09:38:23
 */
@Table(name = "pm_move_receipt")
public class ReceiptMove implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {EditReceiptMove.class, ConfirmReceiptMove.class})
    private String id;

    /**
     * 入库单号
     */
    private String number;

    /**
     * 出库仓库
     */
    @Size(max = 32, message = "出库仓库-最大长度:32")
    @NotNull(message = "出库仓库-不能为空", groups = {EditReceiptMove.class, AddReceiptMove.class})
    @Column(name = "out_category")
    private String outCategory;

    /**
     * 业务日期
     */
    @Size(max = 20, message = "业务日期-最大长度:20")
    @NotNull(message = "业务日期-不能为空", groups = {EditReceiptMove.class, AddReceiptMove.class})
    @Column(name = "bussiness_date")
    private String bussinessDate;

    /**
     * 入库仓库
     */
    @Size(max = 32, message = "入库仓库-最大长度:32")
    @NotNull(message = "入库仓库-不能为空", groups = {EditReceiptMove.class, AddReceiptMove.class})
    @Column(name = "in_category")
    private String inCategory;

    /**
     * 经办人
     */
    @Size(max = 32, message = "经办人-最大长度:32")
    @NotNull(message = "经办人-不能为空", groups = {EditReceiptMove.class, AddReceiptMove.class})
    private String cuser;

    /**
     * 经办时间
     */
    @Size(max = 20, message = "经办时间-最大长度:20")
    @NotNull(message = "经办时间-不能为空", groups = {EditReceiptMove.class, AddReceiptMove.class})
    private String ctime;

    /**
     * 备注
     */
    private String comment;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    /**
     * 状态(1可用2禁用)
     */
    private Integer state;

    /**
     * 机构id
     */
    @Column(name = "org_id")
    private String orgId;
    /**
     * 是否确认
     */
    @NotNull(message = "确认不能为空", groups = {ConfirmReceiptMove.class})
    private Integer confirm;
    /**
     * 确认人
     */
    private String confirmUser;
    /**
     * 确认时间
     */
    private String confirmTime;
    /**
     * 确认备注
     */
    private String confirmComment;
    /**
     * 确认经办日期
     */
    private String confirmBussinessDate;

    @Transient
    @Valid
    private List<ReceiptMoveProduct> receiptMoveProductList;

    @Transient
    private String outCategoryName;

    @Transient
    private String inCategoryName;

    @Transient
    private String confirmUserName;

    @Transient
    private String cuserName;

    @Transient
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getConfirmUserName() {
        return confirmUserName;
    }

    public void setConfirmUserName(String confirmUserName) {
        this.confirmUserName = confirmUserName;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getConfirmComment() {
        return confirmComment;
    }

    public void setConfirmComment(String confirmComment) {
        this.confirmComment = confirmComment;
    }

    public String getConfirmBussinessDate() {
        return confirmBussinessDate;
    }

    public void setConfirmBussinessDate(String confirmBussinessDate) {
        this.confirmBussinessDate = confirmBussinessDate;
    }

    public String getConfirmUser() {
        return confirmUser;
    }

    public void setConfirmUser(String confirmUser) {
        this.confirmUser = confirmUser;
    }

    public String getOutCategoryName() {
        return outCategoryName;
    }

    public void setOutCategoryName(String outCategoryName) {
        this.outCategoryName = outCategoryName;
    }

    public String getInCategoryName() {
        return inCategoryName;
    }

    public void setInCategoryName(String inCategoryName) {
        this.inCategoryName = inCategoryName;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public List<ReceiptMoveProduct> getReceiptMoveProductList() {
        return receiptMoveProductList;
    }

    public void setReceiptMoveProductList(List<ReceiptMoveProduct> receiptMoveProductList) {
        this.receiptMoveProductList = receiptMoveProductList;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取入库单号
     *
     * @return number - 入库单号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置入库单号
     *
     * @param number 入库单号
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取入库仓库
     *
     * @return out_category - 入库仓库
     */
    public String getOutCategory() {
        return outCategory;
    }

    /**
     * 设置入库仓库
     *
     * @param outCategory 入库仓库
     */
    public void setOutCategory(String outCategory) {
        this.outCategory = outCategory == null ? null : outCategory.trim();
    }

    /**
     * 获取业务日期
     *
     * @return bussiness_date - 业务日期
     */
    public String getBussinessDate() {
        return bussinessDate;
    }

    /**
     * 设置业务日期
     *
     * @param bussinessDate 业务日期
     */
    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate == null ? null : bussinessDate.trim();
    }

    /**
     * 获取领用人
     *
     * @return in_category - 领用人
     */
    public String getInCategory() {
        return inCategory;
    }

    /**
     * 设置领用人
     *
     * @param inCategory 领用人
     */
    public void setInCategory(String inCategory) {
        this.inCategory = inCategory == null ? null : inCategory.trim();
    }

    /**
     * 获取经办人
     *
     * @return cuser - 经办人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置经办人
     *
     * @param cuser 经办人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取经办时间
     *
     * @return ctime - 经办时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置经办时间
     *
     * @param ctime 经办时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取备注
     *
     * @return comment - 备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置备注
     *
     * @param comment 备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取机构id
     *
     * @return org_id - 机构id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置机构id
     *
     * @param orgId 机构id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }
}