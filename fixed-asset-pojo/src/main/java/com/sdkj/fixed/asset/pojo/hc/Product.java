package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.*;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录
 *
 * @author 史晨星
 * @date 2020-07-21 : 10:25:25
 */
@Table(name = "pm_product_management")
public class Product implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotEmpty(message = "id-不能为空", groups = {EditProduct.class, ViewProduct.class, DelProduct.class})
    private String id;

    /**
     * 耗材类型
     */
    @Size(max = 32, message = "耗材类型-最大长度:32")
    @NotEmpty(message = "耗材类型-不能为空", groups = {AddProduct.class, EditProduct.class, Surplus.class})
    private String type;

    /**
     * 编码
     */
    @Size(max = 32, message = "编码-最大长度:32")
    @NotNull(message = "编码-不能为空", groups = {AddProduct.class, EditProduct.class, Surplus.class})
    @Excel(name = "物品编码", orderNum = "3", width = 20)
    private String code;

    /**
     * 名称
     */
    @Size(max = 16, message = "名称-最大长度:16")
    @NotNull(message = "名称-不能为空", groups = {AddProduct.class, EditProduct.class, Surplus.class})
    @Excel(name = "物品名称", orderNum = "4", width = 20)
    private String name;

    /**
     * 商品条码
     */
    @Column(name = "bar_code")
    @Size(max = 32, message = "商品条码-最大长度:32")
    @NotNull(message = "商品条码-不能为空", groups = {AddProduct.class, EditProduct.class, Surplus.class})
    @Excel(name = "商品条码", orderNum = "5", width = 20)
    private String barCode;

    /**
     * 品牌商标
     */
    private String brand;

    /**
     * 规格型号
     */
    @Excel(name = "规格型号", orderNum = "7", width = 20)
    private String model;

    /**
     * 默认单价
     */
    private String price;

    /**
     * 单位
     */
    @Excel(name = "单位", orderNum = "8", width = 20)
    private String unit;

    /**
     * 安全库存最低
     */
    @Column(name = "safe_min")
    @Excel(name = "安全库存最低", orderNum = "9", width = 20)
    private Integer safeMin;

    /**
     * 安全库存最高
     */
    @Column(name = "safe_max")
    @Excel(name = "安全库存最高", orderNum = "10", width = 20)
    private Integer safeMax;

    /**
     * 状态(1可用2禁用)
     */
    @NotNull(message = "状态-不能为空", groups = {DelProduct.class})
    private Integer state;

    /**
     * 备注
     */
    private String comment;

    /**
     * 图片
     */
    private String img;

    /**
     * 创建人
     */
    private String cuser;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", orderNum = "11", width = 20)
    private String ctime;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    private Integer temporary;

    private String orgId;

    @Transient
    private String categoryId;

    @Transient
    @Excel(name = "类型名称", orderNum = "2", width = 20)
    private String typeName;

    @Transient
    @Excel(name = "类型编码", orderNum = "1", width = 20)
    private String typeCode;

    @Transient
    private Integer totalNum;

    @Transient
    @Excel(name = "状态", orderNum = "6", width = 20)
    private String state1;

    @Transient
    @Excel(name = "创建人", orderNum = "12", width = 20)
    private String cuserName;

    @Transient
    private String typeTreeCode;

    public String getTypeTreeCode() {
        return typeTreeCode;
    }

    public void setTypeTreeCode(String typeTreeCode) {
        this.typeTreeCode = typeTreeCode;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1 == null ? "" : state1;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode == null ? "" : typeCode;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum == null ? 0 : totalNum;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? "" : typeName;
    }

    public Integer getTemporary() {
        return temporary;
    }

    public void setTemporary(Integer temporary) {
        this.temporary = temporary;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? "" : id.trim();
    }

    /**
     * 获取耗材类型
     *
     * @return type - 耗材类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置耗材类型
     *
     * @param type 耗材类型
     */
    public void setType(String type) {
        this.type = type == null ? "" : type.trim();
    }

    /**
     * 获取编码
     *
     * @return code - 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置编码
     *
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code == null ? "" : code.trim();
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? "" : name.trim();
    }

    /**
     * 获取商品条码
     *
     * @return bar_code - 商品条码
     */
    public String getBarCode() {
        return barCode;
    }

    /**
     * 设置商品条码
     *
     * @param barCode 商品条码
     */
    public void setBarCode(String barCode) {
        this.barCode = barCode == null ? "" : barCode.trim();
    }

    /**
     * 获取品牌商标
     *
     * @return brand - 品牌商标
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置品牌商标
     *
     * @param brand 品牌商标
     */
    public void setBrand(String brand) {
        this.brand = brand == null ? "" : brand.trim();
    }

    /**
     * 获取规格型号
     *
     * @return model - 规格型号
     */
    public String getModel() {
        return model;
    }

    /**
     * 设置规格型号
     *
     * @param model 规格型号
     */
    public void setModel(String model) {
        this.model = model == null ? "" : model.trim();
    }

    /**
     * 获取默认单价
     *
     * @return price - 默认单价
     */
    public String getPrice() {
        return price;
    }

    /**
     * 设置默认单价
     *
     * @param price 默认单价
     */
    public void setPrice(String price) {
        this.price = price == null ? "" : price.trim();
    }

    /**
     * 获取单位
     *
     * @return unit - 单位
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 设置单位
     *
     * @param unit 单位
     */
    public void setUnit(String unit) {
        this.unit = unit == null ? "" : unit.trim();
    }

    /**
     * 获取安全库存最低
     *
     * @return safe_min - 安全库存最低
     */
    public Integer getSafeMin() {
        return safeMin;
    }

    /**
     * 设置安全库存最低
     *
     * @param safeMin 安全库存最低
     */
    public void setSafeMin(Integer safeMin) {
        this.safeMin = safeMin == null ? 0 : safeMin;
    }

    /**
     * 获取安全库存最高
     *
     * @return safe_max - 安全库存最高
     */
    public Integer getSafeMax() {
        return safeMax;
    }

    /**
     * 设置安全库存最高
     *
     * @param safeMax 安全库存最高
     */
    public void setSafeMax(Integer safeMax) {
        this.safeMax = safeMax == null ? 0 : safeMax;
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state == null ? 0 : state;
    }

    /**
     * 获取备注
     *
     * @return comment - 备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置备注
     *
     * @param comment 备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? "" : comment.trim();
    }

    /**
     * 获取图片
     *
     * @return img - 图片
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置图片
     *
     * @param img 图片
     */
    public void setImg(String img) {
        this.img = img == null ? "" : img.trim();
    }

    /**
     * 获取创建人
     *
     * @return cuser - 创建人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置创建人
     *
     * @param cuser 创建人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? "" : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? "" : ctime.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? "" : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? "" : euser.trim();
    }
}