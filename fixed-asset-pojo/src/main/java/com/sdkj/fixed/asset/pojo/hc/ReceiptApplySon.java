package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.ApproveGroup;
import com.sdkj.fixed.asset.pojo.hc.group.ConfirmGroup;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 领用申请单子单
 *
 * @author 史晨星
 * @date 2020-07-27 : 03:28:09
 */
@Table(name = "pm_apply_receipt_son")
public class ReceiptApplySon implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotEmpty(message = "id-不能为空")
    private String id;

    @Column(name = "product_id")
    private String productId;

    private Integer num;
    @NotNull(message = "是否批准-不能为空", groups = {ApproveGroup.class})
    private Integer approve;

    @Column(name = "receipt_id")
    private String receiptId;

    private String approveUser;

    private String approveTime;
    @NotNull(message = "是否确认-不能为空", groups = {ConfirmGroup.class})
    private Integer confirm;

    private String confirmUser;

    private String confirmTime;

    private String orgId;

    private String receiptOutId;
    @Transient
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReceiptOutId() {
        return receiptOutId;
    }

    public void setReceiptOutId(String receiptOutId) {
        this.receiptOutId = receiptOutId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public String getConfirmUser() {
        return confirmUser;
    }

    public void setConfirmUser(String confirmUser) {
        this.confirmUser = confirmUser;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    /**
     * @return num
     */
    public Integer getNum() {
        return num;
    }

    /**
     * @param num
     */
    public void setNum(Integer num) {
        this.num = num;
    }

    /**
     * @return approve
     */
    public Integer getApprove() {
        return approve;
    }

    /**
     * @param approve
     */
    public void setApprove(Integer approve) {
        this.approve = approve;
    }

    /**
     * @return receipt_id
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * @param receiptId
     */
    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId == null ? null : receiptId.trim();
    }
}