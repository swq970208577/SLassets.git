package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author 牛丽伟
 * @date  2020-08-04 : 10:22:38
 */
@Table(name="as_inventory")
public class Inventory implements Serializable {
    /**
     * 盘点id
     */
    @Id
    @Column(name = "inventory_id")
    @KeySql(genId = UUIdGenId.class)
    private String inventoryId;

    /**
     * 盘点单名称
     */
    @Column(name = "inventory_name")
    @Excel(name = "盘点单名称", orderNum = "0")
    @NotBlank(message = "盘点单名称不能为空")
    @Size(max = 32, message = "盘点单名称超长")
    private String inventoryName;

    /**
     * 包含已调出未确认的资产 0未选择 1 选择
     */
    @Column(name = "is_transfer")
    private Integer isTransfer;

    /**
     * 是否全员判断  0未选择 1 选择
     */
    @Column(name = "is_all_user")
    private Integer isAllUser;

    /**
     * 是否允许手工盘点  0未选择 1 选择
     */
    @Column(name = "is_manual")
    @Excel(name = "允许手工盘点", orderNum = "1", replace = {"是_1", "否_0"})
    private Integer isManual;

    /**
     * 分配用户
     */
    @NotBlank(message = "分配用户不能为空")
    @Size(max = 468, message = "分配用户超长")
    private String username;

    /**
     * 盘点完成状态,0未完成1已完成
     */
    @Excel(name = "状态", orderNum = "4", replace = {"已完成_1", "未完成_0"})
    private String state;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @Excel(name = "创建时间", orderNum = "3")
    private String createTime;

    /**
     * 购入日期开始时间
     */
    @Column(name = "purchase_time_start")
    private String purchaseTimeStart;

    /**
     * 购入日期结束时间
     */
    @Column(name = "purchase_time_end")
    private String purchaseTimeEnd;

    /**
     * 所属公司名称
     */
    @Column(name = "company_name")
    private String companyName;

    /**
     * 所属公司id
     */
    @Column(name = "company_id")
    private String companyId;

    /**
     * 使用公司
     */
    @Column(name = "use_company_dept_name")
    private String useCompanyDeptName;

    /**
     * 使用公司id
     */
    @Column(name = "use_company_dept_id")
    private String useCompanyDeptId;

    /**
     * 使用部门
     */
    @Column(name = "use_dept_name")
    private String useDeptName;

    /**
     * 使用部门id
     */
    @Column(name = "use_dept_id")
    private String useDeptId;

    /**
     * 资产分类
     */
    @Column(name = "set_class_name")
    private String setClassName;

    /**
     * 资产分类id
     */
    @Column(name = "set_class_id")
    private String setClassId;

    /**
     * 区域
     */
    @Column(name = "area_name")
    private String areaName;

    /**
     * 区域id
     */
    @Column(name = "area_id")
    private String areaId;

    /**
     * 管理员
     */
    @Column(name = "admin_user_name")
    private String adminUserName;

    /**
     * 总公司Id
     */
    @Column(name = "org_id")
    private String orgId;
    /**
     * 备注
     */
    @Size(max = 128, message = "备注超长")
    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * 获取盘点id
     *
     * @return inventory_id - 盘点id
     */
    public String getInventoryId() {
        return inventoryId;
    }

    /**
     * 设置盘点id
     *
     * @param inventoryId 盘点id
     */
    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId == null ? null : inventoryId.trim();
    }

    /**
     * 获取盘点单名称
     *
     * @return inventory_name - 盘点单名称
     */
    public String getInventoryName() {
        return inventoryName;
    }

    /**
     * 设置盘点单名称
     *
     * @param inventoryName 盘点单名称
     */
    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName == null ? null : inventoryName.trim();
    }

    /**
     * 获取包含已调出未确认的资产 0未选择 1 选择
     *
     * @return is_transfer - 包含已调出未确认的资产 0未选择 1 选择
     */
    public Integer getIsTransfer() {
        return isTransfer;
    }

    /**
     * 设置包含已调出未确认的资产 0未选择 1 选择
     *
     * @param isTransfer 包含已调出未确认的资产 0未选择 1 选择
     */
    public void setIsTransfer(Integer isTransfer) {
        this.isTransfer = isTransfer;
    }

    /**
     * 获取是否全员判断  0未选择 1 选择
     *
     * @return is_all_user - 是否全员判断  0未选择 1 选择
     */
    public Integer getIsAllUser() {
        return isAllUser;
    }

    /**
     * 设置是否全员判断  0未选择 1 选择
     *
     * @param isAllUser 是否全员判断  0未选择 1 选择
     */
    public void setIsAllUser(Integer isAllUser) {
        this.isAllUser = isAllUser;
    }

    /**
     * 获取是否允许手工盘点  0未选择 1 选择
     *
     * @return is_manual - 是否允许手工盘点  0未选择 1 选择
     */
    public Integer getIsManual() {
        return isManual;
    }

    /**
     * 设置是否允许手工盘点  0未选择 1 选择
     *
     * @param isManual 是否允许手工盘点  0未选择 1 选择
     */
    public void setIsManual(Integer isManual) {
        this.isManual = isManual;
    }

    /**
     * 获取分配用户
     *
     * @return username - 分配用户
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置分配用户
     *
     * @param username 分配用户
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * 获取盘点完成状态,0未完成1已完成
     *
     * @return state - 盘点完成状态,0未完成1已完成
     */
    public String getState() {
        return state;
    }

    /**
     * 设置盘点完成状态,0未完成1已完成
     *
     * @param state 盘点完成状态,0未完成1已完成
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取购入日期开始时间
     *
     * @return purchase_time_start - 购入日期开始时间
     */
    public String getPurchaseTimeStart() {
        return purchaseTimeStart;
    }

    /**
     * 设置购入日期开始时间
     *
     * @param purchaseTimeStart 购入日期开始时间
     */
    public void setPurchaseTimeStart(String purchaseTimeStart) {
        this.purchaseTimeStart = purchaseTimeStart == null ? null : purchaseTimeStart.trim();
    }

    /**
     * 获取购入日期结束时间
     *
     * @return purchase_time_end - 购入日期结束时间
     */
    public String getPurchaseTimeEnd() {
        return purchaseTimeEnd;
    }

    /**
     * 设置购入日期结束时间
     *
     * @param purchaseTimeEnd 购入日期结束时间
     */
    public void setPurchaseTimeEnd(String purchaseTimeEnd) {
        this.purchaseTimeEnd = purchaseTimeEnd == null ? null : purchaseTimeEnd.trim();
    }

    /**
     * 获取所属公司名称
     *
     * @return company_name - 所属公司名称
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 设置所属公司名称
     *
     * @param companyName 所属公司名称
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    /**
     * 获取所属公司id
     *
     * @return company_id - 所属公司id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 设置所属公司id
     *
     * @param companyId 所属公司id
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getUseCompanyDeptName() {
        return useCompanyDeptName;
    }

    public void setUseCompanyDeptName(String useCompanyDeptName) {
        this.useCompanyDeptName = useCompanyDeptName;
    }

    public String getUseCompanyDeptId() {
        return useCompanyDeptId;
    }

    public void setUseCompanyDeptId(String useCompanyDeptId) {
        this.useCompanyDeptId = useCompanyDeptId;
    }

    /**
     * 获取使用部门
     *
     * @return use_dept_name - 使用部门
     */
    public String getUseDeptName() {
        return useDeptName;
    }

    /**
     * 设置使用部门
     *
     * @param useDeptName 使用部门
     */
    public void setUseDeptName(String useDeptName) {
        this.useDeptName = useDeptName == null ? null : useDeptName.trim();
    }

    /**
     * 获取使用部门id
     *
     * @return use_dept_id - 使用部门id
     */
    public String getUseDeptId() {
        return useDeptId;
    }

    /**
     * 设置使用部门id
     *
     * @param useDeptId 使用部门id
     */
    public void setUseDeptId(String useDeptId) {
        this.useDeptId = useDeptId == null ? null : useDeptId.trim();
    }

    /**
     * 获取资产分类
     *
     * @return set_class_name - 资产分类
     */
    public String getSetClassName() {
        return setClassName;
    }

    /**
     * 设置资产分类
     *
     * @param setClassName 资产分类
     */
    public void setSetClassName(String setClassName) {
        this.setClassName = setClassName == null ? null : setClassName.trim();
    }

    /**
     * 获取资产分类id
     *
     * @return set_class_id - 资产分类id
     */
    public String getSetClassId() {
        return setClassId;
    }

    /**
     * 设置资产分类id
     *
     * @param setClassId 资产分类id
     */
    public void setSetClassId(String setClassId) {
        this.setClassId = setClassId == null ? null : setClassId.trim();
    }

    /**
     * 获取区域
     *
     * @return area_name - 区域
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * 设置区域
     *
     * @param areaName 区域
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    /**
     * 获取区域id
     *
     * @return area_id - 区域id
     */
    public String getAreaId() {
        return areaId;
    }

    /**
     * 设置区域id
     *
     * @param areaId 区域id
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId == null ? null : areaId.trim();
    }

    /**
     * 获取管理员
     *
     * @return admin_user_name - 管理员
     */
    public String getAdminUserName() {
        return adminUserName;
    }

    /**
     * 设置管理员
     *
     * @param adminUserName 管理员
     */
    public void setAdminUserName(String adminUserName) {
        this.adminUserName = adminUserName == null ? null : adminUserName.trim();
    }

    /**
     * 获取总公司Id
     *
     * @return org_id - 总公司Id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置总公司Id
     *
     * @param orgId 总公司Id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}