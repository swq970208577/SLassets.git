package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-07-23 : 02:30:30
 */
@Table(name = "as_transfer")
public class Transfer implements Serializable {
    /**
     * 调拨ID
     */
    @Id
    @Column(name = "transfer_id")
    @KeySql(genId = UUIdGenId.class)
    private String transferId;

    /**
     * 调拨单号
     */
    @Column(name = "transfer_order_no")
    private String transferOrderNo;

    /**
     * 调出日期
     */
    @Column(name = "transfer_out_time")
    private String transferOutTime;

    /**
     * 调出管理员
     */
    @Column(name = "transfer_out_admin")
    private String transferOutAdmin;

    /**
     * 调出管理员姓名
     */
    @Column(name = "transfer_out_admin_name")
    private String transferOutAdminName;

    /**
     * 调出公司
     */
    @Column(name = "transfer_out_company")
    private String transferOutCompany;

    /**
     * 调入日期
     */
    @Column(name = "transfer_in_time")
    private String transferInTime;

    /**
     * 调入管理员
     */
    @Column(name = "transfer_in_admin")
    private String transferInAdmin;
    /**
     * 调入管理员姓名
     */
    @Column(name = "transfer_in_admin_name")
    private String transferInAdminName;
    /**
     * 调入公司
     */
    @Column(name = "transfer_in_company")
    private String transferInCompany;

    /**
     * 调入部门
     */
    @Column(name = "transfer_in_dept")
    private String transferInDept;

    /**
     * 调入区域
     */
    @Column(name = "transfer_in_area")
    private String transferInArea;

    /**
     * 调入存放地点
     */
    @Column(name = "transfer_in_address")
    private String transferInAddress;

    /**
     * 调出说明
     */
    @Column(name = "transfer_remark")
    private String transferRemark;

    /**
     * 调入说明
     */
    @Column(name = "transfer_in_remark")
    private String transferInRemark;


    /**
     * 办理状态 0未完成 1已完成 2 已取消
     */
    @Column(name = "transfer_state")
    private String transferState;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 总公司ID
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 调出公司treecode
     */
    @Column(name = "transfer_out_company_treecode")
    private String transferOutCompanyTreecode;

    /**
     * 调入公司treecode
     */
    @Column(name = "transfer_in_company_treecode")
    private String transferInCompanyTreecode;

    /**
     * 调入部门treecode
     */
    @Column(name = "transfer_in_dept_treecode")
    private String transferInDeptTreecode;

    private static final long serialVersionUID = 1L;

    public String getTransferInRemark() {
        return transferInRemark;
    }

    public void setTransferInRemark(String transferInRemark) {
        this.transferInRemark = transferInRemark;
    }


    public String getTransferOutAdminName() {
        return transferOutAdminName;
    }

    public void setTransferOutAdminName(String transferOutAdminName) {
        this.transferOutAdminName = transferOutAdminName;
    }

    public String getTransferInAdminName() {
        return transferInAdminName;
    }

    public void setTransferInAdminName(String transferInAdminName) {
        this.transferInAdminName = transferInAdminName;
    }

    /**
     * 获取调拨ID
     *
     * @return transfer_id - 调拨ID
     */
    public String getTransferId() {
        return transferId;
    }

    /**
     * 设置调拨ID
     *
     * @param transferId 调拨ID
     */
    public void setTransferId(String transferId) {
        this.transferId = transferId == null ? null : transferId.trim();
    }

    /**
     * 获取调拨单号
     *
     * @return transfer_order_no - 调拨单号
     */
    public String getTransferOrderNo() {
        return transferOrderNo;
    }

    /**
     * 设置调拨单号
     *
     * @param transferOrderNo 调拨单号
     */
    public void setTransferOrderNo(String transferOrderNo) {
        this.transferOrderNo = transferOrderNo == null ? null : transferOrderNo.trim();
    }

    /**
     * 获取调出日期
     *
     * @return transfer_out_time - 调出日期
     */
    public String getTransferOutTime() {
        return transferOutTime;
    }

    /**
     * 设置调出日期
     *
     * @param transferOutTime 调出日期
     */
    public void setTransferOutTime(String transferOutTime) {
        this.transferOutTime = transferOutTime == null ? null : transferOutTime.trim();
    }

    /**
     * 获取调出管理员
     *
     * @return transfer_out_admin - 调出管理员
     */
    public String getTransferOutAdmin() {
        return transferOutAdmin;
    }

    /**
     * 设置调出管理员
     *
     * @param transferOutAdmin 调出管理员
     */
    public void setTransferOutAdmin(String transferOutAdmin) {
        this.transferOutAdmin = transferOutAdmin == null ? null : transferOutAdmin.trim();
    }

    /**
     * 获取调出公司
     *
     * @return transfer_out_company - 调出公司
     */
    public String getTransferOutCompany() {
        return transferOutCompany;
    }

    /**
     * 设置调出公司
     *
     * @param transferOutCompany 调出公司
     */
    public void setTransferOutCompany(String transferOutCompany) {
        this.transferOutCompany = transferOutCompany == null ? null : transferOutCompany.trim();
    }

    /**
     * 获取调入日期
     *
     * @return transfer_in_time - 调入日期
     */
    public String getTransferInTime() {
        return transferInTime;
    }

    /**
     * 设置调入日期
     *
     * @param transferInTime 调入日期
     */
    public void setTransferInTime(String transferInTime) {
        this.transferInTime = transferInTime == null ? null : transferInTime.trim();
    }

    /**
     * 获取调入管理员
     *
     * @return transfer_in_admin - 调入管理员
     */
    public String getTransferInAdmin() {
        return transferInAdmin;
    }

    /**
     * 设置调入管理员
     *
     * @param transferInAdmin 调入管理员
     */
    public void setTransferInAdmin(String transferInAdmin) {
        this.transferInAdmin = transferInAdmin == null ? null : transferInAdmin.trim();
    }

    /**
     * 获取调入公司
     *
     * @return transfer_in_company - 调入公司
     */
    public String getTransferInCompany() {
        return transferInCompany;
    }

    /**
     * 设置调入公司
     *
     * @param transferInCompany 调入公司
     */
    public void setTransferInCompany(String transferInCompany) {
        this.transferInCompany = transferInCompany == null ? null : transferInCompany.trim();
    }

    /**
     * 获取调入部门
     *
     * @return transfer_in_dept - 调入部门
     */
    public String getTransferInDept() {
        return transferInDept;
    }

    /**
     * 设置调入部门
     *
     * @param transferInDept 调入部门
     */
    public void setTransferInDept(String transferInDept) {
        this.transferInDept = transferInDept == null ? null : transferInDept.trim();
    }

    /**
     * 获取调入区域
     *
     * @return transfer_in_area - 调入区域
     */
    public String getTransferInArea() {
        return transferInArea;
    }

    /**
     * 设置调入区域
     *
     * @param transferInArea 调入区域
     */
    public void setTransferInArea(String transferInArea) {
        this.transferInArea = transferInArea == null ? null : transferInArea.trim();
    }

    /**
     * 获取调入存放地点
     *
     * @return transfer_in_address - 调入存放地点
     */
    public String getTransferInAddress() {
        return transferInAddress;
    }

    /**
     * 设置调入存放地点
     *
     * @param transferInAddress 调入存放地点
     */
    public void setTransferInAddress(String transferInAddress) {
        this.transferInAddress = transferInAddress == null ? null : transferInAddress.trim();
    }

    /**
     * 获取调拨说明
     *
     * @return transfer_remark - 调拨说明
     */
    public String getTransferRemark() {
        return transferRemark;
    }

    /**
     * 设置调拨说明
     *
     * @param transferRemark 调拨说明
     */
    public void setTransferRemark(String transferRemark) {
        this.transferRemark = transferRemark == null ? null : transferRemark.trim();
    }

    /**
     * 获取办理状态 0未完成 1已完成 2 已取消
     *
     * @return transfer_state - 办理状态 0未完成 1已完成 2 已取消
     */
    public String getTransferState() {
        return transferState;
    }

    /**
     * 设置办理状态 0未完成 1已完成 2 已取消
     *
     * @param transferState 办理状态 0未完成 1已完成 2 已取消
     */
    public void setTransferState(String transferState) {
        this.transferState = transferState == null ? null : transferState.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取总公司ID
     *
     * @return org_id - 总公司ID
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置总公司ID
     *
     * @param orgId 总公司ID
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取调出公司treecode
     *
     * @return transfer_out_company_treecode - 调出公司treecode
     */
    public String getTransferOutCompanyTreecode() {
        return transferOutCompanyTreecode;
    }

    /**
     * 设置调出公司treecode
     *
     * @param transferOutCompanyTreecode 调出公司treecode
     */
    public void setTransferOutCompanyTreecode(String transferOutCompanyTreecode) {
        this.transferOutCompanyTreecode = transferOutCompanyTreecode == null ? null : transferOutCompanyTreecode.trim();
    }

    /**
     * 获取调入公司treecode
     *
     * @return transfer_in_company_treecode - 调入公司treecode
     */
    public String getTransferInCompanyTreecode() {
        return transferInCompanyTreecode;
    }

    /**
     * 设置调入公司treecode
     *
     * @param transferInCompanyTreecode 调入公司treecode
     */
    public void setTransferInCompanyTreecode(String transferInCompanyTreecode) {
        this.transferInCompanyTreecode = transferInCompanyTreecode == null ? null : transferInCompanyTreecode.trim();
    }

    /**
     * 获取调入部门treecode
     *
     * @return transfer_in_dept_treecode - 调入部门treecode
     */
    public String getTransferInDeptTreecode() {
        return transferInDeptTreecode;
    }

    /**
     * 设置调入部门treecode
     *
     * @param transferInDeptTreecode 调入部门treecode
     */
    public void setTransferInDeptTreecode(String transferInDeptTreecode) {
        this.transferInDeptTreecode = transferInDeptTreecode == null ? null : transferInDeptTreecode.trim();
    }
}