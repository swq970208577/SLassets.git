package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.*;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录
 *
 * @author 史晨星
 * @date 2020-07-21 : 01:20:28
 */
@Table(name = "pm_product_type_management")
public class ProductType implements Serializable  {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {EditProductType.class, DelProductType.class})
    private String id;

    /**
     * 编码
     */
    @Size(max = 32, message = "编码-最大长度:32")
    @NotNull(message = "编码-不能为空", groups = {EditProductType.class, AddProductType.class})
    @Excel(name="编码",orderNum = "1",width = 20)
    private String code;

    /**
     * 名称
     */
    @Size(max = 16, message = "名称-最大长度:16")
    @NotNull(message = "名称-不能为空", groups = {EditProductType.class, AddProductType.class})
    @Excel(name="名称",orderNum = "2",width = 20)
    private String name;

    /**
     * 父类型
     */
    private String pid;

    /**
     * 状态(1可用2禁用)
     */
    private Integer state;

    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "7",width = 20)
    private String comment;

    /**
     * 创建人
     */
    private String cuser;

    /**
     * 创建时间
     */
    @Excel(name="创建时间",orderNum = "5",width = 20)
    private String ctime;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    /**
     * 机构id
     */
    @Column(name = "org_id")
    private String orgId;

    private String treecode;

    @Transient
    @Excel(name="上级code",orderNum = "3",width = 20)
    private String pcode;

    @Transient
    @Excel(name="上级名称",orderNum = "4",width = 20)
    private String pname;

    @Transient
    @Excel(name="创建人",orderNum = "6",width = 20)
    private String cusername;

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getCusername() {
        return cusername;
    }

    public void setCusername(String cusername) {
        this.cusername = cusername;
    }

    public String getTreecode() {
        return treecode;
    }

    public void setTreecode(String treecode) {
        this.treecode = treecode;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取编码
     *
     * @return code - 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置编码
     *
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取父类型
     *
     * @return pid - 父类型
     */
    public String getPid() {
        return pid;
    }

    /**
     * 设置父类型
     *
     * @param pid 父类型
     */
    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取备注
     *
     * @return comment - 备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置备注
     *
     * @param comment 备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取创建人
     *
     * @return cuser - 创建人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置创建人
     *
     * @param cuser 创建人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取机构id
     *
     * @return org_id - 机构id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置机构id
     *
     * @param orgId 机构id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }
}