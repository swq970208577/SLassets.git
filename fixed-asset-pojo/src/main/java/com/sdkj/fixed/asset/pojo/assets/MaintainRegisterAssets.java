package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author niuliwei
 * @date  2020-07-20 : 08:34:31
 */
@Table(name = "as_maintain_info_register_assets")
public class MaintainRegisterAssets implements Serializable {
    /**
     * 序号
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 维修信息登记id
     */
    @Column(name = "maintain_info_register_id")
    private String maintainInfoRegisterId;

    /**
     * 资产ID
     */
    @Column(name = "asset_id")
    private String assetId;


    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_date")
    private String updateDate;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_deleted")
    private Integer isDeleted;
    /**
     * 维修登记前资产状态
     */
    @Column(name = "before_state")
    private String beforeState;

    /**
     * 预留字段
     */
    private String reserved;

    private static final long serialVersionUID = 1L;

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public String getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取维修信息登记id
     *
     * @return maintain_info_register_id - 维修信息登记id
     */
    public String getMaintainInfoRegisterId() {
        return maintainInfoRegisterId;
    }

    /**
     * 设置维修信息登记id
     *
     * @param maintainInfoRegisterId 维修信息登记id
     */
    public void setMaintainInfoRegisterId(String maintainInfoRegisterId) {
        this.maintainInfoRegisterId = maintainInfoRegisterId == null ? null : maintainInfoRegisterId.trim();
    }

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }


    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_date - 修改时间
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_deleted - 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置是否删除
     *
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    public String getBeforeState() {
        return beforeState;
    }

    public void setBeforeState(String beforeState) {
        this.beforeState = beforeState;
    }
}