package com.sdkj.fixed.asset.pojo.system;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhaozheyu
 * @date  2021-04-08 : 11:58:50
 */
@Table(name = "sys_fed_management")
public class FedManagement implements Serializable {
    /**
     * 主键
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 问题描述
     */
    private String content;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 提交人id
     */
    private String cuser;

    /**
     * 创建时间
     */
    private String ctime;

    /**
     * 反馈类型1:BUG;2：咨询;3:建议
     */
    private Integer type;

    /**
     * 提交公司id
     */
    @Column(name = "company_id")
    private String companyId;

    /**
     * 截图
     */
    private String photo;

    /**
     * 预留字段
     */
    private String reserved;

    /**
     * 状态1.已处理2未处理
     */
    private Integer state;

    /**
     * 解决方案
     */
    private String solution;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取问题描述
     *
     * @return content - 问题描述
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置问题描述
     *
     * @param content 问题描述
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取手机号
     *
     * @return tel - 手机号
     */
    public String getTel() {
        return tel;
    }

    /**
     * 设置手机号
     *
     * @param tel 手机号
     */
    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    /**
     * 获取提交人id
     *
     * @return cuser - 提交人id
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置提交人id
     *
     * @param cuser 提交人id
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return ctime - 创建时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置创建时间
     *
     * @param ctime 创建时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取反馈类型1:BUG;2：咨询;3:建议
     *
     * @return type - 反馈类型1:BUG;2：咨询;3:建议
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置反馈类型1:BUG;2：咨询;3:建议
     *
     * @param type 反馈类型1:BUG;2：咨询;3:建议
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取提交公司id
     *
     * @return company_id - 提交公司id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 设置提交公司id
     *
     * @param companyId 提交公司id
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    /**
     * 获取截图
     *
     * @return photo - 截图
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 设置截图
     *
     * @param photo 截图
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * 获取预留字段
     *
     * @return reserved - 预留字段
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * 设置预留字段
     *
     * @param reserved 预留字段
     */
    public void setReserved(String reserved) {
        this.reserved = reserved == null ? null : reserved.trim();
    }

    /**
     * 获取状态1.已处理2未处理
     *
     * @return state - 状态1.已处理2未处理
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态1.已处理2未处理
     *
     * @param state 状态1.已处理2未处理
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取解决方案
     *
     * @return solution - 解决方案
     */
    public String getSolution() {
        return solution;
    }

    /**
     * 设置解决方案
     *
     * @param solution 解决方案
     */
    public void setSolution(String solution) {
        this.solution = solution == null ? null : solution.trim();
    }
}