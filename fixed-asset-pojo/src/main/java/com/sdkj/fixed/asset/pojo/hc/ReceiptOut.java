package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import java.security.Signature;
import java.util.List;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptOut;
import com.sdkj.fixed.asset.pojo.hc.group.EditReceiptOut;
import com.sdkj.fixed.asset.pojo.hc.group.Sign;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 出库单
 *
 * @author 史晨星
 * @date 2020-07-22 : 09:59:38
 */
@Table(name = "pm_out_receipt")
public class ReceiptOut implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    @Size(max = 32, message = "id-最大长度:32")
    @NotNull(message = "id-不能为空", groups = {EditReceiptOut.class, Sign.class})
    private String id;

    /**
     * 入库单号
     */
    private String number;

    /**
     * 入库仓库
     */
    @Column(name = "category_id")
    @Size(max = 32, message = "出库仓库-最大长度:32")
    @NotNull(message = "出库仓库-不能为空", groups = {EditReceiptOut.class, AddReceiptOut.class})
    private String categoryId;

    /**
     * 业务日期
     */
    @Column(name = "bussiness_date")
    @Size(max = 32, message = "业务日期-最大长度:32")
    @NotNull(message = "业务日期-不能为空", groups = {EditReceiptOut.class, AddReceiptOut.class})
    private String bussinessDate;

    /**
     * 领用人
     */
    @Column(name = "receive_user")
    @Size(max = 32, message = "领用人-最大长度:32")
    private String receiveUser;

    /**
     * 领用部门
     */
    @Column(name = "receive_dept")
    @Size(max = 32, message = "领用部门-最大长度:32")
    private String receiveDept;

    /**
     * 领用公司
     */
    @Column(name = "receive_org")
    @Size(max = 32, message = "领用公司-最大长度:32")
    private String receiveOrg;

    /**
     * 经办人
     */
    @Size(max = 32, message = "经办人-最大长度:32")
    @NotNull(message = "经办人-不能为空", groups = {EditReceiptOut.class, AddReceiptOut.class})
    private String cuser;

    /**
     * 经办时间
     */
    @Size(max = 32, message = "经办时间-最大长度:32")
    @NotNull(message = "经办时间-不能为空", groups = {EditReceiptOut.class, AddReceiptOut.class})
    private String ctime;

    /**
     * 备注
     */
    private String comment;

    /**
     * 修改人
     */
    private String etime;

    /**
     * 修改时间
     */
    private String euser;

    /**
     * 状态(1可用2禁用)
     */
    private Integer state;

    /**
     * 类型（1出库2盘亏）
     */
    private Integer type;

    /**
     * 机构id
     */
    @Column(name = "org_id")
    private String orgId;
    /**
     * 签字（0已签1未签）
     */
    private Integer sign;
    /**
     * 签字图片
     */
    @NotNull(message = "签字图片不能为空", groups = {Sign.class})
    @Size(max = 128, message = "图片长度最长128")
    private String signImg;

    private String receiptApplyId;

    public String getReceiptApplyId() {
        return receiptApplyId;
    }

    public void setReceiptApplyId(String receiptApplyId) {
        this.receiptApplyId = receiptApplyId;
    }

    @Transient
    private String productName;

    @Transient
    private String productCode;

    @Transient
    private String cuserName;

    @Transient
    private String orgTreeCode;

    @Transient
    private String deptTreeCode;

    @Transient
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgTreeCode() {
        return orgTreeCode;
    }

    public void setOrgTreeCode(String orgTreeCode) {
        this.orgTreeCode = orgTreeCode;
    }

    public String getDeptTreeCode() {
        return deptTreeCode;
    }

    public void setDeptTreeCode(String deptTreeCode) {
        this.deptTreeCode = deptTreeCode;
    }

    public String getCuserName() {
        return cuserName;
    }

    public void setCuserName(String cuserName) {
        this.cuserName = cuserName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }

    @Transient
    @Valid
    private List<ReceiptOutProduct> receiptOutProduct;

    @Transient
    private String receiveUserName;

    @Transient
    private String receiveDeptName;

    @Transient
    private String receiveOrgName;

    @Transient
    private String categoryName;

    public String getReceiveUserName() {
        return receiveUserName;
    }

    public void setReceiveUserName(String receiveUserName) {
        this.receiveUserName = receiveUserName;
    }

    public String getReceiveDeptName() {
        return receiveDeptName;
    }

    public void setReceiveDeptName(String receiveDeptName) {
        this.receiveDeptName = receiveDeptName;
    }

    public String getReceiveOrgName() {
        return receiveOrgName;
    }

    public void setReceiveOrgName(String receiveOrgName) {
        this.receiveOrgName = receiveOrgName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<ReceiptOutProduct> getReceiptOutProduct() {
        return receiptOutProduct;
    }

    public void setReceiptOutProduct(List<ReceiptOutProduct> receiptOutProduct) {
        this.receiptOutProduct = receiptOutProduct;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取入库单号
     *
     * @return number - 入库单号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置入库单号
     *
     * @param number 入库单号
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取入库仓库
     *
     * @return category_id - 入库仓库
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 设置入库仓库
     *
     * @param categoryId 入库仓库
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? null : categoryId.trim();
    }

    /**
     * 获取业务日期
     *
     * @return bussiness_date - 业务日期
     */
    public String getBussinessDate() {
        return bussinessDate;
    }

    /**
     * 设置业务日期
     *
     * @param bussinessDate 业务日期
     */
    public void setBussinessDate(String bussinessDate) {
        this.bussinessDate = bussinessDate == null ? null : bussinessDate.trim();
    }

    /**
     * 获取领用人
     *
     * @return receive_user - 领用人
     */
    public String getReceiveUser() {
        return receiveUser;
    }

    /**
     * 设置领用人
     *
     * @param receiveUser 领用人
     */
    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser == null ? null : receiveUser.trim();
    }

    /**
     * 获取领用部门
     *
     * @return receive_dept - 领用部门
     */
    public String getReceiveDept() {
        return receiveDept;
    }

    /**
     * 设置领用部门
     *
     * @param receiveDept 领用部门
     */
    public void setReceiveDept(String receiveDept) {
        this.receiveDept = receiveDept == null ? null : receiveDept.trim();
    }

    /**
     * 获取领用公司
     *
     * @return receive_org - 领用公司
     */
    public String getReceiveOrg() {
        return receiveOrg;
    }

    /**
     * 设置领用公司
     *
     * @param receiveOrg 领用公司
     */
    public void setReceiveOrg(String receiveOrg) {
        this.receiveOrg = receiveOrg == null ? null : receiveOrg.trim();
    }

    /**
     * 获取经办人
     *
     * @return cuser - 经办人
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * 设置经办人
     *
     * @param cuser 经办人
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * 获取经办时间
     *
     * @return ctime - 经办时间
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * 设置经办时间
     *
     * @param ctime 经办时间
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * 获取备注
     *
     * @return comment - 备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置备注
     *
     * @param comment 备注
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * 获取修改人
     *
     * @return etime - 修改人
     */
    public String getEtime() {
        return etime;
    }

    /**
     * 设置修改人
     *
     * @param etime 修改人
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * 获取修改时间
     *
     * @return euser - 修改时间
     */
    public String getEuser() {
        return euser;
    }

    /**
     * 设置修改时间
     *
     * @param euser 修改时间
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * 获取状态(1可用2禁用)
     *
     * @return state - 状态(1可用2禁用)
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态(1可用2禁用)
     *
     * @param state 状态(1可用2禁用)
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取类型（1出库2盘亏）
     *
     * @return type - 类型（1出库2盘亏）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置类型（1出库2盘亏）
     *
     * @param type 类型（1出库2盘亏）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取机构id
     *
     * @return org_id - 机构id
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置机构id
     *
     * @param orgId 机构id
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }
}