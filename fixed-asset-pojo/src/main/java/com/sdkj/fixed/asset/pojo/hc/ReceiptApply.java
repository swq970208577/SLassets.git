package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 领用申请单
 *
 * @author 史晨星
 * @date 2020-07-27 : 12:02:47
 */
@Table(name = "pm_apply_receipt")
public class ReceiptApply implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 编号
     */
    private String number;

    /**
     * 物品id
     */
    @Column(name = "product_id")
    private String productId;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 申请人
     */
    @Column(name = "apply_user")
    private String applyUser;

    /**
     * 申请时间
     */
    @Column(name = "apply_time")
    private String applyTime;

    /**
     * 批准人
     */
    @Column(name = "approve_user")
    private String approveUser;

    /**
     * 批准时间
     */
    @Column(name = "approve_time")
    private String approveTime;

    /**
     * 是否确认（1确认2没有）
     */
    private Integer confirm;

    /**
     * 状态（1可用2禁用）
     */
    private Integer state;

    /**
     * 是否批准通过（1通过2拒绝）
     */
    private Integer approve;

    private String confirmTime;

    private String orgId;

    private Integer process;

    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getProcess() {
        return process;
    }

    public void setProcess(Integer process) {
        this.process = process;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @Transient
    private String name;
    @Transient
    private String model;
    @Transient
    private String unit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取编号
     *
     * @return number - 编号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 设置编号
     *
     * @param number 编号
     */
    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    /**
     * 获取物品id
     *
     * @return product_id - 物品id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 设置物品id
     *
     * @param productId 物品id
     */
    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    /**
     * 获取数量
     *
     * @return num - 数量
     */
    public Integer getNum() {
        return num;
    }

    /**
     * 设置数量
     *
     * @param num 数量
     */
    public void setNum(Integer num) {
        this.num = num;
    }

    /**
     * 获取申请人
     *
     * @return apply_user - 申请人
     */
    public String getApplyUser() {
        return applyUser;
    }

    /**
     * 设置申请人
     *
     * @param applyUser 申请人
     */
    public void setApplyUser(String applyUser) {
        this.applyUser = applyUser == null ? null : applyUser.trim();
    }

    /**
     * 获取申请时间
     *
     * @return apply_time - 申请时间
     */
    public String getApplyTime() {
        return applyTime;
    }

    /**
     * 设置申请时间
     *
     * @param applyTime 申请时间
     */
    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime == null ? null : applyTime.trim();
    }

    /**
     * 获取批准人
     *
     * @return approve_user - 批准人
     */
    public String getApproveUser() {
        return approveUser;
    }

    /**
     * 设置批准人
     *
     * @param approveUser 批准人
     */
    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser == null ? null : approveUser.trim();
    }

    /**
     * 获取批准时间
     *
     * @return approve_time - 批准时间
     */
    public String getApproveTime() {
        return approveTime;
    }

    /**
     * 设置批准时间
     *
     * @param approveTime 批准时间
     */
    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime == null ? null : approveTime.trim();
    }

    /**
     * 获取是否确认（1确认2没有）
     *
     * @return confirm - 是否确认（1确认2没有）
     */
    public Integer getConfirm() {
        return confirm;
    }

    /**
     * 设置是否确认（1确认2没有）
     *
     * @param confirm 是否确认（1确认2没有）
     */
    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    /**
     * 获取状态（1可用2禁用）
     *
     * @return state - 状态（1可用2禁用）
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态（1可用2禁用）
     *
     * @param state 状态（1可用2禁用）
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取是否批准通过（1通过2拒绝）
     *
     * @return approve - 是否批准通过（1通过2拒绝）
     */
    public Integer getApprove() {
        return approve;
    }

    /**
     * 设置是否批准通过（1通过2拒绝）
     *
     * @param approve 是否批准通过（1通过2拒绝）
     */
    public void setApprove(Integer approve) {
        this.approve = approve;
    }
}