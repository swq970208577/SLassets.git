package com.sdkj.fixed.asset.pojo.assets;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-07-23 : 02:30:30
 */
@Table(name = "as_warehouse")
public class Warehouse implements Serializable {
    /**
     * 资产ID
     */
    @Id
    @Column(name = "asset_id")
    @KeySql(genId = UUIdGenId.class)
    private String assetId;

    /**
     * 资产条码
     */
    @Excel(name="资产条码",orderNum = "2")
    @Column(name = "asset_code")
    private String assetCode;

    /**
     * 资产名称
     */
    @NotNull(message = "资产名称不能为空")
    @Excel(name="资产名称",orderNum = "3")
    @Column(name = "asset_name")
    private String assetName;

    /**
     * 资产类别ID
     */
    @NotNull(message = "资产类别不能为空")
    @Column(name = "asset_class_id")
    private String assetClassId;

    /**
     * 资产类别treeCode
     */
    @Column(name = "class_treecode")
    private String classTreecode;

    /**
     * 标准型号=规格型号ID
     */
    @Column(name = "standard_model")
    private String standardModel;

    /**
     * 规格型号
     */
    @Excel(name="规格型号",orderNum = "5")
    @Column(name = "specification_model")
    private String specificationModel;

    /**
     * 计量单位
     */
    @Excel(name="计量单位",orderNum = "7")
    @Column(name = "unit_measurement")
    private String unitMeasurement;

    /**
     * SN号
     */
    @Excel(name="SN号",orderNum = "6")
    @Column(name = "sn_number")
    private String snNumber;

    /**
     * 来源
     */
    @Excel(name="来源",orderNum = "19",replace={"购入_1","自建_2","租赁_3","捐赠_4","其他_5","内部购入_6"})
    private String source;

    /**
     * 购入时间
     */
    @NotNull(message = "购入时间不能为空")
    @Excel(name="购入时间",orderNum = "16")
    @Column(name = "purchase_time")
    private String purchaseTime;

    /**
     * 所属公司ID
     */
    @NotNull(message = "所属公司不能为空")
    private String company;

    /**
     * 所属公司treecode
     */
    @NotNull(message = "所属公司treecode不能为空")
    @Column(name = "company_treecode")
    private String companyTreecode;

    /**
     * 使用公司treecode
     */

    @Column(name = "use_company_treecode")
    private String useCompanyTreecode;

    /**
     * 使用部门treecode
     */
    @Column(name = "use_dept_treecode")
    private String useDeptTreecode;

    /**
     * 金额
     */
    @Excel(name="金额",orderNum = "8")
    @Size(max =10, message = "超过金额数量的最大值")
    private String amount;

    /**
     * 管理员
     */
    @Excel(name="管理员",orderNum = "14")
    private String admin;

    /**
     * 管理员ID
     */
    private String adminId;
    /**
     * 使用公司ID
     */
    @NotNull(message = "使用公司不能为空")
    @Column(name = "use_company")
    private String useCompany;

    /**
     * 使用部门
     */
    @Column(name = "use_department")
    private String useDepartment;

    /**
     * 使用人ID
     */
    @Column(name = "handler_user_id")
    private String handlerUserId;

    /**
     * 使用人
     */
    @Excel(name="使用人",orderNum = "11")
    @Column(name = "handler_user")
    private String handlerUser;

    /**
     * 使用期限(月)
     */
    @Excel(name="使用期限",orderNum = "18")
    @Column(name = "service_life")
    private String serviceLife;

    /**
     * 区域
     */
    private String area;

    /**
     * 存放地点
     */
    @Excel(name="存放地点",orderNum = "13")
    @Column(name = "storage_location")
    private String storageLocation;

    /**
     * 备注
     */
    private String remark;

    /**
     * 合同编号
     */
    @Column(name = "contract_no")
    private String contractNo;

    /**
     * 状态
     */
    @Excel(name="状态",orderNum = "0")
    private String state;

    /**
     * 照片
     */
    @Excel(name="照片",orderNum = "1")
    private String photo;

    /**
     * 创建时间
     */
    @Excel(name="创建时间",orderNum = "21")
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */

    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    @Column(name="org_id")
    private String orgId;

    private static final long serialVersionUID = 1L;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCompanyTreecode() {
        return companyTreecode;
    }

    public void setCompanyTreecode(String companyTreecode) {
        this.companyTreecode = companyTreecode;
    }

    public String getUseCompanyTreecode() {
        return useCompanyTreecode;
    }

    public void setUseCompanyTreecode(String useCompanyTreecode) {
        this.useCompanyTreecode = useCompanyTreecode;
    }

    public String getUseDeptTreecode() {
        return useDeptTreecode;
    }

    public void setUseDeptTreecode(String useDeptTreecode) {
        this.useDeptTreecode = useDeptTreecode;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getClassTreecode() {
        return classTreecode;
    }

    public void setClassTreecode(String classTreecode) {
        this.classTreecode = classTreecode;
    }
    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取资产条码
     *
     * @return asset_code - 资产条码
     */
    public String getAssetCode() {
        return assetCode;
    }

    /**
     * 设置资产条码
     *
     * @param assetCode 资产条码
     */
    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode == null ? null : assetCode.trim();
    }

    /**
     * 获取资产名称
     *
     * @return asset_name - 资产名称
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * 设置资产名称
     *
     * @param assetName 资产名称
     */
    public void setAssetName(String assetName) {
        this.assetName = assetName == null ? null : assetName.trim();
    }

    /**
     * 获取资产类别ID
     *
     * @return asset_class_id - 资产类别ID
     */
    public String getAssetClassId() {
        return assetClassId;
    }

    /**
     * 设置资产类别ID
     *
     * @param assetClassId 资产类别ID
     */
    public void setAssetClassId(String assetClassId) {
        this.assetClassId = assetClassId == null ? null : assetClassId.trim();
    }

    /**
     * 获取标准型号=规格型号ID
     *
     * @return standard_model - 标准型号=规格型号ID
     */
    public String getStandardModel() {
        return standardModel;
    }

    /**
     * 设置标准型号=规格型号ID
     *
     * @param standardModel 标准型号=规格型号ID
     */
    public void setStandardModel(String standardModel) {
        this.standardModel = standardModel == null ? null : standardModel.trim();
    }

    /**
     * 获取规格型号
     *
     * @return specification_model - 规格型号
     */
    public String getSpecificationModel() {
        return specificationModel;
    }

    /**
     * 设置规格型号
     *
     * @param specificationModel 规格型号
     */
    public void setSpecificationModel(String specificationModel) {
        this.specificationModel = specificationModel == null ? null : specificationModel.trim();
    }

    /**
     * 获取计量单位
     *
     * @return unit_measurement - 计量单位
     */
    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    /**
     * 设置计量单位
     *
     * @param unitMeasurement 计量单位
     */
    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement == null ? null : unitMeasurement.trim();
    }

    /**
     * 获取SN号
     *
     * @return sn_number - SN号
     */
    public String getSnNumber() {
        return snNumber;
    }

    /**
     * 设置SN号
     *
     * @param snNumber SN号
     */
    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber == null ? null : snNumber.trim();
    }

    /**
     * 获取来源
     *
     * @return source - 来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 设置来源
     *
     * @param source 来源
     */
    public void setSource(String source) {
        this.source = source == null ? "" : source.trim();
    }

    /**
     * 获取购入时间
     *
     * @return purchase_time - 购入时间
     */
    public String getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * 设置购入时间
     *
     * @param purchaseTime 购入时间
     */
    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime == null ? null : purchaseTime.trim();
    }

    /**
     * 获取所属公司ID
     *
     * @return company - 所属公司ID
     */
    public String getCompany() {
        return company;
    }

    /**
     * 设置所属公司ID
     *
     * @param company 所属公司ID
     */
    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }

    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public String getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(String amount) {
        this.amount = amount == null ? null : amount.trim();
    }

    /**
     * 获取管理员
     *
     * @return admin - 管理员
     */
    public String getAdmin() {
        return admin;
    }

    /**
     * 设置管理员
     *
     * @param admin 管理员
     */
    public void setAdmin(String admin) {
        this.admin = admin == null ? null : admin.trim();
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    /**
     * 获取使用公司ID
     *
     * @return use_company - 使用公司ID
     */
    public String getUseCompany() {
        return useCompany;
    }

    /**
     * 设置使用公司ID
     *
     * @param useCompany 使用公司ID
     */
    public void setUseCompany(String useCompany) {
        this.useCompany = useCompany == null ? null : useCompany.trim();
    }

    /**
     * 获取使用部门
     *
     * @return use_department - 使用部门
     */
    public String getUseDepartment() {
        return useDepartment;
    }

    /**
     * 设置使用部门
     *
     * @param useDepartment 使用部门
     */
    public void setUseDepartment(String useDepartment) {
        this.useDepartment = useDepartment == null ? null : useDepartment.trim();
    }

    /**
     * 获取使用人ID
     *
     * @return handler_user_id - 使用人ID
     */
    public String getHandlerUserId() {
        return handlerUserId;
    }

    /**
     * 设置使用人ID
     *
     * @param handlerUserId 使用人ID
     */
    public void setHandlerUserId(String handlerUserId) {
        this.handlerUserId = handlerUserId == null ? null : handlerUserId.trim();
    }

    /**
     * 获取使用人
     *
     * @return handler_user - 使用人
     */
    public String getHandlerUser() {
        return handlerUser;
    }

    /**
     * 设置使用人
     *
     * @param handlerUser 使用人
     */
    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser == null ? null : handlerUser.trim();
    }

    /**
     * 获取使用期限(月)
     *
     * @return service_life - 使用期限(月)
     */
    public String getServiceLife() {
        return serviceLife;
    }

    /**
     * 设置使用期限(月)
     *
     * @param serviceLife 使用期限(月)
     */
    public void setServiceLife(String serviceLife) {
        this.serviceLife = serviceLife == null ? null : serviceLife.trim();
    }

    /**
     * 获取区域
     *
     * @return area - 区域
     */
    public String getArea() {
        return area;
    }

    /**
     * 设置区域
     *
     * @param area 区域
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    /**
     * 获取存放地点
     *
     * @return storage_location - 存放地点
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * 设置存放地点
     *
     * @param storageLocation 存放地点
     */
    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation == null ? null : storageLocation.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取状态
     *
     * @return state - 状态
     */
    public String getState() {
        return state;
    }

    /**
     * 设置状态
     *
     * @param state 状态
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取照片
     *
     * @return photo - 照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 设置照片
     *
     * @param photo 照片
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}