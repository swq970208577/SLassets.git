package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-07-23 : 02:30:30
 */
@Table(name = "as_asset_supplier")
public class AssetSupplier implements Serializable {
    /**
     * ID
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 资产ID
     */
    @Column(name = "asset_id")
    private String assetId;

    /**
     * 供应商ID
     */
    @Column(name = "supplier_id")
    private String supplierId;

    /**
     * 供应商名称
     */
    @Column(name = "supplier_name")
    private String supplierName;

    /**
     * 负责人ID
     */
    @Column(name = "supplier_user_id")
    private String supplierUserId;
    // 负责人
    @Column(name = "supplier_user")
    private String supplierUser;

    /**
     * 维保到期日
     */
    @Column(name = "expire_time")
    private String expireTime;

    /**
     * 维保说明
     */
    @Column(name = "explain_text")
    private String explainText;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取资产ID
     *
     * @return asset_id - 资产ID
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * 设置资产ID
     *
     * @param assetId 资产ID
     */
    public void setAssetId(String assetId) {
        this.assetId = assetId == null ? null : assetId.trim();
    }

    /**
     * 获取供应商ID
     *
     * @return supplier_id - 供应商ID
     */
    public String getSupplierId() {
        return supplierId;
    }

    /**
     * 设置供应商ID
     *
     * @param supplierId 供应商ID
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId == null ? null : supplierId.trim();
    }

    /**
     * 获取供应商名称
     *
     * @return supplier_name - 供应商名称
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * 设置供应商名称
     *
     * @param supplierName 供应商名称
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName == null ? null : supplierName.trim();
    }

    public String getSupplierUserId() {
        return supplierUserId;
    }

    public void setSupplierUserId(String supplierUserId) {
        this.supplierUserId = supplierUserId;
    }

    public String getSupplierUser() {
        return supplierUser;
    }

    public void setSupplierUser(String supplierUser) {
        this.supplierUser = supplierUser;
    }

    /**
     * 获取维保到期日
     *
     * @return expire_time - 维保到期日
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * 设置维保到期日
     *
     * @param expireTime 维保到期日
     */
    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime == null ? null : expireTime.trim();
    }

    /**
     * 获取维保说明
     *
     * @return explain_text - 维保说明
     */
    public String getExplainText() {
        return explainText;
    }

    /**
     * 设置维保说明
     *
     * @param explainText 维保说明
     */
    public void setExplainText(String explainText) {
        this.explainText = explainText == null ? null : explainText.trim();
    }
}