package com.sdkj.fixed.asset.pojo.hc;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.hc.group.AddReceiptIn;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 入库单-入库耗材详情
 *
 * @author 史晨星
 * @date 2020-07-21 : 01:55:23
 */
@Table(name = "pm_product_in_receipt_config")
public class ReceiptInProduct implements Serializable {
    /**
     * id
     */
    @Id
    @KeySql(genId = UUIdGenId.class)
    private String id;

    /**
     * 单号id
     */
    @Column(name = "receipt_id")
    private String receiptId;

    /**
     * 耗材id
     */
    @Column(name = "product_id")
    @Size(max = 32, message = "耗材id-最大长度:32")
    @NotNull(message = "耗材id-必填", groups = {AddReceiptIn.class})
    private String productId;

    /**
     * 耗材数量
     */
    @NotNull(message = "耗材数量-必填", groups = {AddReceiptIn.class})
    private Integer num;

    /**
     * 耗材单价
     */
    @Size(max = 32, message = "耗材单价-最大长度:32")
    @NotNull(message = "耗材单价-必填", groups = {AddReceiptIn.class})
    private String price;

    /**
     * 耗材金额
     */
    @Size(max = 32, message = "耗材金额-最大长度:32")
    @NotNull(message = "耗材金额-必填", groups = {AddReceiptIn.class})
    @Column(name = "total_price")
    private String totalPrice;

    @Transient
    private Product product;

    @Transient
    private String number;

    @Transient
    private String cuser;

    @Transient
    private String categoryId;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取单号id
     *
     * @return receipt_id - 单号id
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * 设置单号id
     *
     * @param receiptId 单号id
     */
    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId == null ? null : receiptId.trim();
    }

    /**
     * 获取耗材id
     *
     * @return product_id - 耗材id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 设置耗材id
     *
     * @param productId 耗材id
     */
    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    /**
     * 获取耗材数量
     *
     * @return num - 耗材数量
     */
    public Integer getNum() {
        return num;
    }

    /**
     * 设置耗材数量
     *
     * @param num 耗材数量
     */
    public void setNum(Integer num) {
        this.num = num;
    }

    /**
     * 获取耗材单价
     *
     * @return price - 耗材单价
     */
    public String getPrice() {
        return price;
    }

    /**
     * 设置耗材单价
     *
     * @param price 耗材单价
     */
    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    /**
     * 获取耗材金额
     *
     * @return total_price - 耗材金额
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * 设置耗材金额
     *
     * @param totalPrice 耗材金额
     */
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice == null ? null : totalPrice.trim();
    }
}