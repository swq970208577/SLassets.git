package com.sdkj.fixed.asset.pojo.assets;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 更显阶段记录 
 * @author zhangjinfei
 * @date  2020-07-28 : 01:34:01
 */
@Table(name = "as_withdrawal")
public class Withdrawal implements Serializable {
    /**
     * 退库ID
     */
    @Id
    @Column(name = "withdrawal_id")
    @KeySql(genId = UUIdGenId.class)
    private String withdrawalId;

    /**
     * 退库单号
     */
    @Column(name = "withdrawal_no")
    private String withdrawalNo;

    /**
     * 退库日期
     */
    @Column(name = "withdrawal_date")
    private String withdrawalDate;

    /**
     * 退库后使用公司
     */
    @Column(name = "withdrawal_company")
    private String withdrawalCompany;

    /**
     * 退库后区域
     */
    @Column(name = "withdrawal_area")
    private String withdrawalArea;

    /**
     * 退库后存放地点
     */
    @Column(name = "withdrawal_address")
    private String withdrawalAddress;

    /**
     * 退库备注
     */
    @Column(name = "withdrawal_remark")
    private String withdrawalRemark;

    /**
     * 处理人
     */
    @Column(name = "handler_user")
    private String handlerUser;

    /**
     *  使用人
     */
    @Column(name = "withdrawal_user_id")
    private String withdrawalUserId;
    /**
     * 办理状态
     */
    private Integer state;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private String updateTime;

    /**
     * 修改人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 类型0 管理员发放 1 员工申请
     */
    @Column(name = "withdrawal_type")
    private Integer withdrawalType;

    /**
     * 是否审批 0 待审批 1 已同意  2 已驳回
     */
    private Integer approval;

    /**
     * 总公司ID
     */
    @Column(name = "org_id")
    private String orgId;

    /**
     * 退库后使用公司
     */
    @Column(name = "withdrawal_company_treecode")
    private String withdrawalCompanyTreecode;

    @Column(name = "sign_pic")
    private String signPic;

    @Column(name = "asset_num")
    private String assetNum;

    @Column(name = "apply_id")
    private String applyId;



    private static final long serialVersionUID = 1L;

    public String getWithdrawalUserId() {
        return withdrawalUserId;
    }

    public void setWithdrawalUserId(String withdrawalUserId) {
        this.withdrawalUserId = withdrawalUserId;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public String getAssetNum() {
        return assetNum;
    }

    public void setAssetNum(String assetNum) {
        this.assetNum = assetNum;
    }

    public String getSignPic() {
        return signPic;
    }

    public void setSignPic(String signPic) {
        this.signPic = signPic;
    }


    /**
     * 获取退库ID
     *
     * @return withdrawal_id - 退库ID
     */
    public String getWithdrawalId() {
        return withdrawalId;
    }

    /**
     * 设置退库ID
     *
     * @param withdrawalId 退库ID
     */
    public void setWithdrawalId(String withdrawalId) {
        this.withdrawalId = withdrawalId == null ? null : withdrawalId.trim();
    }

    /**
     * 获取退库单号
     *
     * @return withdrawal_no - 退库单号
     */
    public String getWithdrawalNo() {
        return withdrawalNo;
    }

    /**
     * 设置退库单号
     *
     * @param withdrawalNo 退库单号
     */
    public void setWithdrawalNo(String withdrawalNo) {
        this.withdrawalNo = withdrawalNo == null ? null : withdrawalNo.trim();
    }

    /**
     * 获取退库日期
     *
     * @return withdrawal_date - 退库日期
     */
    public String getWithdrawalDate() {
        return withdrawalDate;
    }

    /**
     * 设置退库日期
     *
     * @param withdrawalDate 退库日期
     */
    public void setWithdrawalDate(String withdrawalDate) {
        this.withdrawalDate = withdrawalDate == null ? null : withdrawalDate.trim();
    }

    /**
     * 获取退库后使用公司
     *
     * @return withdrawal_company - 退库后使用公司
     */
    public String getWithdrawalCompany() {
        return withdrawalCompany;
    }

    /**
     * 设置退库后使用公司
     *
     * @param withdrawalCompany 退库后使用公司
     */
    public void setWithdrawalCompany(String withdrawalCompany) {
        this.withdrawalCompany = withdrawalCompany == null ? null : withdrawalCompany.trim();
    }

    /**
     * 获取退库后区域
     *
     * @return withdrawal_area - 退库后区域
     */
    public String getWithdrawalArea() {
        return withdrawalArea;
    }

    /**
     * 设置退库后区域
     *
     * @param withdrawalArea 退库后区域
     */
    public void setWithdrawalArea(String withdrawalArea) {
        this.withdrawalArea = withdrawalArea == null ? null : withdrawalArea.trim();
    }

    /**
     * 获取退库后存放地点
     *
     * @return withdrawal_address - 退库后存放地点
     */
    public String getWithdrawalAddress() {
        return withdrawalAddress;
    }

    /**
     * 设置退库后存放地点
     *
     * @param withdrawalAddress 退库后存放地点
     */
    public void setWithdrawalAddress(String withdrawalAddress) {
        this.withdrawalAddress = withdrawalAddress == null ? null : withdrawalAddress.trim();
    }

    /**
     * 获取退库备注
     *
     * @return withdrawal_remark - 退库备注
     */
    public String getWithdrawalRemark() {
        return withdrawalRemark;
    }

    /**
     * 设置退库备注
     *
     * @param withdrawalRemark 退库备注
     */
    public void setWithdrawalRemark(String withdrawalRemark) {
        this.withdrawalRemark = withdrawalRemark == null ? null : withdrawalRemark.trim();
    }

    /**
     * 获取处理人
     *
     * @return handler_user - 处理人
     */
    public String getHandlerUser() {
        return handlerUser;
    }

    /**
     * 设置处理人
     *
     * @param handlerUser 处理人
     */
    public void setHandlerUser(String handlerUser) {
        this.handlerUser = handlerUser == null ? null : handlerUser.trim();
    }

    /**
     * 获取办理状态
     *
     * @return state - 办理状态
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置办理状态
     *
     * @param state 办理状态
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 获取创建人
     *
     * @return create_user - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return update_user - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取是否删除
     *
     * @return is_delete - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取类型0 管理员发放 1 员工申请
     *
     * @return withdrawal_type - 类型0 管理员发放 1 员工申请
     */
    public Integer getWithdrawalType() {
        return withdrawalType;
    }

    /**
     * 设置类型0 管理员发放 1 员工申请
     *
     * @param withdrawalType 类型0 管理员发放 1 员工申请
     */
    public void setWithdrawalType(Integer withdrawalType) {
        this.withdrawalType = withdrawalType;
    }

    /**
     * 获取是否审批 0 待审批 1 已同意  2 已驳回
     *
     * @return approval - 是否审批 0 待审批 1 已同意  2 已驳回
     */
    public Integer getApproval() {
        return approval;
    }

    /**
     * 设置是否审批 0 待审批 1 已同意  2 已驳回
     *
     * @param approval 是否审批 0 待审批 1 已同意  2 已驳回
     */
    public void setApproval(Integer approval) {
        this.approval = approval;
    }

    /**
     * 获取总公司ID
     *
     * @return org_id - 总公司ID
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * 设置总公司ID
     *
     * @param orgId 总公司ID
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    /**
     * 获取退库后使用公司
     *
     * @return withdrawal_company_treecode - 退库后使用公司
     */
    public String getWithdrawalCompanyTreecode() {
        return withdrawalCompanyTreecode;
    }

    /**
     * 设置退库后使用公司
     *
     * @param withdrawalCompanyTreecode 退库后使用公司
     */
    public void setWithdrawalCompanyTreecode(String withdrawalCompanyTreecode) {
        this.withdrawalCompanyTreecode = withdrawalCompanyTreecode == null ? null : withdrawalCompanyTreecode.trim();
    }
}