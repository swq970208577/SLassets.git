package com.sdkj.fixed.asset.pojo.system;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.DictionaryEntityEdit;
import com.sdkj.fixed.asset.pojo.system.group.IdMustDicEntity;
import tk.mybatis.mapper.annotation.KeySql;

/**
 * 更显阶段记录 
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */
@Table(name = "sys_dictionary_entity")
public class DictionaryEntity implements Serializable {
    @Id
    @KeySql(genId = UUIdGenId.class)
    @NotBlank(message = "id不能为空",groups = {DictionaryEntityEdit.class, IdMustDicEntity.class})
    @Size(max = 32,message = "id长度不匹配" ,groups = {DictionaryEntityEdit.class})
    private String id;

    private String ctime;

    private String cuser;

    private String etime;

    private String euser;

    @NotBlank(message = "名称不能为空")
    @Size(max = 32,message = "名称长度不匹配")
    private String name;

    @NotBlank(message = "编码不能为空")
    @Size(max = 32,message = "编码长度不匹配")
    private String code;

    /**
     * 1:可用；2：删除
     */
    private Integer state;

    @Size(max = 128,message = "备注长度不匹配")
    private String comments;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime
     */
    public void setCtime(String ctime) {
        this.ctime = ctime == null ? null : ctime.trim();
    }

    /**
     * @return cuser
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * @param cuser
     */
    public void setCuser(String cuser) {
        this.cuser = cuser == null ? null : cuser.trim();
    }

    /**
     * @return etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @param etime
     */
    public void setEtime(String etime) {
        this.etime = etime == null ? null : etime.trim();
    }

    /**
     * @return euser
     */
    public String getEuser() {
        return euser;
    }

    /**
     * @param euser
     */
    public void setEuser(String euser) {
        this.euser = euser == null ? null : euser.trim();
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取1:可用；2：删除
     *
     * @return state - 1:可用；2：删除
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1:可用；2：删除
     *
     * @param state 1:可用；2：删除
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }
}