package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.api.system.DictionaryTypeApi;
import com.sdkj.fixed.asset.api.system.in_vo.DicCodeParam;
import com.sdkj.fixed.asset.api.system.in_vo.DicEntityId;
import com.sdkj.fixed.asset.api.system.out_vo.DictionaryResult;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.pojo.system.DictionaryType;
import com.sdkj.fixed.asset.pojo.system.group.EditDicType;
import com.sdkj.fixed.asset.pojo.system.group.IdMustDicType;
import com.sdkj.fixed.asset.pojo.system.group.addDicType;
import com.sdkj.fixed.asset.system.service.DictionaryTypeService;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName DictionaryEntityController
 * @Description 数据字典控制类
 * @Author 张欣
 * @Date 2020/7/21 16:22
 */
@Controller
public class DictionaryTypeController extends BaseController<DictionaryType> implements DictionaryTypeApi {
    @Autowired
    private DictionaryTypeService dictionaryTypeService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtil cacheUtil;
    @Override
    public BaseService getService() {
        return dictionaryTypeService;
    }

    /**
     * 新增数据字典
     * @param entity
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo insertSimple(@RequestBody @Validated(addDicType.class) DictionaryType entity) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        entity.setCuser(loginUser.getUserId());
        entity.setEuser(loginUser.getUserId());
        dictionaryTypeService.addDicEntity(entity);
        return BaseResultVo.success("",entity.getId());
    }
    @Override
    public BaseResultVo updateById(@RequestBody @Validated(EditDicType.class) DictionaryType entity) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        entity.setEuser(loginUser.getUserId());
        dictionaryTypeService.editDicEntity(entity);
        return BaseResultVo.success("",entity.getId());
    }
    /**
     * 删除数据字典
     * @param dicId
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo deleteDic( @Validated(IdMustDicType.class) DictionaryType dicId) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        dicId.setEuser(loginUser.getUserId());
        dictionaryTypeService.deleteDic(dicId);
        return BaseResultVo.success("",dicId.getId());
    }
    /**
     * 分页查询数据字典
     * @param param
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<PageBean<DictionaryType>> getPage( @Validated(PageParams.ParamNotNullGroup.class) PageParams<DicEntityId> param) throws Exception{
        List<DictionaryType> pageList = dictionaryTypeService.getDiactionaryList(param);
        PageBean<DictionaryType> page = new PageBean<DictionaryType>(pageList);
        return BaseResultVo.success(page);

    }
    /**
     *根据code查询数据字典项
     * @param dicCode
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo getDicByCode( @Validated DicCodeParam dicCode ) throws Exception{
        List<DictionaryResult> dicByCode = dictionaryTypeService.getDicByCode(dicCode.getCode());return BaseResultVo.success(dicByCode);
    }
    @Override
    public BaseResultVo queryDicTypeInfo(@Validated(IdMustDicType.class)  DictionaryType idParanEntity) throws Exception{
        DictionaryType dictionaryType = dictionaryTypeService.selectByPrimaryKey(idParanEntity.getId());
        return  BaseResultVo.success(dictionaryType);
    }
}
