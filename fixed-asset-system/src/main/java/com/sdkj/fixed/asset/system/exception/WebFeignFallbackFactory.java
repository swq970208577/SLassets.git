package com.sdkj.fixed.asset.system.exception;


import com.sdkj.fixed.asset.common.base.BaseResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;

@Component
public class WebFeignFallbackFactory implements FallbackFactory<BaseResultVo>{
	private static final Logger log = LoggerFactory.getLogger(WebFeignFallbackFactory.class);

	@Override
	public BaseResultVo create(Throwable arg0) {
		log.error("服务降级处理异常抛出",arg0.getStackTrace());
		return BaseResultVo.failure("服务降级处理异常抛出");
	}
}