package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.system.PushApi;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.PushManagement;
import com.sdkj.fixed.asset.system.service.PushService;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName PushController
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/29 17:08
 */
@Controller
public class PushController implements PushApi  {
    @Autowired
    private PushService pushService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private HttpServletRequest request;
    /**
     * 分页查询用户
     *
     * @param params
     * @return
     */
    @Override
    public BaseResultVo getPagePush(PageParams params) throws Exception {
        List<PushManagement> list = pushService.getPagePush(params, cacheUtil.getUserId(request.getHeader("token")));
        PageBean page = new PageBean(list);
        return BaseResultVo.success(page);
    }
}
