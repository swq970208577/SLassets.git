package com.sdkj.fixed.asset.system.controller;


import cn.hutool.core.date.DateUtil;
import com.sdkj.fixed.asset.api.system.LogApi;
import com.sdkj.fixed.asset.api.system.in_vo.LogEntity;
import com.sdkj.fixed.asset.api.system.in_vo.LogIdGroup;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.pojo.system.LogManagement;
import com.sdkj.fixed.asset.system.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author
 * @ClassName:
 * @Description:日志查询实体控制层
 * @date 2019年10月10日
 */

@Controller

public class LogController extends BaseController<LogManagement> implements LogApi {
    private static Logger log = LoggerFactory.getLogger(LogController.class);

    @Autowired
    private LogService service;

    @Override
    public BaseService getService() {
        return service;
    }


    /**
     * 日志列表查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<PageBean<LogManagement>> getPages(@RequestBody @Validated(PageParams.ParamNotNullGroup.class) PageParams<LogEntity> params )throws Exception {
            List<LogManagement> pageList = service.getPages(params);
            PageBean<LogManagement> page = new PageBean(pageList);
            return BaseResultVo.success(page);
    }

    /**
     * 查看日志详情
     *
     * @param logEntity
     * @return
     */
    @Override
    public BaseResultVo viewLog(@RequestBody @Validated({LogIdGroup.class}) LogEntity logEntity)throws Exception {
        LogManagement log = service.viewLog(logEntity.getId());
        return BaseResultVo.success(log);
    }

    @Override
    public BaseResultVo insertLog(LogManagement log) throws Exception {
        log.setCtime(DateUtil.now());
        log.setEtime(DateUtil.now());
        log.setState(1);
        service.insertSelective(log);

        return BaseResultVo.success(log.getId());
    }
}
