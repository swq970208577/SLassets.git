package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.DictionaryType;

public interface DictionaryTypeMapper extends BaseMapper<DictionaryType> {
}