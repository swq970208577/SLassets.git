package com.sdkj.fixed.asset.system.util;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import cn.hutool.core.util.StrUtil;
import com.sdkj.fixed.asset.pojo.OrgImportManagement;
import com.sdkj.fixed.asset.system.service.OrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * @ClassName OrgImportVerify
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/28 15:05
 */

@Component
public class OrgImportVerify implements IExcelVerifyHandler<OrgImportManagement> {
    @Autowired
    private OrgService orgService;
    @Override
    public ExcelVerifyHandlerResult verifyHandler(OrgImportManagement orgImportManagement) {
        ExcelVerifyHandlerResult result = new ExcelVerifyHandlerResult();
        if((!orgService.isAdmin()) && StrUtil.isBlank(orgImportManagement.getPcode())){
            result.setMsg("【非超级管理员导入公司,上级公司编码必填】");
            result.setSuccess(false);
            return result;
        }
        if(orgService.isAdmin() && StrUtil.isNotBlank(orgImportManagement.getPcode())){
            result.setMsg("【超级管理员导入公司,上级公司编码必需为空】");
            result.setSuccess(false);
            return result;
        }
        result.setSuccess(true);

        return result;
    }
}
