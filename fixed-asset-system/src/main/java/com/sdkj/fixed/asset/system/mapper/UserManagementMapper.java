package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.api.system.in_vo.UserConditionEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.assets.SetArea;
import com.sdkj.fixed.asset.pojo.assets.SetClass;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserManagementMapper extends BaseMapper<UserManagement> {
    /**
     * 通过组织架构查用户
     * @param userConditionEntity
     * @return
     */
    public List<UserManagement> selectUserByOrgId(UserConditionEntity userConditionEntity);
    /**
     * 通过组织架构查用户
     * @param companyId
     * @return
     */
    public List<UserManagement> selectAllUserByOrgId(@Param("companyId") String companyId);
    public List<Map<String,String>> getUserRoles(@Param("userId") String userId);

    /**
     * 查询启用资产类别
     * @param companyId
     * @return
     */
    public List<SetClass> getAllEnableAssetClass(@Param("companyId") String companyId);
    /**
     * 查询启用资产类别
     * @param companyId
     * @return
     */
    public List<SetArea> getAllEnableArea(@Param("companyId") String companyId);
}