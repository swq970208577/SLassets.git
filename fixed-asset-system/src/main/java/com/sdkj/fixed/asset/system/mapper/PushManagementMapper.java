package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.PushManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PushManagementMapper extends BaseMapper<PushManagement> {
    public List<PushManagement>  getLoginUserPush(@Param("userId") String userId);
}