package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.OrgManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @ClassName OrgMapper
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/20 15:51
 */
public interface OrgManagementMapper extends BaseMapper<OrgManagement> {
    /**
     * 查询权限下启用状态的公司
     * @param userId
     * @return
     */
    public List<OrgManagement> getAuthEnableCompany(@Param("userId") String userId);
    /**
     * 查询权限下启用状态的组织架构
     * @param userId
     * @return
     */
    public List<OrgManagement> getAuthEnableOrg(@Param("userId") String userId);

    /**
     * 查询权限下启用状态的部门
     *
     * @param userId
     * @return
     */
    public List<OrgManagement> getAuthEnableDeptByCompanyId(@Param("userId") String userId, @Param("companyId") String companyId);

    public List<OrgManagement> getAuthEnableRootDeptByCompanyId(@Param("userId") String userId, @Param("companyId") String companyId);

    /**
     * 获取部门顶级
     *
     * @param id
     * @return
     */
    OrgManagement getPid(String id);
}
