package com.sdkj.fixed.asset.system.mapper;

import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.RoleAndMenu;

;import java.util.List;
import java.util.Map;

public interface RoleAndMenuMapper extends BaseMapper<RoleAndMenu> {
    /**
     * 查询除园区管理员外绑定某些菜单的角色
     */
    public List<String> getNoParkAdminBindAuth(Map param);
}