package com.sdkj.fixed.asset.system.controller;


import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.assets.in_vo.AssroParam;
import com.sdkj.fixed.asset.api.assets.in_vo.BorrowIssueExtend;
import com.sdkj.fixed.asset.api.system.FedApi;
import com.sdkj.fixed.asset.api.system.LogApi;
import com.sdkj.fixed.asset.api.system.in_vo.FedParam;
import com.sdkj.fixed.asset.api.system.in_vo.FededitParam;
import com.sdkj.fixed.asset.api.system.in_vo.LogEntity;
import com.sdkj.fixed.asset.api.system.in_vo.LogIdGroup;
import com.sdkj.fixed.asset.api.system.out_vo.FedEntity;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.pojo.assets.AssApproval;
import com.sdkj.fixed.asset.pojo.system.FedManagement;
import com.sdkj.fixed.asset.pojo.system.LogManagement;
import com.sdkj.fixed.asset.system.service.FedService;
import com.sdkj.fixed.asset.system.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author
 * @ClassName:
 * @Description:日志查询实体控制层
 * @date 2019年10月10日
 */

@Controller
public class FedController extends BaseController<FedManagement> implements FedApi {
    private static Logger log = LoggerFactory.getLogger(FedController.class);

    @Autowired
    private FedService service;

    @Override
    public BaseService getService() {
        return service;
    }

    /**
     * 新增问题反馈
     *
     * @param entity
     * @param userstate
     * @return
     */
    @Override
    public BaseResultVo add(FedManagement entity, String userstate) throws Exception {
        service.add(entity, userstate);
        return BaseResultVo.success(entity);
    }

    /**
     * 问题反馈查询
     *
     * @param params
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<FedEntity> getAllPage(PageParams<FedParam> params) throws Exception {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<FedEntity> pageList = service.getAllPage(params);
        PageBean<FedEntity> page = new PageBean<FedEntity>(pageList);
        return BaseResultVo.success(page);
    }

    /**
     * 查看日志详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo see(String id) throws Exception {
        FedEntity fedEntity = service.see(id);
        return BaseResultVo.success(fedEntity);
    }

    /**
     * 问题反馈修改
     *
     * @param param
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo edit(FededitParam param) throws Exception {
        FedManagement fedManagement = service.edit(param);
        return BaseResultVo.success(fedManagement);
    }


}
