package com.sdkj.fixed.asset.system.service;


import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.system.in_vo.LogEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.AuthAction;
import com.sdkj.fixed.asset.pojo.system.LogManagement;
import com.sdkj.fixed.asset.pojo.system.UserManagement;
import com.sdkj.fixed.asset.system.mapper.AuthActionMapper;
import com.sdkj.fixed.asset.system.mapper.LogManagementMapper;
import com.sdkj.fixed.asset.system.mapper.UserManagementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/***
 *
 * @author 赵哲宇
 *日志查询
 */
@Service
public class LogService extends BaseService<LogManagement> {


    @Autowired
    private LogManagementMapper mapper;

    @Autowired
    private AuthActionMapper authActionMapper;
    @Autowired
    private UserManagementMapper userManagementMapper;
    @Override
    public BaseMapper<LogManagement> getMapper() {
        return mapper;
    }

    /**
     * 查询日志
     *
     * @param
     * @return
     * @throws Exception
     */
    public List<LogManagement> getPages(PageParams<LogEntity> params) throws Exception {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<LogManagement> pageList = mapper.getLogList(params.getParams());
        return pageList;
    }

    /**
     * 查看日志详情
     *
     * @return
     * @throws Exception
     */
    public LogManagement viewLog(String id) throws Exception {
        LogManagement logManagement = mapper.selectByPrimaryKey(id);
        if (logManagement == null) {     //判断id是否存在,
            throw new RuntimeException("日志不存在【" + id + "】");
        }
        List<AuthAction> authName = mapper.getAuthName(logManagement.getUrl());
        if(authName.size() == 0){
            logManagement.setBname("");
        }else{
            logManagement.setBname(authName.get(0).getName());
        }

        UserManagement userManagement = userManagementMapper.selectByPrimaryKey(logManagement.getCuser());
        if(userManagement == null){
            logManagement.setCuser("");
        }else{
            logManagement.setCuser(userManagement.getName());
        }
        return logManagement;
    }
}
