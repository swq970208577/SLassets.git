package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.DictionaryEntity;

public interface DictionaryEntityMapper extends BaseMapper<DictionaryEntity> {
}