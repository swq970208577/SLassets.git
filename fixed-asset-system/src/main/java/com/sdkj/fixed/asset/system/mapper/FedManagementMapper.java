package com.sdkj.fixed.asset.system.mapper;

import com.sdkj.fixed.asset.api.system.in_vo.FedParam;
import com.sdkj.fixed.asset.api.system.in_vo.FededitParam;
import com.sdkj.fixed.asset.api.system.out_vo.FedEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.FedManagement;

import java.util.List;

public interface FedManagementMapper extends BaseMapper<FedManagement> {


    /**
     * 问题反馈查询
     *
     * @param fedParam
     * @return
     */
    List<FedEntity> getFedList(FedParam fedParam);

    /**
     * 问题反馈详情
     *
     * @param id
     * @return
     */
    FedEntity getFedSee(String id);


}