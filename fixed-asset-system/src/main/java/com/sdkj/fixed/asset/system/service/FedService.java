package com.sdkj.fixed.asset.system.service;


import cn.hutool.core.date.DateUtil;
import com.alibaba.csp.sentinel.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.api.system.in_vo.FedParam;
import com.sdkj.fixed.asset.api.system.in_vo.FededitParam;
import com.sdkj.fixed.asset.api.system.out_vo.FedEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;

import com.sdkj.fixed.asset.common.base.PageParams;

import com.sdkj.fixed.asset.pojo.system.FedManagement;

import com.sdkj.fixed.asset.system.mapper.FedManagementMapper;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/***
 *
 * @author 赵哲宇
 *日志查询
 */
@Service
public class FedService extends BaseService<FedManagement> {


    @Autowired
    private FedManagementMapper fedManagementMapper;

    @Override
    public BaseMapper getMapper() {
        return fedManagementMapper;
    }

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtil cacheUtil;

    /**
     * 新增问题反馈
     *
     * @param entity
     * @param userstate
     * @return
     */
    public FedManagement add(FedManagement entity, String userstate) {
        //userstate 1未登录用户2已登录用户
        if (userstate.equals("1")) {
            entity.setCuser(null);
            entity.setCompanyId(null);
        } else {
            String orgId = request.getHeader("orgId");
            String userId = request.getHeader("userId");
            entity.setCuser(userId);
            entity.setCompanyId(orgId);
        }
        entity.setState(2);
        entity.setCtime(DateUtil.now());
        fedManagementMapper.insertSelective(entity);
        return entity;

    }

    /**
     * 修改问题详情
     *
     * @param param
     * @return
     * @throws Exception
     */
    public FedManagement edit(FededitParam param) {
        FedManagement fedManagement = fedManagementMapper.selectByPrimaryKey(param.getId());
        if (StringUtil.isNotBlank(param.getSolution())) {
            fedManagement.setState(1);
            fedManagement.setSolution(param.getSolution());
        }
        fedManagementMapper.updateByPrimaryKeySelective(fedManagement);
        return fedManagement;
    }

    /**
     * 查看问题详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    public FedEntity see(String id) {
        if (fedManagementMapper.selectByPrimaryKey(id) == null) {
            throw new RuntimeException("问题不存在【" + id + "】");
        }
        FedEntity entity = fedManagementMapper.getFedSee(id);
        if (StringUtil.isBlank(entity.getUsername())) {
            entity.setOrgname(entity.getTel());
            entity.setUsername("未登录用户");
        }

        return entity;

    }

    /**
     * 问题反馈查询
     *
     * @param params
     * @return
     */
    public List<FedEntity> getAllPage(PageParams<FedParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        PageHelper.orderBy("sfm.ctime desc");
        List<FedEntity> pageList = fedManagementMapper.getFedList(params.getParams());
        for (FedEntity entity : pageList) {
            if (StringUtil.isBlank(entity.getUsername())) {
                entity.setOrgname(entity.getTel());
                entity.setUsername("未登录用户");
            }
        }
        return pageList;
    }
}
