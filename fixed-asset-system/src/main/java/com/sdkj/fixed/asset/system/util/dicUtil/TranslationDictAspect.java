package com.sdkj.fixed.asset.system.util.dicUtil;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sdkj.fixed.asset.system.service.DictionaryTypeService;
import com.sdkj.fixed.asset.system.util.RedisUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.alibaba.fastjson.JSON.toJSONString;


/**
 * 翻译字典值处理类
 * @author 张欣
 */

@Aspect
@Component
public class TranslationDictAspect {

    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private DictionaryTypeService dictionaryTypeService;
    /**
     * 非基本类型在 CLASS 中的定义
     */
    private static final String FILED_NAME_TYPE = "TYPE";

    private Map<String,String> dictInfoMap = new ConcurrentHashMap<>();

    @Around("@annotation(translationDict)")
    public Object Translation(final ProceedingJoinPoint pjp, TranslationDict translationDict) throws Throwable {
        Object result = pjp.proceed();
        // 第一步、获取返回值类型
        Class returnType = ((MethodSignature) pjp.getSignature()).getReturnType();

        //首先，取出要翻译字段的字典值
        String returnJsonResult = toJSONString(result,SerializerFeature.WriteMapNullValue);

        DictParam[] dictParams = translationDict.value();
        if(result instanceof List || result instanceof ArrayList){
            for(Object obj : (List) result){
                setDicName(obj,dictParams);
            }
        }else{
            setDicName(result,dictParams);
        }

        return result;
    }
   public void setDicName(Object obj,DictParam[] dictParams) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
       for (DictParam dictParam : dictParams) {
           String dicValues = dictParam.dicValues();
           if(dicValues.equals("")){
               //获取数据字典值
               dictInfoMap = dictionaryTypeService.getDicValueByCode(dictParam.dictCode());
           }else {
               //获取数据字典值
               dictInfoMap =  JSON.parseObject(dicValues,Map.class);
           }
           //需要翻译的字段名
           String filedNme = dictParam.dictValueFiled();
           String filedName = dictParam.dictNameFiled();
           String value = BeanUtils.getProperty(obj,filedNme);
           if(value != null){
               String name = dictInfoMap.get(value);
               if(name == null){
                   name = dictInfoMap.get(Integer.parseInt(name));
               }

               BeanUtils.setProperty(obj,filedName,name == null?value:name);
           }
       }
   }



}


