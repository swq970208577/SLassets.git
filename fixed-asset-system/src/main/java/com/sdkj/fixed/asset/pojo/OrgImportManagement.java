package com.sdkj.fixed.asset.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelDataModel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import com.sdkj.fixed.asset.pojo.UUIdGenId;
import com.sdkj.fixed.asset.pojo.system.group.IdMustOrg;
import com.sdkj.fixed.asset.pojo.system.group.OrgEdit;
import org.hibernate.validator.constraints.Range;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 组织架构实体
 * @author 张欣
 * @date  2020-07-20 : 02:06:39
 */

public class OrgImportManagement implements Serializable , IExcelDataModel, IExcelModel {
    /**
     * 行号
     */

    private int rowNum;

    /**
     * 错误消息
     */

    private String errorMsg;


    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @Size(max = 32, message = "名称长度不匹配")
    @Excel(name = "公司名称（必填）")
    private String name;

    /**
     * 父id
     */
    @Size(min = 4,max = 4, message = "上级公司编码长度为4")
    @Excel(name = "上级公司编码")
    private String pcode;
    /**
     * 编码
     */
    @Size(min = 4,max = 4, message = "公司编码长度为4")
    @NotBlank(message = "公司编码不能为空")
    @Excel(name = "公司编码（必填）")
    private String code;

    @Override
    public int getRowNum() {
        return rowNum;
    }

    @Override
    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}