package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.api.system.out_vo.AuthTreeResultEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.RoleManagement;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface RoleManagementMapper extends BaseMapper<RoleManagement> {
    /**
     * 获取菜单树，并标记当前角色是否绑定
     * @param  roleId
     * @return
     */
    public List<AuthTreeResultEntity> getAllMenu(@Param("roleId") String roleId);
    /**
     * 获取菜单树，并标记当前角色是否绑定
     * @param  roleId
     * @return
     */
    public List<AuthTreeResultEntity> getAuthMenu(@Param("roleId") String roleId,@Param("orgId") String orgId);

    public String getAllParentId(String id);
    /**
     * 获取菜单
     * @param menuIds
     * @return
     */
    public List<AuthTreeResultEntity> getMenusByMenuId(Set<String> menuIds);
}