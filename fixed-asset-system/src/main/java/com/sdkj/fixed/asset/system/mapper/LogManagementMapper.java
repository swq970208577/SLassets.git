package com.sdkj.fixed.asset.system.mapper;

import com.sdkj.fixed.asset.api.system.in_vo.LogEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.AuthAction;
import com.sdkj.fixed.asset.pojo.system.LogManagement;
import feign.Param;

import java.util.List;

/**
 * 日志管理
 *
 */
public interface LogManagementMapper extends BaseMapper<LogManagement> {
    //日志详情
    List<AuthAction> getAuthName(@Param("url") String url);

    //日志查询

    List<LogManagement>getLogList(LogEntity logEntity);


}