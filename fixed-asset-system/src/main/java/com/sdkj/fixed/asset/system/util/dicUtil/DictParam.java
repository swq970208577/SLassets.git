package com.sdkj.fixed.asset.system.util.dicUtil;


import java.lang.annotation.*;

/**
 * 需要翻译的字典值
 * @author 张欣
 * */
@Target(value={ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictParam {

    /**
     * 字典CODE
     * @return
     */
    String dictCode() default "";

    /**
     * 需要翻译的字段名
     * @return
     */
    String dictValueFiled() default "";
    /**
     * 需要翻译的字段名
     * @return
     */
    String dictNameFiled() default "";
    /**
     * 需要翻译的字段名{"0":"是"；"1"："否"}
     * @return
     */
    String dicValues() default "";
    /**
     * parkIdzi字段标识
     * @return
     */
    String parkIdFiled() default "";

}

