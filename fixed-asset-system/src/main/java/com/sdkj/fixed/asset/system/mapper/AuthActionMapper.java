package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.api.system.out_vo.MenuTreeResultEntity;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.AuthAction;
import com.sdkj.fixed.asset.system.util.dicUtil.DictParam;
import com.sdkj.fixed.asset.system.util.dicUtil.TranslationDict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthActionMapper extends BaseMapper<AuthAction> {
    @TranslationDict({@DictParam(dictValueFiled = "level",dictNameFiled = "level",dictCode = "SYS_MENU_LEVEL")})
    public List<MenuTreeResultEntity> getMenuTree(@Param("name")String name);


}