package com.sdkj.fixed.asset.system.mapper;

import com.sdkj.fixed.asset.api.system.in_vo.UsageParam;
import com.sdkj.fixed.asset.api.system.out_vo.UsageResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;

import java.util.List;

public interface UsageManagementMapper extends BaseMapper<UsageManagement> {

    public List<UsageResult> getPages(UsageParam param);

    public UsageResult selById(String id);


    //通过公司id删除角色表
    void getDel(String companyId);
}