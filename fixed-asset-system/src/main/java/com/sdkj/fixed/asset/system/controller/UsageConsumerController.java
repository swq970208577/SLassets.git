package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.system.UsageApi;
import com.sdkj.fixed.asset.api.system.UsageConsumerApi;
import com.sdkj.fixed.asset.api.system.in_vo.UsageModifyParam;
import com.sdkj.fixed.asset.api.system.in_vo.UsageParam;
import com.sdkj.fixed.asset.api.system.out_vo.UsageResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;
import com.sdkj.fixed.asset.system.mapper.UsageManagementMapper;
import com.sdkj.fixed.asset.system.service.UsageService;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/9 10:29
 */
@Controller
public class UsageConsumerController implements UsageConsumerApi {
    @Autowired
    private UsageService usageService;
    @Autowired
    HttpServletRequest request;

    @Override
    public BaseResultVo getDxpireDayByCompanyId(String companyId) throws Exception {
        return BaseResultVo.success(usageService.expireDay(companyId));
    }
}
