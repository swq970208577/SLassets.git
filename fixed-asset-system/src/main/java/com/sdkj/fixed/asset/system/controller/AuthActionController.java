package com.sdkj.fixed.asset.system.controller;


import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.api.system.AuthActionApi;
import com.sdkj.fixed.asset.api.system.out_vo.MenuTreeEntity;
import com.sdkj.fixed.asset.common.base.BaseController;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.pojo.system.AuthAction;
import com.sdkj.fixed.asset.pojo.system.group.AuthMenuEdit;
import com.sdkj.fixed.asset.pojo.system.group.IdMustAuthEntity;
import com.sdkj.fixed.asset.system.service.AuthActionService;

import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 菜单权限控制类
 * @author 张欣
 */
@Controller
public class AuthActionController extends BaseController<AuthAction> implements AuthActionApi {
    @Autowired
    private AuthActionService authActionService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtil cacheUtil;
    /**
     * 新增菜单
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo<String> insertSimple(@RequestBody @Validated AuthAction entity){
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        entity.setCuser(loginUser.getUserId());
        entity.setEuser(loginUser.getUserId());
        entity = authActionService.insertMenu(entity);
        return BaseResultVo.success("",entity.getId());
    }
    /**
     * 修改菜单
     * @param entity
     * @return
     */
    @Override
    public BaseResultVo updateById(@RequestBody @Validated(AuthMenuEdit.class) AuthAction entity) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        entity.setEuser(loginUser.getUserId());
        authActionService.editMenu(entity);
        return BaseResultVo.success("",entity.getId());
    }
    /**
     * 删除菜单
     * @param idParanEntity
     * @return
     */
    @Override
    public BaseResultVo deleteMenu(@Validated(IdMustAuthEntity.class) AuthAction idParanEntity){
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        idParanEntity.setEuser(loginUser.getUserId());
        authActionService.deleteMenu(idParanEntity);
        return  BaseResultVo.success();
    }
    /**
     *获取菜单详情
     * @param idParanEntity
     * @return
     */
    @Override
    public BaseResultVo queryMenuInfo(@Validated(IdMustAuthEntity.class) AuthAction idParanEntity) throws Exception {
        AuthAction authAction = authActionService.selectByPrimaryKey(idParanEntity.getId());
        return  BaseResultVo.success(authAction);
    }
    /**
     * 获取所有菜单
     * @return
     */
    @Override

    public BaseResultVo<List<MenuTreeEntity>> getMenuTrees(String name){
        List<MenuTreeEntity> menuTree = authActionService.getMenuTrees(name);
        return  BaseResultVo.success(menuTree);

    }
    @Override
    public BaseService getService() {
        return authActionService;
    }
}
