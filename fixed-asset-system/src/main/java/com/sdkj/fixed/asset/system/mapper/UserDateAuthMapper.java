package com.sdkj.fixed.asset.system.mapper;


import com.sdkj.fixed.asset.api.system.out_vo.AssestClass;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.pojo.system.UserDateAuth;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDateAuthMapper extends BaseMapper<UserDateAuth> {
    public List<AssestClass> getAssetClassList(@Param("ids") List<String> ids);
    public List<String> getRegionList(@Param("ids") List<String> ids);
    public List<String> getWarehouseList(@Param("ids") List<String> ids);
}