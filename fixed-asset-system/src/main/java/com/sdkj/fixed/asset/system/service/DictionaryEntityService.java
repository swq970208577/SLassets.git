package com.sdkj.fixed.asset.system.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.pojo.system.DictionaryEntity;
import com.sdkj.fixed.asset.pojo.system.DictionaryType;
import com.sdkj.fixed.asset.system.mapper.DictionaryEntityMapper;
import com.sdkj.fixed.asset.system.mapper.DictionaryTypeMapper;
import com.sdkj.fixed.asset.system.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @ClassName DictionaryEntityService
 * @Description 数据字典
 * @Author 张欣
 * @Date 2020/7/21 16:25
 */
@Service
public class DictionaryEntityService extends BaseService<DictionaryEntity> {
    @Autowired
    private DictionaryEntityMapper dictionaryEntityMapper;
    @Autowired
    private DictionaryTypeMapper dictionaryTypeMapper;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public BaseMapper getMapper() {
        return dictionaryEntityMapper;
    }

    /**
     * 新增数据字典
     * @param entity
     */
    @Transactional(rollbackFor = Exception.class)
    public void addDicEntity(DictionaryEntity entity){
        //校验code唯一性
        validateCode(entity.getCode(),"");
        entity.setCtime(DateUtil.now());
        entity.setEtime(DateUtil.now());
        entity.setState(1);
        dictionaryEntityMapper.insertSelective(entity);
    }
    private void validateCode(String code,String id){
        Example example =new Example(DictionaryEntity.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code",code);
        criteria.andEqualTo("state",1);
        if(StrUtil.isNotBlank(id)){
            criteria.andNotEqualTo("id",id);
        }
        List<DictionaryEntity> dictionaryEntities = dictionaryEntityMapper.selectByExample(example);
        if(dictionaryEntities.size() > 0){
            throw new LogicException("编码已存在");
        }
    }

    /**
     * 修改
     * @param entity
     */
    @Transactional(rollbackFor = Exception.class)
    public void editDicEntity(DictionaryEntity entity){
        DictionaryEntity dictionaryEntity = dictionaryEntityMapper.selectByPrimaryKey(entity.getId());
        if(dictionaryEntity == null || dictionaryEntity.getState() == 2){
            throw new LogicException("数据字典不存在");
        }
        //校验code唯一性
        //validateCode(entity.getCode(),entity.getCode());
        dictionaryEntity.setEuser(entity.getEuser());
        dictionaryEntity.setName(entity.getName());
        dictionaryEntity.setEtime(DateUtil.now());
        dictionaryEntity.setComments(entity.getComments());
        dictionaryEntityMapper.updateByPrimaryKey(dictionaryEntity);
    }

    /**
     * 删除
     * @param entity
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteDic(DictionaryEntity entity){
        DictionaryEntity dictionaryEntity = dictionaryEntityMapper.selectByPrimaryKey(entity.getId());
        if(dictionaryEntity == null || dictionaryEntity.getState() == 2){
            throw new LogicException("数据字典不存在");
        }
        //查询其下是否有数据字典项
        Example example = new Example(DictionaryType.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("sdeId",entity.getId());
        criteria.andEqualTo("state",1);
        List<DictionaryType> dictionaryTypes = dictionaryTypeMapper.selectByExample(example);
        if(dictionaryTypes.size() > 0){
            throw new LogicException("数据字典下存在数据字典项，不可以删除");
        }
        DictionaryEntity dictionaryEntity1 = new DictionaryEntity();
        dictionaryEntity1.setId(entity.getId());
        dictionaryEntity1.setState(2);
        dictionaryEntity1.setEuser(entity.getEuser());
        dictionaryEntity1.setEtime(DateUtil.now());
        dictionaryEntityMapper.updateByPrimaryKeySelective(dictionaryEntity1 );
    }
    public List<DictionaryEntity> getDiactionaryList(PageParams<DictionaryEntity> params){
        Example example = new Example(DictionaryEntity.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("state",1);
        DictionaryEntity param = params.getParams();
        if(param != null){
            if(StrUtil.isNotBlank(param.getName())){
                criteria.andLike("name","%"+param.getName()+"%");
            }
            if(StrUtil.isNotBlank(param.getCode())){
                criteria.andLike("code","%"+param.getCode()+"%");
            }
        }
        example.setOrderByClause("ctime DESC");
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        List<DictionaryEntity> dictionaryTypes = dictionaryEntityMapper.selectByExample(example);
        return dictionaryTypes;
    }
}
