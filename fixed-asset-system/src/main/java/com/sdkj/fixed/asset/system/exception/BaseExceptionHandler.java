package com.sdkj.fixed.asset.system.exception;


import com.sdkj.fixed.asset.common.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BaseExceptionHandler extends GlobalExceptionHandler {

}