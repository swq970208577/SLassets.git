package com.sdkj.fixed.asset.system.service;

import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.api.system.in_vo.UsageModifyParam;
import com.sdkj.fixed.asset.api.system.in_vo.UsageParam;
import com.sdkj.fixed.asset.api.system.out_vo.UsageResult;
import com.sdkj.fixed.asset.common.base.BaseMapper;
import com.sdkj.fixed.asset.common.base.BaseService;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.common.utils.TimeTool;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;
import com.sdkj.fixed.asset.system.controller.UsageController;
import com.sdkj.fixed.asset.system.mapper.UsageManagementMapper;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/8 16:02
 */
@Service
public class UsageService extends BaseService<UsageManagement> {
    private static Logger log = LoggerFactory.getLogger(UsageController.class);
    @Autowired
    private UsageManagementMapper mapper;

    @Override
    public BaseMapper getMapper() {
        return mapper;
    }

    public Integer expireDay(String companyId) {
        Example example = new Example(UsageManagement.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("companyId", companyId);
        UsageManagement management = mapper.selectOneByExample(example);
        if(management == null){
            return null;
        }
        int day = TimeTool.countDayMinus(TimeTool.parseDate(TimeTool.getTimeDate10(), "yyyy-MM-dd"), TimeTool.parseDate(management.getDate(), "yyyy-MM-dd"));
        return day;
    }

    public List<UsageResult> getPages(PageParams<UsageParam> params) {
        PageHelper.startPage(params.getCurrentPage(), params.getPerPageTotal());
        PageHelper.orderBy("a.ctime desc");
        return mapper.getPages(params.getParams());
    }

    public void modify(UsageModifyParam param) {
        UsageManagement management = new UsageManagement();
        management.setDate(param.getExpireDate());
        management.setType(param.getType());
        management.setNumber(param.getNumber());
        management.setId(param.getId());
        mapper.updateByPrimaryKeySelective(management);
    }

    public UsageResult deatil(String id) {
        return mapper.selById(id);
    }

    /**
     * 删除公司所有相关内容
     *
     * @param companyId
     */
    @Transactional
    public void delete(String companyId) throws Exception {
        //查过期天数
        int day = expireDay(companyId);
        if (day > -15) {
            throw new LogicException("当前公司过期天数不足15天,不能删除【" + day + "】");
        }
        //删除全部数据
        mapper.getDel(companyId);
        log.info("删除完成");

    }

}
