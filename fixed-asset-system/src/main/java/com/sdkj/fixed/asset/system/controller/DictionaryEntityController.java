package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.login.pojo.LoginUser;
import com.sdkj.fixed.asset.api.system.DictionaryEntityApi;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.pojo.system.DictionaryEntity;
import com.sdkj.fixed.asset.pojo.system.group.DictionaryEntityEdit;
import com.sdkj.fixed.asset.pojo.system.group.IdMustDicEntity;
import com.sdkj.fixed.asset.system.service.DictionaryEntityService;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName DictionaryEntityController
 * @Description 数据字典控制类
 * @Author 张欣
 * @Date 2020/7/21 16:22
 */
@Controller
public class DictionaryEntityController extends BaseController<DictionaryEntity> implements DictionaryEntityApi {
    @Autowired
    private DictionaryEntityService dictionaryEntityService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtil cacheUtil;
    @Override
    public BaseService getService() {
        return dictionaryEntityService;
    }

    /**
     * 新增数据字典
     * @param entity
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo insertSimple(@RequestBody @Validated DictionaryEntity entity) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        entity.setCuser(loginUser.getUserId());
        entity.setEuser(loginUser.getUserId());
        dictionaryEntityService.addDicEntity(entity);
        return BaseResultVo.success("",entity.getId());
    }
    @Override
    public BaseResultVo updateById(@RequestBody @Validated(DictionaryEntityEdit.class) DictionaryEntity entity) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        entity.setEuser(loginUser.getUserId());
        dictionaryEntityService.editDicEntity(entity);
        return BaseResultVo.success("",entity.getId());
    }
    /**
     * 删除数据字典
     * @param dicId
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo deleteDic(@RequestBody @Validated(IdMustDicEntity.class) DictionaryEntity dicId) throws Exception{
        String token = request.getHeader("token");
        LoginUser loginUser = cacheUtil.getUser(token);
        dicId.setEuser(loginUser.getUserId());
        dictionaryEntityService.deleteDic(dicId);
        return BaseResultVo.success("",dicId.getId());
    }
    /**
     * 分页查询数据字典
     * @param param
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo<PageBean<DictionaryEntity>> getPage(@RequestBody PageParams<DictionaryEntity> param) throws Exception{
        List<DictionaryEntity> pageList = dictionaryEntityService.getDiactionaryList(param);
        PageBean<DictionaryEntity> page = new PageBean<DictionaryEntity>(pageList);
        return BaseResultVo.success(page);

    }
    /**
     *获取菜单详情
     * @param idParanEntity
     * @return
     */
    @Override
    public BaseResultVo queryDicInfo(@Validated(IdMustDicEntity.class) DictionaryEntity idParanEntity) throws Exception {
        DictionaryEntity dic = dictionaryEntityService.selectByPrimaryKey(idParanEntity.getId());
        return  BaseResultVo.success(dic);
    }
}
