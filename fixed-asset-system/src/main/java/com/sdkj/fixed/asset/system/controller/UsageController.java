package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.system.UsageApi;
import com.sdkj.fixed.asset.api.system.UsageConsumerApi;
import com.sdkj.fixed.asset.api.system.in_vo.UsageModifyParam;
import com.sdkj.fixed.asset.api.system.in_vo.UsageParam;
import com.sdkj.fixed.asset.api.system.out_vo.UsageResult;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageBean;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.common.core.RoleType;
import com.sdkj.fixed.asset.pojo.system.UsageManagement;
import com.sdkj.fixed.asset.system.mapper.UsageManagementMapper;
import com.sdkj.fixed.asset.system.service.UsageService;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author niuliwei
 * @description
 * @date 2021/4/9 10:29
 */
@Controller
public class UsageController implements UsageApi{
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private UsageService usageService;
    @Autowired
    private UsageManagementMapper mapper;

    @Override
    public BaseResultVo expireDay() throws Exception {
        String token = request.getHeader("token");
        return BaseResultVo.success(usageService.expireDay(cacheUtil.getUserCompanyId(token)));
    }

    @Override
    public BaseResultVo<UsageResult> getPages(PageParams<UsageParam> params) throws Exception {
        PageBean pageBean = new PageBean(usageService.getPages(params));
        return BaseResultVo.success(pageBean);
    }

    @Override
    public BaseResultVo modify(UsageModifyParam param) throws Exception {
        usageService.modify(param);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo<UsageResult> deatil(String id) throws Exception {
        return BaseResultVo.success(usageService.deatil(id));
    }

    @Override
    public BaseResultVo delete(String companyId) throws Exception {
        usageService.delete(companyId);
        return BaseResultVo.success();
    }

    @Override
    public BaseResultVo getUsage() throws Exception {
        String token = request.getHeader("token");
        List<String> roles = cacheUtil.getUserRole(token);
        boolean flag =roles.contains(RoleType.SYSADMIN.getRoleType());
        if(flag){
            UsageManagement ment = new UsageManagement();
            ment.setType(3);
            return BaseResultVo.success(ment);
        } else {
            Example example = new Example(UsageManagement.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("companyId", cacheUtil.getUserCompanyId(token));
            UsageManagement ment = mapper.selectOneByExample(example);
            return BaseResultVo.success(ment);
        }
    }

}
