package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.system.RoleApi;
import com.sdkj.fixed.asset.api.system.in_vo.BindAuthParamEntity;
import com.sdkj.fixed.asset.api.system.in_vo.RoleBindUser;
import com.sdkj.fixed.asset.api.system.in_vo.RoleCondition;
import com.sdkj.fixed.asset.api.system.out_vo.AuthTreeResultEntity;
import com.sdkj.fixed.asset.common.base.*;
import com.sdkj.fixed.asset.common.core.RoleType;
import com.sdkj.fixed.asset.common.exception.LogicException;
import com.sdkj.fixed.asset.pojo.system.RoleManagement;
import com.sdkj.fixed.asset.pojo.system.group.EditRole;
import com.sdkj.fixed.asset.pojo.system.group.IdMustRole;
import com.sdkj.fixed.asset.system.service.RoleService;
import com.sdkj.fixed.asset.system.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName RoleController
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/21 14:36
 */
@Controller
public class RoleController extends BaseController<RoleManagement> implements RoleApi {
    @Override
    public BaseService getService() {
        return null;
    }
    @Autowired
    private RoleService roleService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CacheUtil cacheUtil;
    /**
     * 新增角色
     * @param roleManagement
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo insertSimple(@RequestBody @Validated RoleManagement roleManagement) throws Exception{
        String token = request.getHeader("token");
        if(cacheUtil.getUserRole(token).contains(RoleType.SYSADMIN.getRoleType())){
            throw new LogicException("超级管理员不能添加角色");
        }
        roleManagement.setOrgId(cacheUtil.getUserCompanyId(token));
        roleManagement.setCuser(cacheUtil.getUserId(token));
        roleManagement.setEuser(cacheUtil.getUserId(token));
        roleManagement.setIsDefault(1);
        roleManagement.setLevel("3");
        roleService.addRole(roleManagement);
        return BaseResultVo.success("",roleManagement.getId());
    }
    /**
     * 修改角色
     * @param roleManagement
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo updateById(@RequestBody @Validated(EditRole.class) RoleManagement roleManagement) throws Exception{
        String token = request.getHeader("token");
        roleManagement.setEuser(cacheUtil.getUserId(token));
        roleService.editRole(roleManagement);
        return BaseResultVo.success("",roleManagement.getId());
    }
    /**
     * 角色分配用户
     * @param roleBindUser
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo  bindUser(@RequestBody @Validated RoleBindUser roleBindUser) throws Exception{
        roleService.bindUser(roleBindUser);
        return BaseResultVo.success("",roleBindUser.getRoleId());
    }
    /**
     * 查询角色已分配用户
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public BaseResultVo  queryHasBindUser(@RequestBody @Validated(IdMustRole.class) RoleManagement roleId) throws Exception{
        return BaseResultVo.success(roleService.queryHasBindUser(roleId));
    }
    /**
     * 获取菜单树,用于角色设置权限
     * @param roleId
     * @return
     */
    @Override
    public BaseResultVo<List<AuthTreeResultEntity>> getAuthTree(@RequestBody @Validated(IdMustRole.class) RoleManagement roleId) throws Exception{
        String token = request.getHeader("token");
        List<String> userRole = cacheUtil.getUserRole(token);
        boolean flag = userRole.contains(RoleType.SYSADMIN.getRoleType());
        List<AuthTreeResultEntity> authTree = roleService.getAuthTree(roleId.getId(), flag);
        return BaseResultVo.success(authTree);
    }
    /**
     * 分配权限
     * @return
     */
    @Override
    public BaseResultVo blindAuth(@Validated BindAuthParamEntity roleAuth) throws Exception{
        String token = request.getHeader("token");
        roleService.bindAuth(roleAuth,cacheUtil.getUserId(token));
        return BaseResultVo.success();
    }
    /**
     * 分页查询角色
     * @param params
     * @return
     */
    @Override
    public BaseResultVo<PageBean<RoleManagement>> getPages(@Validated(PageParams.ParamNotNullGroup.class) PageParams<RoleCondition> params) throws Exception {
        String token = request.getHeader("token");
        List<String> userRole = cacheUtil.getUserRole(token);
        boolean flag = userRole.contains(RoleType.SYSADMIN.getRoleType());
        PageBean<RoleManagement> page = null;
        Example example = new Example(RoleManagement.class);
        example.setOrderByClause("ctime DESC");
        // 条件查询
        Example.Criteria criteria = example.createCriteria();
        RoleCondition param =  params.getParams();
        if(param != null){
            String name = params.getParams().getName();
            if(!StringUtil.isEmpty(name)){
                criteria.andLike("name","%"+ params.getParams().getName()+"%" );
            }
        }
        criteria.andEqualTo("state",1);
        if(!flag){
            criteria.andEqualTo("level",RoleType.ORDINARY_USER.getRoleType());
        }/*else{
            criteria.andNotEqualTo("level",RoleType.SYSADMIN.getRoleType());
        }*/
        criteria.andCondition("org_id = '"+param.getCompanyId()+"'");
        List<RoleManagement> pageList = roleService.getPageList(example,params.getCurrentPage(), params.getPerPageTotal());
        page = new PageBean<RoleManagement>( pageList);
        return BaseResultVo.success(page);
    }
    /**
     * 分页查询角色
     * @param params
     * @return
     */
    @Override
    public BaseResultVo<PageBean<RoleManagement>> getAllRole(@Validated RoleCondition params) throws Exception {
        String token = request.getHeader("token");
        List<String> userRole = cacheUtil.getUserRole(token);
        boolean flag = userRole.contains(RoleType.SYSADMIN.getRoleType());
        Example example = new Example(RoleManagement.class);
        example.setOrderByClause("ctime DESC");
        // 条件查询
        Example.Criteria criteria = example.createCriteria();

        criteria.andEqualTo("state",1);
        if(!flag){
            criteria.andEqualTo("level",RoleType.ORDINARY_USER.getRoleType());
        }else{
            criteria.andNotEqualTo("level",RoleType.SYSADMIN.getRoleType());
        }
        criteria.andCondition("org_id = '"+params.getCompanyId()+"'");

        return BaseResultVo.success(roleService.selectByExample(example));
    }
}
