package com.sdkj.fixed.asset.system.controller;

import com.sdkj.fixed.asset.api.system.RegisterApi;
import com.sdkj.fixed.asset.api.system.in_vo.RegisterParam;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.system.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotBlank;

/**
 * @author niuliwei
 * @description 注册
 * @date 2021/4/8 13:14
 */
@Controller
public class RegisterController implements RegisterApi {
    @Autowired
    private RegisterService registerService;
    @Override
    public BaseResultVo register(RegisterParam registerParam) throws Exception{
        return BaseResultVo.success(registerService.register(registerParam));
    }

    @Override
    public BaseResultVo authCode(String email) throws Exception {
        return BaseResultVo.success(registerService.authCode(email));
    }
}
