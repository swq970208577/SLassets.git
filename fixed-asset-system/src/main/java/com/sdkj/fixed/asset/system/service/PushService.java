package com.sdkj.fixed.asset.system.service;

import com.github.pagehelper.PageHelper;
import com.sdkj.fixed.asset.common.base.BaseResultVo;
import com.sdkj.fixed.asset.common.base.PageParams;
import com.sdkj.fixed.asset.pojo.system.PushManagement;
import com.sdkj.fixed.asset.system.mapper.PushManagementMapper;
import com.sdkj.fixed.asset.system.util.dicUtil.DictParam;
import com.sdkj.fixed.asset.system.util.dicUtil.TranslationDict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName PushService
 * @Description TODO
 * @Author 张欣
 * @Date 2020/7/29 17:15
 */
@Service
public class PushService {
    @Autowired
    private PushManagementMapper pushManagementMapper;
    @TranslationDict({@DictParam(dictValueFiled = "result",dictNameFiled = "resultName",dicValues = "{'0':'成功','1':'失败'}")})
    public List<PushManagement> getPagePush(PageParams param,String userId){
        PageHelper.startPage(param.getCurrentPage(),param.getPerPageTotal());
       return  pushManagementMapper.getLoginUserPush(userId);
    }
}
