package com.sdkj.fixed.asset.system.util.dicUtil;


import java.lang.annotation.*;

/**
 * 翻译字典值注解
 * @author 张欣
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.METHOD})
@Documented
public @interface TranslationDict {
    DictParam[] value();
}

